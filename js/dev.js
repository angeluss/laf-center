$(document).ready(function(){
    OP.renderSavedSessionClient();
});

OP = {
    init: function() {

    },
    filterClients: function(btn){
        var form = $('#filter_form').serializeArray();
        var str = $('#Clients_card_id').val();
        var url = $(btn).data('url');

        $.ajax(

            {
                type: 'POST',
                dataType: 'JSON',
                data: {
                    form: form,
                    str: str
                },
                url: url,
                success: function (response) {
                    if (response.success) {
                        $('.clients_table_body').empty().append(response.html);
                        $('#params_output').empty().append(response.html2);
                        $('#noVpu').empty().append(response.noVpu);
                        $('#overdate').empty().append(response.overDate);
                        $('#count_cli').empty().append(response.count);
                        $('.filters_panel').addClass('closed')
                    }
                }
            }
        );
    },
    delParamOutput: function(btn){
        var id = $(btn).data('val');
        $('#' + id).attr('checked', false);
        var button = $('#filter_button');
        OP.filterClients(button);
    },
    delStrSearch: function(btn){
        $('#Clients_card_id').val('');
        var button = $('#filter_button');
        OP.filterClients(button);
    },
    addVpu: function(){
        $('#vpu').prop('checked', true);
        var button = $('#filter_button');
        OP.filterClients(button);
    },
    addPay: function(){
        $('#pay').prop('checked', true);
        var button = $('#filter_button');
        OP.filterClients(button);
    },
    renderSavedSessionClient: function(){
        var url = $('#userLinks').data('url');
        $.ajax(

            {
                type: 'POST',
                dataType: 'JSON',
                url: url,
                success: function (response) {
                    if (response.success) {

                        $('#userLinks').empty().append(response.html2);

                    }

                }
            }
        );
    },

    miniChatBlock: function(s) {
        $(s).closest('#active_chat').toggleClass('minichat');
    },
    hideChatBlock: function(s) {
        $(s).closest('#active_chat').hide();
    },
    showGuide: function(href) {
        var id = $(href).data('id');
        var url = $(href).data('url');
        $.ajax(
            {
                data: {
                    id: id
                },
                type: 'POST',
                dataType: 'JSON',
                url: url,
                success: function (response) {
                    if (response.success) {
                        $('.right_content').empty().append(response.html);
                    }
                }
            }
        );
    },
    closeGuide: function(href){
        $(href).closest('.right_content').empty();
    },
    showUser: function(client){
        var id = $(client).data('id');
        var url = $(client).closest('#clients-grid').data('url');
        $.ajax(
            {
                data: {
                    id: id
                },
                type: 'POST',
                dataType: 'JSON',
                url: url,
                success: function (response) {
                    if (response.success) {
                        var contrepeat =$('#ident-'+id).size();
                        if (contrepeat==0) {
                            $('.left_side').empty().append(response.html);
                            $('#userLinks').append(response.html2);
                        }
                        var count = $('.userLink').size();
                        if (count>3) {
                            $('.userLink:first').remove();
                        }


                    }
                }
            }
        );
    },
    showEmptyPopup: function() {
        console.log('----------- empty ---------');

    },
    getDataByValue: function(inputField){

        var url = $(inputField).data('urlsecond');
        var value = inputField.value;

        $.ajax(

            {
                data: {
                    value: value
                },
                type: 'POST',
                dataType: 'JSON',
                url: url + '?term=' + value,
                success: function (response) {
                    if( value.length > 2 ) {
                        if(response.emptyPopup) {
                            $('#statusClient').empty().append(response.emptyPopup);
                        }
                        if(response.clientList) {

                            $('#statusClient').empty().append(response.clientList);
                        }
                    }

                }
            }
        );
    },

    showUserAutocompleteCustom: function(client){

        var url = $(client).data('url');
        var id = $(client).data('id');

        $.ajax(
            {
                data: {
                    id: id
                },
                type: 'POST',
                dataType: 'JSON',
                url: url,
                success: function (response) {
                    if (response.success) {
                        var contrepeat =$('#ident-'+id).size();
                        //console.log(contrepeat);
                        if (contrepeat==0) {
                            $('.left_side').empty().append(response.html);
                            $('#userLinks').append(response.html2);
                        }
                        if (contrepeat==1) {
                            //alert($('#klient-card-'+id));
                            $('.left_side').empty().append(response.html);
                            $('#klient-card-'+id).parent().show();
                        }
                        var count = $('.userLink').size();
                        if (count>3) {
                            $('.userLink:first').remove();
                        }

                    }
                }
            }
        );
    },

    showUserAutocomplete: function(client, id){

        var url = $(client).data('url');

        $.ajax(
            {
                data: {
                    id: id
                },
                type: 'POST',
                dataType: 'JSON',
                url: url,
                success: function (response) {
                    if (response.success) {
                        var contrepeat =$('#ident-'+id).size();
                        //console.log(contrepeat);
                        if (contrepeat==0) {
                            $('.left_side').empty().append(response.html);
                            $('#userLinks').append(response.html2);
                        }
                        if (contrepeat==1) {
                            //alert($('#klient-card-'+id));
                            $('.left_side').empty().append(response.html);
                            $('#klient-card-'+id).parent().show();
                        }
                        var count = $('.userLink').size();
                        if (count>3) {
                            $('.userLink:first').remove();
                        }

                    }
                }
            }
        );
    },
    showUserFromLink: function(client, id) {

        var url = $(client).data('url');
        $.ajax(
            {
                data: {
                    id: id
                },
                type: 'POST',
                dataType: 'JSON',
                url: url,
                success: function (response) {
                    if (response.success) {
                        $('.left_side').empty().append(response.html).show();
                    }
                }
            }
        );
    },
    deleteUserFromLink : function(client, id) {
        var url = $(client).data('url');
        $.ajax(
            {
                data: {
                    id: id
                },
                type: 'POST',
                dataType: 'JSON',
                url: url,
                success: function (response) {
                    if (response.success) {
                        $('#ident-'+id).remove();
                        $('#klient-card-'+id).parent().fadeOut();
                    }

                }
            }
        );
    },
    addTagBlock: function(link){
        var url = $(link).data('url');
        var id = $('#tag').val();

        $.ajax(
            {
                data: {
                    id: id,
                },
                type: 'POST',
                dataType: 'JSON',
                url: url,
                success: function (response) {
                    if (response.success) {
                        $('.appendedTags').append(response.html);
                        $("#tag option[value="+id+"]").remove();
                    }
                }
            }
        );
    },
    addLangBlock: function(){
        var url = $(link).data('url');

        $.ajax(
            {
                type: 'POST',
                dataType: 'JSON',
                url: url,
                success: function (response) {
                    if (response.success) {
                        $('.appendedTags').append(response.html);
                        //$(link).closest('.tagsSelector').empty().append(response.html2);
                        $("#tag option[value="+id+"]").remove();
                    }
                }
            }
        );
    },
    editSerie: function(){
        $('#edit_ser_num').css('display', 'none');
        $('#vpu_serie').css('display', 'inline');
        $('#vpu_num').css('display', 'inline');
        $('#ok_serie').css('display', 'inline');
        $('#cancel_serie').css('display', 'inline');
    },
    cancelSerie: function(){
        $('#edit_ser_num').css('display', 'block');
        $('#vpu_serie').css('display', 'none');
        $('#vpu_num').css('display', 'none');
        $('#ok_serie').css('display', 'none');
        $('#cancel_serie').css('display', 'none');
    },
    saveSerie: function(){
        var serie = $('#vpu_serie').val();
        var num = $('#vpu_num').val();
        var id = $('#vpu_div').data('id');
        var url = $('.vpu_num_p').data('url');
        $.ajax(
            {
                data: {
                    serie: serie,
                    num: num,
                    id: id
                },
                type: 'POST',
                dataType: 'JSON',
                url: url,
                success: function (response) {
                    if (response.success) {
                        $('.vpu_num_p').empty().html(serie+num);
                    }
                }
            }
        );

    },
    editTill: function(){
        $('#edit_till').css('display', 'none');
        $('#vpu_till').css('display', 'inline');
        $('#ok_till').css('display', 'inline');
        $('#cancel_till').css('display', 'inline');
    },
    cancelTill: function(){
        $('#edit_till').css('display', 'block');
        $('#vpu_till').css('display', 'none');
        $('#ok_till').css('display', 'none');
        $('#cancel_till').css('display', 'none');
    },
    saveTill: function(){
        var till = $('#vpu_till').val();
        var id = $('#vpu_div').data('id');
        var url = $('.vpu_till_p').data('url');
        $.ajax(
            {
                data: {
                    till: till,
                    id: id
                },
                type: 'POST',
                dataType: 'JSON',
                url: url,
                success: function (response) {
                    if (response.success) {
                        $('.vpu_till_p').empty().html(till);
                    }
                }
            }
        );

    },
    editFrom: function(){
        $('#edit_from').css('display', 'none');
        $('#vpu_from').css('display', 'inline');
        $('#ok_from').css('display', 'inline');
        $('#cancel_from').css('display', 'inline');
    },
    cancelFrom: function(){
        $('#edit_from').css('display', 'block');
        $('#vpu_from').css('display', 'none');
        $('#ok_from').css('display', 'none');
        $('#cancel_from').css('display', 'none');
    },
    saveFrom: function(){
        var from = $('#vpu_from').val();
        var id = $('#vpu_div').data('id');

        var url = $('.vpu_from_p').data('url');
        $.ajax(
            {
                data: {
                    from: from,
                    id: id
                },
                type: 'POST',
                dataType: 'JSON',
                url: url,
                success: function (response) {
                    if (response.success) {
                        $('.vpu_from_p').empty().html(from);
                    }
                }
            }
        );
    },
    acceptChat: function(btn){
        var id = $(btn).data('id');
        var url = $(btn).data('url');
        $.ajax(
            {
                data: {
                    id: id
                },
                type: 'POST',
                dataType: 'JSON',
                url: url,
                success: function (response) {
                    $('#all_chats').css('display', 'none').empty().append(response.html);
                    if (response.success) {
                        $('#active_chats').empty().append(response.html2).css('display', 'block');
                        $('#chat_count').html(response.ccount);
                        $('#open_chat_count').html(response.occount);
                    }
                }
            }
        );
    },
    showChat: function(btn){
        var id = $(btn).data('id');
        var url = $(btn).closest('table').data('url');
        $.ajax(
            {
                data: {
                    id: id
                },
                type: 'POST',
                dataType: 'JSON',
                url: url,
                success: function (response) {
                    if (response.success) {
                        //$('#openChatlist').css('display', 'none');
                        $('#messages_block').empty().append(response.html);
                        $('#active_chat').css('display', 'block');
                        $('#messages_block').scrollTop($('#messages_block')[0].scrollHeight);
                        $("#chat_id").val(id);
                    }
                }
            }
        );

    },
    closeChat: function(btn) {
        var id = $('#chat_id_span').data('id');
        var url = $(btn).data('url');
        $.ajax(
            {
                data: {
                    id: id
                },
                type: 'POST',
                dataType: 'JSON',
                url: url,
                success: function (response) {
                    if (response.success) {
                        $('#active_chat').css('display', 'none');
                    }
                }
            }
        );
    }
};

MS = {
    init: function() {

    },
    addLangBlock: function(link){
        var url = $(link).data('url');

        $.ajax(
            {
                type: 'POST',
                dataType: 'JSON',
                url: url,
                success: function (response) {
                    if (response.success) {
                        $('.lang_selects').append(response.html);
                    }
                }
            }
        );
    },
    validate: function(e) {
        e.preventDefault();
        var fullname = $('#fullname').val();
        var name = $('#name').val();
        var birthdate = $('#birthdate').val();
        var country = $('#country').val();
        return false;
    }
};


function validateFormReg () {
    console.log(e);
}


$(document).ready(function(){
    $('#reg_but').click(function (e) {
        e.preventDefault();
        var success = true;
        success = true;
        //var empty = 'Пожалуйста, заполните это поле';
        var empty =$('#text_for_validation').val();
        var fullname = $('#fullname').val();
        if(fullname.length == 0){
            $('#error_fullname').html(empty).css('display', 'block');
            $('#fullname').css('border-bottom', '1px solid red');
            success = false;
        } else {
            $('#error_fullname').html(empty).css('display', 'none');
            $('#fullname').css('border', 'none');
        }
        var name = $('#name').val();
        if(name.length == 0){
            $('#error_name').html(empty).css('display', 'block');
            $('#name').css('border-bottom', '1px solid red');
            success = false;
        }else {
            $('#error_name').html(empty).css('display', 'none');
            $('#name').css('border', 'none');
        }
        var birthdate = $('#birthdate').val();
        if(birthdate.length == 0){
            $('#error_birthdate').html(empty).css('display', 'block');
            $('#birthdate').css('border-bottom', '1px solid red');
            success = false;
        }else {
            $('#error_birthdate').html(empty).css('display', 'none');
            $('#birthdate').css('border', 'none');
        }
        var country = $('#country').val();
        if(country.length == 0){
            $('#error_country').html(empty).css('display', 'block');
            $('#country').closest('div').css('border-bottom', '1px solid red');
            success = false;
        }else {
            $('#error_country').html(empty).css('display', 'none');
            $('#country').closest('div').css('border', 'none');

        }
        var vuz = $('#vuz').val();
        if(vuz.length == 0){
            $('#error_vuz').html(empty).css('display', 'block');
            $('#vuz').closest('div').css('border-bottom', '1px solid red');
            success = false;
        }else {
            $('#error_vuz').html(empty).css('display', 'none');
            $('#vuz').closest('div').css('border', 'none');
        }
        var number_pass = $('#number-pass').val();
        if(number_pass.length == 0){
            $('#error_number_pass').html(empty).css('display', 'block');
            $('#number_pass').css('border-bottom', '1px solid red');
            success = false;
        }else {
            $('#error_number_pass').html(empty).css('display', 'none');
            $('#number_pass').css('border', 'none');
        }
        var date_pass = $('#date_pass').val();
        if(date_pass.length == 0){
            $('#error_date_pass').html(empty).css('display', 'block');
            $('#date_pass').css('border-bottom', '1px solid red');
            success = false;
        }else {
            $('#error_date_pass').html(empty).css('display', 'none');
            $('#date_pass').css('border', 'none');
        }
        var till_pass = $('#till_pass').val();
        if(till_pass.length == 0){
            $('#error_till_pass').html(empty).css('display', 'block');
            $('#till_pass').css('border-bottom', '1px solid red');
            success = false;
        }else {
            $('#error_till_pass').html(empty).css('display', 'none');
            $('#till_pass').css('border', 'none');
        }
        var phone = $('#phone').val();
        if(phone.length == 0){
            $('#error_phone').html(empty).css('display', 'block');
            $('#phone').css('border-bottom', '1px solid red');
            success = false;
        }else {
            $('#error_phone').html(empty).css('display', 'none');
            $('#phone').css('border', 'none');
        }
        var region_areas = $('#region_areas').val();
        if(region_areas.length == 0){
            $('#error_region_areas').html(empty).css('display', 'block');
            $('#region_areas').closest('div').css('border-bottom', '1px solid red');
            success = false;
        }else {
            $('#error_region_areas').html(empty).css('display', 'none');
            $('#region_areas').closest('div').css('border', 'none');
        }
        var oblast = $('#oblast').val();
        if(oblast.length == 0){
            $('#error_oblast').html(empty).css('display', 'block');
            $('#oblast').closest('div').css('border-bottom', '1px solid red');
            success = false;
        }else {
            $('#error_oblast').html(empty).css('display', 'none');
            $('#oblast').closest('div').css('border', 'none');
        }
        var input_address = $('#input-address').val();
        if(input_address.length == 0){
            $('#error_input_address').html(empty).css('display', 'block');
            $('#input-address').css('border-bottom', '1px solid red');
            success = false;
        }else {
            $('#error_input_address').html(empty).css('display', 'none');
            $('#input-address').css('border', 'none');
        }

        var lang_input = $('#lang_input').val();
        if(lang_input.length == 0){
            $('#error_lang_input').html(empty).css('display', 'block');
            $('#lang_input').closest('.select-outer').css('border-bottom', '1px solid red');
            success = false;
        }else {
            $('#error_lang_input').html(empty).css('display', 'none');
            $('#lang_input').closest('.select-outer').css('border', 'none');
        }

        var email = $('#email').val();
        var at=email.indexOf("@");
        var dot=email.indexOf(".");
        //Если поле не содержит эти символы значит email введен не верно
        if (at<1 || dot <1){
            $('#error_email').html('Email введен не верно').css('display', 'block');
            $('#email').css('border-bottom', '1px solid red');
            success = false;
        }else {
            $('#error_email').html(empty).css('display', 'none');
            $('#email').css('border', 'none');
        }
        if(email.length == 0){
            $('#error_email').html(empty).css('display', 'block');
            $('#email').css('border-bottom', '1px solid red');
            success = false;
        }else {
            $('#error_email').html(empty).css('display', 'none');
            $('#email').css('border', 'none');
        }

        var password = $('#password').val();
        if(password.length == 0){
            $('#error_password').html(empty).css('display', 'block');
            $('#password').css('border-bottom', '1px solid red');
            success = false;
        }else {
            $('#error_password').html(empty).css('display', 'none');
            $('#password').css('border', 'none');
        }
        if(password.length < 6){
            $('#pass_tip').css('color', 'red');
            $('#password').css('border-bottom', '1px solid red');
            success = false;
        }else {
            $('#pass_tip').css('color', '#70787a');
            $('#password').css('border', 'none');
        }

        var url = $('#validate_email').data('url');
        if(success) {
            $.ajax(
                {
                    type: 'POST',
                    data: {
                        email: email
                    },
                    dataType: 'JSON',
                    url: url,
                    success: function (response) {
                        if (response.success) {
                            $('#error_email').html('Такой Email уже зарегистрирован в базе').css('display', 'block');
                            $('#email').css('border-bottom', '1px solid red');
                            $('html, body').animate({scrollTop: 0}, 500);
                            $('.form_has_errors').css('display', 'block');
                        } else {
                            $('#form_register').submit();
                            //alert(test);
                        }
                    }
                }
            );
        } else {
            $('html, body').animate({scrollTop: 0}, 500);
            $('.form_has_errors').css('display', 'block');
        }
    });

    $('#send_file').on('change', function(){
        var arr = $(this).val().split('\\');
        $('#file_name_span').html(arr[2]);
    });

    function submitMsgForm($this, e){
        e.preventDefault();
        var $that = $($this),
            formData = new FormData($that.get(0)); // создаем новый экземпляр объекта и передаем ему нашу форму (*)
        $.ajax({
            url: $that.data('url'),
            type: $that.attr('method'),
            contentType: false, // важно - убираем форматирование данных по умолчанию
            processData: false, // важно - убираем преобразование строк по умолчанию
            data: formData,
            dataType: 'json',
            success: function(response){
                if (response.success) {
                    $('#messages_block').empty().html(response.html).scrollTop($('#messages_block')[0].scrollHeight);
                    $('#chat_id').val(response.id);
                    $('#operator_msg').val('');
                    $('#send_file').val('');
                }
            }
        });
    }

    $('#msg_send_form').on('submit', function(e){
        submitMsgForm(this, e);
    });

    document.onkeyup = function (e) {
        e = e || window.event;
        if (e.keyCode === 13) {
            var $this = $('#msg_send_form');
            submitMsgForm($this, e);
        }
        return false;
    };


    $('#discard_search').click(function(){
        $('#filter_form input[type="checkbox"]').each(function(i, e){
            $(e).prop('checked', false);
        });
        var button = $('#filter_button');
        OP.filterClients(button);
    });
});

$(document).ready(function() {
    if($('.guide_side').length !== 0) {
        $('.guide_side').css({
            'height': $(window).height() - $('nav.navbar-inverse').outerHeight()/*,
            'min-height': $('.right_panel').outerHeight()*/
        });
        $('.left_side').css({
            'height': $(window).height() - $('nav.navbar-inverse').outerHeight()
        });
    }
    $('#clientsGridForm').submit(function(e) {
        e.preventDefault();
        var s = $('#search').val();
        var url = $(this).prop('action');

        $.ajax(
            {
                data: {
                    s: s
                },
                type: 'POST',
                dataType: 'JSON',
                url: url,
                success: function (response) {
                    if (response.success) {
                        $('#clients_grid_wrapper').empty().append(response.html);
                    }
                }
            }
        );
    });

    $('#filter_card_id').submit(function(e){
        e.preventDefault();
        var button = $('#filter_button');
        OP.filterClients(button);
    });
});

$(window).resize(function() {
    if($('.guide_side').length !== 0) {
        $('.guide_side').css({
            'height': $(window).height() - $('nav.navbar-inverse').outerHeight()/*,
            'min-height': $('.right_panel').outerHeight()*/
        });
        $('.left_side').css({
            'height': $(window).height() - $('nav.navbar-inverse').outerHeight()
        });
    }
});


