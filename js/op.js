$(document).ready(function(){
    function refreshChat() {
        var id = $('#chat_id_span').data('id');
        if(id !== '0'){
            var url = $('#refresh_chat_span').data('url');
            $.ajax(
                {
                    data: {
                        id: id
                    },
                    type: 'POST',
                    dataType: 'JSON',
                    url: url,
                    success: function (response) {
                        if (response.success) {
                            $('#messages_block').empty().html(response.html);
                        }
                    }
                }
            );
        }
    }

    function refreshChatList(){
        var url = $('#refresh_chats_span').data('url');
        $.ajax(
            {
                type: 'POST',
                dataType: 'JSON',
                url: url,
                success: function (response) {
                    if (response.success) {
                        $('#all_chats').empty().append(response.html);
                        $('#chat_count').html(response.count);
                    }
                }
            }
        );
    }

    function refreshActiveChats(){
        var url = $('#refresh_active_chats_span').data('url');
        $.ajax(
            {
                type: 'POST',
                dataType: 'JSON',
                url: url,
                success: function (response) {
                    if (response.success) {
                        $('#active_chats').empty().append(response.html);
                        $('#open_chat_count').html(response.count);
                    }
                }
            }
        );
    }

    setInterval(function() {refreshChat(); refreshChatList(); refreshActiveChats() }, 3000);

    $('.pager a').each(function(){
        var href = $(this).attr('href');
        console.log($(this));
        var dot = href.indexOf("allClientsPagination");
        if(dot < 1) {
            var at = href.indexOf("?");
            if (at < 1) {
                href += '?allClientsPagination=1';
            } else {
                href += '&allClientsPagination=1';
            }
            $(this).attr('href', href);
        }
    });

    //$('.pager a').click(function(){
    //    e.preventDefault();
    //    var url = $('#paginate_url').data('url');
    //    alert(url);
    //    $.ajax(
    //        {
    //            type: 'POST',
    //            dataType: 'JSON',
    //            url: url,
    //            success: function (response) {
    //                alert('123');
    //            }
    //        }
    //    );
    //});

});
