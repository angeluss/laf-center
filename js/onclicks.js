// views/site/documents/php
function renameDoc(doc,id,type,client_id) {
    var url = $(doc).data('url');
    var block_id = $(doc).data('block');
    var new_name = $('#'+block_id+' input').val();
    var path=$('#pathToDir-'+type);
    if (type=='chat') {
        path=$('#pathToDir-'+id);
    }
    $.ajax(
        {
            data: {
                id: id,
                entity:type,
                new_name:new_name,
                client_id:client_id
            },
            type: 'POST',
            dataType: 'JSON',
            url: url,
            success: function (response) {
                if (response.success) {
                    path.attr("href",response.new_pass);
                    path.html(new_name);
                }
            }
        }
    );
}
jQuery( document ).ready(function() {
    $('#no_reg_modal_link').click(function(){
        $('#no_reg_modal').modal();
    });
    $('#reg_button_top').click(function(){
        location.href=$(this).data('url');
    });
    $('#reg_button_down').click(function(){
        location.href=$(this).data('url');
    });
    $('.mob_app_button').click(function(){
        alert($(this).data('msg'));
    });
    $('#no_reg_consult_link').click(function(){
        $('#no_reg_modal').modal();
    });
    $('#no_chat_consult_link').click(function(){
        $('#no_chat_modal').modal();
    });
    $('.reg-btn').click(function(){
        location.href=$(this).data('url');
    });
    $('.consult_button').click(function(){
        window.open( 'http://alfa-omega.kh.ua/', '_blank' );
    });
    $('#back_from_grey_link').click(function(){
        location.href=$(this).data('url');
    });
    $('#reg_button_consult').click(function(){
        location.href=$(this).data('url');
    });
    $('.no_reg_consult_link').click(function(){
        $('#no_reg_modal').modal();
    });
    $('#reg_button_new').click(function(){
        location.href=$(this).data('url');
    });
    $('#collapseDocPassportLink').click(function(){
        var doc = this;
        var id = $(this).data('id');
        var type = $(this).data('type');
        var client_id = $(this).data('client_id');
        renameDalert(123);oc(doc,id,type,client_id);
     });
    $('.chat_doc_rename_link').click(function(){
        var doc = this;
        var id = $(this).data('id');
        var type = $(this).data('type');
        var client_id = $(this).data('client_id');
        renameDoc(doc,id,type,client_id);
    });
    $('#collapseDocVisaLink').click(function(){
        var doc = this;
        var id = $(this).data('id');
        var type = $(this).data('type');
        var client_id = $(this).data('client_id');
        renameDoc(doc,id,type,client_id);
    });
    $('#collapseDocVpuLink').click(function(){
        var doc = this;
        var id = $(this).data('id');
        var type = $(this).data('type');
        var client_id = $(this).data('client_id');
        renameDoc(doc,id,type,client_id);
    });
    $('#collapseDocPhotoLink').click(function(){
        var doc = this;
        var id = $(this).data('id');
        var type = $(this).data('type');
        var client_id = $(this).data('client_id');
        renameDoc(doc,id,type,client_id);
    });
    $('#error_back_link').click(function(){
        location.href=$(this).data('url');
    });
    $('#index_top_reg_button').click(function(){
        location.href=$(this).data('url');
    });
    $('.close_chat_link').click(function(){
        if(confirm($(this).data('msg'))) {
            closeChat(this);
        }
    });
    $('#pass_icon_show').click(function(){
        $('#LoginForm_password').attr('type', 'text');
        $('#pass_icon_show').css('display', 'none');
        $('#pass_icon_hide').css('display', 'block');
    });
    $('#pass_icon_hide').click(function(){
        $('#LoginForm_password').attr('type', 'password');
        $('#pass_icon_show').css('display', 'block');
        $('#pass_icon_hide').css('display', 'none');
    });
    $('#no-certificate').click(function() {
        if($(this).is(":checked")){
            $('.license_block1').css('display', 'none');
            $('.license_block2').css('display', 'none');
            $('.license_block3').css('display', 'none');
            $('.license_block4').css('display', 'none');
        } else {
            $('.license_block1').css('display', 'block');
            $('.license_block2').css('display', 'block');
            $('.license_block3').css('display', 'block');
            $('.license_block4').css('display', 'block');
        }
    });
    $('#show_pwd_link').click(function() {
        $('.showpassword').slideToggle();
    });
    $('#recover_to_login').click(function(){
        location.href=$(this).data('url');
    });
    function HideInfo() {
        $(".no_pay_msg").slideUp(400);
    }
    setTimeout(HideInfo, 3000);
    $('.result_consult_no_reg_link').click(function(){
        $('#no_reg_modal').modal();
    });
    $('#pay_inv_next_btn').click(function(){
        location.href=$(this).data('url');
    });
    $('#no_header_reg_btn').click(function(){
        location.href=$(this).data('url');
    });
    // changepass.php
    $('#chg_pwd').click(function (e) {
        e.preventDefault();
        var success = true;
        success = true;
        var empty = $('#js_msgs').data('empty');
        var small = $('#js_msgs').data('small');
        var fullname = $('#fullname').val();

        var password = $('#Clients_password').val();
        if(password.length == 0){
            $('#error_password').html(empty).css('display', 'block');
            $('#Clients_password').css('border-bottom', '1px solid red');
            success = false;
        } else if(password.length < 6){
            $('#error_password').html(small).css('display', 'block');
            $('#Clients_password').css('border-bottom', '1px solid red');
            success = false;
        }
        if(success){
            $('#form_change').submit();
        }
    });
    $('#recover_form_pass_btn').click(function(e) {
        e.preventDefault();
        var email = $('#email').val();
        var url = $('#email').data('url');
        var at=email.indexOf("@");
        var dot=email.indexOf(".");
        //Если поле не содержит эти символы значит email введен не верно
        if (at<1 || dot <1){
            $('#error_email').html('Email введен не верно').css('color', 'red').css('display', 'block');
            $('#email').css('border-bottom', '1px solid red');
            success = false;
        } else {
            $.ajax(
                {
                    type: 'POST',
                    data: {
                        email: email
                    },
                    dataType: 'JSON',
                    url: url,
                    success: function (response) {
                        if (response.success) {
                            $('#recover_form_pass').submit();
                        } else {
                            $('#error_email').html('Такой Email не зарегистрирован в базе').css('color', 'red').css('display', 'block');
                            $('#email').css('border-bottom', '1px solid red');
                            $('html, body').animate({scrollTop: 0}, 500);
                            $('.form_has_errors').css('display', 'block');
                        }
                    }
                }
            );
        }
    });
});
