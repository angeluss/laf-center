jQuery( document ).ready(function() {
    jQuery('.lang_icon').on('click', function(){
        jQuery('.lang_menu').toggle();
    });
    jQuery('.useful_information').on('click', function(){
        jQuery('.useful_information_dropdown').toggle();
    });

    jQuery('.pass-icon.show').on('click', function(){
        jQuery('#password').attr('type','text');
        jQuery('.pass-icon.show').hide();
        jQuery('.pass-icon.hide').show();
    });
    jQuery('.pass-icon.hide').on('click', function(){
        jQuery('#password').attr('type','password');
        jQuery('.pass-icon.show').show();
        jQuery('.pass-icon.hide').hide();
    });
    jQuery('.date-input, .fa-calendar:before').datepicker({
        changeMonth: true,
        changeYear: true,
        yearRange: '1900:2100',
        firstDay: 1,
        dateFormat: 'dd/mm/yy'
    });
    jQuery('.dropdown-element').on('click', function(){
        jQuery(this).next('.dropmenu').toggle();
    });
    jQuery("#offerta-agree").on('click', function(){
        jQuery('.agree_block').toggleClass('checked');
        if ($('.reg_button button').attr('disabled')) {
            $('.reg_button button').removeAttr('disabled');
        } else {
            $('.reg_button button').attr('disabled', 'disabled');
        }
    });

    jQuery('#region_areas').on('change', function(){
        var choise =jQuery(this).val();
            if (choise!==1) {
                jQuery('li.input-city').removeClass('hidden');
                jQuery('li.select-district').addClass('hidden');

           }
            if (choise==1) {
                jQuery('li.select-district').removeClass('hidden');
                jQuery('li.input-city').addClass('hidden');
            }
    });
    jQuery('input[type=file]').on('change', function(){

        var file_name = this.value.replace(/\\/g, '/').replace(/.*\//, '');
        //jQuery(this).attr('value', file_name);
        jQuery(this).siblings('.file-name-title').text(file_name);

    });
    jQuery('select').on('change', function(){
        jQuery(this).css('color','#363636');
    });

    jQuery('.consult_main_div a').on('click', function(){
        $('#no_reg_modal').modal();
        return false;
    });

    var windowHeight = jQuery(window).height();
    jQuery('#intercom, #intercom-history').css('height', windowHeight);
    jQuery('body').scroll(function() {
        console.log('11');
    });
    jQuery(".intercom_content").mCustomScrollbar({
        axis: 'y',
        theme: "dark"
    });
    jQuery('#sh_button').on('click', function() {
        jQuery('#intercom').show("slide", { direction: "right" }, 1000);
    });
    jQuery('#index_top_chat_button').on('click', function() {
        jQuery('#intercom').show("slide", { direction: "right" }, 1000);
    });
    jQuery(".close_icon").on('click', function() {
        jQuery('#intercom').hide("slide", { direction: "right" }, 1000);
    });

/* refresh chat */
    function refreshChat() {
        var id = $('#chatID').attr('value');

        if(id !== '0'){
            var url = $('#chatID').data('url');
            $.ajax(
                {
                    data: {
                        id: id
                    },
                    type: 'POST',
                    dataType: 'JSON',
                    url: url,
                    success: function (response) {
                        if (response.success) {
                            $('#intercom .intercom_content').empty().html(response.html).scrollTop($('.intercom_content')[0].scrollHeight);
                        }
                    }
                }
            );
        }
    }

    setInterval(function() {refreshChat(); }, 3000);
/* end refresh chat */

/* start chat */
    function submitMsgForm($this, e){
        e.preventDefault();
        var $that = $($this),
            formData = new FormData($that.get(0)); // создаем новый экземпляр объекта и передаем ему нашу форму (*)
        $.ajax({
            url: $that.data('url'),
            type: $that.attr('method'),
            contentType: false, // важно - убираем форматирование данных по умолчанию
            processData: false, // важно - убираем преобразование строк по умолчанию
            data: formData,
            dataType: 'json',
            success: function(response){
                if (response.success) {
                    $('.intercom_content').empty().html(response.html).scrollTop($('.intercom_content')[0].scrollHeight);
                    $('#chatID').attr('value', response.id);
                    $('#client_message').val('');
                    $('#client_file').val('');
                    $('#file_name').html('');
                }
            }
        });
    }
    $('#send_msg_form').on('submit', function(e){
        submitMsgForm(this, e);
    });

    document.onkeyup = function (e) {
        e = e || window.event;
        if (e.keyCode === 13) {
            var $this = $('#send_msg_form');
            submitMsgForm($this, e);
        }
        return false;
    };

    $('#client_file').on('change', function(){
        var arr = $(this).val().split('\\');
        $('#file_name').html(arr[2]);
    });

/* end start chat */



    $('#prof_but').click(function (e) {
        e.preventDefault();
        var success = true;
        success = true;
        var empty = $('#text_for_validation').val();
        var fullname = $('#fullname').val();
        if (fullname.length == 0) {
            $('#error_fullname').html(empty).css('display', 'block');
            $('#fullname').css('border-bottom', '1px solid red');
            success = false;
        } else {
            $('#error_fullname').html(empty).css('display', 'none');
            $('#fullname').css('border', 'none');
        }
        var name = $('#name').val();
        if (name.length == 0) {
            $('#error_name').html(empty).css('display', 'block');
            $('#name').css('border-bottom', '1px solid red');
            success = false;
        } else {
            $('#error_name').html(empty).css('display', 'none');
            $('#name').css('border', 'none');
        }
        var birthdate = $('#birthdate').val();
        if (birthdate.length == 0) {
            $('#error_birthdate').html(empty).css('display', 'block');
            $('#birthdate').css('border-bottom', '1px solid red');
            success = false;
        } else {
            $('#error_birthdate').html(empty).css('display', 'none');
            $('#birthdate').css('border', 'none');
        }
        var country = $('#country').val();
        if (country.length == 0) {
            $('#error_country').html(empty).css('display', 'block');
            $('#country').closest('div').css('border-bottom', '1px solid red');
            success = false;
        } else {
            $('#error_country').html(empty).css('display', 'none');
            $('#country').closest('div').css('border', 'none');

        }
        var vuz = $('#vuz').val();
        if (vuz.length == 0) {
            $('#error_vuz').html(empty).css('display', 'block');
            $('#vuz').closest('div').css('border-bottom', '1px solid red');
            success = false;
        } else {
            $('#error_vuz').html(empty).css('display', 'none');
            $('#vuz').closest('div').css('border', 'none');
        }

        var number_pass = $('#number-pass').val();
        if (number_pass.length == 0) {
            $('#error_number_pass').html(empty).css('display', 'block');
            $('#number_pass').css('border-bottom', '1px solid red');
            success = false;
        } else {
            $('#error_number_pass').html(empty).css('display', 'none');
            $('#number_pass').css('border', 'none');
        }
        var date_pass = $('#date_pass').val();
        if (date_pass.length == 0) {
            $('#error_date_pass').html(empty).css('display', 'block');
            $('#date_pass').css('border-bottom', '1px solid red');
            success = false;
        } else {
            $('#error_date_pass').html(empty).css('display', 'none');
            $('#date_pass').css('border', 'none');
        }
        var till_pass = $('#till_pass').val();
        if (till_pass.length == 0) {
            $('#error_till_pass').html(empty).css('display', 'block');
            $('#till_pass').css('border-bottom', '1px solid red');
            success = false;
        } else {
            $('#error_till_pass').html(empty).css('display', 'none');
            $('#till_pass').css('border', 'none');
        }
        var phone = $('#phone').val();
        if (phone.length == 0) {
            $('#error_phone').html(empty).css('display', 'block');
            $('#phone').css('border-bottom', '1px solid red');
            success = false;
        } else {
            $('#error_phone').html(empty).css('display', 'none');
            $('#phone').css('border', 'none');
        }
        var region_areas = $('#region_areas').val();
        if (region_areas.length == 0) {
            $('#error_region_areas').html(empty).css('display', 'block');
            $('#region_areas').closest('div').css('border-bottom', '1px solid red');
            success = false;
        } else {
            $('#error_region_areas').html(empty).css('display', 'none');
            $('#region_areas').closest('div').css('border', 'none');
        }
        var oblast = $('#oblast').val();
        if (oblast.length == 0) {
            $('#error_oblast').html(empty).css('display', 'block');
            $('#oblast').closest('div').css('border-bottom', '1px solid red');
            success = false;
        } else {
            $('#error_oblast').html(empty).css('display', 'none');
            $('#oblast').closest('div').css('border', 'none');
        }
        var input_address = $('#input-address').val();
        if (input_address.length == 0) {
            $('#error_input_address').html(empty).css('display', 'block');
            $('#input-address').css('border-bottom', '1px solid red');
            success = false;
        } else {
            $('#error_input_address').html(empty).css('display', 'none');
            $('#input-address').css('border', 'none');
        }

        var lang_input = $('#lang_input').val();
        if (lang_input.length == 0) {
            $('#error_lang_input').html(empty).css('display', 'block');
            $('#lang_input').closest('.select-outer').css('border-bottom', '1px solid red');
            success = false;
        } else {
            $('#error_lang_input').html(empty).css('display', 'none');
            $('#lang_input').closest('.select-outer').css('border', 'none');
        }

        var email = $('#email').val();
        var at = email.indexOf("@");
        var dot = email.indexOf(".");
        //Если поле не содержит эти символы значит email введен не верно
        if (at < 1 || dot < 1) {
            $('#error_email').html('Email введен не верно').css('display', 'block');
            $('#email').css('border-bottom', '1px solid red');
            success = false;
        } else {
            $('#error_email').html(empty).css('display', 'none');
            $('#email').css('border', 'none');
        }
        if (email.length == 0) {
            $('#error_email').html(empty).css('display', 'block');
            $('#email').css('border-bottom', '1px solid red');
            success = false;
        } else {
            $('#error_email').html(empty).css('display', 'none');
            $('#email').css('border', 'none');
        }

        var password = $('#password').val();
        if (password.length == 0) {
            $('#error_password').html(empty).css('display', 'block');
            $('#password').css('border-bottom', '1px solid red');
            success = false;
        } else {
            $('#error_password').html(empty).css('display', 'none');
            $('#password').css('border', 'none');
        }
        if (password.length < 6) {
            $('#pass_tip').css('color', 'red');
            $('#password').css('border-bottom', '1px solid red');
            success = false;
        } else {
            $('#pass_tip').css('color', '#70787a');
            $('#password').css('border', 'none');
        }

        var url = $('#validate_email').data('url');
        if (success) {
                $('#form_register').submit();
            }
         else {
            $('html, body').animate({scrollTop: 0}, 500);
            $('.form_has_errors').css('display', 'block');
        }
    });


});
function closeChat(btn){
    var url = $(btn).data('url');
    var id = $('#chatID').attr('value');
    console.log(id);
    $.ajax(
        {
            data: {
                id: id
            },
            type: 'POST',
            dataType: 'JSON',
            url: url,
            success: function (response) {
                if (response.success) {
                    $('.intercom_content').append(response.html);
                    $('#intercom').hide("slide", { direction: "right" }, 1000);
                }
            }
        }
    );
}


