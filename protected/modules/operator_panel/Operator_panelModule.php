<?php

class Operator_panelModule extends CWebModule
{
	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'operator_panel.models.*',
			'operator_panel.components.*',
            'application.components.*',
            'admin_panel.models.*',
            'admin.models.*',
		));

        Yii::app()->components = array(
            'user' => array(
                'loginUrl'=>array('operator_panel/default/login'),
                'allowAutoLogin'=>true,
            )
        );
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			return true;
		}
		else
			return false;
	}
}
