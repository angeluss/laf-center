<?php

/**
 * This is the model class for table "clients".
 *
 * The followings are the available columns in table 'clients':
 * @property integer $gender
 * @property integer $status
 * @property integer $pay_date
 * @property string $country
 * @property string $vuz
 * @property integer $vpu
 * @property string $district (Харьков или район Харьковской области)
 * @property string $area (Район Харькова)
 */
class ClientsFilter extends CActiveRecord
{
	/**
	 * @return array validation rules for model attributes.
     */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('area, district, vpu, vuz, gender, status, pay_date', 'safe'),
		);
	}

	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
