<?php

class DefaultController extends OperatorPanelController
{
	public function actionIndex()
	{
        $categories = Categories::model()->findAllByAttributes(array('status' => 1));
        $noCatGuides = Guides::model()->findAllByAttributes(array('category_id' => 0, 'status' => 1));
        $clients = new Clients();
        $clients->unsetAttributes();
		$this->render('index', array(
            'categories' => $categories,
            'noCatGuides' => $noCatGuides,
            'clients' => $clients,
        ));

	}

    public function actionAllGridSearch(){
        $s = Yii::app()->getRequest()->getParam('s', '');
        $clients = new Clients;
        $clients = $clients->getClientsAsSearchOP($s);
        $html = $this->renderPartial('_allClientsGrid', array('clients' => $clients), true, false);
        echo CJSON::encode(array(
            'html'   => $html,
            'success' => true,
        ));
        Yii::app()->end();
    }

    public function actionEnablePaginate(){
        $_POST['AllClientsPagination'] = 1;
        Yii::app()->end();
    }


    public function actionRenderSavedSessionClient() {
        $html2 = '';
        if (Yii::app()->session['list_clients_tabs']) {
            $uniq_mass_ids = array_unique(Yii::app()->session['list_clients_tabs']);
            for ($i = 0; $i < count($uniq_mass_ids); $i++) {
                $client = Clients::model()->findByPk($uniq_mass_ids[$i]);
                if ($client) {
                    $html2[] .= $this->renderPartial('_clientLink', array(
                        'model' => $client,
                    ), true, false);
                }
            }
            $success = true;
        } else {
            $success = false;
        }
        echo CJSON::encode(array(
            'html2'   => $html2,
            'success' => $success,
        ));
        Yii::app()->end();
    }

    public function actionAutocomplete() {
        $term = Yii::app()->getRequest()->getParam('term');
        if(Yii::app()->request->isAjaxRequest && $term) {
            $clients = Clients::model()->findAll(array('condition'=>"card_id LIKE '%$term%' OR fullname LIKE '%$term%'"));
            $result = Clients::getAutoCompleteResult($clients);
            echo CJSON::encode($result);
            Yii::app()->end();
        }
    }

    public function actionAutocompletevalue() {
        $term = Yii::app()->getRequest()->getParam('term');
        if(Yii::app()->request->isAjaxRequest && $term) {
            $clients = Clients::model()->findAll(array('condition'=>"card_id LIKE '%$term%' OR fullname LIKE '%$term%'"));
            $result = Clients::getAutoCompleteResultCardId($clients);
            if($result){
                $clientList = is_null($result) ? '' : $this->renderPartial('_userList', array('model' => $result), true, false);
                echo CJSON::encode(array(
                    'clientList' => $clientList,
                ));
            } else {
                $empty = "no content";
                $emptyPopup = $this->renderPartial('_emptyList', array('empty' => $empty), true, false);
                echo CJSON::encode(array(
                    'emptyPopup' => $emptyPopup,
                ));
            }
            Yii::app()->end();
        }
    }

    public function actionShowGuide(){
        $id = Yii::app()->request->getParam('id', false);
        $guide = Guides::model()->findByPk($id);
        $html = is_null($guide) ? '' : $this->renderPartial('_guide', array('guide' => $guide), true, false);
        $success = !is_null($guide);
        echo CJSON::encode(array(
            'html' => $html,
            'success' => $success,
        ));
        Yii::app()->end();
    }

    public function actionGetUser(){
        $id = Yii::app()->request->getParam('id', false);
        $model = new Clients();
        $client = Clients::model()->findByPk($id);
        $model->getListClients($client->id);
        $html = is_null($client) ? '' : $this->renderPartial('_client', array('model' => $client,), true, false);
        $html2 = is_null($client) ? '' : $this->renderPartial('_clientLink', array('model' => $client,), true, false);
        $success = !is_null($client);
        echo CJSON::encode(array(
            'html' => $html,
            'html2' => $html2,
            'success' => $success,
        ));
        Yii::app()->end();

    }

    public function actionDeleteUser(){
        $id = Yii::app()->request->getParam('id', false);
        $model = new Clients();
        Yii::app()->session['list_clients_tabs'] = $model->removeListClient($id);
        echo CJSON::encode(array(
            'success' => true,
        ));
        Yii::app()->end();
    }

    public function actionLogin(){
        $model = new OperatorPanelLogin;
        // if it is ajax validation request
        if (Yii::app()->getRequest()->isAjaxRequest && Yii::app()->getRequest()->getParam('ajax') == 'login-from') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
        // collect user input data
        $post = Yii::app()->request->getPost('OperatorPanelLogin');
        if (!is_null($post)) {
            $model->attributes = $post;
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()){
                $log = new ApLogs();
                $params = array();
                $params['area'] = ApLogs::OPERATOR_PANEL;
                $log->log(ApLogs::LOGIN, $params);
                $this->redirect(array('/operator_panel'));
            }
        }
        $this->layout = 'login';
        $this->render('login', array('model' => $model));
    }

    public function actionLogout(){

        $log = new ApLogs();
        $params = array();
        $params['area'] = ApLogs::OPERATOR_PANEL;
        $log->log(ApLogs::LOGOUT, $params);
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->getBaseUrl(true) . '/operator_panel');
    }

    public function actionError(){
        if(Yii::app()->user->isGuest){
            $this->layout = false;
        }
        if(!is_null(Yii::app()->request->getParam('debug')) && YII_DEBUG){
            CVardumper::dump(Yii::app()->errorHandler->error);
            die;
        }
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }

    public function actionSaveFrom(){
        $id = Yii::app()->request->getParam('id', false);
        $from = Yii::app()->request->getParam('from', false);
        $client = Clients::model()->findByPk($id);
        $success = !is_null($client);
        if(!is_null($client)){
            $client->vpu = 1;
            $client->vpu_from = $from;
            if($client->validate()) {
                $client->save();
            } else {
                CVarDumper::dump($client->getErrors()); die;
            }
        }
        echo CJSON::encode(array(
            'success' => $success,
        ));
        Yii::app()->end();
    }

    public function actionSaveTill(){
        $id = Yii::app()->request->getParam('id', false);
        $till = Yii::app()->request->getParam('till', false);
        $client = Clients::model()->findByPk($id);
        $success = !is_null($client);
        if(!is_null($client)){
            $client->vpu_till = $till;
            $client->vpu = 1;
            if($client->validate()) {
                $client->save();
            } else {
                CVarDumper::dump($client->getErrors()); die;
            }
        }
        echo CJSON::encode(array(
            'success' => $success,
        ));
        Yii::app()->end();
    }

    public function actionSaveSerie(){
        $id = Yii::app()->request->getParam('id', false);
        $serie = Yii::app()->request->getParam('serie', false);
        $num = Yii::app()->request->getParam('num', false);
        $client = Clients::model()->findByPk($id);
        $success = !is_null($client);
        if(!is_null($client)){
            $client->vpu_serie = $serie;
            $client->vpu_number = $num;
            $client->vpu = 1;
            if($client->validate()) {
                $client->save();
            } else {
                CVarDumper::dump($client->getErrors()); die;
            }
        }
        echo CJSON::encode(array(
            'success' => $success,
        ));
        Yii::app()->end();
    }

    public function actionSearch(){

        $model = new Clients();
        $model->unsetAttributes();
        $this->layout = 'search';
        $params = Clients::getSearchParamsOP();

        $this->render('search', array(
            'clients'       => $params['clients'],
            'model'         => $model,
            'noVpu'         => $params['noVpu'],
            'overDate'      => $params['overDate'],
            'countries'     => $params['countries'],
            'districts'     => $params['districts'],
            'khdistricts'   => $params['khdistricts'],
            'vuzez'         => $params['vuzez'],
        ));
    }

    public function actionFilter(){
        $form = Yii::app()->request->getParam('form', array());
        $str = Yii::app()->request->getParam('str', '');
        $params = Clients::getOPFilterParams($form, $str);
        $html = $this->renderPartial('_filterGrid', array('clients' => $params['clients']), true);
        $html2 = $this->renderPartial('_paramsOutput', array('output' => $params['output'], 'vpu' => $params['vpu'],
            'pay' => $params['pay'], 'str' => $str), true);
        echo CJSON::encode(array(
            'success' => true,
            'html' => $html,
            'html2' => $html2,
            'noVpu' => $params['noVpu'],
            'overDate' => $params['overDate'],
            'count' => count($params['clients']),
        ));
        Yii::app()->end();
    }

    public function actionGetChat(){
        $chat_id = Yii::app()->request->getParam('id');
        $chat = Chat::model()->findByPk($chat_id);
        $html = '';
        $html2 = '';
        $success = true;
        if($chat->status == Chat::PROGRESS_STATUS){
            $html = $this->renderPartial('/layouts/_chatList', array('chats' => Chat::getChatList()), true, false);
            $success =  false;
        } else {
            $chat->status = Chat::PROGRESS_STATUS;
            $chat->operator_id = Yii::app()->user->id;
            if($chat->validate()){
                if($chat->save()){
                    $html = $this->renderPartial('/layouts/_chatList', array('chats' => Chat::getChatList()), true, false);
                    $html2 = $this->renderPartial('/layouts/_openChatList', array('chats' => Chat::getOpenChatList()), true, false);
                }
            } else {
                CVarDumper::dump($chat->getErrors());
            }
        }
        echo CJSON::encode(array(
            'success' => $success,
            'ccount' => count(Chat::getChatList()),
            'occount' => count(Chat::getOpenChatList()),
            'html' => $html,
            'html2' => $html2,
        ));
        Yii::app()->end();
    }

    public function actionAddMessageToChat(){
        $post = Yii::app()->request->getPost('operator_msg');
        $msg =  Yii::app()->request->getPost('operator_msg');
        $id =  Yii::app()->request->getPost('id');
        $message = new Message();
        $messages = $message->addOperatorMessage($msg, $id);
        $html = $this->renderPartial('/layouts/_activeChat', array('messages' => $messages, 'chat_id' => $id), true, false);
        echo CJSON::encode(array(
            'html' => $html,
            'success' => true,
            'id' => $id,
        ));
    }

    public function actionShowChat(){
        $chat_id = Yii::app()->request->getParam('id');
        $messages =  Message::model()->findAllByAttributes(array('chat_id' => $chat_id));
        $html = $this->renderPartial('/layouts/_activeChat', array('messages' => $messages, 'chat_id' => $chat_id), true, false);
        echo CJSON::encode(array(
            'success' => true,
            'html' => $html,
        ));
        Yii::app()->end();
    }

    public function actionRefreshChats(){
        $html = $this->renderPartial('/layouts/_chatList', array('chats' => Chat::getChatList()), true, false);
        echo CJSON::encode(array(
            'success' => true,
            'html' => $html,
            'count' => count(Chat::getChatList()),
        ));
        Yii::app()->end();
    }

    public function actionRefreshActiveChats(){
        $html = $this->renderPartial('/layouts/_openChatList', array('chats' => Chat::getOpenChatList()), true, false);
        echo CJSON::encode(array(
            'success' => true,
            'html' => $html,
            'count' => count(Chat::getOpenChatList()),
        ));
        Yii::app()->end();
    }

    public function actionCloseChat(){
        $chat_id = Yii::app()->request->getParam('id');
        $chat = Chat::model()->findByPk($chat_id);
        $chat->status = Chat::CLOSED_STATUS;
        if($chat->validate()){
            if($chat->save()){
                echo CJSON::encode(array(
                    'success' => true,
                ));
                Yii::app()->end();
            }
        } else {
            CVarDumper::dump($chat->getErrors());
        }
    }

}