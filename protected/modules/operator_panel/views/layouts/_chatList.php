<div id="chatlist" class="">
    <div class="all_clients_info">
        <div class="search-block clearfix">
            <div class="title"><?php echo Yii::t('operator_panel','Новые чаты'); ?></div>

            <div class="close-btn">
                <a class="close_all_clients" id="close_chats" href="javascript:" onclick="$('#all_chats').css('display', 'none');"><i class="fa fa-times"></i></a>
            </div>
        </div>
        <table class="chat_table">
            <thead>
            <tr>
                <td><?php echo Yii::t('operator_panel', 'Клиент')?></td>
                <td><?php echo Yii::t('operator_panel', 'Вопрос')?></td>
                <td><?php echo Yii::t('operator_panel', 'Сообщений')?></td>
                <td><?php echo Yii::t('operator_panel', 'Создано')?></td>
                <td></td>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($chats as $chat): ?>
                <tr>
                    <td><?php echo $chat->getClient() ?></td>
                    <td><?php echo $chat->getFirstMessage()?></td>
                    <td><?php echo count($chat->message)?></td>
                    <td><?php echo time() - $chat->created_at . ' секунд назад'?></td>
                    <td>
                        <a href="javascript:" onclick="if(confirm('Принять чат?')) OP.acceptChat(this);" data-id="<?php echo $chat->id ?>" data-url="<?php echo Yii::app()->createUrl('/operator_panel/default/getChat')?>">
                            <?php echo Yii::t('operator_panel','Принять'); ?>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>

    </div>
</div>