<div id="openChatlist" class="">
    <div class="all_clients_info">
        <div class="search-block clearfix">
            <div class="title"><?php echo Yii::t('operator_panel',' Активные чаты'); ?></div>

            <div class="close-btn">
                <a class="close_all_clients" href="javascript:" onclick="$('#active_chats').css('display', 'none');"><i class="fa fa-times"></i></a>
            </div>
        </div>
        <table data-url="<?php echo Yii::app()->createUrl('/operator_panel/default/showChat')?>">
            <thead>
            <tr>
                <td><?php echo Yii::t('operator_panel', 'Клиент')?></td>
                <td><?php echo Yii::t('operator_panel', 'Сообщений')?></td>
                <td><?php echo Yii::t('operator_panel', 'Создано')?></td>
                <td></td>
            </tr>
            </thead>
            <tbody>
            <?php foreach ($chats as $chat): ?>
                <tr>
                    <td><?php echo $chat->getClient() ?></td>
                    <td><?php echo count($chat->message)?></td>
                    <td><?php echo time() - $chat->created_at . ' секунд назад'?></td>
                    <td>
                        <a href="javascript:" onclick="OP.showChat(this)" data-id="<?php echo $chat->id ?>" data-url="<?php echo Yii::app()->createUrl('/operator_panel/default/getChat')?>">
                            <?php echo Yii::t('operator_panel','Открыть'); ?>
                        </a>
                    </td>
                </tr>
            <?php endforeach; ?>
            </tbody>
        </table>
    </div>
</div>