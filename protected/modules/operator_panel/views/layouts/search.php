<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">
    <?php
    $baseUrl = Yii::app()->baseUrl;
    $cs = Yii::app()->getClientScript();
    $cs->registerCssFile('//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
    $cs->registerCssFile('//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css');
    $cs->registerCssFile($baseUrl . '/css/font-awesome.min.css');
    $cs->registerCssFile($baseUrl . '/css/dev.css');
    $cs->registerCssFile($baseUrl . '/css/operator_style.css');
    $cs->registerScriptFile($baseUrl . '/ckeditor/ckeditor.js');
    $cs->registerScriptFile($baseUrl . '/js/jquery.js');
    $cs->registerScriptFile($baseUrl . '/js/bootstrap.min.js');
    $cs->registerScriptFile($baseUrl . '/js/dev.js');
    $cs->registerScriptFile($baseUrl . '/js/op.js');
    $cs->registerScriptFile('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js');
    $cs->registerScriptFile('https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js');
    $cs->registerScriptFile('//code.jquery.com/ui/1.11.4/jquery-ui.js');
    ?>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
<div id="wrapper">

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand home-link" href="<?php echo Yii::app()->createUrl("/operator_panel") ?>">
            <?php echo Yii::t('operator_panel', 'Центр правового содействия иностранным гражданам'); ?>
        </a>
    </div>
    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle operator-name" data-toggle="dropdown">
                <?php echo Yii::app()->user->name; ?>
                <b class="fa fa-sign-out"></b>
            </a>
            <ul class="dropdown-menu">
                <li>
                    <a href="<?php echo Yii::app()->createUrl("/operator_panel/default/logout") ?>">
                        <i class="glyphicon glyphicon-off"></i> <?php echo Yii::t('operator_panel', 'Log Out'); ?>
                    </a>
                </li>
            </ul>
            <script>
                $('.dropdown-toggle').click(function(){
                    $(this).closest('li').toggleClass('open');
                })
            </script>
        </li>
    </ul>
    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <div class="getClientsList search">
            <span class="all-client-icon"></span>
            <a href="javascript:" id="allClientsLink" onclick="$('#allClients').modal('show');" class="all-client-link">
                <?php echo Yii::t('operator_panel', 'Все клиенты')?>
            </a>
            <a href="<?php echo Yii::app()->createUrl("/operator_panel") ?>"  class="all-client-link">
                <?php echo Yii::t('operator_panel', 'Вернуться на главную') ?>
            </a>
        </div>
        <div id="userLinks" data-url="<?php echo Yii::app()->createUrl('operator_panel/default/renderSavedSessionClient');?>" >

        </div>
        <script>
            $('.left_menu_dropdown').click(function(){
                $(this).closest('li').children('.collapse').toggleClass('in');
                $(this).children('.arrow').removeClass('glyphicon-chevron-down').removeClass('glyphicon-chevron-up');
                if($(this).closest('li').children('.collapse').hasClass('in')){
                    $(this).children('.arrow').addClass('glyphicon-chevron-up');
                } else {
                    $(this).children('.arrow').addClass('glyphicon-chevron-down');
                }
            });
        </script>
    </div>
    <!-- /.navbar-collapse -->
</nav>
    <?php if(Yii::app()->getRequest()->getParam('allClientsPagination', false)): ?>
        <script>
            $(document).ready(function(){
                $('#allClientsLink').click();
            });
        </script>
    <?php endif; ?>
<div id="page-wrapper">
    <?php echo $content; ?>
</div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->

</body>
</html>
