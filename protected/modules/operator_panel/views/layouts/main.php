<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">
    <?php
    $baseUrl = Yii::app()->baseUrl;
    $cs = Yii::app()->getClientScript();
    $cs->registerCssFile('//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
    $cs->registerCssFile('//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css');
    $cs->registerCssFile($baseUrl . '/css/font-awesome.min.css');
    $cs->registerCssFile($baseUrl . '/css/dev.css');
    $cs->registerCssFile($baseUrl . '/css/operator_style.css');

    $cs->registerScriptFile($baseUrl . '/ckeditor/ckeditor.js');
    $cs->registerScriptFile($baseUrl . '/js/jquery.js');
    $cs->registerScriptFile($baseUrl . '/js/bootstrap.min.js');
    $cs->registerScriptFile($baseUrl . '/js/dev.js');
    $cs->registerScriptFile($baseUrl . '/js/op.js');
    $cs->registerScriptFile('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js');
    $cs->registerScriptFile('https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js');
    $cs->registerScriptFile('//code.jquery.com/ui/1.11.4/jquery-ui.js');
    ?>
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>
<div id="wrapper">

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand home-link" href="<?php echo Yii::app()->createUrl("/operator_panel") ?>">
            <?php echo Yii::t('operator_panel', 'Центр правового содействия иностранным гражданам'); ?>
        </a>
    </div>
    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle operator-name" data-toggle="dropdown">
                <?php echo Yii::app()->user->name; ?>
                <b class="fa fa-sign-out"></b>
            </a>
            <ul class="dropdown-menu">
                <li>
                    <a href="<?php echo Yii::app()->createUrl("/operator_panel/default/logout") ?>">
                        <i class="glyphicon glyphicon-off"></i> <?php echo Yii::t('operator_panel', 'Log Out'); ?>
                    </a>
                </li>
            </ul>
            <script>
                $('.dropdown-toggle').click(function(){
                    $(this).closest('li').toggleClass('open');
                })
            </script>
        </li>
    </ul>

    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">

        <div class="getClientsList">
            <input maxlength="50" placeholder="имя или код клиента" class="clients_autocomplete ui-autocomplete-input"
                   data-url="/uk/operator_panel/default/getUser" id="Auto_card_id" name="Clients[card_id]"
                   data-urlsecond="<?php echo Yii::app()->createUrl('operator_panel/default/autocompletevalue') ?>"
                   data-urluser="<?php echo Yii::app()->createUrl('operator_panel/default/getuserinfo') ?>"
                   type="text">
            <span id="statusClient"></span>
            <script>

                $('#Auto_card_id').keyup(function(){
                    OP.getDataByValue(this);
                });
                $('#Auto_card_id').focusout(function() {
                    $( "#statusClient" ).fadeOut();
                });
                $('#Auto_card_id').focusin(function() {
                    $( "#statusClient" ).fadeIn('medium');
                });

            </script>

            <div class="operators-popup" style="display: none">
                <div class="popup-wrapper">
                    <hr/>
                    <ul>
                        <li>
                            <img class="popup-img-box" src="/markup/image/empty-profile-img.png" alt=""/>
                            <div class="popup-text">
                                <p>Джон Доу</p>
                                <p>#9210921</p>
                            </div>
                        </li>
                        <li>
                            <img class="popup-img-box" src="/markup/image/fill-profile-img.png" alt=""/>
                            <div class="popup-text">
                                <p>Джонни Бигудь</p>
                                <p>#121322</p>
                            </div>
                        </li>
                        <li>
                            <img class="popup-img-box" src="/markup/image/fill-profile-img.png" alt=""/>
                            <div class="popup-text">
                                <p>Джо Страммер</p>
                                <p>#232344</p>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>

            <span class="all-client-icon"></span>
            <a href="javascript:" id="allClientsLink" onclick="$('#allClients').modal('show');" class="all-client-link">

                <?php echo Yii::t('operator_panel', 'Все клиенты')?>
            </a>
            <a href="<?php echo Yii::app()->createUrl("/operator_panel/default/search") ?>"  class="all-client-link">
                <?php echo Yii::t('operator_panel', 'Расширенный поиск')?>
            </a>

        </div>
        <script>
            $('.left_menu_dropdown').click(function(){
                $(this).closest('li').children('.collapse').toggleClass('in');
                $(this).children('.arrow').removeClass('glyphicon-chevron-down').removeClass('glyphicon-chevron-up');
                if($(this).closest('li').children('.collapse').hasClass('in')){
                    $(this).children('.arrow').addClass('glyphicon-chevron-up');
                } else {
                    $(this).children('.arrow').addClass('glyphicon-chevron-down');
                }

            });
        </script>
        <div id="userLinks" data-url="<?php echo Yii::app()->createUrl('operator_panel/default/renderSavedSessionClient');?>" >

        </div>
        <div class="chat-link-wrapp pull-right">
            <a target="_blank" href="<?php echo Yii::app()->createUrl('admin_panel/chat');?>" class="all-client-link">
                <?php echo Yii::t('operator_panel', 'Завершенные чаты'); ?>
            </a>
            <a href="javascript:"  onclick="$('#all_chats').css('display', 'block');" class="all-client-link">
                <?php echo Yii::t('operator_panel', 'Новые чаты') . '(<span id="chat_count">' . count(Chat::getChatList()) . '</span>)'?>
            </a>
            <a href="javascript:"  onclick="$('#active_chats').css('display', 'block');" class="all-client-link">
                <?php echo Yii::t('operator_panel', 'Активные чаты') . '(<span id="open_chat_count">' . count(Chat::getOpenChatList()) . '</span>)'?>
            </a>
        </div>
    </div>
    <!-- /.navbar-collapse -->
</nav>

<div id="page-wrapper">
    <?php echo $content; ?>
</div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
<div id="all_chats"  style="display: none">
    <?php $this->renderPartial('/layouts/_chatList', array('chats' => Chat::getChatList()))?>
</div>
<div id="active_chats"  style="display: none">
    <?php $this->renderPartial('/layouts/_openChatList', array('chats' => Chat::getOpenChatList()))?>
</div>
<div id="active_chat" style="display: none;">
    <div class="client-chat-position">
        <div class="client-chat">
            <div class="chat-wrapper">
                <div class="chat-header">
                    <span class="quest-icon">
                    </span>
                    <span> <?php echo Yii::t('operator_panel', 'Чат'); ?></span>
                    <span onclick="OP.hideChatBlock(this)"  class="glyphicon glyphicon-remove pull-right"></span>
                    <span onclick="OP.miniChatBlock(this);" class="underscore-icon"></span>
                </div>
                <div class="chat-content">
                    <div id="messages_block">
                        <?php $this->renderPartial('/layouts/_activeChat', array('messages' => array(), 'chat_id' => 0))?>
                    </div>
                    <div class="typing-panel">
                        <a href="javascript:" onclick="if(confirm('Завершить чат?')) OP.closeChat(this);" data-url="<?php echo Yii::app()->createUrl('/operator_panel/default/closeChat')?>">
                            <?php echo Yii::t('operator_panel', 'Завершить чат'); ?>
                        </a>
                        <span style="float: right" id="file_name_span"></span>

                        <form id="msg_send_form" method="post" enctype="multipart/form-data" data-url="<?php echo Yii::app()->createUrl('/operator_panel/default/addMessageToChat')?>">
                            <textarea name="operator_msg" id="operator_msg"></textarea>
                            <div class="upload-file upload-for-chat">
                                <div class="load-title">
                                    <i class="fa fa-paperclip"></i>
                                    <span><?php echo Yii::t('operator_panel', 'Файл'); ?></span>

                                </div>
                                <input type="hidden" name="id" value="0" id="chat_id" />
                                <input id="send_file" type="file" name="send_file"/>
                            </div>
                            <div class="chat-send-mes">
                                <input type="submit" id="send_message" value="<?php echo Yii::t('operator_panel', 'Отправить'); ?>" />
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>
<span style="display: none" id="refresh_chat_span" data-url="<?php echo Yii::app()->createUrl('/operator_panel/default/showChat'); ?>"></span>
<span style="display: none" id="refresh_chats_span" data-url="<?php echo Yii::app()->createUrl('/operator_panel/default/refreshChats'); ?>"></span>
<span style="display: none" id="refresh_active_chats_span" data-url="<?php echo Yii::app()->createUrl('/operator_panel/default/refreshActiveChats'); ?>"></span>
<script>
    $(function() {
        $( "#active_chat" ).draggable();
        $( "#all_chats" ).draggable();
        $( "#active_chats" ).draggable();
    });
</script>
</body>
</html>
