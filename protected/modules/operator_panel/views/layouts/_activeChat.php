<span style="display: none" id="chat_id_span" data-id="<?php echo $chat_id?>"></span>
<?php foreach($messages as $message): ?>
    <div class="chat-mes-box <?php echo $message->sender == 1 ? 'first-user' : 'second-user'; ?>">
        <div><?php echo $message->msg ?></p>

        </div>
        <p>
            <?php if(!is_null($message->filename) && $message->filename !== ''): ?>
                <a href="<?php echo '/uploadFiles/chat/' . $message->chat_id . '/' . $message->file ?>" download>
                    <?php echo $message->filename ?>
                </a>
            <?php endif; ?>
        </p>
        <span class="published-time <?php echo $message->sender == 1 ? 'first-time' : 'second-time'; ?>">
        <?php echo date('H:i', $message->created_at) ?>
        </span>
    </div>
    <div class="clearfix"></div>
<?php endforeach; ?>


