<?php if('' !== $str):?>
    <span class="param_wrapper">
            <span class="param" data-attr="<?php echo 'str'; ?>">
                <span><?php echo $str; ?></span>
                <span class="close_param" data-val="<?php echo 'str'; ?>" onclick="OP.delStrSearch(this)">
                    <i class="fa fa-times"></i>
                </span>
            </span>
        </span>
<?php endif; ?>
<?php foreach($output as $attr => $values): ?>
    <?php foreach($values as $key => $val): ?>
        <span class="param_wrapper">
            <span class="param" data-attr="<?php echo $attr; ?>">
                <span><?php echo $val; ?></span>
                <span class="close_param" data-val="<?php echo $attr . '-' . $key; ?>" onclick="OP.delParamOutput(this)">
                    <i class="fa fa-times"></i>
                </span>
            </span>
        </span>
    <?php endforeach; ?>
<?php endforeach; ?>
<?php if(false !== $vpu):?>
    <span class="param_wrapper">
            <span class="param" data-attr="<?php echo 'vpu'; ?>">
                <span><?php echo 'Без ВНЖ'; ?></span>
                <span class="close_param" data-val="<?php echo 'vpu'; ?>" onclick="OP.delParamOutput(this)">
                    <i class="fa fa-times"></i>
                </span>
            </span>
        </span>
<?php endif; ?>
<?php if(false !== $pay):?>
    <span class="param_wrapper">
            <span class="param" data-attr="<?php echo 'vpu'; ?>">
                <span><?php echo 'Не оплачено'; ?></span>
                <span class="close_param" data-val="<?php echo 'pay'; ?>" onclick="OP.delParamOutput(this)">
                    <i class="fa fa-times"></i>
                </span>
            </span>
        </span>
<?php endif; ?>