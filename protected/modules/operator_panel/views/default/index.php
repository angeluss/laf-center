<?php
/* @var $this DefaultController */
?>
<div class="page">
    <div class="left_side">
    </div>
    <div class="right_side guide_side">
        <ul class="right_panel">
            <div class="category-search">
                <p class="category_title">Частые ситуации и проблемы</p>
            </div>
            <?php foreach ($categories as $cat) : ?>
            <li>
                <p class="category_title"><?php echo $cat->title; ?></p>
                <ul>
                <?php foreach($cat->enabledGuides as $guide):?>
                    <li>
                    <p>
                        <a href="javascript:"
                           class="guide_link"
                           data-id="<?php echo $guide->id?>"
                           onclick="OP.showGuide(this)"
                           data-url="<?php echo Yii::app()->createUrl('/operator_panel/default/showGuide')?>"
                        >
                            <?php echo $guide->title; ?>
                        </a>
                    </p>
                    </li>
                <?php endforeach; ?>
                </ul>
            </li>
            <?php endforeach; ?>
            <li>
                <p class="category_title"><?php echo Yii::t('operator_panel', 'Без категории'); ?></p>
                <ul>
                <?php foreach($noCatGuides as $guide): ?>
                <li>
                    <p>
                        <a href="javascript:;"
                           class="guide_link"
                           data-id="<?php echo $guide->id?>"
                           onclick="OP.showGuide(this)"
                           data-url="<?php echo Yii::app()->createUrl('/operator_panel/default/showGuide')?>"
                        >
                            <?php echo $guide->title; ?>
                        </a>
                    </p>
                </li>
                <?php endforeach; ?>
                </ul>
            </li>
        </ul>
        <div class="right_content">

        </div>
        <div id="allClients" class="modal fade in" style="display: none">
            <div class="all_clients_info">
                <div class="search-block clearfix">
                    <div class="title"><?php echo Yii::t('operator_panel','Все клиенты'); ?></div>
                    <div class="search-wrap for-head-title">
                        <form action="<?php echo Yii::app()->createUrl('operator_panel/default/allGridSearch'); ?>" id="clientsGridForm">
                            <input id="search" class="oper-search" name="search" type="text" placeholder="Имя или код клиента">
                            <input id="search_submit" class="oper-search" value="Rechercher" type="submit">
                        </form>
                    </div>
                    <div class="close-btn">
                        <a class="close_all_clients" href="javascript:" onclick="$('#allClients').modal('hide');">
                            <i class="fa fa-times"></i>
                        </a>
                    </div>
                </div>
                <div id="clients_grid_wrapper">
                    <?php $this->renderPartial('_allClientsGrid', array('clients' => $clients->searchOP(true))); ?>
                    <span id="paginate_url" data-url="<?php echo Yii::app()->createUrl('operator_panel/default/enablePaginate'); ?>" style="display: none"></span>
                </div>
            </div>
        </div>
    </div>
</div>
<?php if(Yii::app()->getRequest()->getParam('allClientsPagination', false)): ?>
<script>
    $(document).ready(function(){
        $('#allClientsLink').click();
    });
</script>
<?php endif; ?>