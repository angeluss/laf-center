<?php
/* @var $this DefaultController */
/* @var $model DefaultController from actionAutocompletevalue() method */
?>

<div class="operators-popup">
    <div class="popup-wrapper">
        <hr/>
        <ul>
            <?php foreach($model as $key => $value):
//                CVarDumper::dump($model);die();
                ?>

            <li data-id="<?php echo $model[$key]['id']; ?>" onclick="OP.showUserAutocompleteCustom(this)"
                data-url="/uk/operator_panel/default/GetUser">
                    <img class='popup-img-box' src="<?= $model[$key]['src'] ?>" alt="" />
                <div class="popup-text">
                    <p><?php echo $model[$key]['label']; ?></p>
                    <p>#<?php echo $model[$key]['card_id']; ?></p>
                </div>
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
</div>