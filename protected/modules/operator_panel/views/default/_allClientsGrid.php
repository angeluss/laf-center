<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'clients-grid',
    'dataProvider' => $clients,
    'htmlOptions' => array(
        'style' => 'cursor: pointer;',
        'data-url' => Yii::app()->urlManager->createUrl('operator_panel/default/getUser'),
    ),
    'itemsCssClass' => 'users-list',
    'rowHtmlOptionsExpression' => 'array("data-id" => $data->id)',
    'columns' => array(
        'fullname',
        array(
            'value' => 'Clients::getGenders($data->gender)',
            'name' => 'gender',
        ),
        'card_id',
        array(
            'value' => '$data->getCountry()',
            'type' => 'raw',
            'name' => 'country',
        ),
        array(
            'value' => '$data->pay_date ? date("F j, Y, g:i a",$data->pay_date) : "" ',
            'name' => 'pay_date',
        ),
    )
)); ?>
<script>
    $('#clients-grid tr').on('click', function(){
        OP.showUser(this);
        $('#allClients').modal('hide');
    });
</script>
