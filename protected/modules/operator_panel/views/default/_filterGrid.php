<ul class="clients_grid">
    <?php foreach ($clients as $model): ?>
        <li class="client_row">
            <div class="first-column">
                <a href='javascript:'
                   data-url="<?php echo Yii::app()->urlManager->createUrl('operator_panel/default/getUser') ?>"
                   onclick="OP.showUserFromLink(this, <?php echo $model->id?>);"
                   class=""
                    >
                    <?php if(isset($model->doc[0]->photo) && $model->doc[0]->photo != ''): ?>
                        <img class='tab_client_avatar' src="/uploadFiles/clientsPhoto/<?php echo $model->id . '/' . $model->doc[0]->photo?>" alt="" />
                    <?php else: ?>
                        <img class='tab_client_avatar' src="/uploadFiles/noavatar.png" alt="" />
                    <?php endif; ?>
                    <?php echo $model->fullname ?>
                </a>
            </div>
            <div class="second-column"><?php echo $model->getCountry() . ' ' . $model->getCountryName() ?></div>
            <div class="third-column"><?php echo $model->getOnlyDistrict(); ?></div>
            <div class="fourth-column vuz-column"><?php echo Vuz::getVuses($model->vuz)?></div>
            <div class="fifth-column">
                <?php if($model->vpu == 1):?>
                    <span class="vpu_op_search <?php echo $model->getIsVpuExpired() ? 'vpu_expired' : '' ?>">
                                        <?php echo $model->vpu_serie . ' ' . $model->vpu_number ?>
                                    </span>
                    <span class="<?php echo $model->getIsVpuExpired() ? 'vpu_expired' : '' ?>">
                                        <?php echo $model->vpu_till ?>
                                    </span>
                <?php else: ?>
                    <span class="no_vpu"><?php echo Yii::t('operator_panel', 'отсутствует'); ?></span>
                <?php endif; ?>
            </div>
            <?php
            $class='';
            if($model->isOverDate()) {
                $class='no_vpu';
            }
            ?>
            <div class="sixth-column">
                <span class="<?php echo $class; ?>">
                    <?php echo $model->getPayDate(); ?>
                </span>
            </div>
        </li>
    <?php endforeach; ?>
</ul>