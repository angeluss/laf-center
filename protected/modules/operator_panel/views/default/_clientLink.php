<div class="userLink" id="ident-<?php echo $model->id ?>">
    <a href='javascript:'
       data-url="<?php echo Yii::app()->urlManager->createUrl('operator_panel/default/getUser') ?>"
       onclick="OP.showUserFromLink(this, <?php echo $model->id?>);"
       class="user-menu-item"
        >
        <?php if($model->doc[0]->photo != ''): ?>
            <img class='tab_client_avatar' src="/uploadFiles/clientsPhoto/<?php echo $model->id . '/' . $model->doc[0]->photo?>" alt="" />
        <?php else: ?>
            <img class='tab_client_avatar' src="/uploadFiles/noavatar.png" alt="" />
        <?php endif; ?>
              <?php echo $model->fullname ?>
    </a>
      <span  data-url="<?php echo Yii::app()->urlManager->createUrl('operator_panel/default/deleteUser') ?>"
             onclick="OP.deleteUserFromLink(this, <?php echo $model->id ?>);" class="del-user-btn"><i class="fa fa-times"></i></span>
</div>
