<?php
/* @var $this DefaultController */
/* @var $model DefaultController from actionAutocompletevalue() method */
?>

<div class="empty-popup">
    <div class="empty-popup-wrapper">
        <hr/>
        <p>Таких клиентов не найдено</p>
        <p>Уточните имя или введите индивидуальный код.</p>
    </div>
</div>