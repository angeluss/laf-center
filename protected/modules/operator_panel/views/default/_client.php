<div class="op_client_top" id="klient-card-<?php echo $model->id ?>">
    <img src="/images/claf-operator-0807-profile-chat.png" alt="" class="top-client-img"/>
    <div class="top_client_info">
        <div class="avatar-box">
            <?php if($model->doc[0]->photo != ''): ?>
                <img class='operator_panel_client_avatar' src="/uploadFiles/clientsPhoto/<?php echo $model->id . '/' . $model->doc[0]->photo?>" alt="" />
            <?php else: ?>
                <img class='operator_panel_client_avatar' src="/uploadFiles/noavatar.png" alt="" />
            <?php endif; ?>
        </div>
        <!-- add div block to vertically align 2 titles-->
        <div class="user-names">

            <span><?php echo $model->fullname?></span>
            <span class='client_name_rus'><?php echo Translit::getBackTranslit($model->fullname); ?></span>
        </div>
        <span class="country-img-box"><?php echo $model->getCountry() ?></span>
    </div>
</div>
<div class="op_client_bottom">
    <div class="client-map">
        <iframe
            frameborder="0" style="border:0"
            src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d5129.60500063874!2d36.237117499343604!3d49.99631214524478!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x4127a0ef418c6db1%3A0xa835224cc4064eb2!2z0LLRg9C7LiDQn9GD0YjQutGW0L3RgdGM0LrQsCwgMjUsINCl0LDRgNC60ZbQsiwg0KXQsNGA0LrRltCy0YHRjNC60LAg0L7QsdC70LDRgdGC0Yw!5e0!3m2!1sru!2sua!4v1439395767192" allowfullscreen>
        </iframe>
    </div>
    <div style="display: none" class="client-chat-position">
        <div class="client-chat">
            <div class="chat-wrapper">
                <div class="chat-header">
                    <span class="quest-icon">
                    </span><span>Чат</span>
                    <span class="min-window-icon"></span>
                    <span class="underscore-icon"></span>
                </div>
                <div class="chat-content">
                    <div class="chat-mes-box first-user">
                        <p>Меня остановил наряд милиции с просьбой предъявить документы, а у меня их с
                        собой нет.</p>
                        <p>Что мне делать??</p>
                        <span class="published-time">17:20</span>
                    </div>
                    <div class="chat-mes-box second-user">
                        <p>В случае, если у вас нет документов и вы не совершили ничего противозаконного,
                        вас не имеют право задерживать, а обязаны связаться с родными и близкими, которые могли
                        бы подтвердить вашу личность.</p>
                        <span class="published-time">17:21</span>
                    </div>
                    <div class="chat-mes-box first-user">
                        <p>Меня остановил наряд милиции с просьбой предъявить документы, а у меня их с
                            собой нет.</p>
                        <p>Что мне делать??</p>
                        <span class="published-time">17:22</span>
                    </div>

                    <div class="typing-panel">
                        <form action="#">
                            <textarea name="" id=""></textarea>
                            <div class="upload-file upload-for-chat">
                                <div class="load-title">
                                    <i class="fa fa-paperclip"></i>
                                    <span>Файл</span>
                                </div>
                                <input type="file" name=""/>
                            </div>
                            <div class="chat-send-mes">
                                <input type="submit"/>
                            </div>

                        </form>

                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="client_left_panel">
        <p class="client_id">
            <?php echo $model->card_id != '' ? '#' . $model->card_id : Yii::t('operator_panel', 'Карта еще не выдана');?>
        </p>
        <br>
        <p>
            <a href="#fio">
                <?php echo Yii::t('operator_panel', 'Имя и Фамилия'); ?>
            </a>
        </p>
        <p>
            <a href="#country">
                <?php echo Yii::t('operator_panel', 'Гражданство'); ?>
            </a>
        </p>
        <p>
            <a href="#lang">
                <?php echo Yii::t('operator_panel', 'Языки'); ?>
            </a>
        </p>
        <br>
        <p>
            <a href="#">
                <?php echo Yii::t('operator_panel', 'Паспорт'); ?>
            </a>
        </p>
        <p>
            <a href="#">
                <?php echo Yii::t('operator_panel', 'Виза'); ?>
            </a>
        </p>
        <p>
            <a href="#">
                <?php echo Yii::t('operator_panel', 'Права'); ?>
            </a>
        </p>
        <br>
        <p>
            <a href="#">
                <?php echo Yii::t('operator_panel', 'Адреса'); ?>
            </a>
        </p>
        <p>
            <a href="#">
                <?php echo Yii::t('operator_panel', 'Телефоны'); ?>
            </a>
        </p>
        <p>
            <a href="#">
                <?php echo Yii::t('operator_panel', 'Родственники'); ?>
            </a>
        </p>
        <p>
            <a href="#">
                <?php echo Yii::t('operator_panel', 'Доверенные лица'); ?>
            </a>
        </p>
        <p>
            <a href="#">
                <?php echo Yii::t('operator_panel', 'Другие контакты'); ?>
            </a>
        </p>
        <br>
        <p>
            <a href="#">
                <?php echo Yii::t('operator_panel', 'Образование'); ?>
            </a>
        </p>
        <br>
        <p>
            <a href="#">
                <?php echo Yii::t('operator_panel', 'Договор'); ?>
            </a>
        </p>
        <p>
            <a href="#">
                <?php echo Yii::t('operator_panel', 'Другие документы'); ?>
            </a>
        </p>
    </div>
    <div class="client_right_panel">
        <!-- nationality -->
        <div class="data-block clearfix">
            <p class="data-title"><?php echo Yii::t('operator_panel', 'Гражданство'); ?></p>
            <div class="one-column">
                <p><?php echo Yii::t('operator_panel', 'Гражданство'); ?></p>
                <p><?php echo Yii::t('operator_panel', 'Языки общения'); ?></p>
            </div>
            <div class="central-column">
                <p><?php echo $model->getCountryName() ?></p>
                <p><?php echo $model->getLangs()?></p>
            </div>
        </div>
        <!-- date of birth -->
        <div class="data-block clearfix">
            <p class="data-title"><?php echo Yii::t('operator_panel', 'Дата рождения'); ?></p>
            <div class="one-column">
                <p><?php echo Yii::t('operator_panel', 'Дата рождения'); ?></p>
                <p><?php echo Yii::t('operator_panel', 'Пол'); ?></p>
            </div>
            <div class="central-column">
                <p><?php echo $model->bitrhdate?></p>
                <p><?php echo Clients::getFullGenders($model->gender)?></p>
            </div>
        </div>
        <!-- passport data -->
        <div class="data-block clearfix">
            <p class="data-title"><?php echo Yii::t('operator_panel', 'Паспорт'); ?></p>
            <div class="one-column">
                <p><a name="fio"></a>   <?php echo Yii::t('operator_panel', 'ФИО'); ?></p>
                <p><?php echo Yii::t('operator_panel', 'Серия, номер'); ?></p>
                <p><?php echo Yii::t('operator_panel', 'Дата выдачи'); ?></p>
                <p><?php echo Yii::t('operator_panel', 'Срок действия'); ?></p>
            </div>
            <div class="central-column uppercase">
                <p><?php echo $model->get_passport_name()?></p>
                <p><?php echo $model->passport?></p>
                <p><?php echo $model->date_passport?></p>
                <p><?php echo $model->passport_till?></p>
            </div>
            <div class="right-column">
                <?php if($model->doc[0]->passport != ''): ?>
                    <img src="/uploadFiles/clientsPhoto/<?php echo $model->id . '/' . $model->doc[0]->passport?>" alt="" />
                <?php endif; ?>
            </div>
        </div>
        <!-- visa -->
       <div class="data-block clearfix">
            <p class="data-title"><?php echo Yii::t('operator_panel', 'Виза'); ?></p>
            <div class="right-column">
                <?php if($model->doc[0]->visa != ''):?>
                    <img src="/uploadFiles/clientsPhoto/<?php echo $model->id . '/' . $model->doc[0]->visa?>" alt="" />
                <?php endif; ?>
            </div>
        </div>
        <!-- Residence -->
        <div class="data-block clearfix" id="vpu_div" data-id="<?php echo $model->id?>">
            <p class="data-title"><?php echo Yii::t('operator_panel', 'Вид на жительство'); ?></p>
            <?php if($model->vpu == 1):?>
            <div class="one-column">
                <p><?php echo Yii::t('operator_panel', 'Серия и номер'); ?></p>
                <p><?php echo Yii::t('operator_panel', 'Срок действия'); ?></p>
                <p><?php echo Yii::t('operator_panel', 'Орган, выдавший удостоверение'); ?></p>
            </div>

            <div class="central-column uppercase">
                <?php if($model->vpu_serie != '' and $model->vpu_number != ''): ?>
                    <p><?php echo $model->vpu_serie . '' . $model->vpu_number?></p>
                <?php else: ?>
                    <p class="vpu_num_p" data-url="<?php echo Yii::app()->createUrl('/operator_panel/default/saveSerie');?>">
                        <a id="edit_ser_num" onclick="OP.editSerie()" href="javascript:"><?php echo Yii::t('operator_panel', 'Редактировать') ?></a>
                        <input id="vpu_serie" type="text" name="serie" style="display: none; width:15%;" />
                        <input id="vpu_num" type="text" name="number" style="display: none; width:31%;" />
                        <button onclick="OP.saveSerie()" id="ok_serie" style="display: none;" >YES</button>
                        <button onclick="OP.cancelSerie()" id="cancel_serie" style="display: none;" >NO</button>
                    </p>
                <?php endif; ?>
                <?php if($model->vpu_till != ''): ?>
                    <p><?php echo $model->vpu_till?></p>
                <?php else: ?>
                    <p class="vpu_till_p" data-url="<?php echo Yii::app()->createUrl('/operator_panel/default/saveTill');?>">
                        <a id="edit_till" onclick="OP.editTill()" href="javascript:"><?php echo Yii::t('operator_panel', 'Редактировать') ?></a>
                        <input id="vpu_till" type="text" name="till" style="display: none; width:49%;"/>
                        <button onclick="OP.saveTill()" id="ok_till" style="display: none;" >YES</button>
                        <button onclick="OP.cancelTill()" id="cancel_till" style="display: none;" >NO</button>
                    </p>
                <?php endif; ?>
                <?php if($model->vpu_from != ''): ?>
                    <p><?php echo $model->vpu_from?></p>
                <?php else: ?>
                    <p class="vpu_from_p" data-url="<?php echo Yii::app()->createUrl('/operator_panel/default/saveFrom');?>">
                        <a id="edit_from" onclick="OP.editFrom()" href="javascript:"><?php echo Yii::t('operator_panel', 'Редактировать') ?></a>
                        <input id="vpu_from" type="text" name="from" style="display: none; width:49%;" />
                        <button onclick="OP.saveFrom()" id="ok_from" style="display: none;" >YES</button>
                        <button onclick="OP.cancelFrom()" id="cancel_from" style="display: none;" >NO</button>
                    </p>
                <?php endif; ?>
            </div>
            <div class="right-column">
                <?php if($model->doc[0]->vpu != ''): ?>
                    <img src="/uploadFiles/clientsPhoto/<?php echo $model->id . '/' . $model->doc[0]->vpu?>" alt="" />
                <?php endif; ?>
            <div>
            </div>
            <?php else: ?>
                <div class="one-column">
                    <p><?php echo Yii::t('operator_panel', 'Серия и номер'); ?></p>
                    <p><?php echo Yii::t('operator_panel', 'Срок действия'); ?></p>
                    <p><?php echo Yii::t('operator_panel', 'Орган, выдавший удостоверение'); ?></p>
                </div>

                <div class="central-column uppercase" id="vpu_div" data-id="<?php echo $model->id?>">
                    <p class="vpu_num_p" data-url="<?php echo Yii::app()->createUrl('/operator_panel/default/saveSerie');?>">
                        <a id="edit_ser_num" onclick="OP.editSerie()" href="javascript:"><?php echo Yii::t('operator_panel', 'Редактировать') ?></a>
                        <input id="vpu_serie" type="text" name="serie" style="display: none; width:15%;" />
                        <input id="vpu_num" type="text" name="number" style="display: none; width:31%;" />
                        <button onclick="OP.saveSerie()" id="ok_serie" style="display: none;" >YES</button>
                        <button onclick="OP.cancelSerie()" id="cancel_serie" style="display: none;" >NO</button>
                    </p>
                    <p class="vpu_till_p" data-url="<?php echo Yii::app()->createUrl('/operator_panel/default/saveTill');?>">
                        <a id="edit_till" onclick="OP.editTill()" href="javascript:"><?php echo Yii::t('operator_panel', 'Редактировать') ?></a>
                        <input id="vpu_till" type="text" name="till" style="display: none; width:49%;"/>
                        <button onclick="OP.saveTill()" id="ok_till" style="display: none;" >YES</button>
                        <button onclick="OP.cancelTill()" id="cancel_till" style="display: none;" >NO</button>
                    </p>
                    <p class="vpu_from_p" data-url="<?php echo Yii::app()->createUrl('/operator_panel/default/saveFrom');?>">
                        <a id="edit_from" onclick="OP.editFrom()" href="javascript:"><?php echo Yii::t('operator_panel', 'Редактировать') ?></a>
                        <input id="vpu_from" type="text" name="from" style="display: none; width:49%;" />
                        <button onclick="OP.saveFrom()" id="ok_from" style="display: none;" >YES</button>
                        <button onclick="OP.cancelFrom()" id="cancel_from" style="display: none;" >NO</button>
                    </p>
                </div>
                <div class="right-column">
                    <?php if($model->doc[0]->vpu != ''): ?>
                        <img src="/uploadFiles/clientsPhoto/<?php echo $model->id . '/' . $model->doc[0]->vpu ?>" alt="" />
                    <?php endif; ?>
                    <div></div>
            <?php endif; ?>
        </div><br><br><br><br><br><br><br>
                <script>
                    jQuery( document). ready(function(){
                        jQuery('#vpu_till').datepicker({
                            changeMonth: true,
                            changeYear: true,
                            yearRange: '1900:2100',
                            firstDay: 1
                        });
                    });
                </script>
        <!-- university -->
        <div class="data-block clearfix">
            <p class="data-title"><?php echo Yii::t('operator_panel', 'ВУЗ'); ?></p>
            <div class="one-column">
                <p><?php echo Yii::t('operator_panel', 'ВУЗ'); ?></p>
            </div>
            <div class="central-column remain-length">
                <p><?php echo Vuz::getVuses($model->vuz)?></p>
            </div>
        </div>
        <!-- contacts -->
        <div class="data-block contacts-block clearfix">
            <p class="data-title"><?php echo Yii::t('operator_panel', 'Контакты'); ?></p>
            <div class="one-row clearfix">
                <div class="one-column">
                    <p><?php echo Yii::t('operator_panel', 'Адрес регистрации'); ?></p>
                </div>
                <div class="central-column remain-length">
                    <p><?php echo $model->getRegAddr()?>
                        <?php if($model->district == 1):?>
                        <span class="vert-dash"></span>
                        <span class="district-name"><?php echo Country::getHDistrict($model->area)?></span>
                        <?php endif; ?>
                    </p>
                </div>
            </div>
            <div class="one-row clearfix">
                <div class="one-column">
                    <p><?php echo Yii::t('operator_panel', 'Фактический адрес'); ?></p>
                </div>
                <div class="central-column remain-length">
                    <p><?php echo $model->address_reg?>
                    </p>
                </div>
            </div>
            <div class="one-row clearfix">
                <div class="one-column">
                    <p><?php echo Yii::t('operator_panel', 'Телефон'); ?></p>
                </div>
                <div class="central-column remain-length">
                    <p><?php echo $model->phone?></p>
                </div>
            </div>
            <div class="one-row clearfix">
                <div class="one-column">
                    <p>Email</p>
                </div>
                <div class="central-column remain-length">
                    <p><?php echo $model->email?></p>
                </div>
            </div>

        </div>

        <div class="data-block clearfix" >
            <p class="data-title table-title"><?php echo Yii::t('operator_panel', 'Документы и файлы'); ?></p>
            <table class="doc-list">
                <thead>
                    <tr>
                        <th><?php echo Yii::t('operator_panel', 'Файл')?></th>
                        <th><?php echo Yii::t('operator_panel', 'Автор')?></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                <?php if($model->doc[0]->passport != ''): ?>
                <tr>
                    <td>
                        <p class="doc-name">
                            <?php echo Yii::t('operator_panel', 'Паспорт') ?>
                        </p>
                        <p>
                            <a href="/uploadFiles/clientsPhoto/<?php echo $model->id . '/' . $model->doc[0]->passport?>" target="_blank">
                                <?php echo $model->getDocName($model->doc[0]->passport)?>
                            </a>
                        </p>
                    </td>
                    <td>Клиент</td>
                    <td>
                    </td>
                </tr>
                <?php endif; ?>
                <?php if($model->doc[0]->visa != ''): ?>
                <tr>
                    <td>
                        <p class="doc-name">
                            <?php echo Yii::t('operator_panel', 'Виза') ?>
                        </p>
                        <p>
                            <a href="/uploadFiles/clientsPhoto/<?php echo $model->id . '/' . $model->doc[0]->visa?>" target="_blank">
                                <?php echo $model->getDocName($model->doc[0]->visa)?>
                            </a>
                        </p>
                    </td>
                    <td>Клиент</td>
                    <td>
                    </td>
                </tr>
                <?php endif; ?>
                <?php if($model->doc[0]->vpu != ''): ?>
                <tr>
                    <td>
                        <p class="doc-name">
                            <?php echo Yii::t('operator_panel', 'Вид на жительство') ?>
                        </p>
                        <p>
                            <a href="/uploadFiles/clientsPhoto/<?php echo $model->id . '/' . $model->doc[0]->vpu?>" target="_blank">
                                <?php echo $model->getDocName($model->doc[0]->vpu)?>
                            </a>
                        </p>
                    </td>
                    <td>Клиент</td>
                    <td>
                    </td>
                </tr>
                <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
    </div>
</div>