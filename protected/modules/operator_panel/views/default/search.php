<?php
/* @var $this DefaultController */
?>
<div class="page">
    <div class="right_side_search">
        <div class="search_top">
            <div class="left_top_search">
                <span>
                    <form id="filter_card_id">
                        <input maxlength="50"
                               placeholder="имя или код клиента"
                               class="clients_autocomplete ui-autocomplete-input"
                               data-url="/uk/operator_panel/default/getUser"
                               id="Clients_card_id"
                               name="Clients[card_id]"
                               type="text"
                        >
                    </form>
                </span>
                <a href="javascript:" onclick="$('.filters_panel').toggleClass('closed')" class="all-client-link">
                    <?php echo Yii::t('operator_panel', 'Расширенный поиск')?>
                </a>
            </div>
            <div class="right_top_search">
                <p>
                    <?php echo Yii::t('operator_panel', 'Всего ') ?>
                    <span id="count_cli">
                        <?php echo count($clients) ?>
                    </span>
                    <?php echo Yii::t('operator_panel', ' клиентов')?>
                </p>
                <p>
                    <a href="javascript:"  onclick="OP.addVpu()">
                        <?php echo Yii::t('operator_panel', 'Без вида на жительство ')?>
                    </a>
                    <span id="noVpu" class="filter_red_round no_vpu">
                        --
                    </span>
                    <a href="javascript:"  onclick="OP.addPay()">
                        <?php echo Yii::t('operator_panel', 'Услуга не оплачена')?>
                    </a>
                    <span id="overdate" class="filter_red_round no_vpu">
                        --
                    </span>
                    <button id="discard_search">
                        <?php echo Yii::t('operator_panel', 'Сбросить поиск')?>
                    </button>
                </p>
                <p id="params_output">

                </p>

            </div>
        </div>
        <div class="search_bottom">
            <div class="filters_panel closed">
                <button class="btn btn-danger" id="filter_button" data-over="0" data-vpu="0" data-url="<?php echo Yii::app()->createUrl('operator_panel/default/filter'); ?>" onclick="OP.filterClients(this)" value="accept">
                    <?php echo Yii::t('operator_panel', 'Применить'); ?>
                </button>
                <button class="btn btn-link" onclick="$('.filters_panel').addClass('closed')" value="close" name="Закрыть">
                    <?php echo Yii::t('operator_panel', 'Закрыть'); ?>
                </button>
                <form id="filter_form">
                <p onclick="$('#countries_filter').toggleClass('closed')">
                    <?php echo Yii::t('operator_panel', 'Гражданство'); ?>
                    <i class="fa fa-chevron-down"></i>
                </p>
                <div id="countries_filter">
                    <?php foreach($countries as $con => $count): ?>
                        <input type="checkbox" name="Country_<?php echo $con ?>" id="country-<?php echo $con ?>" />
                        <label for="country-<?php echo $con; ?>"><span class="checkbox-img"></span><p><?php echo $con . '</p> <span> (' . $count . ')</span>'; ?></label><br>
                    <?php endforeach; ?><br>
                </div>
                <p onclick="$('#district_filter').toggleClass('closed')">
                    <?php echo Yii::t('operator_panel', 'Район области'); ?>
                    <i class="fa fa-chevron-down"></i>
                </p>
                <div id="district_filter">
                    <?php foreach($districts as $con => $count): ?>
                        <input type="checkbox" name="District_<?php echo $con ?>" id="district-<?php echo $con ?>" />
                        <label for="district-<?php echo $con; ?>"><span class="checkbox-img"></span><p><?php echo Country::getDistrict($con) . ' </p> <span> (' . $count . ')</span>'; ?></p></label><br>
                    <?php endforeach; ?><br>
                </div>
                <p onclick="$('#areas_filter').toggleClass('closed')">
                    <?php echo Yii::t('operator_panel', 'Район г. Харькова'); ?>
                    <i class="fa fa-chevron-down"></i>
                </p>
                <div id="areas_filter">
                    <?php foreach($khdistricts as $con => $count): ?>
                        <input type="checkbox" name="Area_<?php echo $con ?>" id="area-<?php echo $con ?>" />
                        <label for="area-<?php echo $con; ?>"><span class="checkbox-img"></span><p><?php echo Country::getHDistrict($con) . '</p> <span> (' . $count . ')</span>'; ?></p></label><br>
                    <?php endforeach; ?><br>
                </div>
                <p onclick="$('#vuzes_filter').toggleClass('closed')">
                    <?php echo Yii::t('operator_panel', 'ВУЗ'); ?>
                    <i class="fa fa-chevron-down"></i>
                </p>
                <div id="vuzes_filter">
                    <?php foreach($vuzez as $con => $count): ?>
                        <input type="checkbox" name="Vuz_<?php echo $con ?>" id="vuz-<?php echo $con ?>" />
                        <label for="vuz-<?php echo $con; ?>"><span class="checkbox-img"></span><p><?php echo Vuz::getVuses($con) . '</p> <span>(' . $count . ')</span>'; ?></p></label><br>
                    <?php endforeach; ?><br>
                </div>
                <div style="display:none">
                    <input type="checkbox" name="vpu" id="vpu" />
                    <input type="checkbox" name="pay" id="pay" />
                </div>
                </form>
            </div>
            <div class="clients_grid> in_table_head">
                <ul class="clients_grid">
                    <li class="client_row">
                        <div class="clients_table_head">
                            <div class="first-column">
                                <span><?php echo Yii::t('operator_panel', 'Имя и код') ?></span>
                            </div>
                            <div class="second-column" >
                                <span><?php echo Yii::t('operator_panel', 'Гражданство') ?></span>
                            </div>
                            <div class="third-column">
                                <span><?php echo Yii::t('operator_panel', 'Район') ?></span>
                            </div>
                            <div class="fourth-column">
                                <span><?php echo Yii::t('operator_panel', 'ВУЗ') ?></span>
                            </div>
                            <div class="fifth-column">
                                <span><?php echo Yii::t('operator_panel', 'Вид на жит-во') ?></span>
                            </div>
                            <div class="sixth-column">
                                <span><?php echo Yii::t('operator_panel', 'Оплачено до') ?></span>
                            </div>
                        </div>
                    </li>
                    </ul>
                <div class="clients_table_body"></div>
            </div>
        </div>
    </div>
    <?php // @TODO ?>
    <!-- left_side -это кусок джаваскрипта. Стили вешаем на  op_search_client_block -->
    <div class="left_side op_search_client_block">
    </div>
    <div id="allClients" class="modal fade in" style="display: none">
        <div class="all_clients_info">
            <div class="search-block clearfix">
                <div class="title"><?php echo Yii::t('operator_panel','Все клиенты'); ?></div>
                <div class="search-wrap for-head-title">
                    <form action="<?php echo Yii::app()->createUrl('operator_panel/default/allGridSearch'); ?>" id="clientsGridForm">
                        <input id="search" class="oper-search" name="search" type="text" placeholder="Имя или код клиента">
                        <input id="search_submit" class="oper-search" value="Rechercher" type="submit">
                    </form>
                </div>
                <div class="close-btn">
                    <a class="close_all_clients" href="javascript:" onclick="$('#allClients').modal('hide');"><i class="fa fa-times"></i></a>
                </div>
            </div>
            <div id="clients_grid_wrapper">
                <?php $this->renderPartial('_allClientsGrid', array('clients' => $model->searchOP(true))); ?>
            </div>
        </div>
    </div>
</div>