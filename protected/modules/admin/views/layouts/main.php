<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sb-admin.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/admin.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/morris.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/admin_panel.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.min.css">
    <script src="/ckeditor/ckeditor.js"></script>
    <link rel="stylesheet" href="/ckeditor/samples/css/samples.css">
    <link rel="stylesheet" href="/ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css">
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

	<title><?php echo Settings::model()->find()->meta_title ?></title>
</head>

<body>
    <div id="wrapper">

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <a class="navbar-brand" href="<?php echo Yii::app()->createUrl("/admin") ?>"  style="color: white" >
                <?php echo Yii::t('admin', 'Admin'); ?>
            </a>
            <a class="navbar-brand" href="<?php echo Yii::app()->createUrl("/admin_panel") ?>">
                <?php echo Yii::t('admin', 'Admin_Panel'); ?>
            </a>
        </div>
        <!-- Top Menu Items -->
        <ul class="nav navbar-right top-nav">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle " data-toggle="dropdown">
                    <i class="glyphicon glyphicon-user"></i>
                    <?php echo Yii::app()->user->name; ?>
                    <b class="caret"></b>
                </a>
                <ul class="dropdown-menu">
                    <li>
                        <a href="<?php echo Yii::app()->createUrl("/admin/default/logout") ?>">
                            <i class="glyphicon glyphicon-off"></i> <?php echo Yii::t('admin', 'Log Out'); ?>
                        </a>
                    </li>
                </ul>
                <script>
                    $('.dropdown-toggle').click(function(){
                        $(this).closest('li').toggleClass('open');
                    })
                </script>
            </li>
        </ul>
        <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
        <div class="collapse navbar-collapse navbar-ex1-collapse">
            <ul class="nav navbar-nav side-nav">
            <?php if(Yii::app()->user->getState('role') == 'admin'): ?>
                <li class="<?php echo Yii::app()->controller->id == 'default' ? 'active' : ''?>">
                    <a href="<?php echo Yii::app()->createUrl("/admin") ?>"><i class="glyphicon glyphicon-list-alt"></i> <?php echo Yii::t('admin', 'Dashboard'); ?></a>
                </li>
                <li class="<?php echo Yii::app()->controller->id == 'admins' ? 'active' : ''?>">
                    <a href="<?php echo Yii::app()->createUrl("/admin/admins") ?>"><i class="glyphicon glyphicon-user"></i> <?php echo Yii::t('admin', 'Адміністратори'); ?></a>
                </li>
                <li class="<?php echo Yii::app()->controller->id == 'settings' || Yii::app()->controller->id == 'requisites' ? 'active' : ''?>">
                    <a href="javascript:" class="left_menu_dropdown">
                        <i class="glyphicon glyphicon-fire"></i>
                        <?php echo Yii::t('admin', 'Налаштування'); ?>
                        <i class="arrow glyphicon glyphicon-chevron-down"></i>
                    </a>
                    <ul id="demo" class="collapse">
                        <li class="<?php echo Yii::app()->controller->id == 'settings' ? 'active' : ''?>">
                            <a href="<?php echo Yii::app()->createUrl("/admin/settings") ?>">
                                <i class="glyphicon glyphicon-file"></i>
                                <?php echo Yii::t('admin', 'Налаштування'); ?>
                            </a>
                        </li>
                        <li class="<?php echo Yii::app()->controller->id == 'requisites' ? 'active' : ''?>">
                            <a href="<?php echo Yii::app()->createUrl("/admin/requisites") ?>">
                                <i class="glyphicon glyphicon-file"></i>
                                <?php echo Yii::t('admin', 'Реквізити'); ?>
                            </a>
                        </li>
                    </ul>
                </li>
            <?php endif; ?>
                <li class="<?php echo Yii::app()->controller->id == 'pages' || Yii::app()->controller->id == 'blocks' || Yii::app()->controller->id == 'mainMenu' ? 'active' : ''?>">
                    <a href="javascript:" class="left_menu_dropdown">
                        <i class="glyphicon glyphicon-fire"></i>
                        <?php echo Yii::t('admin', 'Сторінки'); ?>
                        <i class="arrow glyphicon glyphicon-chevron-down"></i>
                    </a>
                    <ul id="demo" class="collapse">
                        <li class="<?php echo Yii::app()->controller->id == 'pages' ? 'active' : ''?>">
                            <a href="<?php echo Yii::app()->createUrl("/admin/pages") ?>">
                                <i class="glyphicon glyphicon-file"></i>
                                <?php echo Yii::t('admin', 'Сторінки'); ?>
                            </a>
                        </li>
                        <li class="<?php echo Yii::app()->controller->id == 'blocks' ? 'active' : ''?>">
                            <a href="<?php echo Yii::app()->createUrl("/admin/blocks") ?>">
                                <i class="glyphicon glyphicon-file"></i>
                                <?php echo Yii::t('admin', 'Статичні блоки'); ?>
                            </a>
                        </li>
                        <li class="<?php echo Yii::app()->controller->id == 'mainMenu' ? 'active' : ''?>">
                            <a href="<?php echo Yii::app()->createUrl("/admin/mainMenu") ?>">
                                <i class="glyphicon glyphicon-file"></i>
                                <?php echo Yii::t('admin', 'Головне меню'); ?>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="<?php echo Yii::app()->controller->id == 'oferta' ? 'active' : ''?>">
                    <a href="<?php echo Yii::app()->createUrl("/admin/oferta") ?>"><i class="glyphicon glyphicon-info-sign"></i> <?php echo Yii::t('admin', 'Публічна оферта'); ?></a>
                </li>
                <li class="<?php echo Yii::app()->controller->id == 'news' || Yii::app()->controller->id == 'tags' ? 'active' : ''?>">
                    <a href="javascript:;" class="left_menu_dropdown">
                        <i class="glyphicon glyphicon-fire"></i>
                        <?php echo Yii::t('admin', 'Новини'); ?>
                        <i class="arrow glyphicon glyphicon-chevron-down"></i>
                    </a>
                    <ul id="demo" class="collapse">
                        <li class="<?php echo Yii::app()->controller->id == 'tags' ? 'active' : ''?>">
                            <a href="<?php echo Yii::app()->createUrl("/admin/tags") ?>">
                                <i class="glyphicon glyphicon-tag"></i>
                                <?php echo Yii::t('admin_panel', 'Теги'); ?>
                            </a>
                        </li>
                        <li class="<?php echo Yii::app()->controller->id == 'news' ? 'active' : ''?>">
                            <a href="<?php echo Yii::app()->createUrl("/admin/news") ?>">
                                <i class="glyphicon glyphicon-fire"></i>
                                <?php echo Yii::t('admin', 'Статті'); ?>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="<?php echo Yii::app()->controller->id == 'partner' ? 'active' : ''?>">
                    <a href="<?php echo Yii::app()->createUrl("/admin/partner") ?>"><i class="glyphicon glyphicon-info-sign"></i> <?php echo Yii::t('admin', 'Партнери'); ?></a>
                </li>
                <li class="<?php echo Yii::app()->controller->id == 'consultation' || Yii::app()->controller->id == 'lawBranches' ? 'active' : ''?>">
                    <a href="javascript:;" class="left_menu_dropdown">
                        <i class="glyphicon glyphicon-fire"></i>
                        <?php echo Yii::t('admin', 'Правові консультації'); ?>
                        <i class="arrow glyphicon glyphicon-chevron-down"></i>
                    </a>
                    <ul id="demo" class="collapse">
                        <li class="<?php echo Yii::app()->controller->id == 'consultation' ? 'active' : ''?>">
                            <a href="<?php echo Yii::app()->createUrl("/admin/consultation") ?>">
                                <i class="glyphicon glyphicon-tag"></i>
                                <?php echo Yii::t('admin_panel', 'Консультації'); ?>
                            </a>
                        </li>
                        <li class="<?php echo Yii::app()->controller->id == 'lawBranches' ? 'active' : ''?>">
                            <a href="<?php echo Yii::app()->createUrl("/admin/lawBranches") ?>">
                                <i class="glyphicon glyphicon-fire"></i>
                                <?php echo Yii::t('admin', 'Галузі права'); ?>
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
            <script>
                $('.left_menu_dropdown').click(function(){
                    $(this).closest('li').children('.collapse').toggleClass('in');
                    $(this).children('.arrow').removeClass('glyphicon-chevron-down').removeClass('glyphicon-chevron-up');
                    if($(this).closest('li').children('.collapse').hasClass('in')){
                        $(this).children('.arrow').addClass('glyphicon-chevron-up');
                    } else {
                        $(this).children('.arrow').addClass('glyphicon-chevron-down');
                    }

                });
            </script>
        </div>
        <!-- /.navbar-collapse -->
    </nav>

    <div id="page-wrapper">
        <?php echo $content; ?>
    </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- Bootstrap Core JavaScript -->
    <script src="/js/bootstrap.min.js"></script>
    <script src="//cdn.ckeditor.com/4.5.1/full/ckeditor.js"></script>
    <script src="/js/dev.js"></script>
</body>
</html>
