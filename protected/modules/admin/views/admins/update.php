<?php
/* @var $this AdminsController */
/* @var $model Admin */

$this->breadcrumbs=array(
	'Admins'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Перелік адміністраторів', 'url'=>array('index')),
	array('label'=>'Детальний перегляд', 'url'=>array('view', 'id'=>$model->id)),
);
?>

    <h1><?php echo Yii::t('admin', 'Редагувати дані адміністратора #') . $model->id; ?></h1>
<?php
$this->widget('zii.widgets.CMenu', array(
    'items'=>$this->menu,
    'htmlOptions'=>array('class'=>'operations'),
)); ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>