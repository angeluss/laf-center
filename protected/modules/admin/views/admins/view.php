<?php
/* @var $this AdminsController */
/* @var $model Admin */

$this->breadcrumbs=array(
	'Admins' => array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'Перелік адміністраторів', 'url'=>array('index')),
);
?>

<h1><?php echo Yii::t('admin', 'Детальний перегляд даних адміністратора ') . $model->login; ?></h1>
<?php if(Yii::app()->user->hasFlash('success')): ?>
    <?php
    $this->widget('booster.widgets.TbAlert', array(
        'fade' => true,
        'closeText' => '&times;', // false equals no close link
        'events' => array(),
        'htmlOptions' => array(),
        'userComponentId' => 'user',
        'alerts' => array( // configurations per alert type
            // success, info, warning, error or danger
            'success' => array('closeText' => '&times;'),
            'info', // you don't need to specify full config
            'warning' => array('closeText' => false),
            'error' => array('closeText' => 'AAARGHH!!')
        ),
    ));
    ?>
<?php endif; ?>
<?php
$this->widget('zii.widgets.CMenu', array(
    'items'=>$this->menu,
    'htmlOptions'=>array('class'=>'operations'),
)); ?>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'login',
		'email',
        array(
            'name'=>'role',
            'value'=>Admin::getRole($model->role),
        ),
        array(
            'name'=>'created_at',
            'value'=>Yii::app()->dateFormatter->formatDateTime($model->created_at, 'long','long'),
        ),
        array(
            'name'=>'updated_at',
            'value'=>Yii::app()->dateFormatter->formatDateTime($model->updated_at, 'long','long'),
        ),
	),
)); ?>
<a href="<?php echo Yii::app()->createUrl("/admin/admins/create") ?>" class="btn btn-success">
    <?php echo Yii::t('admin_panel', 'Створити ще'); ?>
</a>
<a href="<?php echo Yii::app()->createUrl("/admin/admins/update", array('id' => $model->id)) ?>" class="btn btn-primary">
    <?php echo Yii::t('admin_panel', 'Редагувати'); ?>
</a>
<a href="<?php echo Yii::app()->createUrl("/admin/admins/delete", array('id' => $model->id)) ?>" class="btn btn-danger delete">
    <?php echo Yii::t('admin_panel', 'Видалити'); ?>
</a>
<script>
    $('.delete').click(function(e) {
        e.preventDefault();
        if (confirm("Вы впевнені?")) {
            location.href = $(this).attr('href');
        }
    });
</script>