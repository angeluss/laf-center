<?php
/* @var $this ConsultationController */
/* @var $model Consultation */

?>
<div id="page-heading">
	<h1><?php echo Yii::t('admin', 'Консультації') ?></h1>
</div>
<?php
$this->widget('booster.widgets.TbAlert', array(
    'fade' => true,
    'closeText' => '&times;', // false equals no close link
    'events' => array(),
    'htmlOptions' => array(),
    'userComponentId' => 'user',
    'alerts' => array( // configurations per alert type
        // success, info, warning, error or danger
        'success' => array('closeText' => '&times;'),
        'info', // you don't need to specify full config
        'warning' => array('closeText' => '&times;'),
        'error' => array('closeText' => '&times;')
    ),
));
?>
<div class="options">
    <a href="<?php echo Yii::app()->createUrl("/admin/consultation/create") ?>" class="btn btn-primary">
        <?php echo Yii::t('admin_panel', 'Додати консультацію'); ?>
    </a>
</div>
<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'consultation-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        'id',
        'title',
        array(
            'value' => 'News::getStatus($data->status)',
            'name' => 'status',
            'type' => 'raw',
            'filter' => News::getStatus(),
        ),
        array(
            'value' => 'LawBranches::getLawBranch($data->type_consult)',
            'name' => 'type_consult',
            'type' => 'raw',
            'filter' => LawBranches::model()->getLawBranches(),
        ),
        array(
            'class'=>'CButtonColumn',
            'htmlOptions' => array(
                'style' => 'width:6%',
            ),
        ),
    ),
));

