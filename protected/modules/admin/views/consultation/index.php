<?php
/* @var $this ConsultationController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Consultations',
);

$this->menu=array(
	array('label'=>'Додати консультацію', 'url'=>array('create')),
	array('label'=>'Подивитись консультацію', 'url'=>array('admin')),
);
?>

<h1>Consultations</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
