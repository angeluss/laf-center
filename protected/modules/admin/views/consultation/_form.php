<?php
/* @var $this ConsultationController */
/* @var $model Consultation */
/* @var $form CActiveForm */
?>
<script type="text/javascript" src="/js/jquery.liTranslit.js"></script>
<div class="form">

    <?php $form=$this->beginWidget('CActiveForm', array(
        'id'=>'consultation-form',
        'htmlOptions' => array(
            'class' => 'form-horizontal row-border',
            'enctype'=>"multipart/form-data",
        ),
        'enableAjaxValidation' => true,
        'focus'=>'input[type="text"]:first',
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'errorCssClass' => 'has-error',
        ),
    )); ?>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'status', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->dropDownList($model, 'status', News::getStatus(), array('class' => 'form-control')); ?>

        </div>
        <div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'status'); ?></div></div>
    </div>
    <div class="form-group">
        <?php echo $form->labelEx($model, 'type_consult', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->dropDownList($model, 'type_consult', LawBranches::model()->getLawBranches(), array('class' => 'form-control')); ?>
        </div>
        <div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'type_consult'); ?></div></div>
    </div>
    <ul class="nav nav-tabs">
        <li class="active" data-id="ua"><a href="javascript:"><?php echo Yii::t('admin', 'Українська'); ?></a></li>
        <li data-id="en"><a href="javascript:"><?php echo Yii::t('admin', 'Англійська'); ?></a></li>
        <li data-id="ru"><a href="javascript:"><?php echo Yii::t('admin', 'Російська'); ?></a></li>
        <li data-id="fr"><a href="javascript:"><?php echo Yii::t('admin', 'Французька'); ?></a></li>
        <li data-id="tr"><a href="javascript:"><?php echo Yii::t('admin', 'Туркменська'); ?></a></li>
        <li data-id="ar"><a href="javascript:"><?php echo Yii::t('admin', 'Арабська'); ?></a></li>
    </ul>
    <ul class="contents">
        <li class="content_tab open" id="ua_tab">
            <?php echo Yii::t('admin', 'Український варіант'); ?>
            <?php $this->renderPartial('_contentForm', array('lang' => 'uk', 'model' => $model->ukrainian)) ?>
        </li>
        <li class="content_tab" id="en_tab">
            <?php echo Yii::t('admin', 'Англійський варіант'); ?>
            <?php $this->renderPartial('_contentForm', array('lang' => 'en', 'model' => $model->english)) ?>
        </li>
        <li class="content_tab" id="ru_tab">
            <?php echo Yii::t('admin', 'Російський варіант'); ?>
            <?php $this->renderPartial('_contentForm', array('lang' => 'ru', 'model' => $model->russian)) ?>
        </li>
        <li class="content_tab" id="fr_tab">
            <?php echo Yii::t('admin', 'Французький варіант'); ?>
            <?php $this->renderPartial('_contentForm', array('lang' => 'fr', 'model' => $model->france)) ?>
        </li>
        <li class="content_tab" id="tr_tab">
            <?php echo Yii::t('admin', 'Туркменський варіант'); ?>
            <?php $this->renderPartial('_contentForm', array('lang' => 'tr', 'model' => $model->turkmen)) ?>
        </li>
        <li class="content_tab" id="ar_tab">
            <?php echo Yii::t('admin', 'Арабський варіант'); ?>
            <?php $this->renderPartial('_contentForm', array('lang' => 'ar', 'model' => $model->arabian)) ?>
        </li>
    </ul>

    <script>
        $('.nav-tabs li').click(function() {
            $('.nav-tabs .active').removeClass('active');
            $(this).addClass('active');
            var id = $(this).data('id');
            $('.contents .open').removeClass('open');
            $('#'+id+'_tab').addClass('open');
        });
    </script>

    <div class="panel-footer">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="btn-toolbar">
                    <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('Admin', 'Створити') : Yii::t('Admin', 'Зберегти'), array('class' => 'btn-primary btn')); ?>
                </div>
            </div>
        </div>
    </div>

<?php $this->endWidget(); ?>

    <?php if($model->isNewrecord): ?>
        <script>
            $(document).ready(function(){
                $('#Pages_title').on('keyup', function(){
                    $('#Pages_meta_title').val($(this).val());
                    $(this).liTranslit({
                        elAlias: $('#Pages_alias')
                    });
                })
            });

        </script>
    <?php endif; ?>

</div><!-- form -->