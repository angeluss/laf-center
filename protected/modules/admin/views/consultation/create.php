<?php
/* @var $this ConsultationController */
/* @var $model Consultation */

$this->breadcrumbs=array(
    'Consultations'=>array('index'),
    'Create',
);

$this->menu=array(
    array('label'=>'Перелік консультацій', 'url'=>array('index')),
);
?>

<h1><?php echo Yii::t('admin', 'Додати консультацію'); ?></h1>
<?php
$this->widget('zii.widgets.CMenu', array(
    'items'=>$this->menu,
    'htmlOptions'=>array('class'=>'operations'),
)); ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>


