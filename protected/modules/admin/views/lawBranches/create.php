<?php
/* @var $this LawBranchesController */
/* @var $model LawBranches */

$this->breadcrumbs=array(
	'Law Branches'=>array('index'),
	'Create',
);

$this->menu=array(
    array('label'=>'Перелік галузей права', 'url'=>array('index')),
);
?>

    <h1><?php echo Yii::t('admin', 'Додати галузь права'); ?></h1>
<?php
$this->widget('zii.widgets.CMenu', array(
    'items'=>$this->menu,
    'htmlOptions'=>array('class'=>'operations'),
)); ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>