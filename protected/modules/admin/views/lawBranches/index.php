<?php
/* @var $this LawBranchesController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Law Branches',
);

$this->menu=array(
	array('label'=>'Додати галузь права', 'url'=>array('create')),
	array('label'=>'Подивитись галузь права', 'url'=>array('admin')),
);
?>

<h1>Отрасли права</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
