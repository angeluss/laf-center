<?php
/* @var $this LawBranchesController */
/* @var $model LawBranches */

$this->breadcrumbs=array(
	'Law Branches'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
    array('label'=>'Перелік галузей права', 'url'=>array('index')),
    array('label'=>'Детальний перегляд галузі права', 'url'=>array('view', 'id'=>$model->id)),
); ?>
    <h1><?php echo Yii::t('admin', 'Редагування галузі права ') . $model->title; ?></h1>
<?php

$this->widget('zii.widgets.CMenu', array(
    'items'=>$this->menu,
    'htmlOptions'=>array('class'=>'operations'),
)); ?>


<?php $this->renderPartial('_form', array('model'=>$model)); ?>