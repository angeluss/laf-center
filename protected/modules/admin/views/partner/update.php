<?php
/* @var $this PartnerController */
/* @var $model Partner */

$this->breadcrumbs=array(
	'Partners'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Перелік партнерів', 'url'=>array('index')),
	array('label'=>'Детальний перегляд', 'url'=>array('view', 'id'=>$model->id)),
);
?>

<h1><?php echo Yii::t('admin', 'Редагувати партнера ') . $model->title; ?></h1>
<?php
$this->widget('zii.widgets.CMenu', array(
	'items'=>$this->menu,
	'htmlOptions'=>array('class'=>'operations'),
)); ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>