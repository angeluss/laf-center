<?php
/* @var $this PartnerController */
/* @var $model Partner */
?>

<div id="page-heading">
	<h1><?php echo Yii::t('admin', 'Партнери') ?></h1>
</div>
<?php
$this->widget('booster.widgets.TbAlert', array(
	'fade' => true,
	'closeText' => '&times;', // false equals no close link
	'events' => array(),
	'htmlOptions' => array(),
	'userComponentId' => 'user',
	'alerts' => array( // configurations per alert type
		// success, info, warning, error or danger
		'success' => array('closeText' => '&times;'),
		'info', // you don't need to specify full config
		'warning' => array('closeText' => '&times;'),
		'error' => array('closeText' => '&times;')
	),
));
?>
<div class="options">
	<a href="<?php echo Yii::app()->createUrl("/admin/partner/create") ?>" class="btn btn-primary">
		<?php echo Yii::t('admin_panel', 'Додати партнера'); ?>
	</a>
</div>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'partner-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'title',
		'url',
		'desc',
		array(
			'value' => 'News::getStatus($data->status)',
			'name' => 'status',
			'type' => 'raw',
			'filter' => News::getStatus(),
		),
		array(
			'class'=>'CButtonColumn',
			'htmlOptions' => array(
				'style' => 'width:6%',
			),
		),
	),
)); ?>
