<?php
/* @var $this OfertaController */
/* @var $model Oferta */
/* @var $form CActiveForm */
?>

<div class="form">

<?php
$form = $this->beginWidget('CActiveForm', array(
    'id' => 'oferta-form',
    'htmlOptions' => array('class' => 'form-horizontal row-border'),
    'enableAjaxValidation' => true,
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'errorCssClass' => 'has-error',
    ),
));
?>
    <div class="form-group">
        <?php echo $form->labelEx($model, 'lang', array('class' => 'col-sm-3 control-label')); ?>

        <?php if($model->isNewRecord): ?>
        <div class="col-sm-6">
            <?php echo $form->dropDownList($model, 'lang', Oferta::getLanguage(), array('class' => 'form-control user-identities')); ?>
        </div>
        <?php else :?>
            <div class="col-sm-6 static_lang_div">
                <span><?php echo Oferta::getLanguage($model->lang)?></span>
            </div>
        <?php endif; ?>

        <div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'lang'); ?></div></div>
    </div>
    <?php $disabled = $model->lang == Oferta::getLanguageByCode() ? 'disabled' : false ?>
    <?php $msg = false === $disabled ?
                    Yii::t('admin', 'Ви можете не використовувати цей варіант перекладу. В такому випадку буде показуватися текст за замовчуванням') :
                    Yii::t('admin', 'Цей текст буде показуватися за замовчуванням. Неможливо змінити статус') ?>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'status', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->dropDownList($model, 'status', Oferta::getStatus(), array('class' => 'form-control user-identities', 'disabled'=>$disabled)); ?>
            <span class="required">* </span><span class="lang-info"><?php echo $msg ?></span>
        </div>
        <div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'status'); ?></div></div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'content', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->textArea($model, 'content', array('class' => 'form-control input-static-content', 'id'=>'oferta' )); ?>
        </div>
        <div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'content'); ?></div></div>
    </div>

    <div class="panel-footer">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="btn-toolbar">
                    <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('Admin', 'Створити') : Yii::t('Admin', 'Зберегти'), array('class' => 'btn-primary btn')); ?>
                </div>
            </div>
        </div>
    </div>

<?php $this->endWidget(); ?>

    <script type="text/javascript">
        CKEDITOR.replace( 'oferta' );
    </script>
