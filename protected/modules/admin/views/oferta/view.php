<?php
/* @var $this OfertaController */
/* @var $model Oferta */

$this->breadcrumbs=array(
	'Ofertas'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List Oferta', 'url'=>array('index')),
	array('label'=>'Create Oferta', 'url'=>array('create')),
	array('label'=>'Update Oferta', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Oferta', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Oferta', 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('admin', 'Детальний перегляд ') . $model->getLanguage2() . Yii::t('admin', ' варіанту оферти')?></h1>
<a href="<?php echo Yii::app()->createUrl("/admin/oferta", array('id' => $model->id)) ?>" class="btn btn-link">
    <?php echo Yii::t('admin_panel', 'Назад до списку'); ?>
</a>
<a href="<?php echo Yii::app()->createUrl("/admin/oferta/update", array('id' => $model->id)) ?>" class="btn btn-primary">
    <?php echo Yii::t('admin_panel', 'Редагувати'); ?>
</a>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
        array(
            'name'=>'status',
            'value'=>Oferta::getStatus($model->status),
        ),
        array(
            'name'=>'updated_at',
            'value'=>Yii::app()->dateFormatter->formatDateTime($model->updated_at, 'long','long'),
        ),
	),
)); ?>
<div class="content_for_static_page">
    <?php echo $model->content; ?>
</div>

