<?php
/* @var $this OfertaController */
/* @var $model Oferta */

$this->breadcrumbs=array(
	'Ofertas'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Oferta', 'url'=>array('index')),
	array('label'=>'Create Oferta', 'url'=>array('create')),
	array('label'=>'View Oferta', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Oferta', 'url'=>array('admin')),
);
?>

    <h1><?php echo Yii::t('admin', 'Редагування ') . $model->getLanguage2() . Yii::t('admin', ' варіанту оферти')?></h1>


<?php $this->renderPartial('_form', array('model'=>$model)); ?>