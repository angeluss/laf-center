<?php
/* @var $this OfertaController */
/* @var $model Oferta */
?>

<h1><?php echo Yii::t('admin', 'Варіанти перекладу публічних оферт'); ?></h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'oferta-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
        array(
            'value' => 'Oferta::getLanguage($data->lang)',
            'name' => 'lang',
            'type' => 'raw',
            'filter' => Oferta::getLanguage(),
        ),
        array(
            'value' => 'Oferta::getStatus($data->status)',
            'name' => 'status',
            'type' => 'raw',
            'filter' => Oferta::getStatus(),
        ),
        array(
            'value' => 'date("F j, Y, g:i a",$data->updated_at)',
            'name' => 'updated_at',
            'filter' => false,
        ),
		array(
			'class'=>'CButtonColumn',
            'template' => '{view}{update}'
		),
	),
)); ?>
