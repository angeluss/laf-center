<?php
/* @var $this RequisitesController */
/* @var $model Requisites */
/* @var $form CActiveForm */
?>

<div class="form">

	<?php
	$form=$this->beginWidget('CActiveForm', array(
		'id'=>'requisites-form',
		'htmlOptions' => array('class' => 'form-horizontal row-border'),
		'enableAjaxValidation' => true,
		'clientOptions' => array(
			'validateOnSubmit' => true,
			'validateOnChange' => true,
			'errorCssClass' => 'has-error',
		),
	));
	?>

	<div class="form-group">
		<?php echo $form->labelEx($model, 'recepient', array('class' => 'col-sm-3 control-label')); ?>
		<div class="col-sm-6">
			<?php echo $form->textField($model, 'recepient', array('class' => 'form-control input-settings-recepient' )); ?>
		</div>
		<div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'recepient'); ?></div></div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model, 'account', array('class' => 'col-sm-3 control-label')); ?>
		<div class="col-sm-6">
			<?php echo $form->textField($model, 'account', array('class' => 'form-control input-settings-account' )); ?>
		</div>
		<div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'account'); ?></div></div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model, 'id_code', array('class' => 'col-sm-3 control-label')); ?>
		<div class="col-sm-6">
			<?php echo $form->textField($model, 'id_code', array('class' => 'form-control input-settings-id_code' )); ?>
		</div>
		<div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'id_code'); ?></div></div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model, 'bank', array('class' => 'col-sm-3 control-label')); ?>
		<div class="col-sm-6">
			<?php echo $form->textField($model, 'bank', array('class' => 'form-control input-settings-bank' )); ?>
		</div>
		<div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'bank'); ?></div></div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model, 'bank_code', array('class' => 'col-sm-3 control-label')); ?>
		<div class="col-sm-6">
			<?php echo $form->textField($model, 'bank_code', array('class' => 'form-control input-settings-bank_code' )); ?>
		</div>
		<div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'bank_code'); ?></div></div>
	</div>

	<div class="form-group">
		<?php echo $form->labelEx($model, 'amount', array('class' => 'col-sm-3 control-label')); ?>
		<div class="col-sm-6">
			<?php echo $form->textField($model, 'amount', array('class' => 'form-control input-settings-amount' )); ?>
		</div>
		<div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'amount'); ?></div></div>
	</div>

	<div class="panel-footer">
		<div class="row">
			<div class="col-sm-6 col-sm-offset-3">
				<div class="btn-toolbar">
					<?php echo CHtml::submitButton(Yii::t('Admin', 'Зберегти'), array('class' => 'btn-primary btn')); ?>
				</div>
			</div>
		</div>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->