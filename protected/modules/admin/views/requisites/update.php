<?php
/* @var $this RequisitesController */
/* @var $model Requisites */

$this->breadcrumbs=array(
	'Requisites'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Requisites', 'url'=>array('index')),
	array('label'=>'Create Requisites', 'url'=>array('create')),
	array('label'=>'View Requisites', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Requisites', 'url'=>array('admin')),
);
?>

<h1><?php echo Yii::t('admin', 'Оновити реквізити'); ?></h1>
<?php if(Yii::app()->user->hasFlash('success')): ?>
	<?php
		$this->widget('booster.widgets.TbAlert', array(
			'fade' => true,
			'closeText' => '&times;', // false equals no close link
			'events' => array(),
			'htmlOptions' => array(),
			'userComponentId' => 'user',
			'alerts' => array( // configurations per alert type
				// success, info, warning, error or danger
				'success' => array('closeText' => '&times;'),
				'info', // you don't need to specify full config
				'warning' => array('closeText' => false),
				'error' => array('closeText' => 'AAARGHH!!')
			),
		));
	?>
<?php endif; ?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>