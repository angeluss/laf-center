<?php
/* @var $this SettingsController */
/* @var $model Settings */
/* @var $form CActiveForm */
?>

<div class="form">

    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'settings-form',
        'htmlOptions' => array('class' => 'form-horizontal row-border', 'enctype'=>"multipart/form-data",),
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'errorCssClass' => 'has-error',
        ),
    ));
    ?>
    <div class="form-group">
        <?php echo $form->labelEx($model, 'meta_title', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model, 'meta_title', array('class' => 'form-control input-settings-meta_title' )); ?>
        </div>
        <div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'meta_title'); ?></div></div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'admin_email', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model, 'admin_email', array('class' => 'form-control input-admin-email' )); ?>
        </div>
        <div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'admin_email'); ?></div></div>
    </div>

    <div class="form-group">
            <?php echo $form->labelEx($model, 'uplFile', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php if(is_file(WATSON . '/image/favicon/favicon.ico')): ?>
                <img src="/image/favicon/favicon.ico" alt style="margin: 5px; width: 25px; height: 25px;"/>
            <?php endif; ?>
            <?php echo $form->fileField($model, 'uplFile', array('class' => 'form-control input-favicon')); ?>
        </div>
        <div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'uplFile'); ?></div></div>
    </div>



    <div class="form-group">
        <?php echo $form->labelEx($model, 'contact_email', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model, 'contact_email', array('class' => 'form-control input-admin-email' )); ?>
        </div>
        <div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'contact_email'); ?></div></div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'fb', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model, 'fb', array('class' => 'form-control input-admin-email' )); ?>
        </div>
        <div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'fb'); ?></div></div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'phone', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model, 'phone', array('class' => 'form-control input-admin-email' )); ?>
        </div>
        <div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'phone'); ?></div></div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'call_centr', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model, 'call_centr', array('class' => 'form-control input-admin-email' )); ?>
        </div>
        <div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'call_centr'); ?></div></div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'address', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model, 'address', array('class' => 'form-control input-admin-email' )); ?>
        </div>
        <div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'address'); ?></div></div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'address_en', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->dropDownList($model, 'status_en', Settings::getStatus(), array('class' => 'form-control')); ?>
            <?php echo $form->textField($model, 'address_en', array('class' => 'form-control input-admin-email' )); ?>
        </div>
        <div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'address_en'); ?></div></div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'address_ru', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->dropDownList($model, 'status_ru', Settings::getStatus(), array('class' => 'form-control')); ?>
            <?php echo $form->textField($model, 'address_ru', array('class' => 'form-control input-admin-email' )); ?>
        </div>
        <div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'address_ru'); ?></div></div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'address_fr', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->dropDownList($model, 'status_fr', Settings::getStatus(), array('class' => 'form-control')); ?>
            <?php echo $form->textField($model, 'address_fr', array('class' => 'form-control input-admin-email' )); ?>
        </div>
        <div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'address_fr'); ?></div></div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'address_tr', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->dropDownList($model, 'status_tr', Settings::getStatus(), array('class' => 'form-control')); ?>
            <?php echo $form->textField($model, 'address_tr', array('class' => 'form-control input-admin-email' )); ?>
        </div>
        <div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'address_tr'); ?></div></div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'address_ar', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->dropDownList($model, 'status_ar', Settings::getStatus(), array('class' => 'form-control')); ?>
            <?php echo $form->textField($model, 'address_ar', array('class' => 'form-control input-admin-email' )); ?>
        </div>
        <div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'address_ar'); ?></div></div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'skype', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model, 'skype', array('class' => 'form-control input-admin-email' )); ?>
        </div>
        <div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'skype'); ?></div></div>
    </div>

    <div class="panel-footer">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="btn-toolbar">
                    <?php echo CHtml::submitButton(Yii::t('Admin', 'Зберегти'), array('class' => 'btn-primary btn')); ?>
                </div>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>

</div><!-- form -->