<?php
/* @var $this PagesController */
/* @var $model Pages */

$this->breadcrumbs=array(
	'Pages'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Перелік сторінок', 'url'=>array('index')),
);
?>

<h1><?php echo Yii::t('admin', 'Додати сторінку'); ?></h1>
<?php
$this->widget('zii.widgets.CMenu', array(
    'items'=>$this->menu,
    'htmlOptions'=>array('class'=>'operations'),
)); ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>