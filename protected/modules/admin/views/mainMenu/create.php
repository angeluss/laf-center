<?php
/* @var $this MainMenuController */
/* @var $model MainMenu */

$this->breadcrumbs=array(
	'Main Menus'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List MainMenu', 'url'=>array('index')),
	array('label'=>'Manage MainMenu', 'url'=>array('admin')),
);
?>

<h1>Create MainMenu</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>