<?php
/* @var $this MainMenuController */
/* @var $model MainMenu */

$this->breadcrumbs=array(
	'Main Menus'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'Перелік пунктів', 'url'=>array('index')),
);
?>
<h1><?php echo Yii::t('admin', 'Детальний перегляд пункту ') . $model->title; ?></h1>
<?php
$this->widget('booster.widgets.TbAlert', array(
	'fade' => true,
	'closeText' => '&times;', // false equals no close link
	'events' => array(),
	'htmlOptions' => array(),
	'userComponentId' => 'user',
	'alerts' => array( // configurations per alert type
		// success, info, warning, error or danger
		'success' => array('closeText' => '&times;'),
		'info', // you don't need to specify full config
		'warning' => array('closeText' => false),
		'error' => array('closeText' => 'AAARGHH!!')
	),
));
?>
<?php
$this->widget('zii.widgets.CMenu', array(
	'items'=>$this->menu,
	'htmlOptions'=>array('class'=>'operations'),
)); ?>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'title',
		'link',
		'position',
	),
)); ?>
