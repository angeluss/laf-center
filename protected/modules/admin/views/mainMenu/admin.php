<?php
/* @var $this MainMenuController */
/* @var $model MainMenu */
?>

<div id="page-heading">
	<h1><?php echo Yii::t('admin', 'Головне меню') ?></h1>
</div>
<?php
$this->widget('booster.widgets.TbAlert', array(
	'fade' => true,
	'closeText' => '&times;', // false equals no close link
	'events' => array(),
	'htmlOptions' => array(),
	'userComponentId' => 'user',
	'alerts' => array( // configurations per alert type
		// success, info, warning, error or danger
		'success' => array('closeText' => '&times;'),
		'info', // you don't need to specify full config
		'warning' => array('closeText' => '&times;'),
		'error' => array('closeText' => '&times;')
	),
));
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'main-menu-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'title',
		'link',
		'position',
		array(
			'class'=>'CButtonColumn',
			'template' => '{update}'
		),
	),
)); ?>
