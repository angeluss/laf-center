<?php
/* @var $this MainMenuController */
/* @var $model MainMenu */

$this->breadcrumbs=array(
	'Main Menus'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Перелік пунктів', 'url'=>array('index')),
);
?>

	<h1><?php echo Yii::t('admin', 'Редагувати пункт ') . $model->title; ?></h1>
<?php
$this->widget('zii.widgets.CMenu', array(
	'items'=>$this->menu,
	'htmlOptions'=>array('class'=>'operations'),
)); ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>