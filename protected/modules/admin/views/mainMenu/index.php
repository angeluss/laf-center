<?php
/* @var $this MainMenuController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Main Menus',
);

$this->menu=array(
	array('label'=>'Create MainMenu', 'url'=>array('create')),
	array('label'=>'Manage MainMenu', 'url'=>array('admin')),
);
?>

<h1>Main Menus</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
