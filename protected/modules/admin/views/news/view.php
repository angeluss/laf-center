<?php
/* @var $this NewsController */
/* @var $model News */

$this->breadcrumbs=array(
	'News'=>array('index'),
	$model->title,
);

$this->menu=array(
	array('label'=>'Перелік статей', 'url'=>array('index')),
);
?>

<h1><?php echo Yii::t('admin', 'Детальний перегляд статті ') . $model->title; ?></h1>
<?php
$this->widget('booster.widgets.TbAlert', array(
    'fade' => true,
    'closeText' => '&times;', // false equals no close link
    'events' => array(),
    'htmlOptions' => array(),
    'userComponentId' => 'user',
    'alerts' => array( // configurations per alert type
        // success, info, warning, error or danger
        'success' => array('closeText' => '&times;'),
        'info', // you don't need to specify full config
        'warning' => array('closeText' => false),
        'error' => array('closeText' => 'AAARGHH!!')
    ),
));
?>
<?php
$this->widget('zii.widgets.CMenu', array(
    'items'=>$this->menu,
    'htmlOptions'=>array('class'=>'operations'),
)); ?>

<a href="<?php echo Yii::app()->createUrl("/admin/news/create") ?>" class="btn btn-success">
    <?php echo Yii::t('admin', 'Створити ще'); ?>
</a>
<a href="<?php echo Yii::app()->createUrl("/admin/news/update", array('id' => $model->id)) ?>" class="btn btn-primary">
    <?php echo Yii::t('admin', 'Редагувати'); ?>
</a>
<a href="<?php echo Yii::app()->createUrl("/admin/news/delete", array('id' => $model->id)) ?>" class="btn btn-danger delete">
    <?php echo Yii::t('admin', 'Видалити'); ?>
</a>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'title',
        array(
            'name'=>'img',
            'type' => 'raw',
            'value'=>News::getImg($model->img, $model->id, 'admin_news_img'),
        ),
		'short_desc',
		'content',
        'link',
        array(
            'name'=>'status',
            'value'=>News::getStatus($model->status),
        ),
        array(
            'name'=>'is_attached',
            'value'=> News::getIsAttached($model->is_attached),
        ),
        array(
            'name'=>'created_at',
            'value'=>Yii::app()->dateFormatter->formatDateTime($model->created_at, 'long','long'),
        ),
        array(
            'name'=>'updated_at',
            'value'=>Yii::app()->dateFormatter->formatDateTime($model->updated_at, 'long','long'),
        ),
	),
)); ?>
<?php echo Yii::t('admin', 'Теги:')?>
<div class="appendedTags">
    <?php foreach($model->tags as $tag):?>
        <?php $this->renderPartial('_addTag', array('tag' => $tag, 'view' => true)) ?>
    <?php endforeach; ?>
</div>

<ul class="nav nav-tabs">
    <li class="active" data-id="ua"><a href="javascript:"><?php echo Yii::t('admin', 'Український'); ?></a></li>
    <li data-id="en"><a href="javascript:"><?php echo Yii::t('admin', 'Англійський'); ?></a></li>
    <li data-id="ru"><a href="javascript:"><?php echo Yii::t('admin', 'Російський'); ?></a></li>
    <li data-id="fr"><a href="javascript:"><?php echo Yii::t('admin', 'Французький'); ?></a></li>
    <li data-id="tr"><a href="javascript:"><?php echo Yii::t('admin', 'Туркменський'); ?></a></li>
    <li data-id="ar"><a href="javascript:"><?php echo Yii::t('admin', 'Арабський'); ?></a></li>
</ul>

<ul class="contents">
    <li class="content_tab open" id="ua_tab">
        <p><?php echo Yii::t('admin', 'Український варіант'); ?></p>
        <p><?php echo PageContents::getStatus($model->ukrainian->status); ?></p>
        <p><?php echo $model->ukrainian->title; ?></p>
        <p><?php echo $model->ukrainian->short_desc; ?></p>
        <p><?php echo $model->ukrainian->content; ?></p>
    </li>
    <li class="content_tab" id="en_tab">
        <p><?php echo Yii::t('admin', 'Англійський варіант'); ?></p>
        <p><?php echo PageContents::getStatus($model->english->status) ?></p>
        <p><?php echo $model->english->title ?></p>
        <p><?php echo $model->english->short_desc; ?></p>
        <p><?php echo $model->english->content; ?></p>
    </li>
    <li class="content_tab" id="ru_tab">
        <p><?php echo Yii::t('admin', 'Російський варіант'); ?></p>
        <p><?php echo PageContents::getStatus($model->russian->status) ?></p>
        <p><?php echo $model->russian->title ?></p>
        <p><?php echo $model->russian->short_desc; ?></p>
        <p><?php echo $model->russian->content; ?></p>
    </li>
    <li class="content_tab" id="fr_tab">
        <p><?php echo Yii::t('admin', 'Французький варіант'); ?></p>
        <p><?php echo PageContents::getStatus($model->france->status) ?></p>
        <p><?php echo $model->france->title ?></p>
        <p><?php echo $model->france->short_desc; ?></p>
        <p><?php echo $model->france->content; ?></p>
    </li>
    <li class="content_tab" id="tr_tab">
        <p><?php echo Yii::t('admin', 'Туркменський варіант'); ?></p>
        <p><?php echo PageContents::getStatus($model->turkmen->status) ?></p>
        <p><?php echo $model->turkmen->title ?></p>
        <p><?php echo $model->turkmen->short_desc; ?></p>
        <p><?php echo $model->turkmen->content; ?></p>
    </li>
    <li class="content_tab" id="ar_tab">
        <p><?php echo Yii::t('admin', 'Арабський варіант'); ?></p>
        <p><?php echo PageContents::getStatus($model->arabian->status) ?></p>
        <p><?php echo $model->arabian->title ?></p>
        <p><?php echo $model->arabian->short_desc; ?></p>
        <p><?php echo $model->arabian->content; ?></p>
    </li>
</ul>
<script>
    $('.nav-tabs li').click(function() {
        $('.nav-tabs .active').removeClass('active');
        $(this).addClass('active');
        var id = $(this).data('id');
        $('.contents .open').removeClass('open');
        $('#'+id+'_tab').addClass('open');
    });
</script>

<script>
    $('.delete').click(function(e) {
        e.preventDefault();
        if (confirm("<?php echo Yii::t('admin', 'Ви впевнені?') ?>")) {
            location.href = $(this).attr('href');
        }
    });
</script>