<?php
/* @var $this NewsController */
/* @var $model News */
/* @var $form CActiveForm */
?>

    <div class="form">

        <?php
        $form = $this->beginWidget('CActiveForm', array(
            'id' => 'news-form',
            'htmlOptions' => array(
                'class' => 'form-horizontal row-border',
                'enctype'=>"multipart/form-data",
            ),
            'enableAjaxValidation' => true,
            'focus'=>'input[type="text"]:first',
            'clientOptions' => array(
                'validateOnSubmit' => true,
                'validateOnChange' => true,
                'errorCssClass' => 'has-error',
            ),
        ));
        ?>
        <ul class="nav nav-tabs">
            <li class="active" data-id="ua"><a href="javascript:"><?php echo Yii::t('admin', 'Українська'); ?></a></li>
            <li data-id="en"><a href="javascript:"><?php echo Yii::t('admin', 'Англійська'); ?></a></li>
            <li data-id="ru"><a href="javascript:"><?php echo Yii::t('admin', 'Російська'); ?></a></li>
            <li data-id="fr"><a href="javascript:"><?php echo Yii::t('admin', 'Французька'); ?></a></li>
            <li data-id="tr"><a href="javascript:"><?php echo Yii::t('admin', 'Туркменська'); ?></a></li>
            <li data-id="ar"><a href="javascript:"><?php echo Yii::t('admin', 'Арабська'); ?></a></li>
        </ul>

        <ul class="contents">
            <li class="content_tab open" id="ua_tab">
                <?php echo Yii::t('admin', 'Український варіант'); ?>
                <?php $this->renderPartial('_contentForm', array('lang' => 'uk', 'model' => $model->ukrainian)) ?>
            </li>
            <li class="content_tab" id="en_tab">
                <?php echo Yii::t('admin', 'Англійський варіант'); ?>
                <?php $this->renderPartial('_contentForm', array('lang' => 'en', 'model' => $model->english)) ?>
            </li>
            <li class="content_tab" id="ru_tab">
                <?php echo Yii::t('admin', 'Російський варіант'); ?>
                <?php $this->renderPartial('_contentForm', array('lang' => 'ru', 'model' => $model->russian)) ?>
            </li>
            <li class="content_tab" id="fr_tab">
                <?php echo Yii::t('admin', 'Французький варіант'); ?>
                <?php $this->renderPartial('_contentForm', array('lang' => 'fr', 'model' => $model->france)) ?>
            </li>
            <li class="content_tab" id="tr_tab">
                <?php echo Yii::t('admin', 'Туркменський варіант'); ?>
                <?php $this->renderPartial('_contentForm', array('lang' => 'tr', 'model' => $model->turkmen)) ?>
            </li>
            <li class="content_tab" id="ar_tab">
                <?php echo Yii::t('admin', 'Арабський варіант'); ?>
                <?php $this->renderPartial('_contentForm', array('lang' => 'ar', 'model' => $model->arabian)) ?>
            </li>
        </ul>

        <script>
            $('.nav-tabs li').click(function() {
                $('.nav-tabs .active').removeClass('active');
                $(this).addClass('active');
                var id = $(this).data('id');
                $('.contents .open').removeClass('open');
                $('#'+id+'_tab').addClass('open');
            });
        </script>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'file', array('class' => 'col-sm-3 control-label')); ?>
            <div class="col-sm-6">
                <?php if(!$model->isNewRecord):?>
                    <?php echo News::getImg($model->img, $model->id, 'admin_news_img')?>
                <?php endif; ?>
                <?php echo CHtml::activeFileField($model, 'file', array('class' => 'form-control')); ?>
            </div>
            <div class="col-md-3">
                <div class="help-block">
                    <?php echo $form->error($model, 'file'); ?>
                </div>
            </div>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'link', array('class' => 'col-sm-3 control-label')); ?>
            <div class="col-sm-6">
                <?php echo $form->textField($model, 'link', array('class' => 'form-control' )); ?>
            </div>
            <div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'link'); ?></div></div>
        </div>
        <script type="text/javascript">
            CKEDITOR.replace( 'News_content' );
        </script>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'status', array('class' => 'col-sm-3 control-label')); ?>
            <div class="col-sm-6">
                <?php echo $form->dropDownList($model, 'status', News::getStatus(), array('class' => 'form-control')); ?>

            </div>
            <div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'status'); ?></div></div>
        </div>

        <div class="form-group">
            <?php echo $form->labelEx($model, 'is_attached', array('class' => 'col-sm-3 control-label')); ?>
            <div class="col-sm-6">
                <?php echo $form->dropDownList($model, 'is_attached', News::getIsAttached(), array('class' => 'form-control')); ?>

            </div>
            <div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'is_attached'); ?></div></div>
        </div>
    <div class="form-group">
        <?php
        echo $form->labelEx($model,'updated_at', array('class' => 'col-sm-3 control-label'));
        ?>
        <div class="col-sm-6">
        <?php
            Yii::import('application.extensions.CJuiDateTimePicker.CJuiDateTimePicker');
        $this->widget('CJuiDateTimePicker',array(
            'model'=>$model, //Model object
            'attribute'=>'updated_at', //attribute name
            'mode'=>'datetime', //use "time","date" or "datetime" (default)
            'options'=>array(
                'class'=>'form-control',
            ) // jquery plugin options
        ));
        ?>
            </div>
        </div>
        <div class="form-group" id="tags_div" data-id="<?php echo $model->id?>">
            <label class="col-sm-3 control-label required" for=", 'tags'=>$tags"><?php echo Yii::t('admin', 'Теги')?></label>
            <div class="col-sm-6">
                <div class="appendedTags">
                    <?php foreach($model->tags as $tag):?>
                        <?php $this->renderPartial('_addTag', array('tag' => $tag)) ?>
                    <?php endforeach; ?>
                </div>
                <div class="tagsSelector">
                    <?php $this->renderPartial('_tagsSelector', array('tags' => $tags,));?>
                </div>
            </div>
        </div>

        <div class="panel-footer">
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3">
                    <div class="btn-toolbar">
                        <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('Admin', 'Створити') : Yii::t('Admin', 'Зберегти'), array('class' => 'btn-primary btn')); ?>
                    </div>
                </div>
            </div>
        </div>

        <?php $this->endWidget(); ?>

    </div><!-- form -->

<script>
    $(document).ready(function(){
        var date_selector_val = $('#News_updated_at');
        if (date_selector_val.val()==0) {
            var norm_date = <?php echo '"' . (date('Y-m-d H:i', time())) . '"' ?>;
            date_selector_val.val(norm_date);
        }
        else {
            var norm_date = <?php echo '"' .  (date('Y-m-d H:i', $model->updated_at)) . '"' ?>;
            date_selector_val.val(norm_date);
        }
    });
</script>
