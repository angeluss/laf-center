<?php
/* @var $this NewsController */
/* @var $model News */
?>

<div id="page-heading">
    <h1><?php echo Yii::t('admin', 'Статті') ?></h1>
</div>
<?php
$this->widget('booster.widgets.TbAlert', array(
    'fade' => true,
    'closeText' => '&times;', // false equals no close link
    'events' => array(),
    'htmlOptions' => array(),
    'userComponentId' => 'user',
    'alerts' => array( // configurations per alert type
        // success, info, warning, error or danger
        'success' => array('closeText' => '&times;'),
        'info', // you don't need to specify full config
        'warning' => array('closeText' => '&times;'),
        'error' => array('closeText' => '&times;')
    ),
));
?>
<div class="options">
    <a href="<?php echo Yii::app()->createUrl("/admin/news/create") ?>" class="btn btn-primary">
        <?php echo Yii::t('admin_panel', 'Додати статтю'); ?>
    </a>
</div>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'news-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
        array(
            'value' => 'News::getImg($data->img, $data->id, "admin_news_img")',
            'name' => '',
            'type' => 'raw',
            'htmlOptions' => array(
                'style' => 'width:5%',
            ),
            'filter' => false,
        ),
		'title',
		'short_desc',
        'link',
        array(
            'value' => 'News::getStatus($data->status)',
            'name' => 'status',
            'type' => 'raw',
            'filter' => News::getStatus(),
        ),
        array(
            'value' => 'News::getIsAttached($data->is_attached)',
            'name' => 'is_attached',
            'type' => 'raw',
            'filter' => News::getIsAttached(),
        ),
        array(
            'value' => 'date("F j, Y, g:i a",$data->created_at)',
            'name' => 'created_at',
            'filter' => false,
        ),
        array(
            'value' => 'date("F j, Y, g:i a",$data->updated_at)',
            'name' => 'updated_at',
            'filter' => false,
        ),
		array(
			'class'=>'CButtonColumn',
            'htmlOptions' => array(
                'style' => 'width:6%',
            ),
		),
	),
)); ?>
