<?php $showClose = isset($view) ? false : true; ?>
<span class="one_tag">
    <input type="hidden" name="Tags[]" value="<?php echo $tag->id?>" />
    <span><?php echo $tag->title ?></span>
    <?php if($showClose):?>
    <a href="javascript:" id="tag_<?php echo $tag->id ?>" class="tags_block_close" onclick="">
        <i class="glyphicon glyphicon-remove"></i>
    </a>
</span>
<script>
    $('#tag_<?php echo $tag->id?>').click(function(){
        $(this).closest('.one_tag').empty().removeClass('one_tag');
        $("#tag").append('<option value="<?php echo $tag->id?>"><?php echo $tag->title?></option>');
    });
</script>
<?php else: ?>
    </span>
<?php endif; ?>