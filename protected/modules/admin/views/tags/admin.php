<?php
/* @var $this TagsController */
/* @var $model Tags */
?>

<div id="page-heading">
    <h1><?php echo Yii::t('admin', 'Теги') ?></h1>
</div>
<?php
$this->widget('booster.widgets.TbAlert', array(
    'fade' => true,
    'closeText' => '&times;', // false equals no close link
    'events' => array(),
    'htmlOptions' => array(),
    'userComponentId' => 'user',
    'alerts' => array( // configurations per alert type
        // success, info, warning, error or danger
        'success' => array('closeText' => '&times;'),
        'info', // you don't need to specify full config
        'warning' => array('closeText' => '&times;'),
        'error' => array('closeText' => '&times;')
    ),
));
?>
<div class="options">
    <a href="<?php echo Yii::app()->createUrl("/admin/tags/create") ?>" class="btn btn-primary">
        <?php echo Yii::t('admin_panel', 'Додати тег'); ?>
    </a>
</div>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'tags-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'title',
		array(
			'class'=>'CButtonColumn',
            'template' => '{update}{delete}',
		),
	),
)); ?>
