<?php
/* @var $this TagsController */
/* @var $model Tags */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'tags-form',
    'htmlOptions' => array('class' => 'form-horizontal row-border'),
    'enableAjaxValidation' => true,
    'focus'=>'input[type="text"]:first',
    'clientOptions' => array(
        'validateOnSubmit' => true,
        'validateOnChange' => true,
        'errorCssClass' => 'has-error',
    ),
)); ?>

	<?php echo $form->errorSummary($model); ?>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'title', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model, 'title', array('class' => 'form-control input-tag-title' )); ?>
        </div>
        <div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'title'); ?></div></div>
    </div>

	<div class="row buttons">
        <?php echo CHtml::submitButton(
            $model->isNewRecord ? Yii::t('Admin', 'Створити') : Yii::t('Admin', 'Зберегти'),
            array('class' => 'btn-primary btn', 'style' => 'margin-left: 321px;')); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->