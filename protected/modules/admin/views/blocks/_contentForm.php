<?php
/* @var $lang string */
?>
<?php $status = isset($model->status) ? $model->status : false ?>
<?php //$title = isset($model->title) ? $model->title : false ?>
<?php $text = isset($model->text) ? $model->text : false ?>
<div class="contentBlock">
    <div class="lang_name">
        <?php if($lang != DEFAULT_LANG): ?>
        <?php echo CHtml::dropDownList(
            'Lang['. $lang . '][status]',
            $status,
            array(
                Yii::t('admin', 'Використовувати стандартний варіант (') . LawBranches::getLanguage(LawBranches::getLanguageByCode()) . ')',
                Yii::t('admin', 'Використовувати цей переклад'),
            )
        ); ?>
        <?php else: ?>
            <input type="hidden" name="<?php echo 'Lang['. $lang . '][status]'?>" value="1"/>
            <?php echo Yii::t('admin', '(Варіант за замовчуванням)') ?>
        <?php endif; ?>
    </div>

    <div class="<?php echo $lang; ?>_content_container">
        <?php echo CHtml::textArea('Lang['. $lang . '][text]', $text, array('class' => 'form-control', 'id' => 'lang_' . $lang . '_text' )); ?>
    </div>
</div>
