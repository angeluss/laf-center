<?php

/**
 * This is the model class for table "settings".
 *
 * The followings are the available columns in table 'settings':
 * @property integer $id
 * @property string $meta_title
 * @property string $admin_email
 * @property string $fb
 * @property string $phone
 * @property string $call_centr
 * @property string $address
 * @property string $skype
 * @property string $contact_email
 * @property string $address_en
 * @property string $status_en
 * @property string $address_ru
 * @property string $status_ru
 * @property string $address_fr
 * @property string $status_fr
 * @property string $address_tr
 * @property string $status_tr
 * @property string $address_ar
 * @property string $status_ar
 */
class Settings extends CActiveRecord
{
    public $uplFile;

    public function getAddress($lang = DEFAULT_LANG){
        $content = $this->address;
        switch ($lang){
            case 'uk':
                $content = $this->address;
                break;
            case 'en':
                if($this->status_en == 1){
                    $content = $this->address_en;
                }
                break;
            case 'ru':
                if($this->status_ru == 1){
                    $content = $this->address_ru;
                }
                break;
            case 'fr':
                if($this->status_fr == 1){
                    $content = $this->address_fr;
                }
                break;
            case 'tr':
                if($this->status_tr == 1){
                    $content = $this->address_tr;
                }
                break;
            case 'ar':
                if($this->status_ar == 1){
                    $content = $this->address_ar;
                }
                break;
            default:
                $content = $this->address;
        }
        return $content;
    }

	public static function getStatus($status = false){
		$items = array(
			Yii::t('admin', 'Виикористовувати український варіант'),
			Yii::t('admin', 'Використовувати цей переклад'),
		);

		if(false === $status){
			return $items;
		} else {
			return isset($items[$status]) ? $items[$status] : Yii::t('admin', 'Помилка');
		}
	}
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'settings';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('meta_title, admin_email, fb, phone, call_centr, address, skype', 'length', 'max'=>255),
            array('uplFile', 'file', 'allowEmpty'=>true, 'types'=>'ico', 'safe' => false),
			array('contact_email, id, meta_title, admin_email, fb, phone, call_centr, address, skype, address_ru,
			address_en, address_fr, address_tr, address_ar, status_ru, status_en, status_fr, status_tr, status_ar', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'meta_title' => 'Meta Title',
			'admin_email' => 'Admin Email',
			'fb' => 'Facebook',
			'phone' => 'Телефон',
			'call_centr' => 'Телефон колл-центра',
			'address' => 'Адреса(українською)',
			'address_en' => 'Адреса(англійською)',
			'address_ru' => 'Адреса(російською)',
			'address_fr' => 'Адреса(французською)',
			'address_tr' => 'Адреса(туркменською)',
			'address_ar' => 'Адреса(арабською)',
			'skype' => 'Skype',
            'uplFile' => 'Favicon',
		);
	}
    public static function getNameByLanguage($lang){
        switch ($lang){
            case 'uk':
                $content =Yii::t('front', 'Украинский');
                break;
            case 'en':
                $content = Yii::t('front', 'Английский');
                break;
            case 'ru':
                $content = Yii::t('front', 'Русский');
                break;
            case 'fr':
                $content = Yii::t('front', 'Французский');
                break;
            case 'tr':
                $content = Yii::t('front', 'Турецкий');
                break;
            case 'ar':
                $content = Yii::t('front', 'Арабский');
                break;
            default:
                $content = Yii::t('front', 'Украинский');
        }
        return $content;
    }
	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('meta_title',$this->meta_title,true);
		$criteria->compare('admin_email',$this->admin_email,true);
		$criteria->compare('fb',$this->fb,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('call_centr',$this->call_centr,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('skype',$this->skype,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Settings the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
