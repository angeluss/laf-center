<?php

/**
 * This is the model class for table "a_logs".
 *
 * The followings are the available columns in table 'a_logs':
 * @property integer $id
 * @property integer $user_id
 * @property string $action
 * @property integer $area
 * @property integer $date
 * @property string $entity_name
 */
class ALogs extends CActiveRecord
{
    const LOGIN  = 'login' ;
    const LOGOUT = 'logout';
    const CREATE = 'create';
    const UPDATE = 'update';
    const DELETE = 'delete';

    const ADMIN_PANEL = 1;
    const OPERATOR_PANEL = 2;
    const ADMIN = 3;
    const SITE = 4;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'a_logs';
	}

    public function getLogRow(){
        $login = isset($this->user->login) ? $this->user->login : 'unknown';
        $row = Yii::t('admin', 'Користувач ') . $login . ' ';
        switch($this->action) {
            case self::LOGIN:
                $action = Yii::t('admin', 'увійшов до системи');
                break;
            case self::LOGOUT:
                $action = Yii::t('admin', 'вийшов із системи');
                break;
            case self::CREATE:
                $action = Yii::t('admin', 'створив ');
                break;
            case self::UPDATE:
                $action = Yii::t('admin', 'відредагував ');
                break;
            case self::DELETE:
                $action = Yii::t('admin', 'видалив ');
                break;
            default:
                return false;
                break;
        }
        $row .= $action;
        $name = $this->entity_name != '' ? $this->entity_name : ' #' .$this->entity_id;
        if($this->entity != '')
            $row .= $this->entity . ' ' . $name;

        return $row;
    }

    public function log($action, $params = array()){

        $this->user_id = isset(Yii::app()->user->id) ? Yii::app()->user->id : 3; // @TODO привести в нормальный вид
        $this->date = time();
        $this->area = isset($params['area']) ? $params['area'] : self::ADMIN;
        $this->entity = isset($params['entity']) ? $params['entity'] : '';
        $this->entity_id = isset($params['entity_id']) ? $params['entity_id'] : 0;
        $this->entity_name = isset($params['entity_name']) ? $params['entity_name'] : '';
        $this->action = $action;

        if($this->validate())
            $this->save();
        else {
            CVarDumper::dump($this->getErrors());
            die;
        }
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id', 'required'),
			array('user_id, area, date', 'numerical', 'integerOnly'=>true),
			array('action, entity_name', 'length', 'max'=>255),
			// The following rule is used by search().
			array('id, user_id, action, area, date, entity_name', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'user' => array(self::BELONGS_TO, 'Admin', 'user_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'Користувач',
			'action' => 'Дія',
			'area' => 'Зона',
			'date' => 'Дата/час',
			'entity_name' => 'Назва сутності',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

        $sort = new CSort();
        $sort->defaultOrder = 't.date desc';

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('action',$this->action,true);
		$criteria->compare('area',$this->area);
		$criteria->compare('date',$this->date);
		$criteria->compare('entity_name',$this->entity_name,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => $sort,
            'pagination' => array(
                'pageSize' => '15',
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ALogs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
