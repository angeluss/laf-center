<?php

/**
 * This is the model class for table "law_branches_lang".
 *
 * The followings are the available columns in table 'law_branches_lang':
 * @property integer $id
 * @property integer $branch_id
 * @property string $lang
 * @property string $title
 * @property string $short_desc
 * @property integer $status
 */
class LawBranchesLang extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'law_branches_lang';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
    public static function getStatus($status = false){
        $statuses = array(
            Yii::t('admin', 'Замість цього варіанту використовується стандартний переклад (') . Pages::getLanguage(Pages::getLanguageByCode()) . ')',
            Yii::t('admin', '(Використовується)'),
        );
        return isset($statuses[$status]) ? $statuses[$status] : Yii::t('front', 'Невідома похибка');
    }
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('branch_id, lang, title', 'required'),
			array('branch_id, status', 'numerical', 'integerOnly'=>true),
			array('lang, title', 'length', 'max'=>255),
			array('short_desc', 'safe'),
			array('id, branch_id, lang, title, short_desc, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'branch_id' => 'Галузь',
			'lang' => 'Мова',
			'title' => 'Назва',
			'short_desc' => 'Опис',
			'status' => 'Статус',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('branch_id',$this->branch_id);
		$criteria->compare('lang',$this->lang,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('short_desc',$this->short_desc,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LawBranchesLang the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
