<?php

/**
 * This is the model class for table "requisites".
 *
 * The followings are the available columns in table 'requisites':
 * @property integer $id
 * @property string $recepient
 * @property string $account
 * @property string $id_code
 * @property string $bank
 * @property string $bank_code
 * @property string $amount
 * @property string $fine
 */
class Requisites extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'requisites';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('recepient, account, id_code, bank, bank_code, amount, fine', 'length', 'max'=>255),
			array('id, recepient, account, id_code, bank, bank_code, amount, fine', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'recepient' => 'Отримувач',
			'account' => 'Рахунок отримувача',
			'id_code' => 'Ідентифікаційний код отримувача',
			'bank' => 'Установа банку',
			'bank_code' => 'Код установи банку',
			'amount' => 'Сума',
			'fine' => 'Пеня',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('recepient',$this->recepient,true);
		$criteria->compare('account',$this->account,true);
		$criteria->compare('id_code',$this->id_code,true);
		$criteria->compare('bank',$this->bank,true);
		$criteria->compare('bank_code',$this->bank_code,true);
		$criteria->compare('amount',$this->amount,true);
		$criteria->compare('fine',$this->fine,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Requisites the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
