<?php

/**
 * This is the model class for table "news".
 *
 * The followings are the available columns in table 'news':
 * @property integer $id
 * @property string $title
 * @property string $img
 * @property string $short_desc
 * @property string $content
 * @property integer $status
 * @property integer $is_attached
 * @property integer $created_at
 * @property integer $updated_at
 */
class News extends CActiveRecord
{
    public $file;
    public function getUrlPrev()
    {
        $modelPrev = $this->model()->findBySql("Select * from news where updated_at < '".$this->updated_at."' Order By updated_at desc");
        if($modelPrev!=null)
            echo '<i class="fa fa-long-arrow-left"></i>'. CHtml::link(CHtml::encode($modelPrev->getContent(Yii::app()->session['setlanguage'])->title),'/news/'.$modelPrev->id, $linkOptions=array('class'=>'prevmaterial'));
    }


    public function getUrlNext()
    {
        $modelNext =$this->model()->findBySql("Select * from news where updated_at > '".$this->updated_at."' Order By updated_at");

        if($modelNext!=null)
            echo '<i class="fa fa-long-arrow-right"></i>'. CHtml::link(CHtml::encode($modelNext->getContent(Yii::app()->session['setlanguage'])->title),'/news/'.$modelNext->id, $linkOptions=array('class'=>'nextmaterial'));

    }

    public static function getImg($img, $id, $class=''){
        $dir = DIRECTORY_SEPARATOR . 'uploadFiles' . DIRECTORY_SEPARATOR;
        $file = $dir . 'newsImg' . DIRECTORY_SEPARATOR . $id . DIRECTORY_SEPARATOR . $img;
        $title = '';
        if(is_null($img) || $img == ''){
            $file = $dir . 'no_img.png';
            $title='no img';
        }
        return "<img class='$class' title='$title' src='$file' alt />";
    }

    public static function getStatus($status = false){
        $items = array(Yii::t('admin', 'Не показувати'), Yii::t('admin', 'Показувати'));

        if(false === $status){
            return $items;
        } else {
            return isset($items[$status]) ? $items[$status] : Yii::t('admin', 'Помилка');
        }
    }

    public static function getIsAttached($is_attached = false){
        $items = array(Yii::t('admin', 'Ні'), Yii::t('admin', 'Так'));

        if(false === $is_attached){
            return $items;
        } else {
            return isset($items[$is_attached]) ? $items[$is_attached] : Yii::t('admin', 'Помилка');
        }
    }

	/**
	 * @return string the associated database table name
	 */

	public function tableName()
	{
		return 'news';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, content', 'required'),
			array('status, is_attached, created_at', 'numerical', 'integerOnly'=>true),
			array('title, img, short_desc', 'length', 'max'=>255),
            array('file', 'file', 'allowEmpty'=>true, 'types'=>'png,jpg,gif,jpeg'),
			array('file, id, title, img, short_desc, content, status, is_attached, created_at, updated_at, link', 'safe'),
		);
	}
    public function scopes()
    {
        return array(
            'published'=>array(
                'condition'=>'status=1',
                'order'=>'is_attached DESC',

            ),
            'recently'=>array(
                'order'=>'updated_at DESC',
            ),
        );
    }
	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
            'tags'=>array(self::MANY_MANY, 'Tags', 'news_tags(new_id, tag_id)'),
            'ukrainian' => array(self::HAS_ONE, 'NewsLang', 'new_id', 'condition'=>'lang=:lang',
                'params'=>array(':lang'=>'uk')),
            'english' => array(self::HAS_ONE, 'NewsLang', 'new_id', 'condition'=>'lang=:lang',
                'params'=>array(':lang'=>'en')),
            'russian' => array(self::HAS_ONE, 'NewsLang', 'new_id', 'condition'=>'lang=:lang',
                'params'=>array(':lang'=>'ru')),
            'france' => array(self::HAS_ONE, 'NewsLang', 'new_id', 'condition'=>'lang=:lang',
                'params'=>array(':lang'=>'fr')),
            'turkmen' => array(self::HAS_ONE, 'NewsLang', 'new_id', 'condition'=>'lang=:lang',
                'params'=>array(':lang'=>'tr')),
            'arabian' => array(self::HAS_ONE, 'NewsLang', 'new_id', 'condition'=>'lang=:lang',
                'params'=>array(':lang'=>'ar')),
		);
	}
    public function getDefaultContent(){
        switch (DEFAULT_LANG) {
            case 'uk':
                $defaultContent = $this->ukrainian;
                break;
            case 'en':
                $defaultContent = $this->english;
                break;
            case 'ru':
                $defaultContent = $this->russian;
                break;
            case 'fr':
                $defaultContent = $this->france;
                break;
            case 'tr':
                $defaultContent = $this->turkmen;
                break;
            case 'ar':
                $defaultContent = $this->arabian;
                break;
            default:
                $defaultContent = $this->ukrainian;
        }
        return $defaultContent;
    }

    public function getRelation($lang = DEFAULT_LANG){
        switch ($lang){
            case 'uk':
                $content = array('ukrainian');
                break;
            case 'en':
                    $content = array('english');
                break;
            case 'ru':
                    $content = array('russian');
                break;
            case 'fr':
                    $content = array('france');
                break;
            case 'tr':
                    $content = array('turkmen');
                break;
            case 'ar':
                    $content = array('arabian');
                break;
            default:
                $content = array('ukrainian');
        }
        return $content;
    }

    public function getRelationName($lang){

        switch ($lang){
            case 'uk':
                    $content = 'ukrainian';
                break;
            case 'en':
                     $content = 'english';
                break;
            case 'ru':
                $content = 'russian';
                break;
            case 'fr':
                $content = 'france';
                break;
            case 'tr':
                $content = 'turkmen';
                break;
            case 'ar':
                $content = 'arabian';
                break;
            default:
                $content = 'ukrainian';
        }
        return $content;
    }

    public function getContent($lang = DEFAULT_LANG){
        $content = $this->getDefaultContent();
        switch ($lang){
            case 'uk':
                if($this->ukrainian->status == 1){
                    $content = $this->ukrainian;
                }
                break;
            case 'en':
                if($this->english->status == 1){
                    $content = $this->english;
                }
                break;
            case 'ru':
                if($this->russian->status == 1){
                    $content = $this->russian;
                }
                break;
            case 'fr':
                if($this->france->status == 1){
                    $content = $this->france;
                }
                break;
            case 'tr':
                if($this->turkmen->status == 1){
                    $content = $this->turkmen;
                }
                break;
            case 'ar':
                if($this->arabian->status == 1){
                    $content = $this->arabian;
                }
                break;
            default:
                $content = $this->ukrainian;
        }
        return $content;
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Назва',
			'img' => 'Зображення',
			'short_desc' => 'Короткий опис',
			'content' => 'Текст',
			'status' => 'Статус',
            'link' => 'Ссылка',
			'is_attached' => 'Закріплена',
			'created_at' => 'Створено',
			'updated_at' => 'Оновлено',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('img',$this->img,true);
		$criteria->compare('short_desc',$this->short_desc,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('is_attached',$this->is_attached);
		$criteria->compare('created_at',$this->created_at);
		$criteria->compare('updated_at',$this->updated_at);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => array(
                'defaultOrder' => 'created_at DESC',
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return News the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
