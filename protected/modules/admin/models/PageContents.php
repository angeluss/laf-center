<?php

/**
 * This is the model class for table "page_contents".
 *
 * The followings are the available columns in table 'page_contents':
 * @property integer $id
 * @property integer $page_id
 * @property string $lang
 * @property string $content
 * @property integer $status
 */
class PageContents extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'page_contents';
	}

    public static function getStatus($status = false){
        $statuses = array(
            Yii::t('admin', 'Замість цього варіанту використовується стандартний переклад (') . Pages::getLanguage(Pages::getLanguageByCode()) . ')',
            Yii::t('admin', '(Використовується)'),
        );
        return isset($statuses[$status]) ? $statuses[$status] : Yii::t('front', 'Невідома похибка');
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('page_id, lang', 'required'),
			array('page_id, status', 'numerical', 'integerOnly'=>true),
			array('lang', 'length', 'max'=>255),
			array('content', 'safe'),
			// The following rule is used by search().
			array('id, page_id, lang, content, status', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'page_id' => 'Page',
			'lang' => 'Lang',
			'content' => 'Content',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('page_id',$this->page_id);
		$criteria->compare('lang',$this->lang,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return PageContents the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getLangPage($data){
		$lang_page = PageContents::model()->findByAttributes(
			array(
				'lang'    => Yii::app()->language,
				'page_id' => $data->page_id,
				'status'  => 1,
			)
		);
		if (!$lang_page) {
			$lang_page = PageContents::model()->findByAttributes(
				array(
					'lang'	  => DEFAULT_LANG,
					'page_id' => $data->page_id,
					'status'  => 1,
				)
			);
		}
		return $lang_page;
	}
}
