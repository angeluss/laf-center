<?php

/**
 * This is the model class for table "consultation_lang".
 *
 * The followings are the available columns in table 'consultation_lang':
 * @property integer $id
 * @property integer $con_id
 * @property string $lang
 * @property string $title
 * @property string $content
 * @property integer $status
 */
class ConsultationLang extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName(){
		return 'consultation_lang';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
    public static function getStatus($status = false){
        $statuses = array(
            Yii::t('admin', 'Замість цього варіанту використовується стандартний переклад (') . Pages::getLanguage(Pages::getLanguageByCode()) . ')',
            Yii::t('admin', '(Використовується)'),
        );
        return isset($statuses[$status]) ? $statuses[$status] : Yii::t('front', 'Невідома похибка');
    }
	public function rules(){
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('con_id, lang, title', 'required'),
			array('con_id, status', 'numerical', 'integerOnly'=>true),
			array('lang, title', 'length', 'max'=>255),
			array('content', 'safe'),
			array('id, con_id, lang, title, content, status', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations(){
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels(){
		return array(
			'id' => 'ID',
			'con_id' => 'Консультація',
			'lang' => 'МОва',
			'title' => 'Назва',
			'content' => 'Контент',
			'status' => 'Статус',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search(){
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('con_id',$this->con_id);
		$criteria->compare('lang',$this->lang,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ConsultationLang the static model class
	 */
	public static function model($className = __CLASS__){
		return parent::model($className);
	}

	public static function getConsultsForSearch($search){
		$consult_criteria = new CDbCriteria(array(
			'condition' => 'lang=:lang AND content LIKE :keyword OR title LIKE :keyword',
			'params' => array(
				':keyword' => '%'.$search->string.'%',
				':lang' => Yii::app()->request->getParam('language'),
			),
		));

		$consultCount = ConsultationLang::model()->count($consult_criteria);
		$consult_pagination = new CPagination($consultCount);
		$consult_pagination->pageSize = 20;
		$consult_pagination->applyLimit($consult_criteria);

		$consultations = ConsultationLang::model()->findAll($consult_criteria);
		$params['searchPagination'] = $consult_pagination;
        $params['consultations'] = $consultations;
		return $params;
	}

}
