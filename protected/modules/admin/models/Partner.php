<?php

/**
 * This is the model class for table "partner".
 *
 * The followings are the available columns in table 'partner':
 * @property integer $id
 * @property string $title
 * @property string $url
 * @property string $img
 * @property string $desc
 * @property integer $status
 */
class Partner extends CActiveRecord
{
    public $file;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'partner';
	}


    public static function getImg($img, $id, $class=''){
        $dir = DIRECTORY_SEPARATOR . 'uploadFiles' . DIRECTORY_SEPARATOR;
        $file = $dir . 'partners' . DIRECTORY_SEPARATOR . $id . DIRECTORY_SEPARATOR . $img;
        $title = '';
        if(is_null($img) || $img == ''){
            $file = $dir . 'no_img.png';
            $title='no img';
        }
        return "<a class='fancybox' href='$file' title=''><img class='$class' title='$title' src='$file' alt /></a>";
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, url', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('title, url, img', 'length', 'max'=>255),
            array('file', 'file', 'allowEmpty'=>true, 'types'=>'png,jpg,gif,jpeg'),
			array('id, title, url, img, desc, status, file', 'safe'),
		);
	}


	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(

            'ukrainian' => array(self::HAS_ONE, 'PartnersLang', 'partner_id', 'condition'=>'lang=:lang',
                'params'=>array(':lang'=>'uk')),
            'english' => array(self::HAS_ONE, 'PartnersLang', 'partner_id', 'condition'=>'lang=:lang',
                'params'=>array(':lang'=>'en')),
            'russian' => array(self::HAS_ONE, 'PartnersLang', 'partner_id', 'condition'=>'lang=:lang',
                'params'=>array(':lang'=>'ru')),
            'france' => array(self::HAS_ONE, 'PartnersLang', 'partner_id', 'condition'=>'lang=:lang',
                'params'=>array(':lang'=>'fr')),
            'turkmen' => array(self::HAS_ONE, 'PartnersLang', 'partner_id', 'condition'=>'lang=:lang',
                'params'=>array(':lang'=>'tr')),
            'arabian' => array(self::HAS_ONE, 'PartnersLang', 'partner_id', 'condition'=>'lang=:lang',
                'params'=>array(':lang'=>'ar')),

        );
	}


    public function getDefaultContent(){
        switch (DEFAULT_LANG) {
            case 'uk':
                $defaultContent = $this->ukrainian;
                break;
            case 'en':
                $defaultContent = $this->english;
                break;
            case 'ru':
                $defaultContent = $this->russian;
                break;
            case 'fr':
                $defaultContent = $this->france;
                break;
            case 'tr':
                $defaultContent = $this->turkmen;
                break;
            case 'ar':
                $defaultContent = $this->arabian;
                break;
            default:
                $defaultContent = $this->ukrainian;
        }
        return $defaultContent;
    }

    public function getContent($lang = DEFAULT_LANG){
        $content = $this->getDefaultContent();
        switch ($lang){
            case 'uk':
                if($this->ukrainian->status == 1){
                    $content = $this->ukrainian;
                }
                break;
            case 'en':
                if($this->english->status == 1){
                    $content = $this->english;
                }
                break;
            case 'ru':
                if($this->russian->status == 1){
                    $content = $this->russian;
                }
                break;
            case 'fr':
                if($this->france->status == 1){
                    $content = $this->france;
                }
                break;
            case 'tr':
                if($this->turkmen->status == 1){
                    $content = $this->turkmen;
                }
                break;
            case 'ar':
                if($this->arabian->status == 1){
                    $content = $this->arabian;
                }
                break;
            default:
                $content = $this->ukrainian;
        }
        return $content;
    }
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Назва',
			'url' => 'Url',
			'img' => 'Логотип',
			'desc' => 'Опис',
			'status' => 'Статус',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{

		$criteria=new CDbCriteria;

		$sort = new CSort();
		$sort->defaultOrder = 't.id asc';

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('url',$this->url,true);
		$criteria->compare('img',$this->img,true);
		$criteria->compare('desc',$this->desc,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort' => $sort,
			'pagination' => array(
				'pageSize' => '5',
			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Partner the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
