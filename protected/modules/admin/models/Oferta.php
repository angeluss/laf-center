<?php

/**
 * This is the model class for table "oferta".
 *
 * The followings are the available columns in table 'oferta':
 * @property integer $id
 * @property string $lang
 * @property string $content
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Oferta extends CActiveRecord
{
    const ENGLISH   = 1;
    const FRANCE    = 2;
    const RUSSIAN   = 3;
    const UKRAINIAN = 4;
    const TURKMAN   = 5;
    const ARABIC    = 6;

    const YES = 1;
    const NO = 2;

    public static function getLanguage($lang = false){
        $langs = array(
            self::ENGLISH   => Yii::t('admin', 'Англійська'),
            self::FRANCE    => Yii::t('admin', 'Французька'),
            self::RUSSIAN   => Yii::t('admin', 'Російська'),
            self::UKRAINIAN => Yii::t('admin', 'Українська'),
            self::TURKMAN   => Yii::t('admin', 'Туркменська'),
            self::ARABIC    => Yii::t('admin', 'Арабська'),
        );

        if(false === $lang){
            return $langs;
        } else {
            return isset($langs[$lang]) ? $langs[$lang] : Yii::t('front', 'Невідома похибка');
        }
    }

    public function getLanguage2(){
        $langs = array(
            self::ENGLISH   => Yii::t('admin', 'англійського'),
            self::FRANCE    => Yii::t('admin', 'французького'),
            self::RUSSIAN   => Yii::t('admin', 'російського'),
            self::UKRAINIAN => Yii::t('admin', 'українського'),
            self::TURKMAN   => Yii::t('admin', 'туркменського'),
            self::ARABIC    => Yii::t('admin', 'арабського'),
        );

        return isset($langs[$this->lang]) ? $langs[$this->lang] : Yii::t('front', '');

    }

    public static function getLanguageByCode($lang = DEFAULT_LANG){
        $langs = array(
            self::ENGLISH   => 'en',
            self::FRANCE    => 'fr',
            self::RUSSIAN   => 'ru',
            self::UKRAINIAN => 'uk',
            self::TURKMAN   => 'tr',
            self::ARABIC    => 'ar',
        );
        $langs = array_flip($langs);
        return isset($langs[$lang]) ? $langs[$lang] : Yii::t('front', 'Невідома похибка');
    }

    public static function getStatus($status = false) {
        $statuses = array(
            self::YES => Yii::t('admin', 'Використовується'),
            self::NO => Yii::t('admin', 'Не використовується'),
        );
        if(false === $status){
            return $statuses;
        } else {
            return isset($statuses[$status]) ? $statuses[$status] : Yii::t('front', 'Невідома похибка');
        }
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'oferta';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('lang, content, status', 'required'),
			array('status, created_at, updated_at', 'numerical', 'integerOnly'=>true),
			array('lang', 'length', 'max'=>10),
			// The following rule is used by search().
			array('id, lang, content, status, created_at, updated_at', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'lang' => 'Мова',
			'content' => 'Контент',
			'status' => 'Статус',
			'created_at' => 'Створено',
			'updated_at' => 'Оновлено',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('lang',$this->lang,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_at',$this->created_at);
		$criteria->compare('updated_at',$this->updated_at);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Oferta the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
