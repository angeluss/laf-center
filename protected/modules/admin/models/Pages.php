<?php

/**
 * This is the model class for table "pages".
 *
 * The followings are the available columns in table 'pages':
 * @property integer $id
 * @property string $title
 * @property string $meta_title
 * @property string $alias
 * @property string $meta_keys
 * @property string $meta_desc
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Pages extends CActiveRecord
{
    const ENGLISH   = 1;
    const FRANCE    = 2;
    const RUSSIAN   = 3;
    const UKRAINIAN = 4;
    const TURKMAN   = 5;
    const ARABIC    = 6;


    public static function getLanguage($lang = false){

        $langs = array(
            self::ENGLISH   => Yii::t('admin', 'Англійська'),
            self::FRANCE    => Yii::t('admin', 'Французька'),
            self::RUSSIAN   => Yii::t('admin', 'Російська'),
            self::UKRAINIAN => Yii::t('admin', 'Українська'),
            self::TURKMAN   => Yii::t('admin', 'Туркменська'),
            self::ARABIC    => Yii::t('admin', 'Арабська'),
        );

        if(false === $lang){
            return $langs;
        } else {
            $lang = (int) $lang;
            return isset($langs[$lang]) ? $langs[$lang] : Yii::t('front', 'Невідома похибка');
        }
    }

    public static function getClass(){
        $class = '';
        if(stristr(Yii::app()->request->requestUri, 'consult') !== FALSE) {
            if(Yii::app()->user->isGuest) {
                $class = 'consult_main_div';
            }
        }
        return $class;
    }

    public function getLanguage2(){
        $langs = array(
            self::ENGLISH   => Yii::t('admin', 'англійського'),
            self::FRANCE    => Yii::t('admin', 'французького'),
            self::RUSSIAN   => Yii::t('admin', 'російського'),
            self::UKRAINIAN => Yii::t('admin', 'українського'),
            self::TURKMAN   => Yii::t('admin', 'туркменського'),
            self::ARABIC    => Yii::t('admin', 'арабського'),
        );

        return isset($langs[$this->lang]) ? $langs[$this->lang] : Yii::t('front', '');

    }

    public static function getLanguageByCode($lang = DEFAULT_LANG){
        $langs = array(
            self::ENGLISH   => 'en',
            self::FRANCE    => 'fr',
            self::RUSSIAN   => 'ru',
            self::UKRAINIAN => 'uk',
            self::TURKMAN   => 'tr',
            self::ARABIC    => 'ar',
        );
        $langs = array_flip($langs);
        return isset($langs[$lang]) ? $langs[$lang] : Yii::t('front', 'Невідома похибка');
    }

    /**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'pages';
	}

    public static function getStatus($status = false){
        $items = array(Yii::t('admin', 'Не показувати'), Yii::t('admin', 'Показувати'));

        if(false === $status){
            return $items;
        } else {
            return isset($items[$status]) ? $items[$status] : Yii::t('admin', 'Помилка');
        }
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, meta_title, alias', 'required'),
            array('alias','unique'),
			array('status, created_at, updated_at', 'numerical', 'integerOnly'=>true),
			array('title, meta_title, alias', 'length', 'max'=>255),
            array('alias', 'match', 'pattern' => '/^[A-Za-z0-9_]+$/u', 'message' => Yii::t('admin', 'Лише латинські літери')),
			array('id, title, meta_title, alias, status, created_at, updated_at, meta_keys, meta_desc', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
            'ukrainian' => array(self::HAS_ONE, 'PageContents', 'page_id', 'condition'=>'lang=:lang',
                'params'=>array(':lang'=>'uk')),
            'english' => array(self::HAS_ONE, 'PageContents', 'page_id', 'condition'=>'lang=:lang',
                'params'=>array(':lang'=>'en')),
            'russian' => array(self::HAS_ONE, 'PageContents', 'page_id', 'condition'=>'lang=:lang',
                'params'=>array(':lang'=>'ru')),
            'france' => array(self::HAS_ONE, 'PageContents', 'page_id', 'condition'=>'lang=:lang',
                'params'=>array(':lang'=>'fr')),
            'turkmen' => array(self::HAS_ONE, 'PageContents', 'page_id', 'condition'=>'lang=:lang',
                'params'=>array(':lang'=>'tr')),
            'arabian' => array(self::HAS_ONE, 'PageContents', 'page_id', 'condition'=>'lang=:lang',
                'params'=>array(':lang'=>'ar')),
		);
	}

    public function getDefaultContent(){
        switch (DEFAULT_LANG) {
            case 'uk':
                $defaultContent = $this->ukrainian;
                break;
            case 'en':
                $defaultContent = $this->english;
                break;
            case 'ru':
                $defaultContent = $this->russian;
                break;
            case 'fr':
                $defaultContent = $this->france;
                break;
            case 'tr':
                $defaultContent = $this->turkmen;
                break;
            case 'ar':
                $defaultContent = $this->arabian;
                break;
            default:
                $defaultContent = $this->ukrainian;
        }
        return $defaultContent;
    }

    public function getContent($lang = DEFAULT_LANG){
        if($this->status == 0){
            $content = new PageContents();
            $content->content = Yii::t('front', 'Тимчасово вимкнено');
            return $content;
        }
        $content = $this->getDefaultContent();
        switch ($lang){
            case 'uk':
                if($this->ukrainian->status == 1){
                    $content = $this->ukrainian;
                }
                break;
            case 'en':
                if($this->english->status == 1){
                    $content = $this->english;
                }
                break;
            case 'ru':
                if($this->russian->status == 1){
                    $content = $this->russian;
                }
                break;
            case 'fr':
                if($this->france->status == 1){
                    $content = $this->france;
                }
                break;
            case 'tr':
                if($this->turkmen->status == 1){
                    $content = $this->turkmen;
                }
                break;
            case 'ar':
                if($this->arabian->status == 1){
                    $content = $this->arabian;
                }
                break;
            default:
                $content = $this->ukrainian;
        }
        return $content;
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Назва',
			'meta_title' => 'Meta Title',
			'alias' => 'Аліас',
			'status' => 'Статус',
			'created_at' => 'Створено',
			'updated_at' => 'Оновлено',
			'meta_desc' => 'Meta Description',
			'meta_keys' => 'Meta Keywords',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{

		$criteria=new CDbCriteria;
        $sort = new CSort();
        $sort->defaultOrder = 't.created_at asc';

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('meta_title',$this->meta_title,true);
		$criteria->compare('alias',$this->alias,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_at',$this->created_at);
		$criteria->compare('updated_at',$this->updated_at);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort'=>$sort,
            'pagination'=>array(
                'pageSize' => 10,
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Pages the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
