
<?php

/**
 * This is the model class for table "consultation".
 *
 * The followings are the available columns in table 'consultation':
 * @property integer $id
 * @property string $title
 * @property string $content
 * @property integer $status
 * @property integer $type_consult
 */
class Consultation extends CActiveRecord
{
    /**
     * @return string the associated database table name
     */
    public function tableName()
    {
        return 'consultation';
    }

    /**
     * @return array validation rules for model attributes.
     */
    public function rules()
    {
        // NOTE: you should only define rules for those attributes that
        // will receive user inputs.
        return array(
            array('title, content', 'required'),
            array('status, type_consult', 'numerical', 'integerOnly'=>true),
            array('title', 'length', 'max'=>255),
            array('id, title, content, status, type_consult', 'safe'),
        );
    }

    /**
     * @return array relational rules.
     */
    public function relations()
    {
        return array(
            'branch'=>array(self::BELONGS_TO, 'LawBranches', 'con_law_branch(con_id)'),
            'ukrainian' => array(self::HAS_ONE, 'ConsultationLang', 'con_id', 'condition'=>'lang=:lang',
                'params'=>array(':lang'=>'uk')),
            'english' => array(self::HAS_ONE, 'ConsultationLang', 'con_id', 'condition'=>'lang=:lang',
                'params'=>array(':lang'=>'en')),
            'russian' => array(self::HAS_ONE, 'ConsultationLang', 'con_id', 'condition'=>'lang=:lang',
                'params'=>array(':lang'=>'ru')),
            'france' => array(self::HAS_ONE, 'ConsultationLang', 'con_id', 'condition'=>'lang=:lang',
                'params'=>array(':lang'=>'fr')),
            'turkmen' => array(self::HAS_ONE, 'ConsultationLang', 'con_id', 'condition'=>'lang=:lang',
                'params'=>array(':lang'=>'tr')),
            'arabian' => array(self::HAS_ONE, 'ConsultationLang', 'con_id', 'condition'=>'lang=:lang',
                'params'=>array(':lang'=>'ar')),
        );
    }

    /**
     * @return array customized attribute labels (name=>label)
     */
    public function attributeLabels()
    {
        return array(
            'id' => 'ID',
            'title' => 'Название',
            'content' => 'Контент',
            'status' => 'Статус',
            'type_consult' => 'Галузь права',
        );
    }

    /**
     * Retrieves a list of models based on the current search/filter conditions.
     *
     * Typical usecase:
     * - Initialize the model fields with values from filter form.
     * - Execute this method to get CActiveDataProvider instance which will filter
     * models according to data in model fields.
     * - Pass data provider to CGridView, CListView or any similar widget.
     *
     * @return CActiveDataProvider the data provider that can return the models
     * based on the search/filter conditions.
     */
    public function search()
    {
        $criteria=new CDbCriteria;

        $criteria->compare('id',$this->id);
        $criteria->compare('title',$this->title,true);
        $criteria->compare('content',$this->content,true);
        $criteria->compare('status',$this->status);
        $criteria->compare('type_consult',$this->type_consult);

        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
    }
    public function getDefaultContent(){
        switch (DEFAULT_LANG) {
            case 'uk':
                $defaultContent = $this->ukrainian;
                break;
            case 'en':
                $defaultContent = $this->english;
                break;
            case 'ru':
                $defaultContent = $this->russian;
                break;
            case 'fr':
                $defaultContent = $this->france;
                break;
            case 'tr':
                $defaultContent = $this->turkmen;
                break;
            case 'ar':
                $defaultContent = $this->arabian;
                break;
            default:
                $defaultContent = $this->ukrainian;
        }
        return $defaultContent;
    }

    public function getContent($lang = DEFAULT_LANG){
        $content = $this->getDefaultContent();
        switch ($lang){
            case 'uk':
                if($this->ukrainian->status == 1){
                    $content = $this->ukrainian;
                }
                break;
            case 'en':
                if($this->english->status == 1){
                    $content = $this->english;
                }
                break;
            case 'ru':
                if($this->russian->status == 1){
                    $content = $this->russian;
                }
                break;
            case 'fr':
                if($this->france->status == 1){
                    $content = $this->france;
                }
                break;
            case 'tr':
                if($this->turkmen->status == 1){
                    $content = $this->turkmen;
                }
                break;
            case 'ar':
                if($this->arabian->status == 1){
                    $content = $this->arabian;
                }
                break;
            default:
                $content = $this->ukrainian;
        }
        return $content;
    }
    public static function getTypeCon($status = false){
        $items = array(Yii::t('admin', 'Не показувати'), Yii::t('admin', 'Показувати'));

        if(false === $status){
            return $items;
        } else {
            return isset($items[$status]) ? $items[$status] : Yii::t('admin', 'Помилка');
        }
    }

    /**
     * Returns the static model of the specified AR class.
     * Please note that you should have this exact method in all your CActiveRecord descendants!
     * @param string $className active record class name.
     * @return Consultation the static model class
     */
    public static function model($className=__CLASS__)
    {
        return parent::model($className);
    }
}