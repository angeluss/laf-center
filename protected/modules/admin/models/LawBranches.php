<?php

/**
 * This is the model class for table "law_branches".
 *
 * The followings are the available columns in table 'law_branches':
 * @property integer $id
 * @property string $title
 * @property string $short_desc
 */
class LawBranches extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
    public function scopes()
    {
        return array(
            'published'=>array(
                'condition'=>'status=1',

            ),
        );
    }

	public function tableName()
	{
		return 'law_branches';
	}

    const ENGLISH   = 1;
    const FRANCE    = 2;
    const RUSSIAN   = 3;
    const UKRAINIAN = 4;
    const TURKMAN   = 5;
    const ARABIC    = 6;

    public function getAnchor(){
        $name = $this->getContent('uk')->title;
        $name = str_replace(' ', '_', $name);
        return Translit::getTranslit($name);
    }

    public static function getLanguage($lang = false){

        $langs = array(
            self::ENGLISH   => Yii::t('admin', 'Англійська'),
            self::FRANCE    => Yii::t('admin', 'Французька'),
            self::RUSSIAN   => Yii::t('admin', 'Російська'),
            self::UKRAINIAN => Yii::t('admin', 'Українська'),
            self::TURKMAN   => Yii::t('admin', 'Туркменська'),
            self::ARABIC    => Yii::t('admin', 'Арабська'),
        );

        if(false === $lang){
            return $langs;
        } else {
            $lang = (int) $lang;
            return isset($langs[$lang]) ? $langs[$lang] : Yii::t('front', 'Невідома похибка');
        }
    }

    public static function getLanguageByCode($lang = DEFAULT_LANG){
        $langs = array(
            self::ENGLISH   => 'en',
            self::FRANCE    => 'fr',
            self::RUSSIAN   => 'ru',
            self::UKRAINIAN => 'uk',
            self::TURKMAN   => 'tr',
            self::ARABIC    => 'ar',
        );
        $langs = array_flip($langs);
        return isset($langs[$lang]) ? $langs[$lang] : Yii::t('front', 'Невідома похибка');
    }

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title', 'required'),
			array('title', 'length', 'max'=>255),
			array('id, title, short_desc, status', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
            'consultations'=>array(self::MANY_MANY, 'Consultation', 'con_law_branch(branch_id, con_id)'),
            'ukrainian' => array(self::HAS_ONE, 'LawBranchesLang', 'branch_id', 'condition'=>'lang=:lang',
                'params'=>array(':lang'=>'uk')),
            'english' => array(self::HAS_ONE, 'LawBranchesLang', 'branch_id', 'condition'=>'lang=:lang',
                'params'=>array(':lang'=>'en')),
            'russian' => array(self::HAS_ONE, 'LawBranchesLang', 'branch_id', 'condition'=>'lang=:lang',
                'params'=>array(':lang'=>'ru')),
            'france' => array(self::HAS_ONE, 'LawBranchesLang', 'branch_id', 'condition'=>'lang=:lang',
                'params'=>array(':lang'=>'fr')),
            'turkmen' => array(self::HAS_ONE, 'LawBranchesLang', 'branch_id', 'condition'=>'lang=:lang',
                'params'=>array(':lang'=>'tr')),
            'arabian' => array(self::HAS_ONE, 'LawBranchesLang', 'branch_id', 'condition'=>'lang=:lang',
                'params'=>array(':lang'=>'ar')),
		);
	}

    public function getDefaultContent(){
        switch (DEFAULT_LANG) {
            case 'uk':
                $defaultContent = $this->ukrainian;
                break;
            case 'en':
                $defaultContent = $this->english;
                break;
            case 'ru':
                $defaultContent = $this->russian;
                break;
            case 'fr':
                $defaultContent = $this->france;
                break;
            case 'tr':
                $defaultContent = $this->turkmen;
                break;
            case 'ar':
                $defaultContent = $this->arabian;
                break;
            default:
                $defaultContent = $this->ukrainian;
        }
        return $defaultContent;
    }

    public function getContent($lang = DEFAULT_LANG){
        $content = $this->getDefaultContent();
        switch ($lang){
            case 'uk':
                if($this->ukrainian->status == 1){
                    $content = $this->ukrainian;
                }
                break;
            case 'en':
                if($this->english->status == 1){
                    $content = $this->english;
                }
                break;
            case 'ru':
                if($this->russian->status == 1){
                    $content = $this->russian;
                }
                break;
            case 'fr':
                if($this->france->status == 1){
                    $content = $this->france;
                }
                break;
            case 'tr':
                if($this->turkmen->status == 1){
                    $content = $this->turkmen;
                }
                break;
            case 'ar':
                if($this->arabian->status == 1){
                    $content = $this->arabian;
                }
                break;
            default:
                $content = $this->ukrainian;
        }
        return $content;
    }
	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Назва',
			'short_desc' => 'Опис',
            'status'=>'Статус',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('short_desc',$this->short_desc,true);
        $criteria->compare('status',$this->status,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
    public function getLawBranches(){
        $lawBranches = $this->model()->findAll();
        $lawBranchesarr=array();
        foreach ($lawBranches as $lawBranch) {
            $lawBranchesarr[$lawBranch->id]=$lawBranch->title;
        }
        return $lawBranchesarr;
    }

    public static function getLawBranch($type){
        $lawBranches = self::model()->findAll();
        $lawBranchesarr=array();
        foreach ($lawBranches as $lawBranch) {
            $lawBranchesarr[$lawBranch->id]=$lawBranch->title;
        }
        return $lawBranchesarr[$type];
    }


	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return LawBranches the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
