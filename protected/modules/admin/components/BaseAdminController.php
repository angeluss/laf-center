<?php

class BaseAdminController extends Controller {
    public $layout = 'main';
    public function filters()
    {
        return array(
            'accessControl', // perform access control for CRUD operations
        );
    }

    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow',  // allow all users to perform 'login' and 'logout' actions
                'actions'=>array('login', 'logout', 'error'),
                'users'=>array('*'),
            ),
            array('allow', // allow authenticated user to perform actions
                'users'=>array('@'),
            ),
            array('deny',  // deny all users
                'users'=>array('*'),
            ),
        );
    }

    public function __construct($id,$module=null)
    {
        if(isset(Yii::app()->user->role) && Yii::app()->user->role == 'none')
            throw new CHttpException(404);
        return parent::__construct($id, $module);
    }

}