<?php

class BlocksController extends BaseAdminController
{
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform actions
				'users' => array('@'),
			),
			array('deny',  // deny all users
				'users' => array('*'),
			),
		);
	}


	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new Blocks;

		$this->performAjaxValidation($model);
		$post = Yii::app()->request->getPost('Blocks');
		if(!is_null($post)) {
			$model->attributes = $post;
			if($model->validate()) {
				$model->save();
				$action = ALogs::CREATE;
				$flash = Yii::t('admin', 'Блок "{title}" успішно створений', array('{title}'=>$model->title));
				Yii::app()->user->setFlash('success',$flash);
				$params = array();
				$params['entity'] = Yii::t('admin', 'статичний блок');
				$params['entity_id'] = $model->id;
				$params['entity_name'] = $model->title;
				$log = new ALogs();
				$log->log($action, $params);

				$post = Yii::app()->request->getPost('Lang');
                if(!is_null($post)) {
                    foreach($post as $lang => $params) {
                        $status = $params['status'];
                        $text = $params['text'];
                        if ($params['status'] == 0) {
                            $text = $post['uk']['text'];
                        }
                        $langModel = BlocksLang::model()->findByAttributes(array('block_id' => $model->id, 'lang' => $lang));

                        if(is_null($langModel))
                            $langModel = new BlocksLang();

                        $langModel->block_id = $model->id;
                        $langModel->lang = $lang;
                        $langModel->status = $status;
                        $langModel->title = $model->title;
                        $langModel->text = $text;
                        if($langModel->validate()){
                            $langModel->save();
                        } else {
                            CVarDumper::dump($langModel->getErrors()); die;
                        }
                    }
                }
				$this->redirect(array('index'));
			} else {
				CVarDumper::dump($model->getErrors()); die;
			}
		}

		$this->render('create', array(
			'model' => $model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		$this->performAjaxValidation($model);

		$post = Yii::app()->request->getPost('Blocks');
		if(!is_null($post)) {
			$model->attributes = $post;
			if($model->validate()) {
				$model->save();
				$action = $model->isNewRecord ? ALogs::CREATE : ALogs::UPDATE;
				$flash = Yii::t('admin', 'Блок "{title}" успішно змінений', array('{title}'=>$model->title));
				Yii::app()->user->setFlash('success',$flash);
				$params = array();
				$params['entity'] = Yii::t('admin', 'статичний блок');
				$params['entity_id'] = $model->id;
				$params['entity_name'] = $model->title;
				$log = new ALogs();
				$log->log($action, $params);
				$post = Yii::app()->request->getPost('Lang');
                if(!is_null($post)) {
                    foreach($post as $lang => $params) {
                        $status = $params['status'];
                        $text = $params['text'];
                        if ($params['status'] == 0) {
                            $text = $post['uk']['text'];
                        }
                        $langModel = BlocksLang::model()->findByAttributes(array('block_id' => $model->id, 'lang' => $lang));

                        if(is_null($langModel))
                            $langModel = new BlocksLang();

                        $langModel->block_id = $model->id;
                        $langModel->lang = $lang;
                        $langModel->title = $model->title;
                        $langModel->status = $status;
                        $langModel->text = $text;
                        if($langModel->validate()){
                            $langModel->save();
                        } else {
                            CVarDumper::dump($langModel->getErrors()); die;
                        }
                    }
                }
				$this->redirect(array('index'));
			} else {
				CVarDumper::dump($model->getErrors()); die;
			}
		}
		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		die(Yii::t('admin', 'Тимчасово вимкнено!'));
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		$get = Yii::app()->request->getParam('ajax');
		if(is_null($get))
			$this->redirect(!is_null(Yii::app()->request->getPost('returnUrl')) ? Yii::app()->request->getPost('returnUrl') : array('admin'));
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model = new Blocks('search');
		$model->unsetAttributes();  // clear any default values

		$get = Yii::app()->request->getParam('Blocks');
		if(!is_null($get))
			$model->attributes = $get;

		$this->render('admin', array(
			'model' => $model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Blocks the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = Blocks::model()->findByPk($id);
		if($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Blocks $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		$post = Yii::app()->request->getPost('ajax');

		if(!is_null($post) && $post === 'blocks-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
