<?php

class DefaultController extends BaseAdminController
{
	public function actionIndex()
	{
        $model = new ALogs('search');
        $model->unsetAttributes();  // clear any default values
        $get = Yii::app()->request->getParam('ALogs');
        if(!is_null($get))
            $model->attributes = $get;

        $this->render('index', array(
            'model' => $model,
        ));
	}


    public function actionLogin(){
        $model = new AdminLogin;

        // if it is ajax validation request
        if (Yii::app()->getRequest()->isAjaxRequest && Yii::app()->getRequest()->getParam('ajax') == 'login-from') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        $post = Yii::app()->request->getPost('AdminLogin');
        if (!is_null($post)) {
            $model->attributes = $post;
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()){
                $log = new ALogs();
                $log->log(ALogs::LOGIN);
                if(Yii::app()->user->getState('role') == 'admin') {
                    $this->redirect(array('/admin'));
                } else {
                    $this->redirect(array('/admin/pages'));
                }
            }
        }
        $this->layout = 'login';
        $this->render('login', array('model' => $model));
    }

    public function actionLogout(){
        $log = new ALogs();
        $log->log(ALogs::LOGOUT);
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->getBaseUrl(true) . '/admin');
    }

    public function actionError(){
        if(Yii::app()->user->isGuest){
            $this->layout = false;
        }
        $get = Yii::app()->request->getParam('debug');
        if(!is_null($get) && YII_DEBUG){
            CVardumper::dump(Yii::app()->errorHandler->error);
            die;
        }
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }
}