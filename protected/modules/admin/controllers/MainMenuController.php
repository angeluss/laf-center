<?php

class MainMenuController extends BaseAdminController
{

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform actions
				'users' => array('@'),
			),
			array('deny',  // deny all users
				'users' => array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		die(Yii::t('admin', 'Тимчасово вимкнено!'));
		$model = new MainMenu;

		$this->performAjaxValidation($model);
		$post = Yii::app()->request->getPost('MainMenu');
		if(!is_null($post)) {
			$model->attributes = $post;
			if($model->save())
				$this->redirect(array('index'));
		}

		$this->render('create', array(
			'model' => $model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);

		$this->performAjaxValidation($model);
		$post = Yii::app()->request->getPost('MainMenu');
		if(!is_null($post)) {
			$model->attributes = $post;
			if($model->validate()) {
				$model->save();
				$action = $model->isNewRecord ? ALogs::CREATE : ALogs::UPDATE;
				$flash = Yii::t('admin', 'Пункт "{title}" успішно змінений', array('{title}' => $model->title));
				Yii::app()->user->setFlash('success',$flash);
				$params = array();
				$params['entity'] = Yii::t('admin', 'пункт меню');
				$params['entity_id'] = $model->id;
				$params['entity_name'] = $model->title;
				$log = new ALogs();
				$log->log($action, $params);
				$this->redirect(array('index'));
			} else {
				CVarDumper::dump($model->getErrors()); die;
			}
		}

		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		die(Yii::t('admin', 'Тимчасово вимкнено!'));
		$this->loadModel($id)->delete();
		$post = Yii::app()->request->getPost('returnUrl');
		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		$get = Yii::app()->request->getParam('ajax');
		if(is_null($get))
			$this->redirect(!is_null(Yii::app()->request->getPost('returnUrl')) ? Yii::app()->request->getPost('returnUrl') : array('admin'));
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model = new MainMenu('search');
		$model->unsetAttributes();  // clear any default values
		$get = Yii::app()->request->getParam('MainMenu');
		if(!Is_null($get))
			$model->attributes = $get;

		$this->render('admin', array(
			'model' => $model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return MainMenu the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = MainMenu::model()->findByPk($id);
		if($model === null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param MainMenu $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		$post = Yii::app()->request->getPost('ajax');
		if(!is_null($post) && $post === 'main-menu-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
