<?php

class PagesController extends BaseAdminController
{
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform actions
                'users' => array('@'),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

    public function initSave($model){
        $this->performAjaxValidation($model);
        $post = Yii::app()->request->getPost('Pages');
        if(!is_null($post))
        {
            $model->attributes = $post;
            if($model->isNewRecord){
                $model->created_at = time();
            }
            $model->updated_at = time();
            $action = $model->isNewRecord ? ALogs::CREATE : ALogs::UPDATE;
            $flash = $model->isNewRecord ?
                Yii::t('admin', 'Сторінка успішно створена') :
                Yii::t('admin', 'Сторінка успішно змінена');
            if($model->validate()) {
                if ($model->save()) {
                    Yii::app()->user->setFlash('success',$flash);
                    $params = array();
                    $params['entity'] = Yii::t('admin', 'сторінку');
                    $params['entity_id'] = $model->id;
                    $params['entity_name'] = $model->title;
                    $log = new ALogs();
                    $log->log($action, $params);
                    $l = Yii::app()->request->getPost('Lang');
                    if(!is_null($l)) {
                        foreach($l as $lang => $params) {
                            $status = $params['status'];
                            $content = $params['content'];
                            $langModel = PageContents::model()->findByAttributes(array('page_id' => $model->id, 'lang' => $lang));

                            if(is_null($langModel))
                                $langModel = new PageContents();

                            $langModel->page_id = $model->id;
                            $langModel->lang = $lang;
                            $langModel->status = $status;
                            $langModel->content = $content;
                            if($langModel->validate()){
                                $langModel->save();
                            } else {
                                CVarDumper::dump($langModel->getErrors()); die;
                            }
                        }
                    }
                    $this->redirect(array('view', 'id' => $model->id));
                }
            } else {
                CVarDumper::dump($model->getErrors()); die;
            }
        }
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new Pages;

		$this->initSave($model);

		$this->render('create', array(
			'model' => $model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

        $this->initSave($model);

		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        $model = $this->loadModel($id);
        $title = $model->title;
        $model->delete();
        PageContents::model()->deleteAllByAttributes(array(
            'page_id'=>$id,
        ));
        $params = array();
        $params['entity'] = Yii::t('admin', 'сторінку');
        $params['entity_id'] = $id;
        $params['entity_name'] = $title;
        $log = new ALogs();
        $log->log(ALogs::DELETE, $params);

        if(is_null(Yii::app()->request->getParam('ajax'))) {
            Yii::app()->user->setFlash('success', 'Сторінка успішно видалена');
            $post = Yii::app()->request->getPost('returnUrl');
            $this->redirect(!is_null($post) ? $post : array('index'));
        }
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model = new Pages('search');
		$model->unsetAttributes();  // clear any default values
        $get = Yii::app()->request->getParam('Pages');
		if(!is_null($get))
			$model->attributes = $get;

		$this->render('admin', array(
			'model' => $model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Pages the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = Pages::model()->findByPk($id);
		if($model === null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Pages $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
        $post = Yii::app()->request->getPost('ajax');
		if(!is_null($post) && $post === 'pages-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
