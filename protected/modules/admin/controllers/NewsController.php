<?php

class NewsController extends BaseAdminController
{
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform actions
                'users' => array('@'),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

    /**
     * Saving action for create and update.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @param News $model to be saved
     */
    public function initSave(News $model){

        $this->performAjaxValidation($model);
        $post = Yii::app()->request->getPost('News');
        if(!is_null($post)){
            $model->attributes = $post;

            if($model->isNewRecord)
                $model->created_at = time();
            if (isset($post['updated_at']) && $post['updated_at'] != ''){
                $norm_update_date = strtotime($post['updated_at']);
                $model->updated_at = $norm_update_date;
            } else {
                $model->updated_at = time();
            }

            if (isset($post['name']['file']) && $post['name']['file'] != ''){
                $model->img = md5(time()) . $post['name']['file'];
                $model->file = CUploadedFile::getInstance($model, 'file');
            } else {
                $deletePhoto = Yii::app()->request->getPost('deletePhoto');
                if(!is_null($deletePhoto) || $model->isNewRecord)
                    $model->img = '';
            }
            $action = $model->isNewRecord ? ALogs::CREATE : ALogs::UPDATE;
            $flash = $model->isNewRecord ?
                Yii::t('admin', 'Стаття успішно створена') :
                Yii::t('admin', 'Стаття успішно змінена');
            $l = Yii::app()->request->getPost('Lang');
            if(!is_null($l))
                $model->title = $l['uk']['title'];
            if($model->validate()){
                if($model->save()){
                    if ($model->img != '') {
                        $dir = WATSON . DIRECTORY_SEPARATOR . 'uploadFiles' . DIRECTORY_SEPARATOR . 'newsImg';

                        if(!is_dir($dir)){
                            mkdir($dir, 0777);
                        }
                        $dir .= '/' . $model->id;
                        if(!is_dir($dir)){
                            mkdir($dir, 0777);
                        }
                        $img = $dir . '/' . $model->img;
                        if(!is_file($img))
                            $model->file->saveAs($img);
                    }

                    if(!is_null($l)) {
                        $model->title = $l['uk']['title'];
                        $model->short_desc = $l['uk']['short_desc'];
                        $model->content = $l['uk']['content'];
                        foreach($l as $lang => $params) {
                            $status = $params['status'];
                            $content = $params['content'];
                            $short_desc = $params['short_desc'];
                            $title = $params['title'];
                            if ($params['status'] == 0) {
                                $title = $l['uk']['title'];
                                $content = $l['uk']['content'];
                                $short_desc = $l['uk']['short_desc'];
                            }
                            $langModel = NewsLang::model()->findByAttributes(array('new_id' => $model->id, 'lang' => $lang));

                            if(is_null($langModel))
                                $langModel = new NewsLang();

                            $langModel->new_id = $model->id;
                            $langModel->lang = $lang;
                            $langModel->status = $status;
                            $langModel->content = $content;
                            $langModel->short_desc = $short_desc;
                            $langModel->title = $title;
                            if($langModel->validate()){
                                $langModel->save();
                            } else {
                                CVarDumper::dump($langModel->getErrors()); die;
                            }
                        }
                    }
                    $tags = Yii::app()->request->getPost('Tags');
                    if(!is_null($tags)) {
                        $issetTags = CHtml::listData($model->tags, 'id', 'id');
                        $newTags = array_diff($tags, $issetTags);
                        $removeTags = array_diff($issetTags, $tags);
                        foreach ($removeTags as $id){
                            $m = NewsTags::model()->findByAttributes(array('tag_id' =>(int) $id, 'new_id' => $model->id));
                            $m->delete();
                        }
                        foreach ($newTags as $id){
                            $m = new NewsTags;
                            $m->new_id = $model->id;
                            $m->tag_id = $id;
                            $m->save();
                        }
                    } else {
                        $m = NewsTags::model()->findAllByAttributes(array('new_id' => $model->id));
                        foreach ($m as $t){
                            $t->delete();
                        }
                    }

                    Yii::app()->user->setFlash('success', $flash);
                    $params = array();
                    $params['entity'] = Yii::t('admin', 'статтю');
                    $params['entity_id'] = $model->id;
                    $params['entity_name'] = $model->title;
                    $log = new ALogs();
                    $log->log($action, $params);

                    $this->redirect(array('news/view', 'id' => $model->id));
                }
            } else {
                CVarDumper::dump($model->getErrors()); die;
            }
        }
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new News;
        $model->content = 'empty';
        $modelTag = Tags::model()->findAll();
        $tags = CHtml::listData($modelTag, 'id', 'title');
        $this->initSave($model);

		$this->render('create', array(
			'model' => $model,
            'tags' => $tags,
		));
	}

    public function actionAddTagBlock(){
        $id = Yii::app()->request->getParam('id', false);
        $tag = Tags::model()->findByPk($id);
        $modelTag = Tags::model()->findAll();

        if(is_null($modelTag)){
            echo CJSON::encode(array(
                'success' => false,
            ));
            Yii::app()->end();
        }
        $tags = CHtml::listData($modelTag, 'id', 'title');
        $issettags = CHtml::listData($tag, 'id', 'title');
        $tags = array_diff_key($tags, $issettags);
        $html = $this->renderPartial('_addTag', array('tag' => $tag,), true, false);
        $html2 = $this->renderPartial('_tagsSelector', array('tags' => $tags,), true, false);
        echo CJSON::encode(array(
            'html' => $html,
            'html2' => $html2,
            'success' => true,
        ));
        Yii::app()->end();
    }

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
        $modelTag = Tags::model()->findAll();
        $tags = CHtml::listData($modelTag, 'id', 'title');
        $issettags = CHtml::listData($model->tags, 'id', 'title');
        $tags = array_diff_key($tags, $issettags);
        $this->initSave($model);
		$this->render('update', array(
			'model' => $model,
			'tags' => $tags,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        $model = $this->loadModel($id);
        $title = $model->title;
        $tags = NewsTags::model()->findAllByAttributes(array('new_id' => $model->id));
        $model->delete();
        NewsLang::model()->deleteAllByAttributes(array(
            'new_id' => $id,
        ));
        foreach($tags as $tag){
            $tag->delete();
        }
        $params = array();
        $params['entity'] = Yii::t('admin', 'статтю');
        $params['entity_id'] = $id;
        $params['entity_name'] = $title;
        $log = new ALogs();
        $log->log(ALogs::DELETE, $params);

        if(is_null(Yii::app()->request->getParam('ajax'))) {
            Yii::app()->user->setFlash('success', 'Стаття успішно видалена');
            $url = Yii::app()->request->getPost('returnUrl');
            $this->redirect(!is_null($url) ? $url : array('index'));
        }
	}

	/**
	 * Lists all models.
	 */

	public function actionIndex()
	{
		$model = new News('search');
		$model->unsetAttributes();  // clear any default values
        $get = Yii::app()->request->getParam('News');
		if(!is_null($get))
			$model->attributes = $get;

		$this->render('admin', array(
			'model' => $model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return News the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = News::model()->findByPk($id);
		if($model === null)
			throw new CHttpException(404, 'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param News $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
        $post = Yii::app()->request->getPost('ajax');
		if(!is_null($post) && $post === 'news-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
