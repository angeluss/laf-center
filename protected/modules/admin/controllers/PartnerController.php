<?php

class PartnerController extends BaseAdminController
{
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
			array('allow', // allow authenticated user to perform actions
				'users' => array('@'),
			),
			array('deny',  // deny all users
				'users' => array('*'),
			),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

	/**
	 * Saves a particular model.
	 * @param Partner $model the model to be saved
	 */
	public function initSave($model){
		$this->performAjaxValidation($model);
		$post = Yii::app()->request->getPost('Partner');
		if(!is_null($post))
		{
			$model->attributes = $post;
			$l = Yii::app()->request->getPost('Lang');
			if(!is_null($l)) {
				$model->title = $l['uk']['title'];
			} else {
				$model->title = 'NO_TITLE';
			}
			if (isset($_FILES['Partner']['name']['file']) && $_FILES['Partner']['name']['file'] != ''){
				$model->img = md5(time()) . $_FILES['Partner']['name']['file'];
				$model->file = CUploadedFile::getInstance($model,'file');
			} else {
				if(!is_null(Yii::app()->request->getPost('deletePhoto')) || $model->isNewrecord)
					$model->img = '';
			}
			$action = $model->isNewRecord ? ALogs::CREATE : ALogs::UPDATE;
			$flash = $model->isNewRecord ?
				Yii::t('admin', 'Партнер успішно створений') :
				Yii::t('admin', 'Партнер успішно змінений');
			if($model->validate()) {
				if ($model->save()) {
					if ($model->img != '') {
						$dir = WATSON . DIRECTORY_SEPARATOR . 'uploadFiles' . DIRECTORY_SEPARATOR . 'partners';

						if(!is_dir($dir)){
							mkdir($dir, 0777);
						}
						$dir .= '/' . $model->id;
						if(!is_dir($dir)){
							mkdir($dir, 0777);
						}
						$img = $dir . '/' . $model->img;
						if(!is_file($img))
							$model->file->saveAs($img);
					}
					Yii::app()->user->setFlash('success',$flash);
					$params = array();
					$params['entity'] = Yii::t('admin', 'партнера');
					$params['entity_id'] = $model->id;
					if(!is_null($l)) {
						$params['entity_name'] = $l['uk']['title'];
					} else {
						$params['entity_name'] = '';
					}
					$log = new ALogs();
					$log->log($action, $params);

                    if(!is_null($l)) {
                        foreach($l as $lang => $params) {
                            $status = $params['status'];
                            $content = $params['content'];
                            $title = $params['title'];
                            $langModel = PartnersLang::model()->findByAttributes(array('partner_id' => $model->id, 'lang' => $lang));
                            if ($params['status'] == 0){
                                $title = $l['uk']['title'];
                                $content = $l['uk']['content'];
                            }
                            if(is_null($langModel))
                                $langModel = new PartnersLang();

                            $langModel->partner_id = $model->id;
                            $langModel->lang = $lang;
                            $langModel->status = $status;
                            $langModel->desc = $content;
                            $langModel->title = $title;
                            if($langModel->validate()){
                                $langModel->save();
                            } else {
                                CVarDumper::dump($langModel->getErrors()); die;
                            }
                        }
                    }

					$this->redirect(array('view', 'id' => $model->id));
				}
			} else {
				CVarDumper::dump($model->getErrors()); die;
			}
		}
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new Partner;

		$this->initSave($model);

		$this->render('create', array(
			'model' => $model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		$this->initSave($model);

		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        $model = $this->loadModel($id);
        $title = $model->title;
        $model->delete();
        PartnersLang::model()->deleteAllByAttributes(array(
            'partner_id'=>$id,
        ));
        $params = array();
        $params['entity'] = Yii::t('admin', 'партнера');
        $params['entity_id'] = $id;
        $params['entity_name'] = $title;
        $log = new ALogs();
        $log->log(ALogs::DELETE, $params);

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(is_null(Yii::app()->request->getParam('ajax'))){
            Yii::app()->user->setFlash('success', 'Партнер успішно видалений');
			$post = Yii::app()->request->getPost('returnUrl');
            $this->redirect(!is_null($post) ? $post : array('index'));
        }
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model = new Partner('search');
		$model->unsetAttributes();  // clear any default values
        $get = Yii::app()->request->getParam('Partner');
		if(!is_null($get))
			$model->attributes = $get;

		$this->render('admin', array(
			'model' => $model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Partner the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = Partner::model()->findByPk($id);
		if($model === null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Partner $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
        $post = Yii::app()->request->getPost('ajax');
		if(!is_null($post) && $post === 'partner-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
