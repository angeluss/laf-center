<?php

class OfertaController extends BaseAdminController
{
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform actions
                'users' => array('@'),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
        die(Yii::t('admin', 'Тимчасово вимкнено!'));
		$model = new Oferta;

		$this->performAjaxValidation($model);
		$post = Yii::app()->request->getPost('Oferta');
		if(!is_null($post))
		{
			$model->attributes = $post;
            $model->created_at = time();
            $model->updated_at = time();
			if($model->validate()) {
                if ($model->save()) {
                    $this->redirect(array('index'));
                }
            } else {
                CVarDumper::dump($model->getErrors()); die;
            }

		}

		$this->render('create', array(
			'model' => $model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		$this->performAjaxValidation($model);
		$post = Yii::app()->request->getPost('Oferta');
		if(!is_null($post))
		{
			$model->attributes = $post;
			if($model->validate()) {
                if ($model->save())
                    $this->redirect(array('view', 'id' => $model->id));
            } else {
                CVarDumper::dump($model->getErrors());
            }
		}

		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        die(Yii::t('admin', 'Тимчасово вимкнено!'));
        $this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(is_null(Yii::app()->request->getParam('ajax')))
			$this->redirect(!is_null(Yii::app()->request->getPost('returnUrl')) ? Yii::app()->request->getPost('returnUrl') : array('admin'));
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model = new Oferta('search');
		$model->unsetAttributes();  // clear any default values
		$get = Yii::app()->request->getParam('Oferta');
		if(!is_null($get))
			$model->attributes = $get;

		$this->render('admin' ,array(
			'model' => $model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Oferta the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = Oferta::model()->findByPk($id);
		if($model === null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Oferta $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		$post = Yii::app()->request->getPost('ajax');
		if(!is_null($post) && $post === 'oferta-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
