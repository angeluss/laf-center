<?php

class ConsultationController extends BaseAdminController
{
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
        return array(
            array('allow', // allow authenticated user to perform actions
                'users' => array('@'),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
    public function actionView($id)
    {
        $this->render('view',array(
            'model' => $this->loadModel($id),
        ));
    }

    public function initSave($model){
        $this->performAjaxValidation($model);
        $con_law_branches = new ConLawBranch;
        $post = Yii::app()->request->getPost('Consultation');

        if(!is_null($post)) {
            $lang = Yii::app()->request->getPost('Lang');
            $model->attributes = $post;
            $model->content = $lang['uk']['content'];
            $model->title = $lang['uk']['title'];
            $action = $model->isNewRecord ? ALogs::CREATE : ALogs::UPDATE;
            $flash = $model->isNewRecord ?
                Yii::t('admin', 'Консультація успішно створена') :
                Yii::t('admin', 'Консультація успішно змінена');
            if($model->validate()) {
                if ($model->save()) {
                    $con_law_branches->branch_id = $model->type_consult;
                    $con_law_branches->con_id = $model->id;
                    $con_law_branches->save();
                    Yii::app()->user->setFlash('success', $flash);
                    $params = array();
                    $params['entity'] = Yii::t('admin', 'консультацію');
                    $params['entity_id'] = $model->id;
                    $params['entity_name'] = $model->title;
                    $log = new ALogs();
                    $log->log($action, $params);
                    if(!is_null($lang)) {
                        foreach($lang as $l => $params) {
                            $status = $params['status'];
                            $content = $params['content'];
                            $title = $params['title'];
                            if ($params['status'] == 0) {
                                $title = $lang['uk']['title'];
                                $content = $lang['uk']['content'];
                            }
                            $langModel = ConsultationLang::model()->findByAttributes(array('con_id' => $model->id, 'lang' => $l));

                            if(is_null($langModel))
                                $langModel = new ConsultationLang();

                            $langModel->con_id = $model->id;
                            $langModel->lang = $l;
                            $langModel->status = $status;
                            $langModel->content = $content;
                            $langModel->title = $title;
                            if($langModel->validate()){
                                $langModel->save();
                            } else {
                                CVarDumper::dump($langModel->getErrors()); die;
                            }
                        }
                    }

                    $this->redirect(array('view', 'id' => $model->id));
                }
            } else {
                CVarDumper::dump($model->getErrors()); die;
            }
        }
    }


	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new Consultation;

        $this->initSave($model);
		$this->render('create', array(
			'model' => $model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

        $this->initSave($model);
		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        $model = $this->loadModel($id);
        $title = $model->title;
        $model->delete();
        ConsultationLang::model()->deleteAllByAttributes(array(
            'con_id'=>$id,
        ));

        $params = array();
        $params['entity'] = Yii::t('admin', 'консультацію');
        $params['entity_id'] = $id;
        $params['entity_name'] = $title;
        $log = new ALogs();
        $log->log(ALogs::DELETE, $params);

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        $get = Yii::app()->request->getParam('ajax');
		if(is_null($get)) {
            Yii::app()->user->setFlash('success', 'Консультація успішно видалена');
            $post = Yii::app()->request->getPost('Consultation');
            $this->redirect(!is_null($post) ? $post : array('admin'));
        }
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        $model = new Consultation('search');
        $model->unsetAttributes();  // clear any default values
        $get = Yii::app()->request->getParam('Consultation');
        if(!is_null($get))
            $model->attributes = $get;

        $this->render('admin', array(
            'model' => $model,
        ));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Consultation the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = Consultation::model()->findByPk($id);
		if($model === null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Consultation $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
        $post = Yii::app()->request->getPost('ajax');
		if(!is_null($post) && $post === 'consultation-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
