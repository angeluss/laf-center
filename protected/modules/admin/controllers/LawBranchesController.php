<?php

class LawBranchesController extends BaseAdminController
{
	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	public function accessRules()
	{
		return array(
            array('allow', // allow authenticated user to perform actions
                'users' => array('@'),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
		);
	}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

    public function initSave($model){
        $this->performAjaxValidation($model);
        $post = Yii::app()->request->getPost('Lang');
        if(!is_null($post))
        {
            $model->attributes = $post['uk'];
            $model->short_desc = $post['uk']['short_desc'];
            $model->status = 1;
            $action = $model->isNewRecord ? ALogs::CREATE : ALogs::UPDATE;
            $flash = $model->isNewRecord ?
                Yii::t('admin', 'Галузь права успішно створена') :
                Yii::t('admin', 'Галузь права успішно змінена');
            if($model->validate()) {
                if ($model->save()) {
                    Yii::app()->user->setFlash('success',$flash);
                    $params = array();
                    $params['entity'] = Yii::t('admin', 'галузь права');
                    $params['entity_id'] = $model->id;
                    $params['entity_name'] = $post['uk']['title'];
                    $log = new ALogs();
                    $log->log($action, $params);
                        foreach($post as $lang => $params) {
                            $status = $params['status'];
                            $content = $params['short_desc'];
                            $title = $params['title'];
                            if ($params['status'] == 0) {
                                $title = $post['uk']['title'];
                                $content = $post['uk']['short_desc'];
                            }
                            $langModel = LawBranchesLang::model()->findByAttributes(array('branch_id' => $model->id, 'lang' => $lang));

                            if(is_null($langModel))
                                $langModel = new LawBranchesLang();

                            $langModel->branch_id = $model->id;
                            $langModel->lang = $lang;
                            $langModel->status = $status;
                            $langModel->short_desc = $content;
                            $langModel->title = $title;
                            if($langModel->validate()){
                                $langModel->save();
                            } else {
                                CVarDumper::dump($langModel->getErrors()); die;
                            }
                        }
                    $this->redirect(array('view', 'id' => $model->id));
                }
            } else {
                CVarDumper::dump($model->getErrors()); die;
            }
        }
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new LawBranches;

        $this->initSave($model);
		$this->render('create', array(
			'model' => $model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

        $this->initSave($model);
		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        $model = $this->loadModel($id);

        $title = $model->title;
        $model->delete();
        LawBranchesLang::model()->deleteAllByAttributes(array(
            'branch_id'=>$id,
        ));
        $params = array();
        $params['entity'] = Yii::t('admin', 'галузь права');
        $params['entity_id'] = $id;
        $params['entity_name'] = $title;
        $log = new ALogs();
        $log->log(ALogs::DELETE, $params);

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(is_null(Yii::app()->request->getParam('ajax')))
			$this->redirect(!is_null(Yii::app()->request->getPost('returnUrl')) ? Yii::app()->request->getPost('returnUrl') : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        $model = new LawBranches('search');
        $model->unsetAttributes();  // clear any default values
        $get = Yii::app()->request->getParam('Consultation');
        if(!is_null($get))
            $model->attributes = $get;

        $this->render('admin', array(
            'model' => $model,
        ));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return LawBranches the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = LawBranches::model()->findByPk($id);
		if($model === null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param LawBranches $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
        $post = Yii::app()->request->getPost('ajax');
		if(!is_null($post) && $post === 'law-branches-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
