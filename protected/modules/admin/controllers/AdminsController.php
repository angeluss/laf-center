<?php

class AdminsController extends BaseAdminController
{
    /**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

    /**
     * Saving action for create and update.
     * If creation is successful, the browser will be redirected to the 'view' page.
     */
    public function initSave($model){

        $this->performAjaxValidation($model);
        $post = Yii::app()->request->getPost('Admin');
        if(!is_null($post)){
            $model->attributes = $post;

            if($model->isNewrecord) {
                $model->created_at = time();
                $model->password = md5($model->pwd);
            } else {
                if($model->password !== $model->pwd){
                    $model->password = md5($model->pwd);
                }
            }
            $model->updated_at = time();
            $action = $model->isNewRecord ? ALogs::CREATE : ALogs::UPDATE;
            $flash = $model->isNewRecord ? 'Адміністратор успішно створений' : 'Адміністратор успішно змінений';
            if($model->validate()){
                if($model->save()){

                    Yii::app()->user->setFlash('success',$flash);
                    $params = array();
                    $params['entity'] = Yii::t('admin', 'адміністратора');
                    $params['entity_id'] = $model->id;
                    $params['entity_name'] = $model->login;
                    $log = new ALogs();
                    $log->log($action, $params);

                    $this->redirect(array('admins/view', 'id' => $model->id));
                }
            }
        }
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new Admin;

        $this->initSave($model);

		$this->render('create', array(
			'model' => $model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model=$this->loadModel($id);
        $model->pwd = $model->password;
        $this->initSave($model);

		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$model = $this->loadModel($id);
        if(false === $this->checkIsNotLast($model)) {
            $post = Yii::app()->request->getPost('returnUrl');
            Yii::app()->user->setFlash('error', 'Ви не можете видалити останнього адміністратора');
            $this->redirect(!is_null($post) ? $post : array('index'));
        }
        $title = $model->login;
        $model->delete();

        $params = array();
        $params['entity'] = Yii::t('admin', 'адміністратора');
        $params['entity_id'] = $id;
        $params['entity_name'] = $title;
        $log = new ALogs();
        $log->log(ALogs::DELETE, $params);

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        $get = Yii::app()->request->getParam('ajax');
		if(is_null($get)) {
            Yii::app()->user->setFlash('success', 'Адміністратор успішно видалений');
            $post = Yii::app()->request->getPost('returnUrl');
            $this->redirect(!is_null($post) ? $post : array('index'));
        }

	}

    public function checkIsNotLast($model) {
        if ($model->role != Admin::ADMIN){
            return true;
        } else {
            $admins = Admin::model()->countByAttributes(array('role' => Admin::ADMIN));
            if($admins > 1)
                return true;
        }
        return false;
    }

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
        $model = new Admin('search');
        $model->unsetAttributes();
        $get = Yii::app()->request->getParam('Admin');
        if(!is_null($get))
            $model->attributes = $get;

        $this->render('admin', array(
            'model' => $model,
        ));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Admin the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = Admin::model()->findByPk($id);
		if($model === null)
			throw new CHttpException(404, 'Сторінки не існує');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Admin $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
        $post = Yii::app()->request->getPost('ajax');
		if(!is_null($post) && $post === 'admin-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
