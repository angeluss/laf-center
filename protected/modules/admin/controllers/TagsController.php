<?php

class TagsController extends BaseAdminController
{
    /**
     * Specifies the access control rules.
     * This method is used by the 'accessControl' filter.
     * @return array access control rules
     */
    public function accessRules()
    {
        return array(
            array('allow', // allow authenticated user to perform actions
                'users' => array('@'),
            ),
            array('deny',  // deny all users
                'users' => array('*'),
            ),
        );
    }

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

    /**
     * Saving action for create and update.
     * If creation is successful, the browser will be redirected to the 'index' page.
     * @param Tags $model to be saved
     */
    public function initSave(Tags $model){

        $this->performAjaxValidation($model);
        $post = Yii::app()->request->getPost('Tags');
        if(!is_null($post)){
            $model->attributes = $post;

            $action = $model->isNewRecord ? ALogs::CREATE : ALogs::UPDATE;
            $flash = $model->isNewRecord ?
                Yii::t('admin', 'Тег "{title}" успішно створений', array('{title}' => $model->title)) :
                Yii::t('admin', 'Тег "{title}" успішно змінений', array('{title}' => $model->title));
            if($model->validate()){
                if($model->save()){
                    Yii::app()->user->setFlash('success',$flash);
                    $params = array();
                    $params['entity'] = Yii::t('admin', 'тег');
                    $params['entity_id'] = $model->id;
                    $params['entity_name'] = $model->title;
                    $log = new ALogs();
                    $log->log($action, $params);

                    $this->redirect(array('tags/index', 'id' => $model->id));
                }
            }
        }
    }

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new Tags;

        $this->initSave($model);

		$this->render('create', array(
			'model' => $model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

        $this->initSave($model);

		$this->render('update', array(
			'model' => $model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
        $model = $this->loadModel($id);
        $title = $model->title;
        $tags = NewsTags::model()->findAllByAttributes(array('tag_id'=>$model->id));
        $model->delete();
        foreach($tags as $tag){
            $tag->delete();
        }
        $params = array();
        $params['entity'] = Yii::t('admin', 'тег');
        $params['entity_id'] = $id;
        $params['entity_name'] = $title;
        $log = new ALogs();
        $log->log(ALogs::DELETE, $params);

        if(is_null(Yii::app()->request->getParam('ajax'))) {
            Yii::app()->user->setFlash('success', 'Тег успішно видалений');
            $post = Yii::app()->request->getPost('returnUrl');
            $this->redirect(!is_null($post) ? $post : array('index'));
        }
	}


	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model = new Tags('search');
		$model->unsetAttributes();  // clear any default values
        $get = Yii::app()->request->getParam('Tags');
		if(is_null($get))
			$model->attributes = $get;

		$this->render('admin', array(
			'model' => $model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Tags the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = Tags::model()->findByPk($id);
		if($model === null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Tags $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
        $post = Yii::app()->request->getPost('ajax');
		if(!is_null($post) && $post === 'tags-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
