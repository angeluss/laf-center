<?php

class RequisitesController extends BaseAdminController
{
	public function actionIndex()
	{
		$model = Requisites::model()->find();

		$this->performAjaxValidation($model);
		$post = Yii::app()->request->getPost('Requisites');
		if(!is_null($post)) {
			$model->attributes = $post;
			if($model->save())
				Yii::app()->user->setFlash('success', "Реквізити успішно змінено");
		}

		$this->render('update', array(
			'model' => $model,
		));
	}

	protected function performAjaxValidation($model)
	{
		$post = Yii::app()->request->getPost('ajax');
		if(!is_null($post) && $post === 'requisites-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
