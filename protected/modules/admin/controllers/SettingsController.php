<?php

class SettingsController extends BaseAdminController
{
	public function actionIndex()
	{
		$model = Settings::model()->find();

		$this->performAjaxValidation($model);
		$post = Yii::app()->request->getPost('Settings');
		if(!is_null($post)) {
			$model->attributes = $post;
			$model->uplFile = CUploadedFile::getInstance($model, 'uplFile');
			if($model->save()) {
				$model->uplFile->saveAs(WATSON . '/image/favicon' . '/favicon.ico');
                Yii::app()->user->setFlash('success',"Налаштування успішно змінено");
            }
		}

		$this->render('update', array(
			'model' => $model,
		));
	}

	protected function performAjaxValidation($model)
	{
		$post = Yii::app()->request->getPost('ajax');
		if(!is_null($post) && $post === 'settings-form') {
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
