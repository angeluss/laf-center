<?php

class Admin_panelModule extends CWebModule
{
    public $urlRules = array(

    );

	public function init()
	{
		// this method is called when the module is being created
		// you may place code here to customize the module or the application

		// import the module-level models and components
		$this->setImport(array(
			'admin_panel.models.*',
			'admin.models.*',
			'admin_panel.components.*',
			'admin.components.*',
            'application.components.*',
            'application.models.*',
		));

        Yii::app()->components = array(
            'user' => array(
                'loginUrl'=>array('admin_panel/default/login'),
                'allowAutoLogin'=>true,
            )
        );
	}

	public function beforeControllerAction($controller, $action)
	{
		if(parent::beforeControllerAction($controller, $action))
		{
			// this method is called before any module controller action is performed
			// you may place customized code here
			Yii::app()->errorHandler->errorAction='admin_panel/default/error';
			return true;
		}
		else
			return false;
	}
}
