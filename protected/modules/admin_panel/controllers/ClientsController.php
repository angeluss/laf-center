<?php

class ClientsController extends AdminPanelController
{
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
        $model = $this->loadModel($id);
        if($model->district == 1) {
            $area = array(
                'name' => 'area',
                'value' => Country::getHDistrict($model->area),
            );
        } else {
            $area = 'city';
        }
		$this->render('view', array(
			'model' => $model,
			'area' => $area,
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
        die(Yii::t('admin', 'Тимчасово вимкнено!'));
		$model = new Clients;

		$this->performAjaxValidation($model);

        $this->initSave($model);

        $vuses = Vuz::getVuses();
        $countries = Country::getCountriesForAP();

		$this->render('create',array(
			'model' => $model,
            'vuses' => $vuses,
            'countries' => $countries,
		));
	}

    public function initSave($model){
        $post = Yii::app()->request->getPost('Clients');
        if(!is_null($post))
        {
            $model->attributes = $post;
            if($model->isNewrecord) {
                $model->created_at = time();
                $model->password = md5($model->pwd);
            } else {
                if($model->password !== $model->pwd){
                    $model->password = md5($model->pwd);
                }
            }
            $model->updated_at = time();
            if (isset($_FILES['Clients']['name']['file']) && $_FILES['Clients']['name']['file'] != ''){
                $model->photo = md5(time()) . $_FILES['Clients']['name']['file'];
                $model->file = CUploadedFile::getInstance($model,'file');
            } else {
                if(!is_null(Yii::app()->request->getPost('deletePhoto')) || $model->isNewrecord)
                $model->photo = '';
            }
            $action = $model->isNewRecord ? ApLogs::CREATE : ApLogs::UPDATE;
            if($model->validate()){
                if($model->save()) {
                    if ($model->photo != '') {
                        $dir = WATSON . '/uploadFiles/clientsPhoto';

                        if(!is_dir($dir)){
                            mkdir($dir, 0777);
                        }
                        $dir .= '/' . $model->id;
                        if(!is_dir($dir)){
                            mkdir($dir, 0777);
                        }
                        $img = $dir . '/' . $model->photo;
                        if(!is_file($img))
                            $model->file->saveAs($img);
                    }

                    $params = array();
                    $params['entity'] = Yii::t('admin_panel', 'клиента');
                    $params['entity_id'] = $model->id;
                    $params['entity_name'] = $model->fullname;
                    $log = new ApLogs();
                    $log->log($action, $params);


                    $this->redirect(array('view', 'id' => $model->id));
                }
            }else {
                CVarDumper::dump($model->getErrors()); //die;
            }
        }
    }


	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);
        $model->pwd = $model->password;
        $model->file = $model->photo;

		$this->performAjaxValidation($model);
        $this->initSave($model);
        $vuses = Vuz::getVuses();
        $countries = Country::getCountriesForAP();

		$this->render('update', array(
			'model' => $model,
            'vuses' => $vuses,
			'countries' => $countries,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{

        $model = $this->loadModel($id);
        $model->status = Clients::NOT_ACTIVE_STATUS;
        if($model->save()) {

        }else {
            CVarDumper::dump($model->getErrors()); die;
        }
        $title = $model->fullname;

        $params = array();
        $params['entity'] = Yii::t('admin_panel', 'клиента');
        $params['entity_id'] = $id;
        $params['entity_name'] = $title;
        $log = new ApLogs();
        $log->log(ApLogs::DELETE, $params);

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(is_null(Yii::app()->request->getParam('ajax'))) {
            $post = Yii::app()->request->getPost('returnUrl');
            $this->redirect(!is_null($post) ? $post : array('index'));
        }
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
        $model = new Clients('search');

        $model->unsetAttributes();  // clear any default values
        $get = Yii::app()->request->getParam('Clients');
		if(!is_null($get)) {
            $model->attributes = $get;
            CVarDumper::dump($model);
        }

		$this->render('index', array(
			'model' => $model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Clients the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = Clients::model()->findByPk($id);
		if($model === null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Clients $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
        $post = Yii::app()->request->getPost('ajax');
		if(!is_null($post) && $post === 'clients-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
