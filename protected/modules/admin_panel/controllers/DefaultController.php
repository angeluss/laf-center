<?php

class DefaultController extends AdminPanelController
{
	public function actionIndex()
	{
        $model = new ApLogs('search');
        $model->unsetAttributes();  // clear any default values
        $get = Yii::app()->request->getParam('ApLogs');
        if(is_null($get))
            $model->attributes = $get;

        $this->render('index', array(
            'model' => $model,
        ));
	}

    public function actionLogin(){
        $model = new AdminPanelLogin;

        // if it is ajax validation request
        if (Yii::app()->getRequest()->isAjaxRequest && Yii::app()->getRequest()->getParam('ajax') == 'login-from') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }

        // collect user input data
        $post = Yii::app()->request->getPost('AdminPanelLogin');
        if (!is_null($post)) {
            $model->attributes = $post;
            // validate user input and redirect to the previous page if valid
            if ($model->validate() && $model->login()){
                $log = new ApLogs();
                $log->log(ApLogs::LOGIN);
                $this->redirect(array('/admin_panel'));
            }
        }
        $this->layout = 'login';
        $this->render('login', array('model' => $model));
    }

    public function actionLogout(){

        $log = new ApLogs();
        $log->log(ApLogs::LOGOUT);
        Yii::app()->user->logout();
        $this->redirect(Yii::app()->getBaseUrl(true) . '/admin_panel');
    }

    public function actionError(){
        if(Yii::app()->user->isGuest){
            $this->layout = false;
        }
        if(!is_null(Yii::app()->request->getParam('debug')) && YII_DEBUG){
            CVardumper::dump(Yii::app()->errorHandler->error);
            die;
        }
        if ($error = Yii::app()->errorHandler->error) {
            if (Yii::app()->request->isAjaxRequest)
                echo $error['message'];
            else
                $this->render('error', $error);
        }
    }
}