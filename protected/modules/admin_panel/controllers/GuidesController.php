<?php

class GuidesController extends AdminPanelController
{
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view', array(
			'model' => $this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$model = new Guides;

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		$post = Yii::app()->request->getPost('Guides');
		if(!is_null($post))
		{
			$model->attributes = $post;
            $model->created_at = time();
            $model->updated_at = time();

			if($model->validate()) {
                if ($model->save()) {

                    $params = array();
                    $params['entity'] = Yii::t('admin_panel', 'гайд');
                    $params['entity_id'] = $model->id;
                    $params['entity_name'] = $model->title;
                    $log = new ApLogs();
                    $log->log(ApLogs::CREATE, $params);

                    $this->redirect(array('view', 'id' => $model->id));
                }
            } else {
                CVarDumper::dump($model->getErrors());
            }
		}

		$this->render('create', array(
			'model' => $model,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$model = $this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
		$post = Yii::app()->request->getPost('Guides');
		if(!Is_null($post))
		{
			$model->attributes = $post;
            $model->updated_at = time();
            if($model->validate()) {
                if($model->save()) {

                    $params = array();
                    $params['entity'] = Yii::t('admin_panel', 'гайд');
                    $params['entity_id'] = $model->id;
                    $params['entity_name'] = $model->title;
                    $log = new ApLogs();
                    $log->log(ApLogs::UPDATE, $params);

                    $this->redirect(array('index', 'id' => $model->id));
                }
            } else {
                CVarDumper::dump($model->getErrors());
            }
		}

		$this->render('update',array(
			'model'=>$model,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
//		$this->loadModel($id)->delete();

        $model = $this->loadModel($id);
        $title = $model->title;
        $model->delete();

        $params = array();
        $params['entity'] = Yii::t('admin_panel', 'гайд');
        $params['entity_id'] = $id;
        $params['entity_name'] = $title;
        $log = new ApLogs();
        $log->log(ApLogs::DELETE, $params);

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(is_null(Yii::app()->request->getParam('Consultation'))) {
            $post = Yii::app()->request->getPost('returnUrl');
            $this->redirect(!is_null($post) ? $post : array('index'));
        }
	}
	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model = new Guides('search');
		$model->unsetAttributes();  // clear any default values
        $get = Yii::app()->request->getParam('Guides');
		if(!is_null($get))
			$model->attributes = $get;

		$this->render('index', array(
			'model' => $model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Guides the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = Guides::model()->findByPk($id);
		if($model === null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Guides $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
        $post = Yii::app()->request->getPost('ajax');
		if(!is_null($post) && $post === 'guides-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
