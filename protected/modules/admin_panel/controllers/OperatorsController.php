<?php

class OperatorsController extends AdminPanelController
{
    public function actionIndex()
    {
        $operators = new Operators('search');
        $operators->unsetAttributes();  // clear any default values
        $get = Yii::app()->request->getParam('Operators');
        if(!is_null($get)){
            $operators->attributes = $get;
        }

        $this->render('index', array('operators' => $operators));
    }

    public function actionCreate() {
        $operator = new Operators;

        $this->initSave($operator);

        $operator->type = 2;
        $this->render('create', array('model' => $operator));
    }

    public function actionUpdate($id) {
        $operator = $this->loadModel($id);
        $operator->pwd = $operator->password;

        $this->initSave($operator);

        $this->render('update', array('model' => $operator));
    }

    public function initSave($operator){
        /* ajax validation */
        if (Yii::app()->getRequest()->isAjaxRequest && Yii::app()->getRequest()->getParam('ajax') == 'operators-form') {
            echo CActiveForm::validate($operator);
            Yii::app()->end();
        }
        $post = Yii::app()->request->getPost('Operators');
        if(!is_null($post)){
            $operator->attributes = $post;

            if($operator->isNewrecord) {
                $operator->created_at = time();
                $operator->password = md5($operator->pwd);
            } else {
                if($operator->password !== $operator->pwd){
                    $operator->password = md5($operator->pwd);
                }
            }
            $operator->updated_at = time();
            $action = $operator->isNewRecord ? ApLogs::CREATE : ApLogs::UPDATE;
            if(isset($_FILES['Operators']['name']['uplFile']) && $_FILES['Operators']['name']['uplFile'] !== ''){
                $operator->avatar = $_FILES['Operators']['name']['uplFile'];
                $operator->uplFile = CUploadedFile::getInstance($operator, 'uplFile');
            }
            if($operator->validate()){
                if($operator->save()){
                    $flash = $operator->isNewRecord ? 'Оператор успешно создан' : 'Оператор успешно изменен';
                    if (isset($_FILES['Operators']['name']['uplFile']) && $_FILES['Operators']['name']['uplFile'] !== ''){
                        $dir = WATSON . DIRECTORY_SEPARATOR . 'uploadFiles' . DIRECTORY_SEPARATOR . 'operators';

                        if(!is_dir($dir)){
                            mkdir($dir, 0777);
                        }
                        $dir .= '/' . $operator->id;
                        if(!is_dir($dir)){
                            mkdir($dir, 0777);
                        }
                        $dir .= '/' . $_FILES['Operators']['name']['uplFile'];

                        if(!is_file($dir))
                            $operator->uplFile->saveAs($dir);
                    }
                    Yii::app()->user->setFlash('success',$flash);
                    $params = array();
                    $params['entity'] = Yii::t('admin_panel', 'оператора');
                    $params['entity_id'] = $operator->id;
                    $params['entity_name'] = $operator->login;
                    $log = new ApLogs();
                    $log->log($action, $params);

                    $this->redirect(array('operators/view', 'id' => $operator->id));
                }
            }
        }
    }

    public function actionDelete($id) {
        $model = $this->loadModel($id);
        $title = $model->login;
        $model->delete();

        $params = array();
        $params['entity'] = Yii::t('admin_panel', 'оператора');
        $params['entity_id'] = $id;
        $params['entity_name'] = $title;
        $log = new ApLogs();
        $log->log(ApLogs::DELETE, $params);

        // if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
        if(is_null(Yii::app()->request->getParam('ajax'))) {
            $post = Yii::app()->request->getPost('returnUrl');
            $this->redirect(!is_null($post) ? $post : array('index'));
        }
    }

    public function actionView($id) {
        $operator = Operators::model()->findByPk($id);
        $this->render('view', array('model' => $operator));
    }

    /*
     * Returns the data model based on the primary key given in the GET variable.
     * If the data model is not found, an HTTP exception will be raised.
     * @param integer the ID of the model to be loaded
     */
    public function loadModel($id) {
        $model = Operators::model()->findByPk((int) $id);
        if ($model === null)
            throw new CHttpException(404, 'The requested page does not exist.');
        return $model;
    }

    protected function performAjaxValidation($model) {
        $post = Yii::app()->request->getPost('ajax');
        if (!is_null($post) && $post === 'operators-form') {
            echo CActiveForm::validate($model);
            Yii::app()->end();
        }
    }

}