<?php

class ChatController extends AdminPanelController
{
	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{

		$this->render('view', array(
			'model' => $this->loadModel($id),
            'messages' => Message::model()->findAllByAttributes(array('chat_id' => $id)),
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(is_null(Yii::app()->request->getParam('ajax'))) {
			$post = Yii::app()->request->getPost('returnUrl');
			$this->redirect(!is_null($post) ? $post : array('index'));
		}
	}

	/**
	 * Manages all models.
	 */
	public function actionIndex()
	{
		$model = new Chat('search');
		$model->unsetAttributes();  // clear any default values
		$get = Yii::app()->request->getParam('Chat');
		if(is_null($get))
			$model->attributes = $get;

		$this->render('admin',array(
			'model' => $model,
		));
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Vuz the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model = Chat::model()->findByPk($id);
		if($model === null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Vuz $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		$post = Yii::app()->request->getPost('ajax');
		if(!is_null($post) && $post === 'chat-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
