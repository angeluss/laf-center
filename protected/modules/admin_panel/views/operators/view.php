<div id="wrap">
    <div id="page-heading">
        <h1><?php echo Yii::t('Operators', 'Подробный просмотр данных оператора #') . $model->id ?></h1>
    </div>

    <div class="container">
        <?php if(Yii::app()->user->hasFlash('success')): ?>
            <div class="flash_msg">
                <?php echo Yii::app()->user->getFlash('success'); ?>
            </div>
        <script>
            $(document).ready(function(){
                setTimeout(function(){
                    $('.flash_msg').fadeOut('slow');
                }, 3000)
            });
        </script>
        <?php endif; ?>
        <div class="panel panel-midnightblue">
            <div class="panel-heading ">
                <div class="options">
                    <a href="<?php echo Yii::app()->createUrl("/admin_panel/operators/index") ?>" class="panel-collapse">
                        <?php echo Yii::t('admin_panel', 'Список операторов'); ?>
                    </a>

                </div>
            </div>
            <?php $this->widget('zii.widgets.CDetailView', array(
                'data'=>$model,
                'attributes'=>array(
                    'id',
                    'login',
                    'email',
                    array(
                        'name'=>'type',
                        'type'=>'raw',
                        'value'=>Operators::getTypes($model->type),
                    ),
                    array(
                        'name'=>'created_at',
                        'value'=> Yii::app()->dateFormatter->formatDateTime($model->created_at, 'long','long'),
                    ),
                    array(
                        'name'=>'updated_at',
                        'value'=> Yii::app()->dateFormatter->formatDateTime($model->updated_at, 'long','long'),
                    ),
                    array(
                        'name'=>'avatar',
                        'type' => 'raw',
                        'value'=> $model->getAvatar('max-width: 140px; max-height: 140px;'),
                    ),
                ),
            ));
            ?>
            <a href="<?php echo Yii::app()->createUrl("/admin_panel/operators/update", array('id' => $model->id)) ?>" class="btn btn-primary">
                <?php echo Yii::t('admin_panel', 'Редактировать'); ?>
            </a>
            <a href="<?php echo Yii::app()->createUrl("/admin_panel/operators/delete", array('id' => $model->id)) ?>" class="btn btn-danger delete">
                <?php echo Yii::t('admin_panel', 'Удалить'); ?>
            </a>
        </div>
    </div>
</div>
