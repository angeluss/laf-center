<div id="wrap">
    <div id="page-heading">
        <h1><?php echo Yii::t('admin_panel', 'Добавить оператора') ?></h1>
    </div>

    <div class="container">
        <div class="panel panel-midnightblue">
            <div class="panel-heading ">
                <div class="options">
                    <a href="/admin_panel/operators/index" class="panel-collapse">
                        <?php echo Yii::t('admin_panel', 'Список операторов'); ?>
                    </a>
                </div>
            </div>
            <div class="container">
                <div class="panel panel-midnightblue">
                    <div class="panel-heading">
                    </div>
                    <div class="panel-body collapse in">
                        <?php
                        $form = $this->beginWidget('CActiveForm', array(
                            'id' => 'operators-form',
                            'htmlOptions' => array(
                                'class' => 'form-horizontal row-border',
                                'enctype'=>"multipart/form-data",
                                ),
                            'enableAjaxValidation' => true,
                            'clientOptions' => array(
                                'validateOnSubmit' => true,
                                'validateOnChange' => true,
                                'errorCssClass' => 'has-error',
                                'afterValidate' => 'js:function(form, data, hasError) {
								form.find("input").parents(".form-group").removeClass("has-error");
								form.find(".validation-icon").show();
								if(hasError) {
									for(var i in data) {
										$("#"+i).parents(".form-group").addClass("has-error");
										$("#"+i).next(".validation-icon").show();
									}
									return false;
								}
								else {
									return true;
								}
							}',
                                'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
								if(hasError){
									$("#"+attribute.id).parents(".form-group").addClass("has-error");
									$("#"+attribute.id).next(".validation-icon").show();
								} else {
									$("#"+attribute.id).parents(".form-group").removeClass("has-error");
									$("#"+attribute.id).next(".validation-icon").show();
								}
							}'
                            ),
                        ));
                        ?>

                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'login', array('class' => 'col-sm-3 control-label')); ?>
                            <div class="col-sm-6">
                                <?php echo $form->textField($model, 'login', array('maxlength' => 18, 'class' => 'form-control input-user-login')); ?>
                            </div>
                            <div class="col-md-3">
                                <div class="help-block">
                                    <?php echo $form->error($model, 'login'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'pwd', array('class' => 'col-sm-3 control-label')); ?>
                            <div class="col-sm-6">
                                <?php echo $form->passwordField($model, 'pwd', array('maxlength' => 18, 'class' => 'form-control input-user-pwd')); ?>
                            </div>
                            <div class="col-md-3">
                                <div class="help-block">
                                    <?php echo $form->error($model, 'pwd'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'email', array('class' => 'col-sm-3 control-label')); ?>
                            <div class="col-sm-6">
                                <?php echo $form->textField($model, 'email', array('class' => 'form-control input-user-email')); ?>
                            </div>
                            <div class="col-md-3">
                                <div class="help-block">
                                    <?php echo $form->error($model, 'email'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'uplFile', array('class' => 'col-sm-3 control-label')); ?>
                            <div class="col-sm-6">
                                <?php echo $form->fileField($model, 'uplFile', array('class' => 'form-control input-uplFile')); ?>
                            </div>
                            <div class="col-md-3">
                                <div class="help-block">
                                    <?php echo $form->error($model, 'uplFile'); ?>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <?php echo $form->labelEx($model, 'type', array('class' => 'col-sm-3 control-label')); ?>
                            <div class="col-sm-6">
                                <?php echo $form->dropDownList($model, 'type', Operators::getTypes(), array('class' => 'form-control')); ?>
                            </div>
                            <div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'type'); ?></div></div>
                        </div>

                        <div class="panel-footer">
                            <div class="row">
                                <div class="col-sm-6 col-sm-offset-3">
                                    <div class="btn-toolbar">
                                        <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('admin_panel', 'Создать') : Yii::t('Admin', 'Save'), array('class' => 'btn-primary btn')); ?>
                                        <?php echo CHtml::resetButton(Yii::t('admin_panel', 'Сброс'), array('class' => 'btn-common btn')); ?>
                                    </div>
                                    <div class="btn-toolbar">

                                    </div>
                                </div>
                            </div>
                        </div>

                        <?php $this->endWidget(); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>



