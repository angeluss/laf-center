<?php /* @var $this OperatorsController */ ?>
<?php
/**
 * $operators Operators
 */
?>

<div id="wrap">
    <div id="page-heading">
        <h1><?php echo Yii::t('admin_panel', 'Операторы') ?></h1>
    </div>

    <div class="container">
        <div class="panel panel-midnightblue">
            <div class="panel-heading ">
                <div class="options">
                    <a href="<?php echo Yii::app()->createUrl("/admin_panel/operators/create") ?>" class="btn btn-primary">
                        <?php echo Yii::t('admin_panel', 'Добавить оператора'); ?>
                    </a>
                </div>
            </div>
                <?php $this->widget('zii.widgets.grid.CGridView', array(
                    'dataProvider'=>$operators->search(),
                    'ajaxUpdate' => true,
                    'id'=>'operators-grid',
                    'columns'=>array(
                        array(
                            'header' => '#',
                            'value' => '$row + 1',
                            'htmlOptions' => array('style'=>'width:7%'),
                        ),
                        'login',
                        'email',
                        array(
                            'header' => Yii::t('admin_panel', 'Type'),
                            'value' => 'Operators::getTypes($data->type)',
                            'name' => 'type',
                        ),
                        array(
                            'header' => Yii::t('admin_panel', 'Created At'),
                            'value' => 'date("F j, Y, g:i a",$data->created_at)',
                            'name' => 'created_at',
                        ),
                        array(
                            'header' => Yii::t('admin_panel', 'Updated At'),
                            'value' => 'date("F j, Y, g:i a",$data->updated_at)',
                            'name' => 'updated_at',
                        ),
                        array(
                            'class'=>'CButtonColumn',
                            'htmlOptions' => array('style'=>'width:7%'),
                            'template' => '{view}{update}{delete}',
                        )
                    ),
                )); ?>
        </div>
    </div>
</div>
