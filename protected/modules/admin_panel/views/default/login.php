<div class="panel panel-primary admin_panel_login">
    <div class="panel-body">
        <h4 class="text-center" style="margin-bottom: 25px;"><?php echo Yii::t('admin_panel', 'Авторизируйтесь, чтобы продолжить');?></h4>
            <?php $form=$this->beginWidget('CActiveForm', array(
                            'id'=>'login-from',
                            'htmlOptions' => array('class' => 'form-horizontal'),
                            'enableAjaxValidation'=>true,
                            'enableClientValidation'=>true,
                            'focus'=>array($model,'login'),
                            'clientOptions'=>array(
                                  'validateOnSubmit'=>true,
                                  'afterValidate' => 'js:function(form, data, hasError) {
                                      if(hasError) {
                                          for(var i in data) {
                                            $("#"+i).addClass("input-error");
                                            $("#"+i).next(".validation-icon").show();
                                          }
                                          return false;
                                      }
                                      else {
                                          form.find("input").removeClass("input-error");
                                          return true;
                                      }
                                  }',
                            'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
                               if(hasError) {$("#"+attribute.id).addClass("input-error");$("#"+attribute.id).next(".validation-icon").show();}
                               else {$("#"+attribute.id).removeClass("input-error"); $("#"+attribute.id).next(".validation-icon").show();}
                              }'
                            ),
                )); ?>
                <div class="form-group">
                    <label for="Admin_Form_Login_login" class="control-label col-sm-4" style="text-align: left;"><?php echo $model->getAttributeLabel('login') ?></label>
                    <div class="col-sm-8">
                        <?php echo $form->textField($model, 'login', array('class' => 'form-control', 'placeholder' => 'Login')); ?>
                        <?php echo $form->error($model, 'login'); ?>
                    </div>
                </div>
                <div class="form-group">
                    <label for="password" class="control-label col-sm-4" style="text-align: left;"><?php echo $model->getAttributeLabel('password') ?></label>
                    <div class="col-sm-8">
                        <?php echo $form->passwordField($model, 'password', array('class' => 'form-control', 'placeholder' => 'Password')); ?>
                        <?php echo $form->error($model, 'password'); ?>
                    </div>
                </div>
                <div class="clearfix"></div>
                <input type="submit" class="btn btn-primary btn-block" value="<?php echo Yii::t('admin_panel', 'Логин'); ?>"/>
            <?php $this->endWidget(); ?>
    </div>
    <div class="panel-footer"></div>
</div>
