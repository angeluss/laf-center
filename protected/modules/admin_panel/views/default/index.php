<?php

$this->breadcrumbs=array(
    'Ap Logs'=>array('index'),
    'Manage',
);

$this->menu=array(
    array('label'=>'List ApLogs', 'url'=>array('index')),
    array('label'=>'Create ApLogs', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#ap-logs-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

    <h1><?php echo Yii::t('admin_panel', 'Логи'); ?></h1>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'ap-logs-grid',
    'dataProvider'=>$model->search(),
    'columns'=>array(
        array(
            'value' => 'date("F j, Y, g:i a",$data->date)',
            'name' => 'date',
            'htmlOptions' => array('style' => 'width:14%'),
        ),
        array(
            'value' => '$data->getLogRow()',
            'name' => 'action',
        ),
    ),
)); ?>
