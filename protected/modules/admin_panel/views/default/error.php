<!DOCTYPE html>
<html lang="ru">
<head>
    <meta charset="utf-8" />
    <title><?php echo $message; ?></title>
    <?php
    $baseUrl = Yii::app()->baseUrl;
    $cs = Yii::app()->getClientScript();
    $cs->registerCssFile($baseUrl . '/css/reset.css');
    $cs->registerCssFile($baseUrl . '/css/page_404.css');
    ?>
</head>
<body style="background:#ccc">

<div class="block_404">
    <a href="http://xabina.intwall.com/"><img alt="" src="/images/logo.png" /></a>

    <hgroup>
        <h2 style="margin-left:-2px;"><?php echo $message; ?></h2>
        <h6><?php echo Yii::t('Front', 'Error :code', array(':code' => $code)); ?></h6>
    </hgroup>
</div>

</body>
</html>