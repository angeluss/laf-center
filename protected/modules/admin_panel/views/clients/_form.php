<?php
/* @var $this ClientsController */
/* @var $model Clients */
/* @var $form CActiveForm */
?>

<div class="panel-body collapse in">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'clients-form',
        'htmlOptions' => array('class' => 'form-horizontal row-border', 'enctype'=>"multipart/form-data"),
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'errorCssClass' => 'has-error',
            'afterValidate' => 'js:function(form, data, hasError) {
                            form.find("input").parents(".form-group").removeClass("has-error");
                            form.find(".validation-icon").show();
                            if(hasError) {
                                for(var i in data) {
                                    $("#"+i).parents(".form-group").addClass("has-error");
                                    $("#"+i).next(".validation-icon").show();
                                }
                                return false;
                            }
                            else {
                                return true;
                            }
                        }',
            'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
                            if(hasError){
                                $("#"+attribute.id).parents(".form-group").addClass("has-error");
                                $("#"+attribute.id).next(".validation-icon").show();
                            } else {
                                $("#"+attribute.id).parents(".form-group").removeClass("has-error");
                                $("#"+attribute.id).next(".validation-icon").show();
                            }
                        }'
        ),
    ));
    ?>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'fullname', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model, 'fullname', array('class' => 'form-control')); ?>
        </div>
        <div class="col-md-3">
            <div class="help-block">
                <?php echo $form->error($model, 'fullname'); ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'fullname_uk', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model, 'fullname_uk', array('class' => 'form-control')); ?>
        </div>
        <div class="col-md-3">
            <div class="help-block">
                <?php echo $form->error($model, 'fullname_uk'); ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'gender', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->dropDownList($model, 'gender', Clients::getGenders(), array('class' => 'form-control', 'style' => 'width: 11%')); ?>
        </div>
        <div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'gender'); ?></div></div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'bitrhdate', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'attribute' => 'bitrhdate',
                'name' => 'Clients_bitrhdate',
                // additional javascript options for the date picker plugin
                'options'=>array(
                    'showAnim'=>'fold',
                    'changeMonth'=> true,
                    'changeYear'=> true,
                    'yearRange'=> '1900:2100',
                    'firstDay'=> 1
                ),
                'htmlOptions'=>array(
                    'class' => 'form-control date-input'
                ),
            ));
            ?>
        </div>
        <div class="col-md-3">
            <div class="help-block">
                <?php echo $form->error($model, 'bitrhdate'); ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'card_id', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model, 'card_id', array('class' => 'form-control')); ?>
        </div>
        <div class="col-md-3">
            <div class="help-block">
                <?php echo $form->error($model, 'card_id'); ?>
            </div>
        </div>
    </div>
    <div class="form-group">
        <?php echo $form->labelEx($model, 'country', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->dropDownList($model, 'country', $countries, array('class' => 'form-control')); ?>
        </div>
        <div class="col-md-3">
            <div class="help-block">
                <?php echo $form->error($model, 'country'); ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'vuz', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <!--            --><?php //echo $form->textField($model, 'country', array('class' => 'form-control')); ?>
            <?php echo $form->dropDownList($model, 'vuz', $vuses, array('class' => 'form-control')); ?>
        </div>
        <div class="col-md-3">
            <div class="help-block">
                <?php echo $form->error($model, 'vuz'); ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'oblast', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <!--            --><?php //echo $form->textField($model, 'country', array('class' => 'form-control')); ?>
            <?php echo $form->dropDownList($model, 'oblast', array(1 => 'Харьковская область'), array('class' => 'form-control')); ?>
        </div>
        <div class="col-md-3">
            <div class="help-block">
                <?php echo $form->error($model, 'oblast'); ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'district', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <!--            --><?php //echo $form->textField($model, 'country', array('class' => 'form-control')); ?>
            <?php echo $form->dropDownList($model, 'district', Country::getDistrict(), array('class' => 'form-control')); ?>
        </div>
        <div class="col-md-3">
            <div class="help-block">
                <?php echo $form->error($model, 'district'); ?>
            </div>
        </div>
    </div>

    <div class="form-group" style="display:<?php echo $model->district == 1 ? 'block' : 'none'?>">
        <?php echo $form->labelEx($model, 'area', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->dropDownList($model, 'area', Country::getHDistrict(), array('class' => 'form-control')); ?>
        </div>
        <div class="col-md-3">
            <div class="help-block">
                <?php echo $form->error($model, 'area'); ?>
            </div>
        </div>
    </div>

    <div class="form-group" style="display:<?php echo $model->district != 1 ? 'block' : 'none'?>">
        <?php echo $form->labelEx($model, 'city', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model, 'city', array('class' => 'form-control')); ?>
        </div>
        <div class="col-md-3">
            <div class="help-block">
                <?php echo $form->error($model, 'city'); ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'street', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model, 'street', array('class' => 'form-control')); ?>
        </div>
        <div class="col-md-3">
            <div class="help-block">
                <?php echo $form->error($model, 'street'); ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'passport', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model, 'passport', array('class' => 'form-control')); ?>
        </div>
        <div class="col-md-3">
            <div class="help-block">
                <?php echo $form->error($model, 'passport'); ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'date_passport', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'attribute' => 'date_passport',
                'name' => 'Clients_date_passport',
                // additional javascript options for the date picker plugin
                'options'=>array(
                    'showAnim'=>'fold',
                    'changeMonth'=> true,
                    'changeYear'=> true,
                    'yearRange'=> '1900:2100',
                    'firstDay'=> 1
                ),
                'htmlOptions'=>array(
                    'class' => 'form-control date-input'
                ),
            ));
            ?>
        </div>
        <div class="col-md-3">
            <div class="help-block">
                <?php echo $form->error($model, 'date_passport'); ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'passport_till', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'attribute' => 'passport_till',
                'name' => 'Clients_passport_till',
                // additional javascript options for the date picker plugin
                'options'=>array(
                    'showAnim'=>'fold',
                    'changeMonth'=> true,
                    'changeYear'=> true,
                    'yearRange'=> '1900:2100',
                    'firstDay'=> 1
                ),
                'htmlOptions'=>array(
                    'class' => 'form-control date-input'
                ),
            ));
            ?>
        </div>
        <div class="col-md-3">
            <div class="help-block">
                <?php echo $form->error($model, 'passport_till'); ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'tax_number', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model, 'tax_number', array('class' => 'form-control')); ?>
        </div>
        <div class="col-md-3">
            <div class="help-block">
                <?php echo $form->error($model, 'tax_number'); ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'vpu', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->dropDownList(
                $model,
                'vpu',
                array(
                    Yii::t('admin_panel', 'не выдан'),
                    Yii::t('admin_panel', 'выдан'),
                ),
                array('class' => 'form-control')); ?>
        </div>
        <div class="col-md-3">
            <div class="help-block">
                <?php echo $form->error($model, 'vpu'); ?>
            </div>
        </div>
    </div>

    <div class="form-group"  style="display:<?php echo $model->vpu == 1 ? 'block' : 'none'?>">
        <?php echo $form->labelEx($model, 'vpu_serie', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model, 'vpu_serie', array('class' => 'form-control')); ?>
        </div>
        <div class="col-md-3">
            <div class="help-block">
                <?php echo $form->error($model, 'vpu_serie'); ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'vpu_number', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model, 'vpu_number', array('class' => 'form-control')); ?>
        </div>
        <div class="col-md-3">
            <div class="help-block">
                <?php echo $form->error($model, 'vpu_number'); ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'vpu_till', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php
            $this->widget('zii.widgets.jui.CJuiDatePicker', array(
                'model' => $model,
                'attribute' => 'vpu_till',
                'name' => 'Clients_vpu_till',
                // additional javascript options for the date picker plugin
                'options'=>array(
                    'showAnim'=>'fold',
                    'changeMonth'=> true,
                    'changeYear'=> true,
                    'yearRange'=> '1900:2100',
                    'firstDay'=> 1
                ),
                'htmlOptions'=>array(
                    'class' => 'form-control date-input'
                ),
            ));
            ?>
        </div>
        <div class="col-md-3">
            <div class="help-block">
                <?php echo $form->error($model, 'vpu_till'); ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'vpu_from', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model, 'vpu_from', array('class' => 'form-control')); ?>
        </div>
        <div class="col-md-3">
            <div class="help-block">
                <?php echo $form->error($model, 'vpu_from'); ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'phone', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model, 'phone', array('class' => 'form-control')); ?>
        </div>
        <div class="col-md-3">
            <div class="help-block">
                <?php echo $form->error($model, 'phone'); ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'email', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model, 'email', array('class' => 'form-control')); ?>
        </div>
        <div class="col-md-3">
            <div class="help-block">
                <?php echo $form->error($model, 'email'); ?>
            </div>
        </div>
    </div>

    <?php if($model->photo != ''): ?>
    <div class="form-group">
        <label class="col-sm-3 control-label" for="deletePhoto"><?php echo Yii::t('admin_panel', 'Удалить фото'); ?></label>
        <div class="col-sm-6">
            <?php echo CHtml::checkBox('deletePhoto', false, array('class' => 'form-control', 'style' => 'margin-left: -264px;')); ?>
        </div>
    </div>
    <?php endif; ?>

    <div class="form-group pwd-field <?php echo $model->isNewRecord ? '' : 'closed'?>">
        <?php echo $form->labelEx($model, 'pwd', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->passwordField($model, 'pwd', array('class' => 'form-control')); ?>
        </div>
        <div class="col-md-3">
            <div class="help-block">
                <?php echo $form->error($model, 'pwd'); ?>
            </div>
        </div>
    </div>

    <?php if(!$model->isNewRecord): ?>
        <button id="change_pwd" style="margin-left: 295px; margin-bottom: 15px">
            <span><?php echo Yii::t('admin_panel', 'Изменить пароль'); ?></span>
        </button>
        <script>
            $('#change_pwd').click(function(e){
                e.preventDefault();
                $('.pwd-field').toggleClass('closed');
            });
        </script>
    <?php endif; ?>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'status', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->dropDownList($model, 'status', Clients::getstatuses(), array('class' => 'form-control')); ?>
        </div>
        <div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'status'); ?></div></div>
    </div>

    <div class="panel-footer">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="btn-toolbar">
                    <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('Admin', 'Создать') : Yii::t('Admin', 'Сохранить'), array('class' => 'btn-primary btn')); ?>
                    <?php echo CHtml::resetButton(Yii::t('Admin', 'Сброс'), array('class' => 'btn-common btn')); ?>
                </div>
                <div class="btn-toolbar">

                </div>
            </div>
        </div>
    </div>

    <?php $this->endWidget(); ?>
</div>
<script>
    jQuery('#Clients_district').on('change', function(){
        var choise =jQuery(this).val();
        if (choise!==1) {
            $('#Clients_city').closest('.form-group').css('display','block');
            $('#Clients_area').closest('.form-group').css('display','none');

        }
        if (choise==1) {
            $('#Clients_city').closest('.form-group').css('display','none');
            $('#Clients_area').closest('.form-group').css('display','block');
        }
    });
    jQuery('#Clients_vpu').on('change', function(){
        var choise =jQuery(this).val();
        if (choise==0) {
            $('#Clients_vpu_serie').closest('.form-group').css('display','none');
            $('#Clients_vpu_number').closest('.form-group').css('display','none');
            $('#Clients_vpu_till').closest('.form-group').css('display','none');
            $('#Clients_vpu_from').closest('.form-group').css('display','none');
        }
        if (choise==1) {
            $('#Clients_vpu_serie').closest('.form-group').css('display','block');
            $('#Clients_vpu_number').closest('.form-group').css('display','block');
            $('#Clients_vpu_till').closest('.form-group').css('display','block');
            $('#Clients_vpu_from').closest('.form-group').css('display','block');
        }
    });
</script>

