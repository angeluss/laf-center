<?php
/* @var $this ClientsController */
/* @var $model Clients */
$this->menu=array(
	array('label'=>'К списку клиентов', 'url'=>array('index')),
);
?>

<h1><?php echo Yii::t('admin_panel', 'Подробный просмотр данных клиента #') . $model->id; ?></h1>
<?php
$this->widget('zii.widgets.CMenu', array(
    'items'=>$this->menu,
    'htmlOptions'=>array('class'=>'operations'),
)); ?>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'fullname',
        array(
            'name'=>'gender',
            'value'=>Clients::getGenders($model->gender),
        ),
        'card_id',
        'bitrhdate',
        'country',
        array(
            'name'=>'vuz',
            'value'=>Vuz::getVuses($model->vuz),
        ),
        array(
            'name'=>'oblast',
            'value'=>Country::getOblast($model->oblast),
        ),
        array(
            'name'=>'district',
            'value'=>Country::getDistrict($model->district),
        ),
        $area,
        'street',
        array(
            'name'=>'vpu',
            'value'=>$model->vpu==1 ? 'выдан' : 'не выдан',
        ),
        'vpu_serie',
        'vpu_number',
        'vpu_till',
        'vpu_from',
		'phone',
		'email',
        array(
            'name'=>'status',
            'value'=>Clients::getStatuses($model->status),
        ),
        array(
            'name'=>'pay_date',
            'value'=> Yii::app()->dateFormatter->formatDateTime($model->pay_date, 'long','long'),
        ),
        array(
            'name'=>'created_at',
            'value'=>Yii::app()->dateFormatter->formatDateTime($model->created_at, 'long','long'),
        ),
        array(
            'name'=>'updated_at',
            'value'=>Yii::app()->dateFormatter->formatDateTime($model->updated_at, 'long','long'),
        ),
	),
)); ?>
<a href="<?php echo Yii::app()->createUrl("/admin_panel/clients/update", array('id' => $model->id)) ?>" class="btn btn-primary">
    <?php echo Yii::t('admin_panel', 'Редактировать'); ?>
</a>
<a href="<?php echo Yii::app()->createUrl("/admin_panel/clients/delete", array('id' => $model->id)) ?>" class="btn btn-danger delete">
    <?php echo Yii::t('admin_panel', 'Удалить'); ?>
</a>
