<?php
/* @var $this ClientsController */
/* @var $model Clients */

$this->breadcrumbs=array(
	'Clients'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
    array('label'=>'К списку клиентов', 'url'=>array('index')),
    array('label'=>'Подробный просмотр', 'url'=>array('view', 'id'=>$model->id)),
);
?>

<h1><?php echo Yii::t('admin_panel', 'Редактировать данные клиента #') . $model->id; ?></h1>
<?php
$this->widget('zii.widgets.CMenu', array(
    'items'=>$this->menu,
    'htmlOptions'=>array('class'=>'operations'),
)); ?>
<?php $this->renderPartial('_form', array(
    'model'=>$model,
    'vuses' => $vuses,
    'countries' => $countries,
    )); ?>