<?php
/* @var $this ClientsController */
/* @var $dataProvider CActiveDataProvider */
?>

<div id="page-heading">
    <h1><?php echo Yii::t('admin_panel', 'Клиенты') ?></h1>
</div>
<?php
$this->widget('booster.widgets.TbAlert', array(
    'fade' => true,
    'closeText' => '&times;', // false equals no close link
    'events' => array(),
    'htmlOptions' => array(),
    'userComponentId' => 'user',
    'alerts' => array( // configurations per alert type
        // success, info, warning, error or danger
        'success' => array('closeText' => '&times;'),
        'info', // you don't need to specify full config
        'warning' => array('closeText' => '&times;'),
        'error' => array('closeText' => '&times;')
    ),
));
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'clients-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        'fullname',
        'fullname_uk',
        array(
            'value' => 'Clients::getGenders($data->gender)',
            'name' => 'gender',
            'filter' => Clients::getGenders(),
            'htmlOptions' => array('style'=>'width:5%'),
        ),
        'card_id',
        'phone',
        'email',
        array(
            'value' => 'Clients::getStatuses($data->status)',
            'name' => 'status',
            'filter' => Clients::getStatuses(),
        ),
        'country',
        'vuz',
        'passport',
        'tax_number',
        array(
            'value' => 'News::getIsAttached($data->vpu)',
            'name' => 'vpu',
            'filter' => News::getIsAttached(),
        ),
        'vpu_serie',
        'vpu_number',
        'vpu_from',
        array(
            'class'=>'CButtonColumn',
            'htmlOptions' => array('style'=>'width:7%'),
        ),
    ),
));

