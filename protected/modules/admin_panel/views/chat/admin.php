<?php
/* @var $this VuzController */
/* @var $model Vuz */

$this->breadcrumbs=array(
	'Chats'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Vuz', 'url'=>array('index')),
	array('label'=>'Create Vuz', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#vuz-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div id="page-heading">
	<h1><?php echo Yii::t('admin_panel', 'Завершені чати');?></h1>
</div>
<?php
$this->widget('booster.widgets.TbAlert', array(
	'fade' => true,
	'closeText' => '&times;', // false equals no close link
	'events' => array(),
	'htmlOptions' => array(),
	'userComponentId' => 'user',
	'alerts' => array( // configurations per alert type
		// success, info, warning, error or danger
		'success' => array('closeText' => '&times;'),
		'info', // you don't need to specify full config
		'warning' => array('closeText' => '&times;'),
		'error' => array('closeText' => '&times;')
	),
));
?>

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'vuz-grid',
	'dataProvider'=>$model->search(),
	'columns'=>array(
		array(
			'value' => '$data->id',
			'name' => 'id',
			'htmlOptions' => array('style'=>'width:3%; text-align:center'),
		),
		array(
			'value' => '$data->getClient()',
			'name' => 'client',
		),
		array(
			'value' => '$data->getOperator()',
			'name' => 'operator_id',
		),
        array(
            'value' => '$data->getMessagesCount()',
            'header' => Yii::t('admin_panel','Сообщений'),
            'htmlOptions' => array('style'=>'width:7%; text-align:center'),
        ),
		array(
			'value' => 'date("F j, Y, g:i a",$data->created_at)',
			'name' => 'created_at',
            'htmlOptions' => array('style'=>'width:15%'),
		),
		array(
			'class'=>'CButtonColumn',
			'template' => '{view}'
		),
	),
)); ?>
