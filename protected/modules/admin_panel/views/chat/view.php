<?php
/* @var $this VuzController */
/* @var $model Vuz */

$this->menu=array(
	array('label'=>'Перелік чатів	', 'url'=>array('index')),
);
?>

<h1><?php echo Yii::t('admin', 'Детальний перегляд '); ?></h1>
<?php
$this->widget('booster.widgets.TbAlert', array(
	'fade' => true,
	'closeText' => '&times;', // false equals no close link
	'events' => array(),
	'htmlOptions' => array(),
	'userComponentId' => 'user',
	'alerts' => array( // configurations per alert type
		// success, info, warning, error or danger
		'success' => array('closeText' => '&times;'),
		'info', // you don't need to specify full config
		'warning' => array('closeText' => false),
		'error' => array('closeText' => 'AAARGHH!!')
	),
));
?>
<?php
$this->widget('zii.widgets.CMenu', array(
	'items'=>$this->menu,
	'htmlOptions'=>array('class'=>'operations'),
)); ?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		array(
			'name'=>'client',
			'value'=>$model->getClient(),
		),
		array(
			'name'=>'operator_id',
			'value'=>$model->getOperator(),
		),
		array(
			'value' => date("F j, Y, g:i a",$model->created_at),
			'name' => 'created_at',
		),
	),
)); ?>
<div class="chat_block">
<?php foreach($messages as $m):?>
    <p class="<?php echo $m->sender == 1 ? 'client-msg' : 'operator-msg' ;?>">
        <span class="sender-name">
            <?php echo $m->sender == 1 ? $model->getClient() : $model->getOperator(); ?>:
        </span>
        <?php echo $m->msg ?>
    </p>
<?php endforeach; ?>
</div>
<script>
	$('.chat_block').scrollTop($('.chat_block')[0].scrollHeight);
</script>