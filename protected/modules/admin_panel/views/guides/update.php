<?php
/* @var $this GuidesController */
/* @var $model Guides */

$this->breadcrumbs=array(
	'Guides'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
    array('label'=>'К списку гайдов', 'url'=>array('index')),
    array('label'=>'Подробный просмотр', 'url'=>array('view', 'id'=>$model->id)),
);
?>

<h1><?php echo Yii::t('admin_panel', 'Редактировать гайд #') . $model->id; ?></h1>
<?php
$this->widget('zii.widgets.CMenu', array(
    'items'=>$this->menu,
    'htmlOptions'=>array('class'=>'operations'),
)); ?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>