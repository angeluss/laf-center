<?php
/* @var $this GuidesController */
/* @var $dataProvider CActiveDataProvider */
?>


<div id="page-heading">
    <h1><?php echo Yii::t('admin_panel', 'Гайды') ?></h1>
</div>

<div class="options">
    <a href="<?php echo Yii::app()->createUrl("/admin_panel/guides/create") ?>" class="btn btn-primary">
        <?php echo Yii::t('admin_panel', 'Добавить гайд'); ?>
    </a>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'guides-grid',
    'dataProvider'=>$model->search(),
    'columns'=>array(
        array(
            'header' => '#',
            'value' => '$row + 1',
        ),
        'title',
        array(
            'value' => 'Guides::getCategory($data->category_id)',
            'name' => 'category_id',
        ),
        array(
            'value' => 'Categories::getStatuses($data->status)',
            'name' => 'status',
        ),
        array(
            'value' => 'date("F j, Y, g:i a",$data->created_at)',
            'name' => 'created_at',
        ),
        array(
            'value' => 'date("F j, Y, g:i a",$data->updated_at)',
            'name' => 'updated_at',
        ),
        array(
            'class'=>'CButtonColumn',
            'htmlOptions' => array('style'=>'width:7%'),
        ),
    ),
)); ?>
