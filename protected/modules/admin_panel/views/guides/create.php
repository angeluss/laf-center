<?php
/* @var $this GuidesController */
/* @var $model Guides */

$this->menu=array(
	array('label'=>'Список гайдов', 'url'=>array('index')),
);
?>

<h1><?php echo Yii::t('admin_panel', 'Добавить гайд'); ?></h1>
<?php
$this->widget('zii.widgets.CMenu', array(
    'items'=>$this->menu,
    'htmlOptions'=>array('class'=>'operations'),
)); ?>
<?php $this->renderPartial('_form', array('model'=>$model)); ?>