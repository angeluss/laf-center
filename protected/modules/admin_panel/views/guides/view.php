<?php
/* @var $this GuidesController */
/* @var $model Guides */
$this->menu=array(
    array('label'=>'<<< Назад к списку гайдов', 'url'=>array('index')),
);
?>

<h1><?php echo Yii::t('admin_panel', 'Подробный просмотр гайда #') . $model->id; ?></h1>
<?php
$this->widget('zii.widgets.CMenu', array(
    'items'=>$this->menu,
    'htmlOptions'=>array('class'=>'operations'),
)); ?>
<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
        array(
            'name'=>'category_id',
            'value'=>Guides::getCategory($model->category_id),
        ),
		'content',
        array(
            'name'=>'status',
            'value'=>Categories::getStatuses($model->status),
        ),
        array(
            'name'=>'created_at',
            'value'=> Yii::app()->dateFormatter->formatDateTime($model->created_at, 'long','long'),
        ),
        array(
            'name'=>'updated_at',
            'value'=> Yii::app()->dateFormatter->formatDateTime($model->updated_at, 'long','long'),
        ),
	),
)); ?>
<a href="<?php echo Yii::app()->createUrl("/admin_panel/guides/update", array('id' => $model->id)) ?>" class="btn btn-primary">
    <?php echo Yii::t('admin_panel', 'Редактировать'); ?>
</a>
<a href="<?php echo Yii::app()->createUrl("/admin_panel/guides/delete", array('id' => $model->id)) ?>" class="btn btn-danger delete">
    <?php echo Yii::t('admin_panel', 'Удалить'); ?>
</a>