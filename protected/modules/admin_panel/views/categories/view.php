<?php
/* @var $this CategoriesController */
/* @var $model Categories */

$this->menu=array(
	array('label'=>'<<< Назад к списку категорий', 'url'=>array('index')),
);
?>

<h1><?php echo Yii::t('admin_panel', 'Просмотр категории #') . $model->id; ?></h1>
<?php if(Yii::app()->user->hasFlash('success')): ?>
    <div class="flash_msg">
        <?php echo Yii::app()->user->getFlash('success'); ?>
    </div>
    <script>
        $(document).ready(function(){
            setTimeout(function(){
                $('.flash_msg').fadeOut('slow');
            }, 3000)
        });
    </script>
<?php endif; ?>
<?php
$this->widget('zii.widgets.CMenu', array(
    'items'=>$this->menu,
    'htmlOptions'=>array('class'=>'operations'),
)); ?>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'title',
        array(
            'name'=>'status',
            'value'=>Categories::getStatuses($model->status),
        ),
        array(
            'name'=>'created_at',
            'value'=> Yii::app()->dateFormatter->formatDateTime($model->created_at, 'long','long'),
        ),
        array(
            'name'=>'updated_at',
            'value'=> Yii::app()->dateFormatter->formatDateTime($model->updated_at, 'long','long'),
        ),
	),
)); ?>
<a href="<?php echo Yii::app()->createUrl("/admin_panel/categories/update", array('id' => $model->id)) ?>" class="btn btn-primary">
    <?php echo Yii::t('admin_panel', 'Редактировать'); ?>
</a>
<a href="<?php echo Yii::app()->createUrl("/admin_panel/categories/delete", array('id' => $model->id)) ?>" class="btn btn-danger delete">
    <?php echo Yii::t('admin_panel', 'Удалить'); ?>
</a>
