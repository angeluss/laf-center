<?php
/* @var $this CategoriesController */
/* @var $dataProvider CActiveDataProvider */
?>

    <div id="page-heading">
        <h1><?php echo Yii::t('admin_panel', 'Категории') ?></h1>
    </div>

<div class="options">
    <a href="<?php echo Yii::app()->createUrl("/admin_panel/categories/create") ?>" class="btn btn-primary">
        <?php echo Yii::t('admin_panel', 'Добавить категорию'); ?>
    </a>
</div>

<?php $this->widget('zii.widgets.grid.CGridView', array(
    'id'=>'categories-grid',
    'dataProvider'=>$model->search(),
    'filter'=>$model,
    'columns'=>array(
        array(
            'header' => '#',
            'value' => '$row + 1',
        ),
        'title',
        array(
            'value' => 'Categories::getStatuses($data->status)',
            'name' => 'status',
            'filter' => Categories::getStatuses(),
        ),
        array(
            'value' => 'date("F j, Y, g:i a",$data->created_at)',
            'name' => 'created_at',
        ),
        array(
            'value' => 'date("F j, Y, g:i a",$data->updated_at)',
            'name' => 'updated_at',
        ),
        array(
            'class'=>'CButtonColumn',
            'htmlOptions' => array('style'=>'width:7%'),
        ),
    ),
)); ?>
