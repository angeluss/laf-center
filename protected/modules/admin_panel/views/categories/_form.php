<?php
/* @var $this CategoriesController */
/* @var $model Categories */
/* @var $form CActiveForm */
?>
<div class="panel-body collapse in">
    <?php
    $form = $this->beginWidget('CActiveForm', array(
        'id' => 'categories-form',
        'htmlOptions' => array('class' => 'form-horizontal row-border'),
        'enableAjaxValidation' => true,
        'clientOptions' => array(
            'validateOnSubmit' => true,
            'validateOnChange' => true,
            'errorCssClass' => 'has-error',
            'afterValidate' => 'js:function(form, data, hasError) {
								form.find("input").parents(".form-group").removeClass("has-error");
								form.find(".validation-icon").show();
								if(hasError) {
									for(var i in data) {
										$("#"+i).parents(".form-group").addClass("has-error");
										$("#"+i).next(".validation-icon").show();
									}
									return false;
								}
								else {
									return true;
								}
							}',
            'afterValidateAttribute' => 'js:function(form, attribute, data, hasError) {
								if(hasError){
									$("#"+attribute.id).parents(".form-group").addClass("has-error");
									$("#"+attribute.id).next(".validation-icon").show();
								} else {
									$("#"+attribute.id).parents(".form-group").removeClass("has-error");
									$("#"+attribute.id).next(".validation-icon").show();
								}
							}'
        ),
    ));
    ?>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'title', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->textField($model, 'title', array('class' => 'form-control')); ?>
        </div>
        <div class="col-md-3">
            <div class="help-block">
                <?php echo $form->error($model, 'title'); ?>
            </div>
        </div>
    </div>

    <div class="form-group">
        <?php echo $form->labelEx($model, 'status', array('class' => 'col-sm-3 control-label')); ?>
        <div class="col-sm-6">
            <?php echo $form->dropDownList($model, 'status', Categories::getstatuses(), array('class' => 'form-control')); ?>
        </div>
        <div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'status'); ?></div></div>
    </div>

    <div class="panel-footer">
        <div class="row">
            <div class="col-sm-6 col-sm-offset-3">
                <div class="btn-toolbar">
                    <?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('Admin', 'Создать') : Yii::t('Admin', 'Сохранить'), array('class' => 'btn-primary btn')); ?>
                    <?php echo CHtml::resetButton(Yii::t('Admin', 'Сброс'), array('class' => 'btn-common btn')); ?>
                </div>
                <div class="btn-toolbar">

                </div>
            </div>
        </div>
    </div>
    <?php $this->endWidget(); ?>
</div>