<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">

    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/sb-admin.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/morris.css">
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/admin_panel.css">
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/font-awesome.min.css">
    <script src="/ckeditor/ckeditor.js"></script>
    <link rel="stylesheet" href="/ckeditor/samples/css/samples.css">
    <link rel="stylesheet" href="/ckeditor/samples/toolbarconfigurator/lib/codemirror/neo.css">
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>

</head>

<body>
<div id="wrapper">

<!-- Navigation -->
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="<?php echo Yii::app()->createUrl("/admin") ?>">
            <?php echo Yii::t('admin_panel', 'Admin'); ?>
        </a>
        <a class="navbar-brand" href="<?php echo Yii::app()->createUrl("/admin_panel") ?>" style="color: white">
            <?php echo Yii::t('admin_panel', 'Admin_Panel'); ?>
        </a>
    </div>
    <!-- Top Menu Items -->
    <ul class="nav navbar-right top-nav">
        <li class="dropdown">
            <a href="#" class="dropdown-toggle " data-toggle="dropdown">
                <i class="glyphicon glyphicon-user"></i>
                <?php echo Yii::app()->user->name; ?>
                <b class="caret"></b>
            </a>
            <ul class="dropdown-menu">
                <li>
                    <a href="<?php echo Yii::app()->createUrl("/admin_panel/default/logout") ?>">
                        <i class="glyphicon glyphicon-off"></i> <?php echo Yii::t('admin_panel', 'Log Out'); ?>
                    </a>
                </li>
            </ul>
            <script>
                $('.dropdown-toggle').click(function(){
                    $(this).closest('li').toggleClass('open');
                })
            </script>
        </li>
    </ul>
    <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
    <div class="collapse navbar-collapse navbar-ex1-collapse">
        <ul class="nav navbar-nav side-nav">
            <?php if(Yii::app()->user->getState('role') == 'admin'): ?>
            <li class="<?php echo Yii::app()->controller->id == 'default' ? 'active' : ''?>">
                <a href="<?php echo Yii::app()->createUrl("/admin_panel") ?>"><i class="glyphicon glyphicon-list-alt"></i> <?php echo Yii::t('admin_panel', 'Dashboard'); ?></a>
            </li>
            <li class="<?php echo Yii::app()->controller->id == 'operators' ? 'active' : ''?>">
                <a href="<?php echo Yii::app()->createUrl("/admin_panel/operators") ?>"><i class="glyphicon glyphicon-headphones"></i> <?php echo Yii::t('admin_panel', 'Операторы'); ?></a>
            </li>
            <li class="<?php echo Yii::app()->controller->id == 'clients' ? 'active' : ''?>">
                <a href="<?php echo Yii::app()->createUrl("/admin_panel/clients") ?>"><i class="glyphicon glyphicon-user"></i> <?php echo Yii::t('admin_panel', 'Клиенты'); ?></a>
            </li>
            <?php endif; ?>
            <li class="<?php echo Yii::app()->controller->id == 'chat' ? 'active' : ''?>">
                <a href="<?php echo Yii::app()->createUrl("/admin_panel/chat") ?>"><i class="glyphicon glyphicon-user"></i> <?php echo Yii::t('admin_panel', 'Чаты'); ?></a>
            </li>
            <li class="<?php echo Yii::app()->controller->id == 'vuz' ? 'active' : ''?>">
                <a href="<?php echo Yii::app()->createUrl("/admin_panel/vuz") ?>"><i class="glyphicon glyphicon-user"></i> <?php echo Yii::t('admin_panel', 'Вузы'); ?></a>
            </li>
            <li class="<?php echo Yii::app()->controller->id == 'categories' or Yii::app()->controller->id == 'guides' ? 'active' : ''?>">
                <a href="javascript:;" class="left_menu_dropdown">
                    <i class="glyphicon glyphicon-folder-close"></i>
                    <?php echo Yii::t('admin_panel', 'Гайды'); ?>
                    <i class="arrow glyphicon glyphicon-chevron-down"></i>
                </a>
                <ul id="demo" class="collapse">
                    <li class="<?php echo Yii::app()->controller->id == 'categories' ? 'active' : ''?>">
                        <a href="<?php echo Yii::app()->createUrl("/admin_panel/categories") ?>"><?php echo Yii::t('admin_panel', 'Категории'); ?></a>
                    </li>
                    <li class="<?php echo Yii::app()->controller->id == 'guides' ? 'active' : ''?>">
                        <a href="<?php echo Yii::app()->createUrl("/admin_panel/guides") ?>"><?php echo Yii::t('admin_panel', 'Гайды'); ?></a>
                    </li>
                </ul>
            </li>
        </ul>
        <script>
            $('.left_menu_dropdown').click(function(){
                $(this).closest('li').children('.collapse').toggleClass('in');
                $(this).children('.arrow').removeClass('glyphicon-chevron-down').removeClass('glyphicon-chevron-up');
                if($(this).closest('li').children('.collapse').hasClass('in')){
                    $(this).children('.arrow').addClass('glyphicon-chevron-up');
                } else {
                    $(this).children('.arrow').addClass('glyphicon-chevron-down');
                }

            });
        </script>
    </div>
    <!-- /.navbar-collapse -->
</nav>

<div id="page-wrapper">
    <?php echo $content; ?>
</div>
    <!-- /#page-wrapper -->

</div>
<!-- /#wrapper -->
<script>
    $('.delete').click(function(e) {
        e.preventDefault();
        if (confirm("Вы уверены?")) {
            location.href = $(this).attr('href');
        }
    });
</script>

<!-- Bootstrap Core JavaScript -->
<script src="/js/bootstrap.min.js"></script>

<script src="//cdn.ckeditor.com/4.5.1/full/ckeditor.js"></script>
<script src="/js/dev.js"></script>

</body>
</html>
