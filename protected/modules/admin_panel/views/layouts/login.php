<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>Admin Panel</title>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Admin Panel"><?php
    $baseUrl = Yii::app()->baseUrl;
    $cs = Yii::app()->getClientScript();
    $cs->registerCssFile('http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600');
    $cs->registerCssFile($baseUrl . '/css/sb-admin.css');
    $cs->registerCssFile($baseUrl . '/css/admin_panel.css');
    ?>
    <?php Yii::app()->clientScript->registerCoreScript('jquery'); ?>
	<?php Yii::app()->clientScript->registerCoreScript('jquery.ui'); ?>
</head>
<body class="focusedform">
    <div class="verticalcenter">
        <?php echo $content ?>
    </div>
</body>
</html>