<?php
/* @var $this VuzController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Vuzs',
);

$this->menu=array(
	array('label'=>'Create Vuz', 'url'=>array('create')),
	array('label'=>'Manage Vuz', 'url'=>array('admin')),
);
?>

<h1>Vuzs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
