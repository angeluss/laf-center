<?php
/* @var $this VuzController */
/* @var $model Vuz */

$this->breadcrumbs=array(
	'Vuzs'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'Перелік вузів', 'url'=>array('index')),
	array('label'=>'Детальний перегляд', 'url'=>array('view', 'id'=>$model->id)),
);
?>

<h1><?php echo Yii::t('admin', 'Редагувати вуз ') . $model->title; ?></h1>
<?php
$this->widget('zii.widgets.CMenu', array(
	'items'=>$this->menu,
	'htmlOptions'=>array('class'=>'operations'),
)); ?>


<?php $this->renderPartial('_form', array('model'=>$model)); ?>