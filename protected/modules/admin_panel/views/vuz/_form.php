<?php
/* @var $this VuzController */
/* @var $model Vuz */
/* @var $form CActiveForm */
?>

	<div class="form">

		<?php
		$form = $this->beginWidget('CActiveForm', array(
			'id' => 'vuz-form',
			'htmlOptions' => array(
				'class' => 'form-horizontal row-border',
				'enctype'=>"multipart/form-data",
			),
			'enableAjaxValidation' => true,
			'focus'=>'input[type="text"]:first',
			'clientOptions' => array(
				'validateOnSubmit' => true,
				'validateOnChange' => true,
				'errorCssClass' => 'has-error',
			),
		));
		?>
		<div class="form-group">
			<?php echo $form->labelEx($model, 'title', array('class' => 'col-sm-3 control-label')); ?>
			<div class="col-sm-6">
				<?php echo $form->textField($model, 'title', array('class' => 'form-control' )); ?>
			</div>
			<div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'title'); ?></div></div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model, 'status', array('class' => 'col-sm-3 control-label')); ?>
			<div class="col-sm-6">
				<?php echo $form->dropDownList($model, 'status', News::getStatus(), array('class' => 'form-control')); ?>

			</div>
			<div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'status'); ?></div></div>
		</div>

		<div class="form-group">
			<?php echo $form->labelEx($model, 'code', array('class' => 'col-sm-3 control-label')); ?>
			<div class="col-sm-6">
				<?php echo $form->textField($model, 'code', array('class' => 'form-control' )); ?>
			</div>
			<div class="col-md-3"><div class="help-block"><?php echo $form->error($model, 'code'); ?></div></div>
		</div>

		<div class="panel-footer">
			<div class="row">
				<div class="col-sm-6 col-sm-offset-3">
					<div class="btn-toolbar">
						<?php echo CHtml::submitButton($model->isNewRecord ? Yii::t('Admin', 'Створити') : Yii::t('Admin', 'Зберегти'), array('class' => 'btn-primary btn')); ?>
					</div>
				</div>
			</div>
		</div>

		<?php $this->endWidget(); ?>

	</div><!-- form -->
