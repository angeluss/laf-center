<?php
/* @var $this VuzController */
/* @var $model Vuz */

$this->breadcrumbs=array(
	'Vuzs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'Перелік Вузів', 'url'=>array('index')),
);
?>
<h1><?php echo Yii::t('admin', 'Додати вуз'); ?></h1>
<?php
$this->widget('zii.widgets.CMenu', array(
	'items'=>$this->menu,
	'htmlOptions'=>array('class'=>'operations'),
)); ?>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>