<?php
/* @var $this VuzController */
/* @var $model Vuz */

$this->breadcrumbs = array(
	'Vuzs' => array('index'),
	'Manage',
);

$this->menu=array(
	array('label' => 'List Vuz', 'url' => array('index')),
	array('label' => 'Create Vuz', 'url' => array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#vuz-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<div id="page-heading">
	<h1><?php echo Yii::t('admin_panel', 'Редагувати Вузи');?></h1>
</div>
<?php
$this->widget('booster.widgets.TbAlert', array(
	'fade' => true,
	'closeText' => '&times;', // false equals no close link
	'events' => array(),
	'htmlOptions' => array(),
	'userComponentId' => 'user',
	'alerts' => array( // configurations per alert type
		// success, info, warning, error or danger
		'success' => array('closeText' => '&times;'),
		'info', // you don't need to specify full config
		'warning' => array('closeText' => '&times;'),
		'error' => array('closeText' => '&times;')
	),
));
?>
<div class="options">
	<a href="<?php echo Yii::app()->createUrl("/admin_panel/vuz/create") ?>" class="btn btn-primary">
		<?php echo Yii::t('admin_panel', 'Додати вуз'); ?>
	</a>
</div>
<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id' => 'vuz-grid',
	'dataProvider' => $model->search(),
	'filter' => $model,
	'columns' => array(
		'title',
		array(
			'value' => 'News::getStatus($data->status)',
			'name' => 'status',
			'type' => 'raw',
			'filter' => News::getStatus(),
		),
		'code',
		array(
			'class' => 'CButtonColumn',
		),
	),
)); ?>
