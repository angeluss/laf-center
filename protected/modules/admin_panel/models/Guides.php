<?php

/**
 * This is the model class for table "guides".
 *
 * The followings are the available columns in table 'guides':
 * @property integer $id
 * @property string $title
 * @property integer $category_id
 * @property string $content
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Guides extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'guides';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, category_id, content, status', 'required'),
			array('category_id, status, created_at, updated_at', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>255),
			array('id, title, category_id, content, status, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

    public static function getCategory($id) {
        $category = Categories::model()->findByPk($id);
        if(is_null($category)){
            return 'Без категории';
        }
        return $category->title;
    }

    public static function getCategories() {
        $model = Categories::model()->findAll();
        $categories = array();
        $categories[0] = Yii::t('admin_panel', 'Без категории');
        foreach ( $model as $c){
            $categories[$c->id] = $c->title;
        }
        return $categories;
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
            'category' => array(self::BELONGS_TO, 'Categories', 'category_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Название',
			'category_id' => 'Категория',
			'content' => 'Контент',
			'status' => 'Статус',
			'created_at' => 'Создано',
			'updated_at' => 'Обновлено',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{

		$criteria = new CDbCriteria;

        $sort = new CSort();
        $sort->defaultOrder = 't.id asc';

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('category_id',$this->category_id);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_at',$this->created_at);
		$criteria->compare('updated_at',$this->updated_at);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => $sort,
            'pagination' => array(
                'pageSize' => '5',
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Guides the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
