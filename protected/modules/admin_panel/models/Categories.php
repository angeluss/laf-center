<?php

/**
 * This is the model class for table "categories".
 *
 * The followings are the available columns in table 'categories':
 * @property integer $id
 * @property string $title
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Categories extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'categories';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, status', 'required'),
            array('title','unique'),
			array('status, created_at, updated_at', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>255),
			// The following rule is used by search().
			array('id, title, status, created_at, updated_at', 'safe', 'on'=>'search'),
		);
	}

    public static function getStatuses($status = false){
        $statuses = array(Yii::t('admin_panel', 'Не показывать'), Yii::t('admin_panel', 'Показывать'));

        if(false !== $status) {
            if(isset($statuses[$status])){
                return $statuses[$status];
            } else {
                return Yii::t('admin_panel', 'Ошибка. Нет такого статуса');
            }
        }

        return $statuses;
    }

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
            'guide' => array(self::HAS_MANY, 'Guides', 'category_id', 'order' => 'created_at desc'),
            'enabledGuides' => array(
                self::HAS_MANY,
                'Guides',
                'category_id',
                'order' => 'created_at desc',
                'condition' => 'status=1'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Название',
			'status' => 'Статус',
			'created_at' => 'Создано',
			'updated_at' => 'Обновлено',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{

		$criteria=new CDbCriteria;

        $sort = new CSort();
        $sort->defaultOrder = 't.id asc';

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('created_at',$this->created_at);
		$criteria->compare('updated_at',$this->updated_at);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => $sort,
            'pagination' => array(
                'pageSize' => '5',
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Categories the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
