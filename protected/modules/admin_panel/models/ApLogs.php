<?php

/**
 * This is the model class for table "ap_logs".
 *
 * The followings are the available columns in table 'ap_logs':
 * @property integer $id
 * @property integer $user_id
 * @property string  $action
 * @property integer $area
 * @property integer $date
 * @property string  $entity
 * @property string  $entity_name
 * @property integer $entity_id
 */
class ApLogs extends CActiveRecord
{

    const LOGIN  = 'login' ;
    const LOGOUT = 'logout';
    const CREATE = 'create';
    const UPDATE = 'update';
    const DELETE = 'delete';

    const ADMIN_PANEL = 1;
    const OPERATOR_PANEL = 2;
    const ADMIN = 3;
    const SITE = 4;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'ap_logs';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('user_id', 'required'),
			array('user_id, area, date', 'numerical', 'integerOnly'=>true),
			array('action, entity', 'length', 'max'=>255),
			array('id, user_id, action, area, date, entity, entity_id', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
            'user' => array(self::BELONGS_TO, 'Operators', 'user_id'),
		);
	}

    public function getLogRow(){
        $name = isset($this->user->login) ? $this->user->login : '%name%';
        $row = Yii::t('admin_panel', 'Пользователь ') . $name . ' ';
        switch($this->action) {
            case self::LOGIN:
                $action = Yii::t('admin_panel', 'вошел в систему');
                break;
            case self::LOGOUT:
                $action = Yii::t('admin_panel', 'вышел из системы');
                break;
            case self::CREATE:
                $action = Yii::t('admin_panel', 'добавил ');
                break;
            case self::UPDATE:
                $action = Yii::t('admin_panel', 'отредактировал ');
                break;
            case self::DELETE:
                $action = Yii::t('admin_panel', 'удалил ');
                break;
            default:
                return false;
                break;
        }
        $row .= $action;
        $name = $this->entity_name != '' ? $this->entity_name : ' #' .$this->entity_id;
        if($this->entity != '')
            $row .= $this->entity . ' ' . $name;

        return $row;
    }

    public function log($action, $params = array()){

        $this->user_id = Yii::app()->user->id;
        $this->date = time();
        $this->area = isset($params['area']) ? $params['area'] : self::ADMIN_PANEL;
        $this->entity = isset($params['entity']) ? $params['entity'] : '';
        $this->entity_id = isset($params['entity_id']) ? $params['entity_id'] : 0;
        $this->entity_name = isset($params['entity_name']) ? $params['entity_name'] : '';
        $this->action = $action;
        if($this->validate())
            $this->save();
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'user_id' => 'Пользователь',
			'action' => 'Действие',
			'area' => 'Зона',
			'date' => 'Дата/Время',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

        $sort = new CSort();
        $sort->defaultOrder = 't.date desc';

		$criteria->compare('id',$this->id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('action',$this->action, true);
		$criteria->compare('area',$this->area);
		$criteria->compare('date',$this->date);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => $sort,
            'pagination' => array(
                'pageSize' => '15',
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ApLogs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
