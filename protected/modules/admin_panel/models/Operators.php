<?php

/**
 * This is the model class for table "operators".
 *
 * The followings are the available columns in table 'operators':
 * @property integer $id
 * @property string $login
 * @property string $password
 * @property string $email
 * @property integer $type
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $avatar
 */
class Operators extends CActiveRecord
{
    const DISABLED = 4;
    const ADMIN = 1;
    const OPERATOR = 2;
    const SOPERATOR = 3;

    public $pwd;
	public $uplFile;

	public function getStringRole(){
		$roles = array(
			self::DISABLED =>'disabled',
			self::ADMIN =>'admin',
			self::OPERATOR => 'operator',
			self::SOPERATOR => 'soperator',

		);
		return $roles[$this->type];
	}

    public static function getTypes($type = false){
        $types = array(
            self::DISABLED => Yii::t('admin_panel', 'disabled'),
            self::ADMIN => Yii::t('admin_panel', 'admin'),
            self::OPERATOR => Yii::t('admin_panel', 'operator'),
            self::SOPERATOR => Yii::t('admin_panel', 'super operator'),
        );
        if($type){
            return isset($types[$type]) ? $types[$type] : Yii::t('admin_panel', 'Unknown type');
        }else {
            return $types;
        }
    }

	public function getAvatar($style= ''){
		if(is_file(WATSON . '/uploadFiles/operators/' . $this->id . '/' . $this->avatar)){
			return "<img src='/uploadFiles/operators/$this->id/$this->avatar' style='$style'/>";
		} else {
			return "<img src='/uploadFiles/noavatar.png' style='$style' />";
		}
	}

	public static function getSenderAvatar($id){
		$sender = self::model()->findByPk(Chat::model()->findByPk($id)->operator_id);

		return $sender->getAvatar();
	}
    public static function getOperatorLogin($id){
        $sender = self::model()->findByPk(Chat::model()->findByPk($id)->operator_id);

        return $sender->login;
    }

    public static function getDate($date){

        return date('Y-m-d H:i:s', $date);
    }

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'operators';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('login, password, email, type, pwd', 'required'),
            array('login, email', 'unique'),
			array('type', 'numerical', 'integerOnly' => true),
			array('login, password, email', 'length', 'max' => 255),
			array('uplFile', 'file', 'allowEmpty' => true, 'types'=>'jpg, gif, png, jpeg, bmp', 'safe' => false),
			array('pwd, id, login, password, email, type, created_at, updated_at, avatar', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'login' => 'Логин',
			'password' => 'Пароль',
			'pwd' => 'Пароль',
			'email' => 'Email',
			'type' => 'Тип',
			'created_at' => 'Created At',
			'updated_at' => 'Updated At',
			'uplFile' => 'Avatar',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{

		$criteria=new CDbCriteria;

        $sort = new CSort();
        $sort->defaultOrder = 't.id asc';

		$criteria->compare('id',$this->id);
		$criteria->compare('login',$this->login,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('created_at',$this->created_at);
		$criteria->compare('updated_at',$this->updated_at);
		$criteria->compare('avatar',$this->avatar);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => $sort,
            'pagination' => array(
                'pageSize' => '5',
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Operators the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
