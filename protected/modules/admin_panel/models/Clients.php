<?php

/**
 * This is the model class for table "clients".
 *
 * The followings are the available columns in table 'clients':
 * @property integer $id
 * @property string $fullname
 * @property integer $gender
 * @property string $phone
 * @property string $email
 * @property string $photo
 * @property integer $status
 * @property integer $pay_date
 * @property string $password
 * @property string $code
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $country
 * @property string $card_id
 * @property string $fullname_uk
 * @property string $bitrhdate
 * @property string $vuz
 * @property string $address_reg
 * @property string $passport
 * @property string $date_passport
 * @property string $passport_till
 * @property string $tax_number
 * @property integer $vpu
 * @property string $vpu_serie
 * @property string $vpu_number
 * @property string $vpu_till
 * @property string $vpu_from
 * @property string $district (loc1, Харьков или район Харьковской области)
 * @property string $street
 * @property string $house
 * @property string $flat
 * @property string $city (населенный пункт в Харьковской области)
 * @property string $oblast
 * @property string $area
 */
class Clients extends CActiveRecord
{
    public $pwd;
    public $file;

    public $checkLang;

    const MALE = 1;
    const FEMALE = 2;

    const PAID_STATUS = 1;
    const NOT_PAID_STATUS = 0;

    const NOT_ACTIVE_STATUS = 0;
    const OFFER_ACCEPTED = 1;
    const GET_INVOICE = 2;
    const PAID_INVOICE = 3;
    const GET_CARD = 4;
    const CONTRACT_READY = 5;

    const AMOUNT=2500;

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'clients';
	}

    public static function getPaidTill(){
        $date =  self::model()->findByPk(Yii::app()->user->num)->pay_date;
        $date = date("Y-m-d", $date);
        $date = date("Y-m-d", strtotime(date("Y-m-d", strtotime($date . " + 1 year"))));
        return $date;
    }

    public static function getCurrentUserStatus() {
        if(isset(Yii::app()->user->num) && !is_null(self::model()->findByPk(Yii::app()->user->num))) {
            return self::model()->findByPk(Yii::app()->user->num)->status;
        } else {
            return false;
        }
    }

    public static function getCurrentUserCardId(){
        if(isset(Yii::app()->user->num) && !is_null(self::model()->findByPk(Yii::app()->user->num))) {
            return self::model()->findByPk(Yii::app()->user->num)->card_id;
        } else {
            return false;
        }
    }

    public function getOnlyDistrict() {
        return $this->district == 1 ? Country::getHDistrict($this->area) : Country::getDistrict($this->district);
    }

    public function isOverDate(){
        if(in_array($this->status, array(Clients::NOT_ACTIVE_STATUS, Clients::OFFER_ACCEPTED, Clients::GET_INVOICE))){
            return true;
        } else {
            return false;
        }
    }

    public function getListClients($id){
      $list  = Yii::app()->session['list_clients_tabs'];
      if (empty($list)) {
          $list[] = $id;
      }
       elseif ($list) {
            $key = array_search($id, $list);
            if ($key) {
                $list[$key] = $id;
            }
            else {
                $i = count($list);
                switch ($i) {
                    case 0:
                        $list[$i] = $id;
                        break;
                    case 1:
                        $list[$i] = $id;
                        break;
                    case 2:
                        $list[$i] = $id;
                        break;
                    case 3:
                        $list[0] = $list[1];
                        $list[1] = $list[2];
                        $list[2] = $id;
                        break;
                }

            }
        }
      $uniqmass = array_unique($list);
      Yii::app()->session['list_clients_tabs'] = $uniqmass;
        return $uniqmass;
    }
    public function getTabOpen($id){
        $list  = Yii::app()->session['list_clients_tabs'];
        if ($list) {
            $key = array_search($id, $list);
            if ($key) {
                return true;
            }
        }
        return false;
    }
    public function removeListClient($id){
        $list  = Yii::app()->session['list_clients_tabs'];
        if(is_null($list)){
            $list = array();
        }
        if ($list) {
            $key = array_search($id,$list);
                unset($list[$key]);
                sort($list);
        }

        Yii::app()->session['list_clients_tabs'] = $list;
        return $list;

    }
    public function getAvatar($class = ''){
        $file = WATSON . '/uploadFiles/clientsPhoto/' . $this->id . '/' . $this->photo;

        if(is_file($file)){
            $file = '/uploadFiles/clientsPhoto/' . $this->id . '/' . $this->photo;
        } else {
            $file = "/uploadFiles/noavatar.png";
        }

        return '<img src="' . $file . '" class="' . $class . '"  alt=""/> ';
    }
    public function getOperatorAvatar($client) {
        if(!empty($client->doc[0]->photo) && $client->doc[0]->photo != '') {
                 $src= '/uploadFiles/clientsPhoto/' . $client->id . '/' . $client->doc[0]->photo;
             } else {
                  $src = '/uploadFiles/noavatar.png';
             }
        return $src;
          }

    public function getRegAddr(){
        $addr = '';
        $addr .= Country::getDistrict($this->district) ;
        if($this->district != 1) {
            $addr .= ' ' . Yii::t('front','район') . ', г.' . $this->city;
        }
        $addr .=  ', ' . $this->street;
        return $addr;
    }

    public function getLangs(){
        $lang = '';
        foreach($this->lang as $l){
            $lang .= ClientsLanguages::getLang($l->lang) . ', ';
        }
        if($lang != ''){
            $lang = substr($lang, 0, -2);
        }
        return $lang;
    }

    public function getCountry($class="op_client_country"){
        $dir = '/images/flags/';
        $country_array = Country::$countries;

        foreach ($country_array as $key=>$country) {
          if ($country_array[$key]['title']==$this->country){
              return '<img src="'.$dir . $country_array[$key]['img'].'" alt="'.$country_array[$key]['title'].'" title="'.$country_array[$key]['title'].'" class="' . $class . '" />';
          }
        }
        return '';
    }

    //Заготовка для расширения. Не удалять.
    public function getIsVpuExpired(){
        if(false){
            return true;
        } else {
            return false;
        }
    }

    public function getCountryName(){
        $country_array = Country::$countries;

        foreach ($country_array as $key=>$country) {
            if ($country_array[$key]['title']==$this->country){
                return $country_array[$key]["title"];
            }
        }
        return $this->country;

    }
    public function get_passport_name(){
        $name = $this->fullname;
        if(stristr($name, ',') === FALSE) {
            if(stristr($name, ' ') !== FALSE) {
                $name = explode(' ', $name);
                $name = $name[0] . ', ' . $name[1];
            }
        }
        return $name;
    }

    public function getCountryFront($c){
        $country = Country::getCountryNameById($c);
        return $country;
    }

    public static function getStatuses($status = false){
        $statuses = array(
            self::NOT_ACTIVE_STATUS => 'Не активен',
            self::OFFER_ACCEPTED => 'Офферта принята',
            self::GET_INVOICE => 'Счет выставлен',
            self::PAID_INVOICE => 'Счет оплачен',
            self::GET_CARD => 'ID-карта выдана',
            self::CONTRACT_READY => 'Договор подписан',
        );

        if(false === $status)
            return $statuses;

        if(isset($statuses[$status]))
            return $statuses[$status];

        return Yii::t('admin_panel', 'Неизвестный статус');
    }

    public function getDocName($doc){
        $doc = explode('&_&', $doc);
        if(isset($doc[1])) {
            $doc = $doc[1];
        } else {
            $doc = '';
        }
        return $doc;
    }

    public static function getGenders($g = false){
        $genders = array(
            self::MALE => Yii::t('admin_panel','М'),
            self::FEMALE => Yii::t('admin_panel','Ж'),
        );
        if(false === $g)
            return $genders;

        if (isset($genders[$g])) {
            return $genders[$g];
        } else {
            return 'unknown error';
        }
    }

    public static function getFullGenders($g = false){
        $genders = array(
            self::MALE => Yii::t('admin_panel','Мужской'),
            self::FEMALE => Yii::t('admin_panel','Женский'),
        );
        if(false === $g)
            return $genders;

        if (isset($genders[$g])) {
            return $genders[$g];
        } else {
            return 'unknown error';
        }
    }

	/**
	 * @return array validation rules for model attributes.
     */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('email, password', 'required'),
            array('email','unique'),
			array('area, district, street, house, flat, oblast, city, vpu_from, vpu_till, vpu_number, vpu_serie, vpu, tax_number, passport_till, date_passport, passport, address_reg, fullname_uk, bitrhdate, vuz, card_id, country, id, fullname, gender, phone, email, photo, status, pay_date, password, code, created_at, updated_at', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
            'doc' => array(self::HAS_MANY, 'ClientsDocs', 'client_id'),
            'lang' => array(self::HAS_MANY, 'ClientsLanguages', 'client_id'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => Yii::t('front', 'ID'),
			'card_id' => Yii::t('front', 'ID карты'),
			'fullname' => Yii::t('front', 'ФИО (лат.)'),
			'fullname_uk' => Yii::t('front', 'ФИО (укр.)'),
			'gender' => Yii::t('front', 'Пол'),
			'phone' => Yii::t('front', 'Телефон'),
			'email' => Yii::t('front', 'Email'),
			'photo' => Yii::t('front', 'Фото'),
			'status' => Yii::t('front', 'Статус оплаты'),
			'pay_date' => Yii::t('front', 'Дата оплаты'),
			'password' => Yii::t('front', 'Пароль'),
			'pwd' => Yii::t('front', 'Пароль'),
			'file' => Yii::t('front', 'Фото'),
			'code' => Yii::t('front', 'Кодовое слово'),
			'created_at' => Yii::t('front', 'Создан'),
			'updated_at' => Yii::t('front', 'Обновлен'),
			'country' => Yii::t('front', 'Гражданство'),
			'vuz' => Yii::t('front', 'ВУЗ'),
			'passport' => Yii::t('front', 'Паспорт'),
			'date_passport' => Yii::t('front', 'Дата выдачи паспорта'),
			'passport_till' => Yii::t('front', 'Срок действия паспорта'),
			'tax_number' => Yii::t('front', 'И/Н'),
			'vpu' => Yii::t('front', 'ВНЖ'),
			'vpu_serie' => Yii::t('front', 'ВНЖ (серия)'),
			'vpu_number' => Yii::t('front', 'ВНЖ (номер)'),
			'vpu_till' => Yii::t('front', 'ВНЖ (срок действия)'),
			'vpu_from' => Yii::t('front', 'ВНЖ (кем выдан)'),
			'district' => Yii::t('front', 'Район'),
			'street' => Yii::t('front', 'Адрес'),
			'house' => Yii::t('front', 'Дом'),
			'flat' => Yii::t('front', 'Квартира'),
			'city' => Yii::t('front', 'Город'),
			'oblast' => Yii::t('front', 'Область'),
			'area' => Yii::t('front', 'Район города'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search($show = false)
	{
		$criteria = new CDbCriteria;

        $sort = new CSort();
        $sort->defaultOrder = 't.id desc';

		$criteria->compare('id', $this->id);
		$criteria->compare('fullname', $this->fullname,true);
		$criteria->compare('gender', $this->gender);
		$criteria->compare('phone', $this->phone,true);
		$criteria->compare('email', $this->email,true);
		$criteria->compare('photo', $this->photo,true);
		$criteria->compare('status', $this->status);
		$criteria->compare('pay_date', $this->pay_date);
		$criteria->compare('password', $this->password,true);
		$criteria->compare('code', $this->code,true);
		$criteria->compare('created_at', $this->created_at);
		$criteria->compare('updated_at', $this->updated_at);
		$criteria->compare('fullname_uk', $this->fullname_uk, true);
		$criteria->compare('bitrhdate', $this->bitrhdate, true);
		$criteria->compare('vuz', $this->vuz, true);
		$criteria->compare('address_reg', $this->address_reg, true);
		$criteria->compare('passport', $this->passport, true);
		$criteria->compare('date_passport', $this->date_passport, true);
		$criteria->compare('passport_till', $this->passport_till, true);
		$criteria->compare('tax_number', $this->tax_number, true);
		$criteria->compare('vpu', $this->vpu, true);
		$criteria->compare('vpu_serie', $this->vpu_serie, true);
		$criteria->compare('vpu_number', $this->vpu_number, true);
		$criteria->compare('vpu_till', $this->vpu_till, true);
		$criteria->compare('vpu_from', $this->vpu_from, true);
		$criteria->compare('country', $this->country, true);
		$criteria->compare('district', $this->district, true);
		$criteria->compare('street', $this->street, true);
		$criteria->compare('house', $this->house, true);
		$criteria->compare('flat', $this->flat, true);
		$criteria->compare('city', $this->city, true);
		$criteria->compare('oblast', $this->oblast, true);
		$criteria->compare('area', $this->area, true);
		$criteria->compare('card_id', $this->card_id, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
            'sort' => $sort,
            'pagination' => array(
                'pageSize' => '5',
            ),
		));
	}

    public function searchOP($show = false)
    {
        $criteria = new CDbCriteria;

        $sort = new CSort();
        $sort->defaultOrder = 't.id desc';

        $criteria->compare('id',  $this->id);
        $criteria->compare('fullname', $this->fullname,true);
        $criteria->compare('gender', $this->gender);
        $criteria->compare('phone', $this->phone,true);
        $criteria->compare('email', $this->email,true);
        $criteria->compare('photo', $this->photo,true);
        $criteria->compare('status', $this->status);
        $criteria->compare('pay_date', $this->pay_date);
        $criteria->compare('password', $this->password,true);
        $criteria->compare('code', $this->code,true);
        $criteria->compare('created_at', $this->created_at);
        $criteria->compare('updated_at', $this->updated_at);
        $criteria->compare('fullname_uk', $this->fullname_uk);
        $criteria->compare('bitrhdate', $this->bitrhdate);
        $criteria->compare('vuz', $this->vuz);
        $criteria->compare('address_reg', $this->address_reg);
        $criteria->compare('passport', $this->passport);
        $criteria->compare('date_passport', $this->date_passport);
        $criteria->compare('passport_till', $this->passport_till);
        $criteria->compare('tax_number', $this->tax_number);
        $criteria->compare('vpu', $this->vpu);
        $criteria->compare('vpu_serie', $this->vpu_serie);
        $criteria->compare('vpu_number', $this->vpu_number);
        $criteria->compare('vpu_till', $this->vpu_till);
        $criteria->compare('vpu_from', $this->vpu_from);
        $criteria->compare('country', $this->country);

        if(!$show) {
            $criteria->addCondition('status<>:status');
            $criteria->params = array(
                ':status' => Clients::NOT_ACTIVE_STATUS,
            );
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => $sort,
            'pagination' => array(
                'pageSize' => '10',
            ),
        ));
    }

    public function getClientsAsSearchOP($s) {
        $criteria = new CDbCriteria;

        $sort = new CSort();
        $sort->defaultOrder = 't.id desc';

        if($s != '') {
            $criteria->addCondition("card_id LIKE '%$s%' OR fullname LIKE '%$s%'");
        }

        return new CActiveDataProvider($this, array(
            'criteria' => $criteria,
            'sort' => $sort,
            'pagination' => array(
                'pageSize' => '0',
            ),
        ));
    }

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Clients the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function sendRegSuccessLetter($msg){
        $from = Settings::model()->find()->admin_email;
        $headers  = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=UTF-8" . "\r\n";
        $headers .= "From: $from";
        mail($this->email, Yii::t('front', 'Успешная регистрация'), $msg, $headers);
    }

    public static function sendChangePass(){
        $from = Settings::model()->find()->admin_email;
        $msg = "<p>Для учетной записи, к которой привязан данный E-mail, было запрошено восстановление пароля.
			Если это были не вы, просто проигнорируйте это письмо. Если же это были вы, пожалуйста, пройдите
			по ссылке ниже для установки нового пароля. </p>";
        $post = Yii::app()->request->getPost('email');
        $msg .= '<p>' .
            Yii::app()->request->getBaseUrl(true) .
            Yii::app()->createUrl('/site/changePass', array('e' => $post)) .
            '</p>';
        $headers  = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=UTF-8" . "\r\n";
        $headers .= "From: $from";
        mail($post, Yii::t('front', 'Восстановление пароля'), $msg, $headers);
    }

    public static function sendQuestion()
    {
        $post = Yii::app()->request->getPost('Send');
        $from = $post['email'];
        $user = Clients::model()->findByPk(Yii::app()->user->num);
        $msg = 'Вопрос от клиента ' . $user->fullname_uk .'(#' . $user->card_id . '): <br>';
        $msg .= $post['text'];
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=UTF-8" . "\r\n";
        $headers .= "From: $from";

        mail(Settings::model()->find()->contact_email, Yii::t('front', 'Вопрос от клиента'), $msg, $headers);
    }

    public static function getSearchParamsOP(){
        $criteria=new CDbCriteria;
        $criteria->order = 't.id desc';
        $clients = Clients::model()->findAll($criteria);
        $noVpu = 0;
        $overDate = 0;
        $countries = array();
        $districts = array();
        $khdistricts = array();
        $vuzez = array();
        foreach($clients as $c){
            if($c->isOverDate())
                $overDate++;
            if($c->vpu == 0)
                $noVpu++;
            if(isset($countries[$c->country])){
                $countries[$c->country]++;
            } else {
                $countries[$c->country] = 1;
            }
            if(isset($districts[$c->district])){
                $districts[$c->district]++;
            } else {
                $districts[$c->district] = 1;
            }
            if(isset($khdistricts[$c->area])){
                $khdistricts[$c->area]++;
            } else {
                $khdistricts[$c->area] = 1;
            }
            if(isset($vuzez[$c->vuz])){
                $vuzez[$c->vuz]++;
            } else {
                $vuzez[$c->vuz] = 1;
            }
        }
        $params = array(
            'clients' => $clients,
            'noVpu' => $noVpu,
            'overDate' => $overDate,
            'countries' => $countries,
            'districts' => $districts,
            'khdistricts' => $khdistricts,
            'vuzez' => $vuzez,
        );
        return $params;
    }

    public static function getOPFilterParams($form, $card_id = ''){
        $vpu = false;
        $pay = false;

        $countries = '';
        $districts = '';
        $areas = '';
        $vuzes = '';
        $c = array();
        $d = array();
        $a = array();
        $v = array();
        foreach($form as $item){
            if($item['name'] == 'vpu'){
                $vpu = true;
            } elseif($item['name'] == 'pay') {
                $pay = true;
            } else {
                $item = explode("_", $item['name']);
                $attr = $item[0];
                $val = '';

                foreach ($item as $key => $str) {
                    if ($key != 0)
                        $val .= $str;
                }

                switch ($attr) {
                    case('Country'):
                        $countries .= "'" . $val . "',";
                        $c[$val] = $val;
                        break;
                    case('District'):
                        $districts .= $val . ',';
                        $d[$val] = Country::getDistrict($val);
                        break;
                    case('Area'):
                        $areas .= $val . ',';
                        $a[$val] = Country::getHDistrict($val);
                        break;
                    case('Vuz'):
                        $vuzes .= $val . ',';
                        $v[$val] = Vuz::getVuses($val);
                        break;
                }
            }
        }

        $criteria = new CDbCriteria();
        $criteria->condition = '';
        $criteria->params = array();
        if($countries != ''){
            $countries = substr($countries, 0, -1);
            $criteria->condition .= '(country IN (' . $countries . ')) ';
        }
        if($districts != ''){
            $districts = substr($districts, 0, -1);
            $criteria->condition .= 'AND (district IN (' . $districts . ')) ';
        }
        if($areas != ''){
            $areas = substr($areas, 0, -1);
            $criteria->condition .= 'AND (area IN (' . $areas . ')) ';
        }
        if($vuzes != ''){
            $vuzes = substr($vuzes, 0, -1);
            $criteria->condition .= 'AND (vuz in (' . $vuzes . ')) ';
        }

        if(false !== $vpu){
            $criteria->condition .= 'AND (vpu=0)';
        }
        if(false !== $pay){
            $criteria->condition .=
                "AND (status IN (" .
                Clients::NOT_ACTIVE_STATUS . "," .
                Clients::OFFER_ACCEPTED . "," .
                Clients::GET_INVOICE .
                "))";
        }

        if(substr($criteria->condition, 0, 3) == 'OR '){
            $criteria->condition = substr($criteria->condition, 3);
        }

        if(substr($criteria->condition, 0, 4) == 'AND '){
            $criteria->condition = substr($criteria->condition, 4);
        }
        if($card_id != ''){
            if($criteria->condition != ''){
                $criteria->condition .= ' AND ';
            }
            $criteria->condition .= "fullname LIKE '%". $card_id . "%' OR card_id LIKE '%" . $card_id . "%'";
        }
        $clients = Clients::model()->findAll($criteria);

        $overDate = 0;
        $noVpu = 0;


        foreach($clients as $cl){
            if($cl->isOverDate())
                $overDate++;
            if($cl->vpu == 0)
                $noVpu++;
        }

        $params = array(
            'clients' => $clients,
            'noVpu' => $noVpu,
            'overDate' => $overDate,
            'output' => array('country' => $c, 'district' => $d, 'area' => $a, 'vuz' => $v),
            'pay' => $pay,
            'vpu' => $vpu,
        );
        return $params;
    }

    public static function getAutoCompleteResult($clients) {
        $result = array();
        foreach($clients as $client) {
            $label = $client['fullname'] . '(#' .$client['card_id'] . ')' ;
            $result[] = array('id'=>$client['id'], 'label'=>$label, 'value'=>$label);
        }
        return $result;
    }

    public static function getAutoCompleteResultCardId($clients) {
        $result = array();
        $model = new Clients();
        foreach($clients as $client) {
            $src =$model->getOperatorAvatar($client);
            $result[] = array('id'=>$client['id'], 'label'=>$client['fullname'], 'card_id'=>$client['card_id'], 'src'=>$src);
        }
        return $result;
    }

    public function getPayDate(){
        if(is_null($this->pay_date)){
            $date = Yii::t('operator_panel', 'Не оплачено');
        } else {
            $date = date('d/m/Y', strtotime('+1 years', $this->pay_date));
        }
        return $date;
    }

}
