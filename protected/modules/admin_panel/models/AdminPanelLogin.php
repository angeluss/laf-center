<?php

/**
 * LoginForm class.
 * LoginForm is the data structure for keeping
 * user login form data. It is used by the 'login' action of 'SiteController'.
 */
class AdminPanelLogin extends CFormModel
{
	public $login;
	public $password;
    public $userModel;
	private $_identity;

	/**
	 * Declares the validation rules.
	 * The rules state that username and password are required,
	 * and password needs to be authenticated.
	 */
	public function rules()
	{
		return array(
			array('login', 'required', 'message' => Yii::t('Front', 'Insert Login or Email')),
			array('password', 'required', 'message' => Yii::t('Front', 'Password is incorrect')),
			array('login', 'match', 'pattern' => '/^[0-9a-zA-Z\-]{1,}$/', 'message' => Yii::t('Front', 'Insert Your login using latin alphabet')),
			// password needs to be authenticated
			array('password', 'authenticate'),
		);
	}

	/**
	 * Declares attribute labels.
	 */
	public function attributeLabels()
	{
		return array(
			'login' => Yii::t('Front', 'Логин'),
			'password' => Yii::t('Front', 'Пароль'),
			'shortSession'=>Yii::t('Front', 'Запомнить меня'),
		);
	}

	/**
	 * Authenticates the password.
	 * This is the 'authenticate' validator as declared in rules().
	 */
	public function authenticate($attribute, $params)
	{
		if(!$this->hasErrors())
		{
			$this->password = trim($this->password);
			$this->_identity=new APUserIdentity($this->login,md5($this->password));
			if(!$this->_identity->authenticate()){
				$this->addError('login', Yii::t('Front', 'E-Mail or Password is incorrect'));
			}
		}
	}

	public function login()
	{
		if($this->_identity===null)
		{
			$this->_identity=new APUserIdentity($this->login,$this->password);
			$this->_identity->authenticate();
		}
		if($this->_identity->errorCode===APUserIdentity::ERROR_NONE)
		{
			$duration= 60 * 15;
			Yii::app()->user->login($this->_identity,$duration);
			return true;
		}
		else
			return false;
	}
}
