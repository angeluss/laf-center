<?php

class Country extends CActiveRecord
{
	public static $countries = array(
		array(
			'title' => 'Абхазия',
			'code' => 'ABH', // didn't find real 3-letter code
			'img' => 'Abkhazia.png',
		),
		array(
			'title' => 'Австралия',
			'code' => 'AUS',
			'img' => 'Australia.png',
		),
		array(
			'title' => 'Австрия',
			'code' => 'AUT',
			'img' => 'Austria.png',
		),
		array(
			'title' => 'Азербайджан',
			'code' => 'AZE',
			'img' => 'azerbaijan_small_flag.gif',
		),
		array(
			'title' => 'Албания',
			'code' => 'ALB',
			'img' => 'Albania.png',
		),
		array(
			'title' => 'Алжир',
			'code' => 'DZA',
			'img' => 'Algeria.png',
		),
		array(
			'title' => 'Американские Виргинские острова',
			'code' => 'VIR',
			'img' => 'US-Virgin-Islands.png',
		),
		array(
			'title' => 'Американское Самоа',
			'code' => 'ASM',
			'img' => 'American-Samoa.png',
		),
		array(
			'title' => 'Ангилья',
			'code' => 'AIA',
			'img' => 'Anguilla.png',
		),
		array(
			'title' => 'Ангола',
			'code' => 'AGO',
			'img' => 'Angola.png',
		),
		array(
			'title' => 'Андорра',
			'code' => 'AND',
			'img' => 'Andorra.png',
		),
		array(
			'title' => 'Антарктида',
			'code' => 'ATA',
			'img' => 'Antarctica.png',
		),
		array(
			'title' => 'Антигуа и Барбуда',
			'code' => 'ATG',
			'img' => 'Antigua-and-Barbuda.png',
		),
		array(
			'title' => 'Аргентина',
			'code' => 'ARG',
			'img' => 'Argentina.png',
		),
		array(
			'title' => 'Армения',
			'code' => 'ARM',
			'img' => 'Armenia.png',
		),
		array(
			'title' => 'Аруба',
			'code' => 'ABW',
			'img' => 'Aruba.png',
		),
		array(
			'title' => 'Афганистан',
			'code' => 'AFG',
			'img' => 'Afghanistan.png',
		),
		array(
			'title' => 'Багамские острова',
			'code' => 'BHS',
			'img' => 'Bahamas.png',
		),
		array(
			'title' => 'Бангладеш',
			'code' => 'BGD',
			'img' => 'Bangladesh.png',
		),
		array(
			'title' => 'Барбадос',
			'code' => 'BRB',
			'img' => 'Barbados.png',
		),
		array(
			'title' => 'Бахрейн',
			'code' => 'BHR',
			'img' => 'Bahrain.png',
		),
		array(
			'title' => 'Белиз',
			'code' => 'BLZ',
			'img' => 'Belize.png',
		),
		array(
			'title' => 'Белоруссия',
			'code' => 'BLR',
			'img' => 'Belarus.png',
		),
		array(
			'title' => 'Бельгия',
			'code' => 'BEL',
			'img' => 'Belgium.png',
		),
		array(
			'title' => 'Бенин',
			'code' => 'BEN',
			'img' => 'Benin.png',
		),
		array(
			'title' => 'Бермуды',
			'code' => 'BMU',
			'img' => 'Bermuda.png',
		),
		array(
			'title' => 'Болгария',
			'code' => 'BGR',
			'img' => 'Bulgaria.png',
		),
		array(
			'title' => 'Боливия',
			'code' => 'BOL',
			'img' => 'Bolivia.png',
		),
		array(
			'title' => 'Бонайре, Синт-Эстатиус и Саба',
			'code' => 'BES',
			'img' => 'Unknown.png',
		),
		array(
			'title' => 'Босния и Герцеговина',
			'code' => 'BIH',
			'img' => 'Bosnia-and-Herzegovina.png',
		),
		array(
			'title' => 'Ботсвана',
			'code' => 'BWA',
			'img' => 'Botswana.png',
		),
		array(
			'title' => 'Бразилия',
			'code' => 'BRA',
			'img' => 'Brazil.png',
		),
		array(
			'title' => 'Британская территория в Индийском океане',
			'code' => 'IOT',
			'img' => 'British-Antarctic-Territory.png',
		),
		array(
			'title' => 'Британские Виргинские острова',
			'code' => 'VGB',
			'img' => 'British-Virgin-Islands.png',
		),
		array(
			'title' => 'Бруней',
			'code' => 'BRN',
			'img' => 'Brunei.png',
		),
		array(
			'title' => 'Буве',
			'code' => 'BVT',
			'img' => 'Unknown.png',
		),
		array(
			'title' => 'Буркина-Фасо',
			'code' => 'BFA',
			'img' => 'Burkina-Faso.png',
		),
		array(
			'title' => 'Бурунди',
			'code' => 'BDI',
			'img' => 'Burundi.png',
		),
		array(
			'title' => 'Бутан',
			'code' => 'BTN',
			'img' => 'Bhutan.png',
		),
		array(
			'title' => 'Вануату',
			'code' => 'VUT',
			'img' => 'Vanuatu.png',
		),
		array(
			'title' => 'Ватикан',
			'code' => 'VAT',
			'img' => 'Vatican-City.png',
		),
		array(
			'title' => 'Великобритания',
			'code' => 'GBR',
			'img' => 'United-Kingdom.png',
		),
		array(
			'title' => 'Венгрия',
			'code' => 'HUN',
			'img' => 'Hungary.png',
		),
		array(
			'title' => 'Венесуэла',
			'code' => 'VEN',
			'img' => 'Venezuela.png',
		),
		array(
			'title' => 'Восточный Тимор (Тимор-Лешти)',
			'code' => 'TLS',
			'img' => 'East-Timor.png',
		),
		array(
			'title' => 'Вьетнам',
			'code' => 'VNM',
			'img' => 'Vietnam.png',
		),
		array(
			'title' => 'Габон',
			'code' => 'GAB',
			'img' => 'Gabon.png',
		),
		array(
			'title' => 'Гаити',
			'code' => 'HTI',
			'img' => 'Haiti.png',
		),
		array(
			'title' => 'Гайана',
			'code' => 'GUY',
			'img' => 'Guyana.png',
		),
		array(
			'title' => 'Гамбия',
			'code' => 'GMB',
			'img' => 'Gambia.png',
		),
		array(
			'title' => 'Гана',
			'code' => 'GHA',
			'img' => 'Ghana.png',
		),
		array(
			'title' => 'Гваделупа',
			'code' => 'GLP',
			'img' => 'Unknown.png',
		),
		array(
			'title' => 'Гватемала',
			'code' => 'GTM',
			'img' => 'Guatemala.png',
		),
		array(
			'title' => 'Гвинея',
			'code' => 'GIN',
			'img' => 'Guinea.png',
		),
		array(
			'title' => 'Гвинея-Бисау',
			'code' => 'GNB',
			'img' => 'Guinea-Bissau.png',
		),
		array(
			'title' => 'Германия',
			'code' => 'DEU',
			'img' => 'Germany.png',
		),
		array(
			'title' => 'Гернси',
			'code' => 'GGY',
			'img' => 'Guernsey.png',
		),
		array(
			'title' => 'Гибралтар',
			'code' => 'GIB',
			'img' => 'Gibraltar.png',
		),
		array(
			'title' => 'Гондурас',
			'code' => 'HND',
			'img' => 'Honduras.png',
		),
		array(
			'title' => 'Гонконг',
			'code' => 'HKG',
			'img' => 'Hong-Kong.png',
		),
		array(
			'title' => 'Гренада',
			'code' => 'GRD',
			'img' => 'Grenada.png',
		),
		array(
			'title' => 'Гренландия',
			'code' => 'GRL',
			'img' => 'Greenland.png',
		),
		array(
			'title' => 'Греция',
			'code' => 'GRC',
			'img' => 'Greece.png',
		),
		array(
			'title' => 'Грузия',
			'code' => 'GEO',
			'img' => 'Georgia.png',
		),
		array(
			'title' => 'Гуам',
			'code' => 'GUM',
			'img' => 'Guam.png',
		),
		array(
			'title' => 'Дания',
			'code' => 'DNK',
			'img' => 'Denmark.png',
		),
		array(
			'title' => 'Демократическая Республика Конго',
			'code' => 'COD',
			'img' => 'Democratic-Republic-of-the-Congo.png',
		),
		array(
			'title' => 'Джерси',
			'code' => 'JEY',
			'img' => 'Jersey.png',
		),
		array(
			'title' => 'Джибути',
			'code' => 'DJI',
			'img' => 'Djibouti.png',
		),
		array(
			'title' => 'Доминика',
			'code' => 'DMA',
			'img' => 'Dominica.png',
		),
		array(
			'title' => 'Доминикана',
			'code' => 'DOM',
			'img' => 'Dominican-Republic.png',
		),
		array(
			'title' => 'Египет',
			'code' => 'EGY',
			'img' => 'Egypt.png',
		),
		array(
			'title' => 'Замбия',
			'code' => 'ZMB',
			'img' => 'Zambia.png',
		),
		array(
			'title' => 'Западная Сахара (Сахарская Арабская Демократическая Республика)',
			'code' => 'ESH',
			'img' => 'Western-Sahara.png',
		),
		array(
			'title' => 'Зимбабве',
			'code' => 'ZWE',
			'img' => 'Zimbabwe.png',
		),
		array(
			'title' => 'Израиль',
			'code' => 'ISR',
			'img' => 'Israel.png',
		),
		array(
			'title' => 'Индия',
			'code' => 'IND',
			'img' => 'India.png',
		),
		array(
			'title' => 'Индонезия, Бали',
			'code' => 'IDN',
			'img' => 'Indonesia.png',
		),
		array(
			'title' => 'Иордания',
			'code' => 'JOR',
			'img' => 'Jordan.png',
		),
		array(
			'title' => 'Ирак',
			'code' => 'IRQ',
			'img' => 'Iraq.png',
		),
		array(
			'title' => 'Иран',
			'code' => 'IRN',
			'img' => 'Iran.png',
		),
		array(
			'title' => 'Ирландия',
			'code' => 'IRL',
			'img' => 'Ireland.png',
		),
		array(
			'title' => 'Исландия',
			'code' => 'ISL',
			'img' => 'Iceland.png',
		),
		array(
			'title' => 'Испания',
			'code' => 'ESP',
			'img' => 'Spain.png',
		),
		array(
			'title' => 'Италия',
			'code' => 'ITA',
			'img' => 'Italy.png',
		),
		array(
			'title' => 'Йемен',
			'code' => 'YEM',
			'img' => 'Yemen.png',
		),
		array(
			'title' => 'Кабо-Верде (Острова Зеленого Мыса)',
			'code' => 'CPV',
			'img' => 'Cape-Verde.png',
		),
		array(
			'title' => 'Казахстан',
			'code' => 'KAZ',
			'img' => 'Kazakhstan.png',
		),
		array(
			'title' => 'Каймановы острова',
			'code' => 'CYM',
			'img' => 'Cayman-Islands.png',
		),
		array(
			'title' => 'Камбоджа',
			'code' => 'KHM',
			'img' => 'Cambodia.png',
		),
		array(
			'title' => 'Камерун',
			'code' => 'CMR',
			'img' => 'Cameroon.png',
		),
		array(
			'title' => 'Канада',
			'code' => 'CAN',
			'img' => 'Canada.png',
		),
		array(
			'title' => 'Катар',
			'code' => 'QAT',
			'img' => 'Qatar.png',
		),
		array(
			'title' => 'Кения',
			'code' => 'KEN',
			'img' => 'Kenya.png',
		),
		array(
			'title' => 'Кипр',
			'code' => 'CYP',
			'img' => 'Cyprus.png',
		),
		array(
			'title' => 'Киргизия',
			'code' => 'KGZ',
			'img' => 'Kyrgyzstan.png',
		),
		array(
			'title' => 'Кирибати',
			'code' => 'KIR',
			'img' => 'Kiribati.png',
		),
		array(
			'title' => 'Китай',
			'code' => 'CHN',
			'img' => 'China.png',
		),
		array(
			'title' => 'КНДР',
			'code' => 'PRK',
			'img' => 'North-Korea.png',
		),
		array(
			'title' => 'Кокосовые острова (Килинг)',
			'code' => 'CCK',
			'img' => 'Cocos-Keeling-Islands.png',
		),
		array(
			'title' => 'Колумбия',
			'code' => 'COL',
			'img' => 'Colombia.png',
		),
		array(
			'title' => 'Коморские острова',
			'code' => 'COM',
			'img' => 'Comoros.png',
		),
		array(
			'title' => 'Корея',
			'code' => 'KOR',
			'img' => 'South-Korea.png',
		),
		array(
			'title' => 'Косово',
			'code' => 'KOS', // didn't find real 3-letter code
			'img' => 'Kosovo.png',
		),
		array(
			'title' => 'Коста-Рика',
			'code' => 'CRI',
			'img' => 'Costa-Rica.png',
		),
		array(
			'title' => 'Кот-д’Ивуар (Берег Слоновой Кости)',
			'code' => 'CIV',
			'img' => 'Cote-dIvoire.png',
		),
		array(
			'title' => 'Куба',
			'code' => 'CUB',
			'img' => 'Cuba.png',
		),
		array(
			'title' => 'Кувейт',
			'code' => 'KWT',
			'img' => 'Kuwait.png',
		),
		array(
			'title' => 'Кюрасао',
			'code' => 'CUW',
			'img' => 'Curacao.png',
		),
		array(
			'title' => 'Лаос',
			'code' => 'LAO',
			'img' => 'Laos.png',
		),
		array(
			'title' => 'Латвия',
			'code' => 'LVA',
			'img' => 'Latvia.png',
		),
		array(
			'title' => 'Лесото',
			'code' => 'LSO',
			'img' => 'Lesotho.png',
		),
		array(
			'title' => 'Либерия',
			'code' => 'LBR',
			'img' => 'Liberia.png',
		),
		array(
			'title' => 'Ливан',
			'code' => 'LBN',
			'img' => 'Lebanon.png',
		),
		array(
			'title' => 'Ливия',
			'code' => 'LBY',
			'img' => 'Libya.png',
		),
		array(
			'title' => 'Литва',
			'code' => 'LTU',
			'img' => 'Lithuania.png',
		),
		array(
			'title' => 'Лихтенштейн',
			'code' => 'LIE',
			'img' => 'Liechtenstein.png',
		),
		array(
			'title' => 'Люксембург',
			'code' => 'LUX',
			'img' => 'Luxembourg.png',
		),
		array(
			'title' => 'Маврикий',
			'code' => 'MUS',
			'img' => 'Mauritius.png',
		),
		array(
			'title' => 'Мавритания',
			'code' => 'MRT',
			'img' => 'Mauritania.png',
		),
		array(
			'title' => 'Мадагаскар',
			'code' => 'MDG',
			'img' => 'Madagascar.png',
		),
		array(
			'title' => 'Майотта',
			'code' => 'MYT',
			'img' => 'Mayotte.png',
		),
		array(
			'title' => 'Макао (Аомынь)',
			'code' => 'MAC',
			'img' => 'Macau.png',
		),
		array(
			'title' => 'Македония',
			'code' => 'MKD',
			'img' => 'Macedonia.png',
		),
		array(
			'title' => 'Малави',
			'code' => 'MWI',
			'img' => 'Malawi.png',
		),
		array(
			'title' => 'Малайзия',
			'code' => 'MYS',
			'img' => 'Malaysia.png',
		),
		array(
			'title' => 'Мали',
			'code' => 'MLI',
			'img' => 'Mali.png',
		),
		array(
			'title' => 'Мальдивы',
			'code' => 'MDV',
			'img' => 'Maldives.png',
		),
		array(
			'title' => 'Мальта',
			'code' => 'MLT',
			'img' => 'Malta.png',
		),
		array(
			'title' => 'Марокко',
			'code' => 'MAR',
			'img' => 'Morocco.png',
		),
		array(
			'title' => 'Мартиника',
			'code' => 'MTQ',
			'img' => 'Martinique.png',
		),
		array(
			'title' => 'Маршалловы острова',
			'code' => 'MHL',
			'img' => 'Marshall-Islands.png',
		),
		array(
			'title' => 'Мексика',
			'code' => 'MEX',
			'img' => 'Mexico.png',
		),
		array(
			'title' => 'Мелилья',
			'code' => 'MEL',  // didn't find real 3-letter code
			'img' => '',
		),
		array(
			'title' => 'Мозамбик',
			'code' => 'MOZ',
			'img' => 'Mozambique.png',
		),
		array(
			'title' => 'Молдавия',
			'code' => 'MDA',
			'img' => 'Moldova.png',
		),
		array(
			'title' => 'Монако',
			'code' => 'MCO',
			'img' => 'Monaco.png',
		),
		array(
			'title' => 'Монголия',
			'code' => 'MNG',
			'img' => 'Mongolia.png',
		),
		array(
			'title' => 'Монтсеррат',
			'code' => 'MSR',
			'img' => 'Montserrat.png',
		),
		array(
			'title' => 'Мьянма (Бирма)',
			'code' => 'MMR',
			'img' => 'Myanmar.png',
		),
		array(
			'title' => 'Нагорно-Карабахская Республика',
			'code' => 'NKR', // didn't find real 3-letter code
			'img' => 'Nagorno-Karabakh.png',
		),
		array(
			'title' => 'Намибия',
			'code' => 'NAM',
			'img' => 'Namibia.png',
		),
		array(
			'title' => 'Науру',
			'code' => 'NRU',
			'img' => 'Nauru.png',
		),
		array(
			'title' => 'Непал',
			'code' => 'NPL',
			'img' => 'Nepal.png',
		),
		array(
			'title' => 'Нигер',
			'code' => 'NER',
			'img' => 'Niger.png',
		),
		array(
			'title' => 'Нигерия',
			'code' => 'NGA',
			'img' => 'Nigeria.png',
		),
		array(
			'title' => 'Нидерландские Антильские острова',
			'code' => 'ANT',
			'img' => 'Netherlands-Antilles.png',
		),
		array(
			'title' => 'Нидерланды (Голландия)',
			'code' => 'NLD',
			'img' => 'Netherlands.png',
		),
		array(
			'title' => 'Никарагуа',
			'code' => 'NIC',
			'img' => 'Nicaragua.png',
		),
		array(
			'title' => 'Ниуэ',
			'code' => 'NIU',
			'img' => 'Niue.png',
		),
		array(
			'title' => 'Новая Зеландия',
			'code' => 'NZL',
			'img' => 'New-Zealand.png',
		),
		array(
			'title' => 'Новая Каледония',
			'code' => 'NCL',
			'img' => 'New-Caledonia.png',
		),
		array(
			'title' => 'Норвегия',
			'code' => 'NOR',
			'img' => 'Norway.png',
		),
		array(
			'title' => 'ОАЭ',
			'code' => 'ARE',
			'img' => 'United-Arab-Emirates.png',
		),
		array(
			'title' => 'Оман',
			'code' => 'OMN',
			'img' => 'Oman.png',
		),
		array(
			'title' => 'Остров Норфолк',
			'code' => 'NFK',
			'img' => 'Norfolk-Island.png',
		),
		array(
			'title' => 'Остров Рождества',
			'code' => 'CXR',
			'img' => 'Christmas-Island.png',
		),
		array(
			'title' => 'Остров Святой Елены',
			'code' => 'SHN',
			'img' => 'Saint-Helena.png',
		),
		array(
			'title' => 'Остров Херд и острова Макдональд',
			'code' => 'HMD',
			'img' => 'Unknown.png',
		),
		array(
			'title' => 'Острова Кука',
			'code' => 'COK',
			'img' => 'Cook-Islands.png',
		),
		array(
			'title' => 'Острова Питкэрн',
			'code' => 'PCN',
			'img' => 'Pitcairn-Islands.png',
		),
		array(
			'title' => 'Острова Тёркс и Кайкос',
			'code' => 'TCA',
			'img' => 'Turks-and-Caicos-Islands.png',
		),
		array(
			'title' => 'Острова Уоллис и Футуна',
			'code' => 'WLF',
			'img' => 'Unknown.png',
		),
		array(
			'title' => 'Пакистан',
			'code' => 'PAK',
			'img' => 'Pakistan.png',
		),
		array(
			'title' => 'Палау',
			'code' => 'PLW',
			'img' => 'Palau.png',
		),
		array(
			'title' => 'Палестина',
			'code' => 'PSE',
			'img' => 'Palestine.png',
		),
		array(
			'title' => 'Панама',
			'code' => 'PAN',
			'img' => 'Panama.png',
		),
		array(
			'title' => 'Папуа — Новая Гвинея',
			'code' => 'PNG',
			'img' => 'Papua-New-Guinea.png',
		),
		array(
			'title' => 'Парагвай',
			'code' => 'PRY',
			'img' => 'Paraguay.png',
		),
		array(
			'title' => 'Перу',
			'code' => 'PER',
			'img' => 'Peru.png',
		),
		array(
			'title' => 'Польша',
			'code' => 'POL',
			'img' => 'Poland.png',
		),
		array(
			'title' => 'Португалия',
			'code' => 'PRT',
			'img' => 'Portugal.png',
		),
		array(
			'title' => 'Приднестровье',
			'code' => 'PMR', // didn't find real 3-letter code
			'img' => 'Unknown.png',
		),
		array(
			'title' => 'Пуэрто-Рико',
			'code' => 'PRI',
			'img' => 'Puerto-Rico.png',
		),
		array(
			'title' => 'Республика Конго',
			'code' => 'COG',
			'img' => 'Republic-of-the-Congo.png',
		),
		array(
			'title' => 'Реюньон',
			'code' => 'REU',
			'img' => 'Unknown.png',
		),
		array(
			'title' => 'Россия',
			'code' => 'RUS',
			'img' => 'Russia.png',
		),
		array(
			'title' => 'Руанда',
			'code' => 'RWA',
			'img' => 'Rwanda.png',
		),
		array(
			'title' => 'Румыния',
			'code' => 'ROU',
			'img' => 'Romania.png',
		),
		array(
			'title' => 'Сальвадор',
			'code' => 'SLV',
			'img' => 'El-Salvador.png',
		),
		array(
			'title' => 'Самоа',
			'code' => 'WSM',
			'img' => 'Samoa.png',
		),
		array(
			'title' => 'Сан-Марино',
			'code' => 'SMR',
			'img' => 'San-Marino.png',
		),
		array(
			'title' => 'Сан-Томе и Принсипи',
			'code' => 'STP',
			'img' => 'Sao-Tome-and-Principe.png',
		),
		array(
			'title' => 'Саудовская Аравия',
			'code' => 'SAU',
			'img' => 'Saudi-Arabia.png',
		),
		array(
			'title' => 'Свазиленд',
			'code' => 'SWZ',
			'img' => 'Swaziland.png',
		),
		array(
			'title' => 'Северные Марианские острова',
			'code' => 'MNP',
			'img' => 'Northern-Mariana-Islands.png',
		),
		array(
			'title' => 'Северный Кипр',
			'code' => 'SKI', // didn't find real 3-letter code
			'img' => 'Northern-Cyprus.png',
		),
		array(
			'title' => 'Сейшелы',
			'code' => 'SYC',
			'img' => 'Seychelles.png',
		),
		array(
			'title' => 'Сен-Пьер и Микелон',
			'code' => 'SPM',
			'img' => 'Unknown.png',
		),
		array(
			'title' => 'Сенегал',
			'code' => 'SEN',
			'img' => 'Senegal.png',
		),
		array(
			'title' => 'Сент-Винсент и Гренадины',
			'code' => 'VCT',
			'img' => 'Saint-Vincent-and-the-Grenadines.png',
		),
		array(
			'title' => 'Сент-Китс и Невис',
			'code' => 'KNA',
			'img' => 'Saint-Kitts-and-Nevis.png',
		),
		array(
			'title' => 'Сент-Люсия',
			'code' => 'LCA',
			'img' => 'Saint-Lucia.png',
		),
		array(
			'title' => 'Сербия',
			'code' => 'SRB',
			'img' => 'Serbia.png',
		),
		array(
			'title' => 'Сеута',
			'code' => 'SEU', // didn't find real 3-letter code
			'img' => 'Unknown.png',
		),
		array(
			'title' => 'Сингапур',
			'code' => 'SGP',
			'img' => 'Singapore.png',
		),
		array(
			'title' => 'Сирия',
			'code' => 'SYR',
			'img' => 'Syria.png',
		),
		array(
			'title' => 'Словакия',
			'code' => 'SVK',
			'img' => 'Slovakia.png',
		),
		array(
			'title' => 'Словения',
			'code' => 'SVN',
			'img' => 'Slovenia.png',
		),
		array(
			'title' => 'Соломоновы острова',
			'code' => 'SLB',
			'img' => 'Solomon-Islands.png',
		),
		array(
			'title' => 'Сомали',
			'code' => 'SOM',
			'img' => 'Somalia.png',
		),
		array(
			'title' => 'Судан',
			'code' => 'SDN',
			'img' => 'Sudan.png',
		),
		array(
			'title' => 'Суринам',
			'code' => 'SUR',
			'img' => 'Suriname.png',
		),
		array(
			'title' => 'США',
			'code' => 'USA',
			'img' => 'United-States.png',
		),
		array(
			'title' => 'Сьерра-Леоне',
			'code' => 'SLE',
			'img' => 'Sierra-Leone.png',
		),
		array(
			'title' => 'Таджикистан',
			'code' => 'TJK',
			'img' => 'Tajikistan.png',
		),
		array(
			'title' => 'Таиланд',
			'code' => 'THA',
			'img' => 'Thailand.png',
		),
		array(
			'title' => 'Тайвань',
			'code' => 'TWN',
			'img' => 'Taiwan.png',
		),
		array(
			'title' => 'Танзания',
			'code' => 'TZA',
			'img' => 'Tanzania.png',
		),
		array(
			'title' => 'Того',
			'code' => 'TGO',
			'img' => 'Togo.png',
		),
		array(
			'title' => 'Токелау',
			'code' => 'TKL',
			'img' => 'Tokelau.png',
		),
		array(
			'title' => 'Тонга',
			'code' => 'TON',
			'img' => 'Tonga.png',
		),
		array(
			'title' => 'Тринидад и Тобаго',
			'code' => 'TTO',
			'img' => 'Trinidad-and-Tobago.png',
		),
		array(
			'title' => 'Тувалу',
			'code' => 'TUV',
			'img' => 'Tuvalu.png',
		),
		array(
			'title' => 'Тунис',
			'code' => 'TUN',
			'img' => 'Tunisia.png',
		),
		array(
			'title' => 'Туркмения',
			'code' => 'TKM',
			'img' => 'Turkmenistan.png',
		),
		array(
			'title' => 'Турция',
			'code' => 'TUR',
			'img' => 'Turkey.png',
		),
		array(
			'title' => 'Уганда',
			'code' => 'UGA',
			'img' => 'Uganda.png',
		),
		array(
			'title' => 'Узбекистан',
			'code' => 'UZB',
			'img' => 'Uzbekistan.png',
		),
		array(
			'title' => 'Украина',
			'code' => 'UKR',
			'img' => 'Ukraine.png',
		),
		array(
			'title' => 'Уругвай',
			'code' => 'URY',
			'img' => 'Uruguay.png',
		),
		array(
			'title' => 'Фарерские острова',
			'code' => 'FRO',
			'img' => 'Faroes.png',
		),
		array(
			'title' => 'Федеративные Штаты Микронезии',
			'code' => 'FSM',
			'img' => 'Micronesia.png',
		),
		array(
			'title' => 'Фиджи',
			'code' => 'FJI',
			'img' => 'Fiji.png',
		),
		array(
			'title' => 'Филиппины',
			'code' => 'PHL',
			'img' => 'Philippines.png',
		),
		array(
			'title' => 'Финляндия',
			'code' => 'FIN',
			'img' => 'Finland.png',
		),
		array(
			'title' => 'Фолклендские (Мальвинские) острова',
			'code' => 'FLK',
			'img' => 'Falkland-Islands.png',
		),
		array(
			'title' => 'Франция',
			'code' => 'FRA',
			'img' => 'France.png',
		),
		array(
			'title' => 'Французская Гвиана',
			'code' => 'GUF',
			'img' => 'Unknown.png',
		),
		array(
			'title' => 'Французская Полинезия (Таити)',
			'code' => 'PYF',
			'img' => 'French-Polynesia.png',
		),
		array(
			'title' => 'Французские Южные и Антарктические Территории',
			'code' => 'ATF',
			'img' => 'French-Southern-Territories.png',
		),
		array(
			'title' => 'Хорватия',
			'code' => 'HRV',
			'img' => 'Croatia.png',
		),
		array(
			'title' => 'Центральноафриканская Республика',
			'code' => 'CAF',
			'img' => 'Central-African-Republic.png',
		),
		array(
			'title' => 'Чад',
			'code' => 'TCD',
			'img' => 'Chad.png',
		),
		array(
			'title' => 'Черногория',
			'code' => 'MNE',
			'img' => 'Montenegro.png',
		),
		array(
			'title' => 'Чехия',
			'code' => 'CZE',
			'img' => 'Czech-Republic.png',
		),
		array(
			'title' => 'Чили, о. Пасхи',
			'code' => 'CHL',
			'img' => 'Chile.png',
		),
		array(
			'title' => 'Чуук (Трук)',
			'code' => 'FSM',
			'img' => 'Unknown.png',
		),
		array(
			'title' => 'Швейцария',
			'code' => 'CHE',
			'img' => 'Switzerland.png',
		),
		array(
			'title' => 'Швеция',
			'code' => 'SWE',
			'img' => 'Sweden.png',
		),
		array(
			'title' => 'Шпицберген и Ян-Майен',
			'code' => 'SJM',
			'img' => 'Unknown.png',
		),
		array(
			'title' => 'Шри-Ланка (Цейлон)',
			'code' => 'LKA',
			'img' => 'Sri-Lanka.png',
		),
		array(
			'title' => 'Эквадор, Галапагосы',
			'code' => 'ECU',
			'img' => 'Ecuador.png',
		),
		array(
			'title' => 'Экваториальная Гвинея',
			'code' => 'GNQ',
			'img' => 'Equatorial-Guinea.png',
		),
		array(
			'title' => 'Эритрея',
			'code' => 'ERI',
			'img' => 'Eritrea.png',
		),
		array(
			'title' => 'Эстония',
			'code' => 'EST',
			'img' => 'Estonia.png',
		),
		array(
			'title' => 'Эфиопия',
			'code' => 'ETH',
			'img' => 'Ethiopia.png',
		),
		array(
			'title' => 'ЮАР',
			'code' => 'ZAF',
			'img' => 'South-Africa.png',
		),
		array(
			'title' => 'Южная Георгия и Южные Сандвичевы острова',
			'code' => 'SGS',
			'img' => 'South-Georgia-and-the-South-Sandwich-Islands.png',
		),
		array(
			'title' => 'Южная Осетия',
			'code' => 'SOS', // didn't find real 3-letter code
			'img' => 'South-Ossetia.png',
		),
		array(
			'title' => 'Южный Судан',
			'code' => 'SSU', // didn't find real 3-letter code
			'img' => 'South-Sudan.png',
		),
		array(
			'title' => 'Ямайка',
			'code' => 'JAM',
			'img' => 'Jamaica.png',
		),
		array(
			'title' => 'Яп',
			'code' => 'FSM',
			'img' => '',
		),
		array(
			'title' => 'Япония',
			'code' => 'JPN',
			'img' => 'Japan.png',
		),


	);
	/**
	 * @return array of countries.
	 */
	public static function getCountries(){
		return self::$countries;
	}

	public static function getCountriesforAP(){
		$countries = array();
		foreach(self::$countries as $c){
			$countries[$c['title']] = $c['title'];
		}
		return $countries;
	}

	public static function getCountryCode($id){
		return self::$countries[$id]['code'];
	}

	public static function getCountryNameById($id){
		return self::$countries[$id]['title'];
	}

	public static function getDistrict($code = false){
		$list =  array(
			1 => Yii::t('front', 'г. Харьков'),
			2 => Yii::t('front', 'Балаклейский'),
			3 => Yii::t('front', 'Барвенковский'),
			4 => Yii::t('front', 'Близнюковский'),
			5 => Yii::t('front', 'Богодуховский'),
			6 => Yii::t('front', 'Боровской'),
			7 => Yii::t('front', 'Валковский'),
			8 => Yii::t('front', 'Великобурлукский'),
			9 => Yii::t('front', 'Волчанский'),
			10 => Yii::t('front', 'Двуречанский'),
			11 => Yii::t('front', 'Дергачевский'),
			12 => Yii::t('front', 'Зачепиловский'),
			13 => Yii::t('front', 'Змиевской'),
			14 => Yii::t('front', 'Золочевский'),
			15 => Yii::t('front', 'Изюмский'),
			16 => Yii::t('front', 'Кегичевский'),
			17 => Yii::t('front', 'Коломакский'),
			18 => Yii::t('front', 'Красноградский'),
			19 => Yii::t('front', 'Краснокутский'),
			20 => Yii::t('front', 'Купянский'),
			21 => Yii::t('front', 'Лозовской'),
			22 => Yii::t('front', 'Нововодолажский'),
			23 => Yii::t('front', 'Первомайский'),
			24 => Yii::t('front', 'Печенежский'),
			25 => Yii::t('front', 'Сахновщинский'),
			26 => Yii::t('front', 'Харьковский'),
			27 => Yii::t('front', 'Чугуевский'),
			28 => Yii::t('front', 'Шевченковский'),
		);
		if(false === $code) {
			return $list;
		} else {
			return isset($list[$code]) ? $list[$code] : $code;
		}
	}

	public static function getOblast($code = false){
		$list =  array(
			1 => Yii::t('front', 'Харьковская область'),
		);
		if(false === $code) {
			return $list;
		} else {
            return isset($list[$code]) ? $list[$code] : $code;
		}
	}

	public static function getHDistrict($code = false){
		$list = array(
			1 => Yii::t('front', 'Дзержинский район'),
			2 => Yii::t('front', 'Коминтерновский район'),
			3 => Yii::t('front', 'Ленинский район'),
			4 => Yii::t('front', 'Московский район'),
			5 => Yii::t('front', 'Орджоникидзевский район'),
			6 => Yii::t('front', 'Червонозаводский район'),
			7 => Yii::t('front', 'Киевский район'),
			8 => Yii::t('front', 'Октябрьский район'),
			9 => Yii::t('front', 'Фрунзенский район')
		);

		if(false === $code) {
			return $list;
		} else {
			return isset($list[$code]) ? $list[$code] : $code;
		}
	}
}
