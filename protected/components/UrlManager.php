<?php
class UrlManager extends CUrlManager
{
    public function createUrl($route,$params=array(),$ampersand='&')
    {
        if (!isset($params['language'])) {
            if(isset(Yii::app()->session['setlanguage']) && !is_null(Yii::app()->session['setlanguage'])) {
                Yii::app()->language = Yii::app()->session['setlanguage'];
            } else {
                Yii::app()->session['setlanguage'] = DEFAULT_LANG;
                Yii::app()->language = DEFAULT_LANG;
            }
            $params['language']=Yii::app()->language;
        }
        return parent::createUrl($route, $params, $ampersand);
    }
}
