<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	const USER_IS_NOT_ACTIVE = 3;
	const ACTION_IS_NOT_ALLOWED = 5;
	protected $id;
	protected $firstTime = false;
	protected $ap_user;

	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
//		$user = Clients::model()->find('LOWER(email)=?', array(strtolower($this->username)));
		$user = Clients::model()->findByAttributes(array('card_id' => $this->username));
		$this->password = md5($this->password);
		if($user===null) {
			$this->errorCode = self::ERROR_USERNAME_INVALID;
		} elseif(trim($this->password)!=$user->password) {
			$this->errorCode = self::ERROR_PASSWORD_INVALID;
		}else {
			$this->id = $user->id;
			$this->setState('num', $user->id);
			$this->setState('email', $user->email);
			$this->setState('name', $user->fullname);
			$this->setState('role', 'none');
			$this->errorCode = self::ERROR_NONE;
		}

		return !$this->errorCode;
	}

}