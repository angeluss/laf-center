<div id="search">
    <div id="search_div" data-url="<?php echo Yii::app()->createUrl('/site/putStringToGet')?>">
        <form action="<?php echo $this->getController()->createUrl('site/search'); ?>" method="get">
            <div class="row search_block">
                <input placeholder="<?php echo Yii::t('front', 'Поиск') ?>" class="search_input" name="s" id="SiteSearchForm_string" value="<?php echo !is_null(Yii::app()->request->getParam('s')) ? Yii::app()->request->getParam('s') : '' ?>" type="text">
                <?php echo CHtml::error($form,'string'); ?>
                <input class="search_submit" type="submit" name="" value="Пошук">
            </div>
        </form>
    </div>
</div>