<?php

class ExportService
{
    private static function preparePdf()
    {
        Yii::import('application.extensions.mpdf.mpdf');
        $mpdf = new mpdf('utf-8', 'A4', '8', '', 10, 10, 7, 20, 10, 10);
        $mpdf->charset_in = 'utf-8';

        $stylesheet = file_get_contents(dirname(__FILE__) . '/../../css/print.css'); /*подключаем css*/

        $mpdf->WriteHTML($stylesheet, 1);

        $mpdf->list_indent_first_level = 0;
        return $mpdf;
    }

    /**
     * @param string $html
     * @param string $fileName
     */
    public static function exportDetailPdf($html, $fileName, $autoPrint=false)
    {
        $mpdf = self::preparePdf();
        $mpdf->SetHtmlFooter('<div class="pdf-footer">
                        <table class="footer-info">
                            <tr>
                                <td width="25%" class="left">Лаф-Центр</td>
                                <td width="30%">' . date('d.m.Y H:i:s', time()) . '</td>
                                <td width="10%" class="right">{PAGENO}/{nbpg}</td>
                            </tr>
                        </table>
                    </div>');
        if($autoPrint){
            $mpdf->SetJS('this.print();');
        }
        $mpdf->WriteHTML($html, 0); /*формируем pdf*/

        $mpdf->Output($fileName, $autoPrint ? 'I' : 'D');
    }

}