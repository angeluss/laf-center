<?php
Yii::import('admin.models.*');
Yii::import('admin_panel.models.*');
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='//layouts/main';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */
	public $breadcrumbs=array();

    public function __construct($id,$module=null)
    {
        $url = Yii::app()->request->requestUri;
        $a = explode('/', $url);
        if(strlen($a[1]) != 2) {
            $url = '/uk';
            unset ($a[0]);
            foreach($a as $u){
                $url .= '/' . $u;
            }
            $this->redirect($url);
        }
        $this->pageTitle  = Settings::model()->find()->meta_title;
        if(isset(Yii::app()->session['setlanguage']) && !is_null(Yii::app()->session['setlanguage'])) {
            Yii::app()->language = Yii::app()->session['setlanguage'];
        } else {
            Yii::app()->session['setlanguage'] = DEFAULT_LANG;
        }
        $get = Yii::app()->request->getParam('language');
        if(!is_null($get)){
            Yii::app()->language = $get;
            Yii::app()->session['setlanguage'] = $get;
        } else {
            Yii::app()->language = DEFAULT_LANG;
            Yii::app()->session['setlanguage'] = DEFAULT_LANG;
        }
        parent::__construct($id, $module);

    }



}