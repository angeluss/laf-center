<?php

return array(
    'О нас' => 'À PROPOS DE NOUS',
    'Новости'=>'NOUVELLES',
    'Партнеры'=>'PARTENAIRES',
    'Услуги и Цены'=>'SERVICES ET PRIX',
    'Правовые консультации'=>'AVIS JURIDIQUE',
    'Полезная информация'=>'INFORMATIONS UTILES',
    'Форум'=>'FORUM',
    'Контакты'=>'CONTACTS',
    'Поиск'=>'Recherche',
    'Онлайн-консультация'=>'Consultation en ligne',
    'Вход'=>'Entrée',
    'Центр правового содействия иностранным гражданам'=>'CENTRE D\'ASSISTANCE JURIDIQUE AUX CITOYENS ?TRANGERS',
    'Публичная оферта'=>'OFFRE PUBLIQUE',
    'Не нашли ответ на нужный вопрос?'=>'Vous ne trouvez pas une réponse à la bonne question?',
    'Наши юристы всегда готовы предоставить детальную информацию по интересующим Вас темам.'=>'Nos avocats sont toujours prêts à fournir des informations détaillées sur des sujets intéressants.',
);

