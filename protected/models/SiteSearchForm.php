<?php

class SiteSearchForm extends CFormModel
{
    public $string;

    public function rules()
    {
        return array(
            array('string', 'required'),
            array('string', 'length', 'min' => 3, 'message' => 'You must enter minimum 3 characters',));
    }

    public function safeAttributes()
    {
        return array('string');
    }

    public function getParams()
    {
        $s = Yii::app()->request->getParam('s');
        if (!is_null($s)) {
            $this->string = $s;
            Yii::app()->session['search_string'] = $this->string;
        }
    }
}