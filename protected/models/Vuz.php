<?php

/**
 * This is the model class for table "vuz".
 *
 * The followings are the available columns in table 'vuz':
 * @property integer $id
 * @property string $title
 * @property string $rector
 * @property string $address
 * @property integer $status
 * @property integer $code
 */
class Vuz extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'vuz';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, code', 'required'),
			array('status, code', 'numerical', 'integerOnly'=>true),
			array('title, rector, address', 'length', 'max'=>255),
			array('id, title, rector, address, status, code', 'safe'),
		);
	}

	public static function getVuses($code = false){
		$model = self::model()->findAllByAttributes(array('status' => 1));
		$list = CHtml::listData($model,
			'code', 'title');
		if(false === $code) {
			return $list;
		} else {
			return isset($list[$code]) ? $list[$code] : $code;;
		}
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => Yii::t('front', 'Назва'),
			'rector' => Yii::t('front', 'Ректор'),
			'address' => Yii::t('front', 'Адреса'),
			'status' => Yii::t('front', 'Статус'),
			'code' =>Yii::t('front', 'Код'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria = new CDbCriteria;

		$sort = new CSort();
		$sort->defaultOrder = 't.id asc';

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('rector',$this->rector,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('status',$this->status, true);
		$criteria->compare('code',$this->code, true);

		return new CActiveDataProvider($this, array(
			'criteria' => $criteria,
			'sort' => $sort,
			'pagination' => array(
				'pageSize' => '10',
			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Vuz the static model class
	 */
	public static function model($className = __CLASS__)
	{
		return parent::model($className);
	}
}
