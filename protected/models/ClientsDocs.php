<?php

/**
 * This is the model class for table "clients_docs".
 *
 * The followings are the available columns in table 'clients_docs':
 * @property integer $id
 * @property integer $client_id
 * @property string $photo
 * @property string $visa
 * @property string $passport
 * @property string $vpu
 */
class ClientsDocs extends CActiveRecord
{
    public $photo_file;
    public $vpu_file;
    public $visa_file;
    public $passport_file;

	public $uplPhoto;
	public $uplVisa;
	public $uplPassport;
	public $uplVpu;
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'clients_docs';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('client_id', 'required'),
            array('photo_file, vpu_file, visa_file, passport_file', 'file', 'allowEmpty'=>true, 'types'=>'jpeg,doc,xml,zip,doc,xls,ppt,jpg,gif,png,txt,docx,pptx,xlsx,pdf,csv,bmp'),
			array('client_id', 'numerical', 'integerOnly'=>true),
			array('photo, visa, passport, vpu', 'length', 'max'=>255),
			array('uplPhoto, uplVisa, uplPassport, uplVpu', 'file', 'allowEmpty' => true, 'types'=>'jpg, gif, png, jpeg, bmp, doc, docx, xls, xlsx, pdf, txt, csv, odf, rtf', 'safe' => false),
			array('photo_file, id, client_id, photo, visa, passport, vpu', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'client_id' => 'Client',
			'photo' => 'Photo',
			'visa' => 'Visa',
			'passport' => 'Passport',
			'vpu' => 'Vpu',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria = new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('photo',$this->photo,true);
		$criteria->compare('visa',$this->visa,true);
		$criteria->compare('passport',$this->passport,true);
		$criteria->compare('vpu',$this->vpu,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ClientsDocs the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public function rename($entity, $new_name){
		$old_pass = '/uploadFiles/clientsPhoto/' . $this->client_id . '/' . $this->$entity;
		$new_pass = '/uploadFiles/clientsPhoto/' . $this->client_id . '/' . md5(time()) . '&_&' .$new_name;
		rename (WATSON . $old_pass , WATSON . $new_pass );
		$this->$entity = $new_name;
		if($this->save()) {
			echo CJSON::encode(array(
				'success' => true,
				'new_name' => $new_name,
				'new_pass' => $new_pass,
			));
			Yii::app()->end();
		} else {
			echo CJSON::encode(array(
				'success' => false,
			));
		}
	}
}
