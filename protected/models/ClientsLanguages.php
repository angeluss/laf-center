<?php

/**
 * This is the model class for table "clients_languages".
 *
 * The followings are the available columns in table 'clients_languages':
 * @property integer $id
 * @property integer $client_id
 * @property integer $lang
 */
class ClientsLanguages extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'clients_languages';
	}

	public static function getLang($code = false){
		$list = array(
			1 => 'English',
			2 => 'Français',
			3 => 'Русский',
			4 => 'Українська',
			5 => 'Türkmen',
			6 => 'العربية',
		);
		if(false === $code) {
			return $list;
		} else {
			return $list[$code];
		}
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('client_id, lang', 'required'),
			array('client_id, lang', 'numerical', 'integerOnly'=>true),
			array('id, client_id, lang', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'client_id' => 'Client',
			'lang' => 'Lang',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{

		$criteria = new CDbCriteria;

		$criteria->compare('id', $this->id);
		$criteria->compare('client_id', $this->client_id);
		$criteria->compare('lang', $this->lang);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return ClientsLanguages the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
