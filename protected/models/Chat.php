<?php

/**
 * This is the model class for table "chat".
 *
 * The followings are the available columns in table 'chat':
 * @property integer $id
 * @property integer $operator_id
 * @property integer $client
 * @property integer $created_at
 * @property integer $updated_at
 * @property integer $status
 * @property integer $is_admin
 */
class Chat extends CActiveRecord
{
	const OPEN_STATUS = 0;
	const PROGRESS_STATUS = 1;
	const CLOSED_STATUS = 2;

	public static function getStatus($status = false){
		$statuses = array(
			self::OPEN_STATUS => Yii::t('front', 'Открыт'),
			self::PROGRESS_STATUS => Yii::t('front', 'Ожидает оператора'),
			self::CLOSED_STATUS => Yii::t('front', 'Закрыт'),
		);
		if(false === $status){
			return $statuses;
		} else {
			return isset($statuses[$status]) ? $statuses[$status] : $status;
		}
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'chat';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('client', 'required'),
			array('operator_id, client, created_at, updated_at', 'numerical', 'integerOnly' => true),
			array('id, operator_id, client, created_at, updated_at, status, is_admin', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array(
            'message' 	=> array(self::HAS_MANY, 'Message', 'chat_id'),
            'client' 	=> array(self::BELONGS_TO, 'Clients', 'client'),
            'operator' 	=> array(self::HAS_ONE, 'Operators', 'operator_id'),
        );
	}

    /**
     * @return Message first message
     */
    public function getFirstMessage(){
        $model = Message::model()->findByAttributes(array('chat_id' => $this->id));
        $msg = is_null($model) ? '' : $model->msg;
        return $msg;
    }

    public function getClient(){
        return Clients::model()->findByAttributes(array('id' => $this->client))->fullname;
    }

    public function getOperator(){
        return Operators::model()->findByAttributes(array('id' => $this->operator_id))->login;
    }

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'operator_id' => 'Оператор',
			'client' => 'Клиент',
			'created_at' => 'Создан',
			'updated_at' => 'Обновлен',
			'status' => 'Статус',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{

		$criteria=new CDbCriteria;

        $sort = new CSort();
        $sort->defaultOrder = 't.created_at desc';

		$criteria->compare('id',$this->id);
		$criteria->compare('operator_id',$this->operator_id);
		$criteria->compare('client',$this->client);
		$criteria->compare('created_at',$this->created_at);
		$criteria->compare('updated_at',$this->updated_at);
		$criteria->compare('status',$this->status);
		$criteria->compare('is_admin',$this->is_admin);

		$criteria->condition = 'status = :status';
		$criteria->params = array(':status' => self::CLOSED_STATUS);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
            'sort' => $sort,
            'pagination' => array(
                'pageSize' => '10',
            ),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Chat the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

    public function getMessagesCount(){
        return count(Message::model()->findAllByAttributes(array('chat_id' => $this->id)));
    }

	/**
	 * Closes chat. It could be called from front or operator_panel.
	 */
	public static function closeChat($id){
		$chat = self::model()->findByPk($id);
		if(is_null($chat)){
			throw new CHttpException(404);
		} else {
			$chat->status = self::CLOSED_STATUS;
			if($chat->validate()) {
				$chat->save();
			} else {
				CVarDumper::dump($chat->getErrors());
			}
		}
	}

    public static function issetOperator(){
        $chat = self::getChat();

        return $chat->status == self::PROGRESS_STATUS;
    }

	public static function issetChat(){
        $chat = self::getChat();

        return is_null($chat) ? 0 : $chat->id;
	}

    public static function getChat(){
        $chat = self::model()->find(
            'client = :id AND status <> :status ',
            array(
                ':status' => Chat::CLOSED_STATUS,
                ':id' => isset(Yii::app()->user->num) ? Yii::app()->user->num : 0,
            )
        );
        return $chat;
    }

    public static function getChatList(){
        $chats = self::model()->findAllByAttributes(
            array(
                'status' => self::OPEN_STATUS,
            )
        );
        return $chats;
    }

    public static function getOpenChatList(){
        $chats = self::model()->findAllByAttributes(
            array(
                'status' => self::PROGRESS_STATUS,
                'operator_id' => Yii::app()->user->id,
            )
        );
        return $chats;
    }

	public function getMessages(){
		return Message::model()->findAllByAttributes(array('chat_id'=>$this->id));
	}

	public static function rus_date() {
		$translate = array(
			"Jan" =>Yii::t('profile', 'Января'),
			"Feb" =>Yii::t('profile', 'Февраля'),
			"Mar" =>Yii::t('profile', 'Марта'),
			"Apr" =>Yii::t('profile', 'Апреля'),
			"May" =>Yii::t('profile', 'Мая'),
			"Jun" =>Yii::t('profile', 'Июня'),
			"Jul" =>Yii::t('profile', 'Июля'),
			"Aug" => Yii::t('profile', 'Августа'),
			"Sep" => Yii::t('profile', 'Сентября'),
			"Oct" => Yii::t('profile', 'Октября'),
			"Nov" =>Yii::t('profile', 'Ноября'),
			"Dec" =>Yii::t('profile', 'Декабря'),
		);
		if (func_num_args() > 1) {
			$timestamp = func_get_arg(1);
			return strtr(date(func_get_arg(0), $timestamp), $translate);
		} else {
			return strtr(date(func_get_arg(0)), $translate);
		}
	}

	public static function getJson($chats){
		$json = '';
		foreach ($chats as $chat) {
			$chatData=date('Y-m-d', $chat->created_at);
			$json .=  CJSON::encode(array(
				'date' => $chatData,
				'badge'=>false,
				'title'=>'chat'
			));
			$json .= ',';
		}
		return $json;
	}

	public static function getMessagesForHistory($chats){
		$messages = array();
		foreach ($chats as $chat) {
			$msg = Message::model()->findAllByAttributes(array(
				'chat_id'=>$chat->id,
			));
			$messages = array_merge($messages, $msg);
		}
		return $messages;
	}

}
