<?php

/**
 * This is the model class for table "message".
 *
 * The followings are the available columns in table 'message':
 * @property integer $id
 * @property integer $chat_id
 * @property integer $sender
 * @property string $msg
 * @property string $file
 * @property string $filename
 * @property integer $created_at
 */
class Message extends CActiveRecord
{
    const SENDER_OPERATOR=1;
    const SENDER_CLIENT=2;


    public function getSender($code = false){
        $senders = array(
            self::SENDER_OPERATOR => Yii::t('front', 'Оператор'),
            self::SENDER_CLIENT => Yii::t('front', 'Клиент'),
        );
        if($code){
            return isset ($senders[$code]) ? $senders[$code] : $code;
        } else {
            return $senders;
        }
    }

	public static function getMessages(){
		$id = !Yii::app()->user->isGuest && isset(Yii::app()->user->num) ? Yii::app()->user->num : 0;
		$chat = Chat::model()->findByAttributes(
			array(
				'client' => $id,
			),
		'status <> :status',
			array(
				':status' => Chat::CLOSED_STATUS,
			)
		);
		return is_null($chat) ? array() : Message::model()->findAllByAttributes(array('chat_id' => $chat->id));
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'message';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		return array(
			array('chat_id, sender, msg', 'required'),
			array('chat_id, sender, created_at', 'numerical', 'integerOnly'=>true),
			array('id, chat_id, sender, created_at, msg, file, filename', 'safe'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		return array();
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'chat_id' => Yii::t('front', 'Чат'),
			'sender' => Yii::t('front', 'Отправитель'),
			'created_at' =>Yii::t('front', 'Создано'),
            'msg' => Yii::t('front','Текст'),
            'file' => Yii::t('front','Реальное имя файла'),
            'filename' => Yii::t('front','Отображаемое имя файла'),
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		$criteria=new CDbCriteria;

		$sort = new CSort();
		$sort->defaultOrder = 't.created_at desc';

		$criteria->compare('id',$this->id);
		$criteria->compare('chat_id',$this->chat_id);
		$criteria->compare('sender',$this->sender);
		$criteria->compare('created_at',$this->created_at);
		$criteria->compare('msg',$this->msg);
		$criteria->compare('file',$this->file);
		$criteria->compare('filename',$this->filename);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
			'sort' => $sort,
			'pagination' => array(
				'pageSize' => '10',
			),
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Message the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	public static function getFileExtensions(){
		return array("jpg","jpeg","gif","pdf","png","doc","docx","csv","xls","xlsx","rtf","txt");
	}

	public static function uplFiles(){
		$error = false;
		$files = array();

		$uploaddir = './uploads/';

		if( ! is_dir( $uploaddir ) ) mkdir( $uploaddir, 0777 );

		foreach( $_FILES as $file ){
			$ext = explode('.', $file['name']);
			$ext = array_pop($ext);
			if(in_array($ext, self::getFileExtensions())) {
				if (move_uploaded_file($file['tmp_name'], $uploaddir . basename($file['name']))) {
					$files[] = realpath($uploaddir . $file['name']);
				} else {
					$error = true;
				}
			}
		}

		return $error ? array('error' => 'Ошибка загрузки файлов.') : array('files' => $files );
	}

	public function addMsg($msg, $chat_id){
		$this->chat_id = $chat_id;
		$this->sender = 1; //1 - client, 2 - operator
		$this->created_at = time();
		$this->msg = $msg;

		if($this->validate()){
			if($this->save()){

				$messages = self::getMessages();
				$html = Yii::app()->controller->renderPartial('/layouts/_chat', array(
					'messages' => $messages,
				), true, false);

				echo CJSON::encode(array(
					'html' => $html,
					'success' => true,
					'id' => $chat_id,
				));
			}
		} else {
			CVarDumper::dump($this->getErrors()); die;
		}
	}

	public function addMsg2($msg, $chat_id){
		$this->chat_id = $chat_id;
		$this->sender = 1; //1 - client, 2 - operator
		$this->created_at = time();
		$this->msg = $msg;
		$this->file = time() . '_' . $_FILES['choose_file']['name'];
		$this->filename = $_FILES['choose_file']['name'];

		if($this->validate()){
			if($this->save()){
				if(isset($_FILES['choose_file']) && $_FILES['choose_file']['name'] !== '') {
					$uploaddir = WATSON . '/uploadFiles/chat/';
					if (!is_dir($uploaddir)) mkdir($uploaddir, 0777);

					$uploaddir .= $chat_id . '/';
					if (!is_dir($uploaddir)) mkdir($uploaddir, 0777);

					if ( move_uploaded_file($_FILES['choose_file']['tmp_name'], $uploaddir . $this->file) ) {}
				}
				$messages = Message::getMessages();
				$html = Yii::app()->controller->renderPartial('/layouts/_chat', array(
					'messages' => $messages,
				), true, false);

				echo CJSON::encode(array(
					'html' => $html,
					'success' => true,
					'id' => $chat_id,
				));
			}
		} else {
			CVarDumper::dump($this->getErrors()); die;
		}
	}

    public function rename($new_name){
        $old_pass = '/uploadFiles/chat/' . $this->chat_id.'/' . $this->created_at.'_' . $this->filename;
        $new_pass = '/uploadFiles/chat/' . $this->chat_id.'/' . $this->created_at.'_' . $new_name;
        rename (WATSON . $old_pass , WATSON . $new_pass );
        $this->filename = $new_name;

        if($this->save()) {
            echo CJSON::encode(array(
                'success' => true,
                'new_name' => $new_name,
                'new_pass'=>$new_pass
            ));
            Yii::app()->end();
        } else {
            echo CJSON::encode(array(
                'success' => false,
            ));
        }
    }

	public function addOperatorMessage($msg, $id){
		$this->chat_id = $id;
		$this->sender = 2; //1 - client, 2 - operator
		$this->created_at = time();
		$this->msg = $msg;
		$this->file = time() . '_' . $_FILES['send_file']['name'];
		$this->filename = $_FILES['send_file']['name'];
		$messages = array();
		if($this->validate()){
			if($this->save()){
				if(isset($_FILES['send_file']) && $_FILES['send_file']['name'] !== '') {
					$uploaddir = WATSON . '/uploadFiles/chat/';
					if (!is_dir($uploaddir)) mkdir($uploaddir, 0777);
					$uploaddir .= $id . '/';
					if (!is_dir($uploaddir)) mkdir($uploaddir, 0777);
					if ( move_uploaded_file($_FILES['send_file']['tmp_name'], $uploaddir . $this->file) ) {}
				}
				$messages =  self::model()->findAllByAttributes(array('chat_id' => $id));
			}
		} else {
			CVarDumper::dump($this->getErrors()); die;
		}
		return $messages;
	}

}
