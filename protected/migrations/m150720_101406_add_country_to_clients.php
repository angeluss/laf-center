<?php

class m150720_101406_add_country_to_clients extends CDbMigration
{
	public function up()
	{
        $this->execute("
            ALTER TABLE `clients`
            ADD `country` varchar(255) NULL;
        ");
	}

	public function down()
	{
        $this->execute("
            ALTER TABLE `clients`
            DROP `country`;
        ");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}