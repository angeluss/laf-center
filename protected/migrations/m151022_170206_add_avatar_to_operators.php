<?php

class m151022_170206_add_avatar_to_operators extends CDbMigration
{
	public function up()
	{
		$this->execute("
		 	ALTER TABLE `operators`
            ADD `avatar` varchar(255)  NULL DEFAULT NULL;
		");
	}

	public function down()
	{
		echo "m151022_170206_add_avatar_to_operators does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}