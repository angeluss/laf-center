<?php

class m151026_120038_add_metatags_to_pages extends CDbMigration
{
	public function up()
	{
		$this->execute("
		 	ALTER TABLE `pages`
            ADD `meta_keys` varchar(255) NOT NULL DEFAULT '',
            ADD `meta_desc` varchar(255) NOT NULL DEFAULT '';
		");
	}

	public function down()
	{
		echo "m151026_120038_add_metatags_to_pages does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}