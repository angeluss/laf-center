<?php

class m150721_114012_create_table_ap_logs extends CDbMigration
{
	public function up()
	{
        $this->execute("
          CREATE TABLE IF NOT EXISTS `ap_logs` (
          `id` int(20) NOT NULL AUTO_INCREMENT,
          `user_id` int(20) NOT NULL,
          `desc` varchar(255) NULL,
          `area` int(2) NULL,
          `date` int(11) DEFAULT NULL,
          PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ");
	}

	public function down()
	{
        $this->execute("DROP TABLE ap_logs");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}