<?php

class m150810_164916_add_contact_email_to_settings extends CDbMigration
{
	public function up()
	{
        $this->execute("ALTER TABLE  `settings` ADD  `contact_email` VARCHAR( 255 ) NOT NULL DEFAULT  '' AFTER  `admin_email` ;");
	}

	public function down()
	{
		echo "m150810_164916_add_contact_email_to_settings does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}