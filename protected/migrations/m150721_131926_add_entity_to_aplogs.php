<?php

class m150721_131926_add_entity_to_aplogs extends CDbMigration
{
	public function up()
	{
        $this->execute("
            ALTER TABLE `ap_logs`
            ADD `entity` varchar(255) NULL,
            ADD `entity_id` int(20) NULL;
        ");
	}

	public function down()
	{
        $this->execute("
            ALTER TABLE `ap_logs`
            DROP `entity`,
            DROP `entity_id`;
        ");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}