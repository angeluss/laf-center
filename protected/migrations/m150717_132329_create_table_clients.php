<?php

class m150717_132329_create_table_clients extends CDbMigration
{
	public function up()
	{
        $this->execute("
          CREATE TABLE IF NOT EXISTS `clients` (
          `id` int(20) NOT NULL AUTO_INCREMENT,
          `fullname` varchar(255) NOT NULL,
          `gender` int(1) NOT NULL,
          `phone` varchar(255) NULL,
          `email` varchar(255) NOT NULL,
          `photo` varchar(255) NULL,
          `status` int(1) NOT NULL,
          `pay_date` int(11) NULL,
          `password` varchar(255) NOT NULL,
          `code` varchar(255) NOT NULL,
          `created_at` int(11) NOT NULL DEFAULT '0',
          `updated_at` int(11) DEFAULT NULL,
          PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ");
	}

	public function down()
	{
        $this->execute("DROP TABLE clients");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}