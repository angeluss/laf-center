<?php

class m150918_142818_create_table_message extends CDbMigration
{
	public function up()
	{
		$this->execute("
			CREATE TABLE IF NOT EXISTS `message` (
          `id` int(20) NOT NULL AUTO_INCREMENT,
          `chat_id` int(20) NOT NULL,
          `sender` int(20) NOT NULL,
          `created_at` int(11) NOT NULL DEFAULT '0',
          PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
		");
	}

	public function down()
	{
		echo "m150918_142818_create_table_message does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}