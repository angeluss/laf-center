<?php

class m150817_134915_create_table_user_languages extends CDbMigration
{
	public function up()
	{
		$this->execute("
          CREATE TABLE IF NOT EXISTS `clients_languages` (
          `id` int(20) NOT NULL AUTO_INCREMENT,
          `client_id` int(20) NOT NULL,
          `lang` int(20) NOT NULL,
          PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ");
	}

	public function down()
	{
		$this->execute("DROP TABLE clients_languages");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}