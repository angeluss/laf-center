<?php

class m150819_090731_add_oblast_to_clients extends CDbMigration
{
	public function up()
	{
		$this->execute("
            ALTER TABLE `clients`
            ADD `oblast` varchar(255) NULL;
        ");
	}

	public function down()
	{
		echo "m150819_090731_add_oblast_to_clients does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}