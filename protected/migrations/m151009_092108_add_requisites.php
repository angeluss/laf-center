<?php

class m151009_092108_add_requisites extends CDbMigration
{
	public function up()
	{
		$this->execute("
		INSERT INTO `requisites`(
		`recepient`, `account`, `id_code`, `bank`, `bank_code`, `amount`, `fine`
		) VALUES  (
		'УДКСУ у Печерському районі м. Києва', '31212253700007', '38004897', 'ГУ УДКСУ у м. Києві', '820019', '2500', ''
		);
		");
	}

	public function down()
	{
		echo "m151009_092108_add_requisites does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}