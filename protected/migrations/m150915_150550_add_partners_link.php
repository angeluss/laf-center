<?php

class m150915_150550_add_partners_link extends CDbMigration
{
	public function up()
	{
		$this->execute("
			INSERT INTO `main_menu` (`id`, `title`, `link`, `status`) VALUE
			(8, 'Партнеры', 'partners', 1);
		");
	}

	public function down()
	{
		echo "m150915_150550_add_partners_link does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}