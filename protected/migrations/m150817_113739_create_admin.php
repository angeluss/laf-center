<?php

class m150817_113739_create_admin extends CDbMigration
{
	public function up()
	{
		$this->execute("
			INSERT INTO `admin` (`id`, `login`, `password`, `email`, `role`, `created_at`, `updated_at`)
			VALUES
			(NULL, 'admin', '202cb962ac59075b964b07152d234b70', 'admin@laf.centr', '1', '1439811423', '1439811423');
		");
	}

	public function down()
	{
		echo "m150817_113739_create_admin does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}