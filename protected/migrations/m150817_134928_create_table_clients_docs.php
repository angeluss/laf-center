<?php

class m150817_134928_create_table_clients_docs extends CDbMigration
{
	public function up()
	{
		$this->execute("
          CREATE TABLE IF NOT EXISTS `clients_docs` (
          `id` int(20) NOT NULL AUTO_INCREMENT,
          `client_id` int(20) NOT NULL,
          `photo` varchar(255) NULL DEFAULT NULL,
          `visa` varchar(255) NULL DEFAULT NULL,
          `passport` varchar(255) NULL DEFAULT NULL,
          `vpu` varchar(255) NULL DEFAULT NULL,
          PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ");
	}

	public function down()
	{
		$this->execute("DROP TABLE clients_docs");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}