<?php

class m150922_090239_add_column_link_table_news extends CDbMigration
{
	public function up()
	{
        $this->execute("
            ALTER TABLE `news`
            ADD `link` varchar(255) NULL;
        ");
	}

	public function down()
	{
		echo "m150922_090239_add_column_link_table_news does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}