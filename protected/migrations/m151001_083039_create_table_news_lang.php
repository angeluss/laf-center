<?php

class m151001_083039_create_table_news_lang extends CDbMigration
{
	public function up()
	{
        $this->execute("
          CREATE TABLE IF NOT EXISTS `news_lang` (
          `id` int(20) NOT NULL AUTO_INCREMENT,
          `new_id` int(20) NOT NULL,
          `lang` varchar(255) NOT NULL,
          `title` varchar(255) NOT NULL,
          `short_desc` varchar(255) NOT NULL DEFAULT '',
          `content` text NOT NULL DEFAULT '',
          `status` int(2) NOT NULL DEFAULT 0,
          PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ");
	}

	public function down()
	{
		echo "m151001_083039_create_table_news_lang does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}