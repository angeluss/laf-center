<?php

class m150729_122737_add_entity_to_alogs extends CDbMigration
{
	public function up()
	{
        $this->execute("
            ALTER TABLE `a_logs`
            ADD `entity` varchar(255) NULL,
            ADD `entity_id` int(20) NULL;
        ");
	}

	public function down()
	{
        $this->execute("
            ALTER TABLE `a_logs`
            DROP `entity`,
            DROP `entity_id`;
        ");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}