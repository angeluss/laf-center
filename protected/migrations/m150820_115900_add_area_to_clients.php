<?php

class m150820_115900_add_area_to_clients extends CDbMigration
{
	public function up()
	{
		$this->execute("
            ALTER TABLE `clients`
            ADD `area` varchar(255) NULL;
        ");
	}

	public function down()
	{
		echo "m150820_115900_add_area_to_clients does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}