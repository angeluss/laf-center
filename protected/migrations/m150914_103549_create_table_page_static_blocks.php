<?php

class m150914_103549_create_table_page_static_blocks extends CDbMigration
{
	public function up()
	{
		$this->execute("
			CREATE TABLE IF NOT EXISTS `blocks` (
          `id` int(20) NOT NULL AUTO_INCREMENT,
          `title` varchar(255) NOT NULL,
          `text` TEXT,
          `status` int(1) NOT NULL DEFAULT 1,
          PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
		");
	}

	public function down()
	{
		echo "m150914_103549_create_table_page_static_blocks does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}