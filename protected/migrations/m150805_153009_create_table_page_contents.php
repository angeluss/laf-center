<?php

class m150805_153009_create_table_page_contents extends CDbMigration
{
	public function up()
	{
        $this->execute("
          CREATE TABLE IF NOT EXISTS `page_contents` (
          `id` int(20) NOT NULL AUTO_INCREMENT,
          `page_id` int(20) NOT NULL,
          `lang` varchar(255) NOT NULL,
          `content` text,
          `status` int(2) NOT NULL DEFAULT 0,
          PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ");
	}

	public function down()
	{
        $this->execute("DROP TABLE page_contents");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}