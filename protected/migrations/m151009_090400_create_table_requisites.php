<?php

class m151009_090400_create_table_requisites extends CDbMigration
{
	public function up()
	{
		$this->execute("
          CREATE TABLE IF NOT EXISTS `requisites` (
          `id` int(20) NOT NULL AUTO_INCREMENT,
          `recepient` varchar(255) NOT NULL DEFAULT '',
          `account` varchar(255) NOT NULL DEFAULT '',
          `id_code` varchar(255) NOT NULL DEFAULT '',
          `bank` varchar(255) NOT NULL DEFAULT '',
          `bank_code` varchar(255) NOT NULL DEFAULT '',
          `amount` varchar(255) NOT NULL DEFAULT '',
          `fine` varchar(255) NOT NULL DEFAULT '',
          PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ");
	}

	public function down()
	{
		echo "m151009_090400_create_table_requisites does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}