<?php

class m150924_133316_create_table_consultation extends CDbMigration
{
	public function up()
	{
        $this->execute("
          CREATE TABLE IF NOT EXISTS `consultation` (
          `id` int(20) NOT NULL AUTO_INCREMENT,
          `title` varchar(255) NOT NULL,
          `content` text NOT NULL DEFAULT '',
          `status` int(2) NOT NULL DEFAULT 0,
          `type_consult` int(20) NOT NULL DEFAULT 1,
          PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ");
	}

	public function down()
	{
		echo "m150924_133316_create_table_consultation does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}