<?php

class m150817_134941_add_clients_params extends CDbMigration
{
	public function up()
	{
		$this->execute("
            ALTER TABLE `clients`
            ADD `fullname_uk` varchar(255) NULL,
            ADD `bitrhdate` varchar(255) NULL,
            ADD `vuz` varchar(255) NULL,
            ADD `address_reg` varchar(255) NULL,
            ADD `passport` varchar(255) NULL,
            ADD `date_passport` varchar(255) NULL,
            ADD `passport_till` varchar(255) NULL,
            ADD `tax_number` varchar(255) NULL,
            ADD `vpu` int(1) NULL,
            ADD `vpu_serie` varchar(255) NULL,
            ADD `vpu_number` varchar(255) NULL,
            ADD `vpu_till` varchar(255) NULL,
            ADD `vpu_from` varchar(255) NULL;
        ");
	}

	public function down()
	{
		echo "m150817_134941_add_clients_params does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}