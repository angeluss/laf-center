<?php

class m151015_124155_add_translates_to_address extends CDbMigration
{
	public function up()
	{
		$this->execute("
          ALTER TABLE  `settings`
          ADD  `address_en` VARCHAR( 255 ) NOT NULL DEFAULT  '',
          ADD  `status_en` int( 1 ) NOT NULL DEFAULT  0,
          ADD  `address_ru` VARCHAR( 255 ) NOT NULL DEFAULT  '',
          ADD  `status_ru` int( 1 ) NOT NULL DEFAULT  0,
          ADD  `address_fr` VARCHAR( 255 ) NOT NULL DEFAULT  '',
          ADD  `status_fr` int( 1 ) NOT NULL DEFAULT  0,
          ADD  `address_tr` VARCHAR( 255 ) NOT NULL DEFAULT  '',
          ADD  `status_tr` int( 1 ) NOT NULL DEFAULT  0,
          ADD  `address_ar` VARCHAR( 255 ) NOT NULL DEFAULT  '',
          ADD  `status_ar` int( 1 ) NOT NULL DEFAULT  0;
          ");
	}

	public function down()
	{
		echo "m151015_124155_add_translates_to_address does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}