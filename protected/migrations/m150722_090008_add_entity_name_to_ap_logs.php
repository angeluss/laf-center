<?php

class m150722_090008_add_entity_name_to_ap_logs extends CDbMigration
{
	public function up()
	{$this->execute("
            ALTER TABLE `ap_logs`
            ADD `entity_name` varchar(255) NULL;
        ");
	}

	public function down()
	{
        $this->execute("
            ALTER TABLE `ap_logs`
            DROP `entity_name`;
        ");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}