<?php

class m150805_153000_create_table_pages extends CDbMigration
{
	public function up()
	{
        $this->execute("
          CREATE TABLE IF NOT EXISTS `pages` (
          `id` int(20) NOT NULL AUTO_INCREMENT,
          `title` varchar(255) NOT NULL,
          `meta_title` varchar(255) NOT NULL,
          `alias` varchar(255) NOT NULL,
          `status` int(2) NOT NULL DEFAULT 0,
          `created_at` int(11) NOT NULL DEFAULT '0',
          `updated_at` int(11) DEFAULT NULL DEFAULT '0',
          PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ");
	}

	public function down()
	{
        $this->execute("DROP TABLE pages");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}