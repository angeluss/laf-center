<?php

class m150818_163227_add_code_to_vuz extends CDbMigration
{
	public function up()
	{
		$this->execute("
            ALTER TABLE `vuz`
            ADD `code` varchar(4) NOT NULL;
        ");
	}

	public function down()
	{
		echo "m150818_163227_add_code_to_vuz does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}