<?php

class m151007_143503_chat_enable_null_for_operator extends CDbMigration
{
	public function up()
	{
		$this->execute("
		ALTER TABLE  `chat` CHANGE  `operator_id`  `operator_id` INT( 20 ) NULL DEFAULT NULL ;
		");
	}

	public function down()
	{
		echo "m151007_143503_chat_enable_null_for_operator does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}