<?php

class m150925_084554_create_table_con_law_branch extends CDbMigration
{
	public function up()
	{
        $this->execute("
          CREATE TABLE IF NOT EXISTS `con_law_branch` (
          `id` int(20) NOT NULL AUTO_INCREMENT,
          `branch_id` int(20) NOT NULL,
          `con_id` int(20) NOT NULL,
          PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ");
	}

	public function down()
	{
		echo "m150925_084554_create_table_con_law_branch does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}