<?php

class m150930_082208_create_table_consultation_lang extends CDbMigration
{
	public function up()
	{
        $this->execute("
          CREATE TABLE IF NOT EXISTS `consultation_lang` (
          `id` int(20) NOT NULL AUTO_INCREMENT,
          `con_id` int(20) NOT NULL,
          `lang` varchar(255) NOT NULL,
          `title` varchar(255) NOT NULL,
          `content` text,
          `status` int(2) NOT NULL DEFAULT 0,
          PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ");
	}

	public function down()
	{
        $this->execute("DROP TABLE consultation_lang");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}