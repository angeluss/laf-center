<?php

class m150914_102553_insert_menu_items extends CDbMigration
{
	public function up()
	{
		$this->execute("
			INSERT INTO `main_menu` (`id`, `title`, `link`, `status`) VALUES
			(1, 'О нас', 'page/about_us', 1),
			(2, 'Новости', 'news', 1),
			(3, 'Услуги и Цены', 'page/prices', 1),
			(4, 'Правовые консультации', 'page/consult', 1),
			(5, 'Полезная информация', 'page/info', 1),
			(6, 'Форум', 'http://laf-center.ukrainianforum.net/', 2),
			(7, 'Контакты', 'page/contact_us', 1);
		");
	}

	public function down()
	{
		echo "m150914_102553_insert_menu_items does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}