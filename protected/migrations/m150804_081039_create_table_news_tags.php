<?php

class m150804_081039_create_table_news_tags extends CDbMigration
{
	public function up()
	{
        $this->execute("
          CREATE TABLE IF NOT EXISTS `news_tags` (
          `id` int(20) NOT NULL AUTO_INCREMENT,
          `new_id` int(20) NOT NULL,
          `tag_id` int(20) NOT NULL,
          PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ");
	}

	public function down()
	{
		echo "m150804_081039_create_table_news_tags does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}