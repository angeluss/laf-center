<?php

class m151026_121327_add_favicon_to_settings extends CDbMigration
{
	public function up()
	{
		$this->execute("
		 	ALTER TABLE `settings`
            ADD `favicon` varchar(255)  NULL DEFAULT NULL;
		");
	}

	public function down()
	{
		echo "m151026_121327_add_favicon_to_settings does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}