<?php

class m150721_133953_change_desc_to_action extends CDbMigration
{
	public function up()
	{
        $this->execute("
          ALTER TABLE `ap_logs` CHANGE  `desc`  `action` VARCHAR( 255 ) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL ;
        ");
	}

	public function down()
	{
		echo "m150721_133953_change_desc_to_action does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}