<?php

class m150730_120703_create_table_oferta extends CDbMigration
{
	public function up()
	{
        $this->execute("
          CREATE TABLE IF NOT EXISTS `oferta` (
          `id` int(20) NOT NULL AUTO_INCREMENT,
          `lang` varchar(10) NOT NULL,
          `content` text NOT NULL DEFAULT '',
          `status` int(3) NOT NULL,
          `created_at` int(11) NOT NULL DEFAULT '0',
          `updated_at` int(11) DEFAULT NULL DEFAULT '0',
          PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ");
	}

	public function down()
	{
        $this->execute("DROP TABLE oferta");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}