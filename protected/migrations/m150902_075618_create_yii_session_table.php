<?php

class m150902_075618_create_yii_session_table extends CDbMigration
{
	public function up()
	{
        $this->createTable('YiiSession', array(
            'id' => 'char(32)',
            'expire' => 'INTEGER',
            'data' => 'text',
        ));
	}

	public function down()
	{
		$this->dropTable('YiiSession');
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}