<?php

class m150729_141414_add_settings extends CDbMigration
{
	public function up()
	{
        $this->execute("
          INSERT INTO `settings`
          (`id`, `meta_title`, `admin_email`, `fb`, `phone`, `call_centr`, `address`, `skype`)
          VALUES
          (NULL, '', '', '', '', '', '', '');
        ");
	}

	public function down()
	{
		echo "m150729_141414_add_settings does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}

