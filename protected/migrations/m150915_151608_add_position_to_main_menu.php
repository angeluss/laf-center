<?php

class m150915_151608_add_position_to_main_menu extends CDbMigration
{
	public function up()
	{
		$this->execute("
            ALTER TABLE `main_menu`
            ADD `position` int(5) NOT NULL DEFAULT 0;
        ");
	}

	public function down()
	{
		echo "m150915_151608_add_position_to_main_menu does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}