<?php

class m150722_115749_add_cardID_to_clients extends CDbMigration
{
	public function up()
	{
        $this->execute("
            ALTER TABLE `clients`
            ADD `card_id` varchar(255) NULL;
        ");
	}

	public function down()
	{
        $this->execute("
            ALTER TABLE `clients`
            DROP `card_id`;
        ");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}