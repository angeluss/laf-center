<?php

class m150729_140735_create_table_settings extends CDbMigration
{
	public function up()
	{
        $this->execute("
          CREATE TABLE IF NOT EXISTS `settings` (
          `id` int(20) NOT NULL AUTO_INCREMENT,
          `meta_title` varchar(255) NOT NULL DEFAULT '',
          `admin_email` varchar(255) NOT NULL DEFAULT '',
          `fb` varchar(255) NOT NULL DEFAULT '',
          `phone` varchar(255) NOT NULL DEFAULT '',
          `call_centr` varchar(255) NOT NULL DEFAULT '',
          `address` varchar(255) NOT NULL DEFAULT '',
          `skype` varchar(255) NOT NULL DEFAULT '',
          PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ");
	}

	public function down()
	{
        $this->execute("DROP TABLE settings");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}