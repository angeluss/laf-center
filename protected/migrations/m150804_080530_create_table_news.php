<?php

class m150804_080530_create_table_news extends CDbMigration
{
	public function up()
	{
        $this->execute("
          CREATE TABLE IF NOT EXISTS `news` (
          `id` int(20) NOT NULL AUTO_INCREMENT,
          `title` varchar(255) NOT NULL,
          `img` varchar(255) NULL DEFAULT NULL,
          `short_desc` varchar(255) NOT NULL DEFAULT '',
          `content` text NOT NULL DEFAULT '',
          `status` int(2) NOT NULL DEFAULT 0,
          `is_attached` int(2) NOT NULL DEFAULT 0,
          `created_at` int(11) NOT NULL DEFAULT '0',
          `updated_at` int(11) DEFAULT NULL DEFAULT '0',
          PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
        ");
	}

	public function down()
	{
        $this->execute("DROP TABLE news");
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}