<?php

class m151028_131051_add_file_to_message extends CDbMigration
{
	public function up()
	{
		$this->execute("
		 	ALTER TABLE `message`
            ADD `file` varchar(255)  NULL DEFAULT NULL,
            ADD `filename` varchar(255)  NULL DEFAULT NULL;
		");
	}

	public function down()
	{
		echo "m151028_131051_add_file_to_message does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}