<?php

class m150819_083301_add_address_fields_to_clients extends CDbMigration
{
	public function up()
	{
		$this->execute("
            ALTER TABLE `clients`
            ADD `district` varchar(255) NULL,
            ADD `street` varchar(255) NULL,
            ADD `house` varchar(255) NULL,
            ADD `flat` varchar(255) NULL;
        ");
	}

	public function down()
	{
		echo "m150819_083301_add_address_fields_to_clients does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}