<?php

class m151006_112331_add_msg_to_message extends CDbMigration
{
	public function up()
	{
		$this->execute("
            ALTER TABLE `message`
            ADD `msg` TEXT;
        ");
	}

	public function down()
	{
		echo "m151006_112331_add_msg_to_message does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}