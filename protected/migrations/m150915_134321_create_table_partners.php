<?php

class m150915_134321_create_table_partners extends CDbMigration
{
	public function up()
	{
		$this->execute("
			CREATE TABLE IF NOT EXISTS `partner` (
          `id` int(20) NOT NULL AUTO_INCREMENT,
          `title` varchar(255) NOT NULL,
          `url` varchar(255) NOT NULL,
          `img` varchar(255) NULL DEFAULT NULL,
          `desc` TEXT,
          `status` int(1) NOT NULL DEFAULT 1,
          PRIMARY KEY (`id`)
          ) ENGINE=InnoDB DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;
		");
	}

	public function down()
	{
		echo "m150915_134321_create_table_partners does not support migration down.\n";
		return false;
	}

	/*
	// Use safeUp/safeDown to do migration with transaction
	public function safeUp()
	{
	}

	public function safeDown()
	{
	}
	*/
}