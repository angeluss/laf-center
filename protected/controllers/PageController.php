<?php

class PageController extends Controller
{
    public function actions()
    {
        return array();
    }

    public function actionIndex($alias)
    {
        $page = Pages::model()->findByAttributes(array('alias' => $alias));
        if(is_null($page)){
            throw new CHttpException(404, Yii::t('front', 'Сторінки не існує'));
        }
        $content = $page->getContent(Yii::app()->language);

        if($page->meta_keys !== '') {
            Yii::app()->clientScript->registerMetaTag($page->meta_keys, 'keywords');
        }

        if($page->meta_desc !== '') {
            Yii::app()->clientScript->registerMetaTag($page->meta_desc, 'description');
        }
        if($page->meta_title !== '') {
            $meta_title = $page->meta_title;
        } else {
            $meta_title = $page->title;
        }
        Yii::app()->clientScript->registerMetaTag($meta_title, 'title');
        $this->pageTitle = Settings::model()->find()->meta_title . ' - ' . $page->title;
        $this->render('index', array(
            'model' => $page,
            'content' => $content,
        ));
    }
}