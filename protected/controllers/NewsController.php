<?php

class NewsController extends Controller
{
    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array();
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     * @param string $alias page alias.
     */
    public function actionIndex()
    {
        $random_consult = Consultation::model()->findAll(
            array(
                "condition" => "status=1",
                "order" => "rand()",
                "limit" => 5,
            )
        );
        $criteria = new CDbCriteria(
            array(
                'order'=>'is_attached DESC, updated_at DESC'
            )
        );
        $criteria->condition = "t.status = :status ";
        $criteria->params    = array( ':status' => 1);
        $content = News::model()->getRelation(Yii::app()->session['setlanguage']);
        $criteria->with = $content;
        $dataProvider = new CActiveDataProvider('News', array(
            'criteria'=>$criteria,
            'pagination'=>array(
                'pageSize'=>6,
            ),
        ));
        $tag_model='';
        Yii::app()->clientScript->registerMetaTag('News', 'title');
        $this->pageTitle = Settings::model()->find()->meta_title . ' - News';
        $this->render('index', array(
            'tag_model'=>$tag_model,
            'dataProvider' => $dataProvider,
            'random_consult'=>$random_consult
        ));
    }
    public function actionView($id)
    {

        $model = News::model()->findByPk($id);
        $tags = News::model()->findByPk($id)->tags;
        $random_news = News::model()->findAll(
            array(
                "condition" => "status=1",
                "order" => "rand()",
                "limit" => 5,
            )
         );
        Yii::app()->clientScript->registerMetaTag($model->title, 'title');
        $this->pageTitle = Settings::model()->find()->meta_title . ' - ' . $model->title;
        $this->render('view', array(
            'model' => $model,
            'tags'=>$tags,
            'random_news'=>$random_news
        ));
    }
    public function actionGetByTag($id) {
        $news_for_tag = Tags::model()->findByPk($id)->new_tag;

        $dataProvider = new CArrayDataProvider($news_for_tag, array(
            'pagination'=>array(
                'pageSize'=>6,
            ),
        ));

        $random_consult = Consultation::model()->findAll(
            array(
                "condition" => "status=1",
                "order" => "rand()",
                "limit" => 5,
            )
        );
        Yii::app()->clientScript->registerMetaTag('Search News', 'title');
        $this->pageTitle = Settings::model()->find()->meta_title . ' - Search News';
        $tag_model = Tags::model()->findByPk($id);
        $this->render('index', array(
            'tag_model'=>$tag_model,
            'dataProvider' => $dataProvider,
            'random_consult' => $random_consult,
        ));
    }
}