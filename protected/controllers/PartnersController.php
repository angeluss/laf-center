<?php

class PartnersController extends Controller
{
    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array();
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     * @param string $alias page alias.
     */
    public function actionIndex()
    {
        $partners = Partner::model()->findAllByAttributes(array(
            'status' => 1,
        ));

        Yii::app()->clientScript->registerMetaTag('Partners', 'title');
        $this->pageTitle = Settings::model()->find()->meta_title . ' - Partners';
        $this->render('index', array(
            'partners' => $partners,
        ));
    }
}