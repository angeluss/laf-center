<?php

class ConsultationController extends Controller
{
    /**
     * Declares class-based actions.
     */
    public function actions()
    {
        return array();
    }

    /**
     * This is the default 'index' action that is invoked
     * when an action is not explicitly requested by users.
     * @param string $alias page alias.
     */
    public function actionIndex()
    {
        $law_branches = LawBranches::model()->findAll(array(
                "condition" => "status=1",
        ));
        Yii::app()->clientScript->registerMetaTag('Consultations', 'title');
        $this->pageTitle  = Settings::model()->find()->meta_title . ' - Consultations';
        $this->render('index', array(
            'law_branches'=>$law_branches,
        ));
    }
    public function actionView($id)
    {
        if(Yii::app()->user->isGuest)
            $this->redirect(array('site/login'));
        $model = Consultation::model()->findByPk($id);
        Yii::app()->clientScript->registerMetaTag($model->title, 'title');
        $this->pageTitle = Settings::model()->find()->meta_title . ' - ' . $model->title;
        $this->render('view', array(
            'model' => $model,
        ));
    }

}