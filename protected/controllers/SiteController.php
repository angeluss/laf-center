<?php

class SiteController extends Controller
{
	/**
	 * Declares class-based actions.
	 */
	public function actions()
	{
		return array(
			'captcha' => array(
				'class' => 'CCaptchaAction',
				'backColor' => 0xFFFFFF,
			),
			'page' => array(
				'class' => 'CViewAction',
			),
		);
	}

	/**
	 * This is the default 'index' action that is invoked
	 * when an action is not explicitly requested by users.
	 */
	public function actionIndex()
	{
        Yii::app()->language = Yii::app()->session['setlanguage'];
		Yii::app()->clientScript->registerMetaTag('CLAF', 'title');
		$random_consult = Consultation::model()->findAll(
			array(
				"condition" => "status=1",
				"order" => "rand()",
				"limit" => 5,
			)
		);
		$this->render('index', array(
			'random_consult' => $random_consult,
		));
	}

	/**
	 * This is the action to handle external exceptions.
	 */
	public function actionError()
	{
		$this->layout = '//layouts/error';
		if($error = Yii::app()->errorHandler->error)
		{
			if(Yii::app()->request->isAjaxRequest)
				echo $error['message'];
			else
				$this->render('error', $error);
		}
	}

    public function actionProfile() {
        if (Yii::app()->user->isGuest || !isset(Yii::app()->user->num) || Yii::app()->user->num == 0) {
           throw new CHttpException(404);
        }
        $this->layout = '//layouts/main';
        $this->pageTitle = Yii::t('front', 'Профиль');
        $model = Clients::model()->findByAttributes(array('id' => Yii::app()->user->num));
		$doc = ClientsDocs::model()->findByAttributes(array('client_id' => $model->id));
        $this->performAjaxValidation($model);
        $post = Yii::app()->request->getPost('Clients');
        if(!is_null($post)){
            $model->attributes = $post;
            $model->updated_at = time();
            $model->country = $model->getCountryFront($model->country);
            if ($model->pwd != $model->password && $model->pwd != '' && md5($model->pwd) != $model->password) {
                $model->password = md5($model->pwd);
            }
            if(is_null($model->vpu))$model->vpu = 1;
            if($model->validate()) {
                if ($model->save()) {
                    $post = Yii::app()->request->getPost('Lang');
                    if (!is_null($post)) {
                        foreach ($post as $l) {
                            $lang = ClientsLanguages::model()->findByAttributes(array('client_id' => $model->id));
                            $lang->lang = $l;
                            if ($lang->save()) {

                            } else {
                                CVarDumper::dump($lang->getErrors());
                                die;
                            }
                        }
                    }
                    $doc = ClientsDocs::model()->findByAttributes(array('client_id' => $model->id));
					$docs = Yii::app()->request->getPost('ClientsDocs');
					if(!is_null($docs)) {
						$doc->attributes = $docs;
					}
                    if (isset($_FILES['ClientsDocs']['name']['uplPhoto']) && $_FILES['ClientsDocs']['name']['uplPhoto'] != '') {
                        $doc->photo = md5(time()) . '&_&' . $_FILES['ClientsDocs']['name']['uplPhoto'];
                        $doc->photo_file = $_FILES['ClientsDocs']['name']['uplPhoto'];
						$doc->uplPhoto = CUploadedFile::getInstance($doc,'uplPhoto');
                    }
                    if (isset($_FILES['ClientsDocs']['name']['uplVisa']) && $_FILES['ClientsDocs']['name']['uplVisa'] != '') {
                        $doc->visa = md5(time()) . '&_&' . $_FILES['ClientsDocs']['name']['uplVisa'];
                        $doc->visa_file = $_FILES['ClientsDocs']['name']['uplVisa'];
						$doc->uplVisa = CUploadedFile::getInstance($doc,'uplVisa');
                    }
                    if (isset($_FILES['ClientsDocs']['name']['uplPassport']) && $_FILES['ClientsDocs']['name']['uplPassport'] != '') {
                        $doc->passport = md5(time()) . '&_&' . $_FILES['ClientsDocs']['name']['uplPassport'];
                        $doc->passport_file = $_FILES['ClientsDocs']['name']['uplPassport'];
						$doc->uplPassport = CUploadedFile::getInstance($doc,'uplPassport');
                    }
                    if (isset($_FILES['ClientsDocs']['name']['uplVpu']) && $_FILES['ClientsDocs']['name']['uplVpu'] != '') {
                        $doc->vpu = md5(time()) . '&_&' . $_FILES['ClientsDocs']['name']['uplVpu'];
                        $doc->vpu_file = $_FILES['ClientsDocs']['name']['uplVpu'];
						$doc->uplVpu = CUploadedFile::getInstance($doc,'uplVpu');
                    }
                    if ($doc->save()) {
                        $dir = WATSON . '/uploadFiles/clientsPhoto/';
                        $old_umask = umask(0);

                        if (!is_dir($dir)) {
                            mkdir($dir, 0777, true);
                        }
                        $dir .= '/' . $model->id;
                        if (!is_dir($dir)) {
                            mkdir($dir, 0777, true);
                        }
                        umask($old_umask);
						if (isset($_FILES['ClientsDocs']['name']['uplPhoto']) && $_FILES['ClientsDocs']['name']['uplPhoto'] != '') {
							$doc->uplPhoto->saveAs($dir . '/' . $doc->photo);
						}
						if (isset($_FILES['ClientsDocs']['name']['uplPassport']) && $_FILES['ClientsDocs']['name']['uplPassport'] != '') {
							$doc->uplPassport->saveAs($dir . '/' . $doc->passport);
						}
						if (isset($_FILES['ClientsDocs']['name']['uplVisa']) && $_FILES['ClientsDocs']['name']['uplVisa'] != '') {
							$doc->uplVisa->saveAs($dir . '/' . $doc->visa);
						}
						if (isset($_FILES['ClientsDocs']['name']['uplVpu']) && $_FILES['ClientsDocs']['name']['uplVpu'] != '') {
							$doc->uplVpu->saveAs($dir . '/' . $doc->vpu);
						}
                    } else {
                        CVarDumper::dump($doc->getErrors()); die;
                    }
                } else {
                    CVarDumper::dump($model->getErrors()); die;
                }
            } else {
                $model->getErrors(); die;
            }
        }
        $vuses = Vuz::getVuses();
        $countries = Country::getCountries();
        $lang = ClientsLanguages::model()->findByAttributes(array('client_id' => $model->id));
        $this->render('profile', array(
            'vuses' => $vuses,
            'countries' => $countries,
            'model' => $model,
			'doc' => $doc,
            'lang' => $lang
        ));
    }

    public function actionOferta(){
        $lang = Yii::app()->language;
        $this->layout = '//layouts/grey';
        if(!is_null(Yii::app()->request->getPost('offerta-agree'))){
            $this->redirect('register');
        }
        $model = Oferta::model()->findByAttributes(array('lang' => Oferta::getLanguageByCode($lang)));
		$this->pageTitle = Yii::t('front', 'Публичная оферта');
		Yii::app()->clientScript->registerMetaTag('oferta', 'title');
        $this->render('oferta', array('model' => $model, 'lang' => $lang));
    }

    public function actionRegister(){
		$model = new Clients();
		$doc = new ClientsDocs();
        $post = Yii::app()->request->getPost('Clients');
		if(!is_null($post)){
			$model->attributes = $post;
			$model->country = $model->getCountryFront($model->country);
			$model->created_at = time();
			$model->password = md5($model->password);
			$model->updated_at = time();
			if(is_null($model->vpu))$model->vpu = 1;
			$model->code = '';
			$model->status = Clients::OFFER_ACCEPTED;
			$model->pwd = $model->password;
			$model->card_id = Country::getCountryCode($post['country']). '-' . $model->vuz . '-' . substr(time(), -5);
			if($model->validate()){
				if($model->save()){
                    $post = Yii::app()->request->getPost('Lang');
					if(!is_null($post)){
						foreach($post as $l){
							$lang = new ClientsLanguages();
							$lang->lang = $l;
							$lang->client_id = $model->id;
							if($lang->save()){

							} else {
								CVarDumper::dump($lang->getErrors()); die;
							}
						}
					}
					$docs = Yii::app()->request->getPost('ClientsDocs');
					if(!is_null($docs)) {
						$doc->attributes = $docs;
					}
                    if (isset($_FILES['ClientsDocs']['name']['uplPhoto']) && $_FILES['ClientsDocs']['name']['uplPhoto'] != '') {
                        $doc->photo = md5(time()) . '&_&' . $_FILES['ClientsDocs']['name']['uplPhoto'];
                        $doc->photo_file = $_FILES['ClientsDocs']['name']['uplPhoto'];
						$doc->uplPhoto = CUploadedFile::getInstance($doc,'uplPhoto');
                    }
                    if (isset($_FILES['ClientsDocs']['name']['uplVisa']) && $_FILES['ClientsDocs']['name']['uplVisa'] != '') {
                        $doc->visa = md5(time()). '&_&' . $_FILES['ClientsDocs']['name']['uplVisa'];
                        $doc->visa_file = $_FILES['ClientsDocs']['name']['uplVisa'];
						$doc->uplVisa = CUploadedFile::getInstance($doc,'uplVisa');
                    }
                    if (isset($_FILES['ClientsDocs']['name']['uplPassport']) && $_FILES['ClientsDocs']['name']['uplPassport'] != '') {
                        $doc->passport = md5(time()). '&_&' . $_FILES['ClientsDocs']['name']['uplPassport'];
                        $doc->passport_file = $_FILES['ClientsDocs']['name']['uplPassport'];
						$doc->uplPassport = CUploadedFile::getInstance($doc,'uplPassport');
                    }
                    if (isset($_FILES['ClientsDocs']['name']['uplVpu']) && $_FILES['ClientsDocs']['name']['uplVpu']!= '') {
                        $doc->vpu = md5(time()) . '&_&'. $_FILES['ClientsDocs']['name']['uplVpu'];
                        $doc->vpu_file = $_FILES['ClientsDocs']['name']['uplVpu'];
						$doc->uplVpu = CUploadedFile::getInstance($doc,'uplVpu');
                    }
					$doc->client_id = $model->id;
					if($doc->save()){
                        $dir = WATSON . '/uploadFiles/clientsPhoto/';
                        $old_umask = umask(0);

                        if (!is_dir($dir)) {
                                mkdir($dir, 0777, true);
                            }
                            $dir .= '/' . $model->id;
                            if (!is_dir($dir)) {
                                mkdir($dir, 0777, true);
                            }
                            umask($old_umask);
						if (isset($_FILES['ClientsDocs']['name']['uplPhoto']) && $_FILES['ClientsDocs']['name']['uplPhoto'] != '') {
							$doc->uplPhoto->saveAs($dir . '/' . $doc->photo);
						}
						if (isset($_FILES['ClientsDocs']['name']['uplPassport']) && $_FILES['ClientsDocs']['name']['uplPassport'] != '') {
							$doc->uplPassport->saveAs($dir . '/' . $doc->passport);
						}
						if (isset($_FILES['ClientsDocs']['name']['uplVisa']) && $_FILES['ClientsDocs']['name']['uplVisa'] != '') {
							$doc->uplVisa->saveAs($dir . '/' . $doc->visa);
						}
						if (isset($_FILES['ClientsDocs']['name']['uplVpu']) && $_FILES['ClientsDocs']['name']['uplVpu'] != '') {
							$doc->uplVpu->saveAs($dir . '/' . $doc->vpu);
						}
					} else {
						CVarDumper::dump($doc->getErrors()); die;
					}
					$this->redirect(array('site/showPay', 'id' => $model->id));
				} else {
                    CVarDumper::dump($model->getErrors()); die;
                }
			} else {
				$model->getErrors(); die;
			}
		}
		$vuses = Vuz::getVuses();
		$countries = Country::getCountries();
		$this->pageTitle = Yii::t('front', 'Регистрация');
        $this->layout = '//layouts/grey';
		Yii::app()->clientScript->registerMetaTag('Registration', 'title');
        $this->render('register', array(
			'vuses' => $vuses,
			'countries' => $countries,
			'model' => $model,
			'doc' => $doc,
		));
    }

	public function actionShowPay($id){
		$model = Clients::model()->findByPk($id);
        $post = Yii::app()->request->getPost('location');
		if(!is_null($post)){
			if($post == 1){
				$this->redirect(Yii::app()->createUrl('/site/payLiq/', array('id' => $model->id)));
			} else {
				$this->redirect(Yii::app()->createUrl('/site/payInv/', array('id' => $model->id)));
			}
		}
		$this->pageTitle = Yii::t('front', 'Оплата услуг');
		$this->layout = '//layouts/grey';
		Yii::app()->clientScript->registerMetaTag('showPay', 'title');
		$this->render('showPay');
	}

	public function actionPayLiq($id){
		$this->layout = '//layouts/grey';
		$this->pageTitle = Yii::t('front', 'Оплата');
		$data = LiqPay::getData($id);
		$signature = base64_encode( sha1( LiqPay::PRIVATE_KEY . $data . LiqPay::PRIVATE_KEY, 1 ) );
		Yii::app()->clientScript->registerMetaTag('LiqPay', 'title');
        $this->pageTitle  = Settings::model()->find()->meta_title . ' - LiqPay';
		$this->render('payLiq', array('data' => $data, 'signature' => $signature));
	}

	public function actionPrintInvoice($id){
		$this->renderPartial('_invoice', array(
            'model' => Clients::model()->findByPk($id),
            'req'=> Requisites::model()->find(),
            'print' => true,
        ));
		Yii::app()->clientScript->reset();
	}

	public function actionPayInv($id){
		$model = Clients::model()->findByPk($id);
		$model->status = Clients::GET_INVOICE;
		$model->save();
		$this->layout = '//layouts/grey';
		$this->pageTitle = Yii::t('front', 'Квитанция');
		Yii::app()->clientScript->registerMetaTag('Receipt', 'title');
//        $this->pageTitle = Settings::model()->find()->meta_title . ' - Receipt';
		$this->render('payInv', array('model' => $model, 'req' => Requisites::model()->find(),));
	}

	public function actionRegSuccess($id){
        $this->pageTitle = Yii::t('front', 'Регистрация');
        $this->layout = '//layouts/grey';
        $model = Clients::model()->findByPk($id);
		$msg = $this->renderPartial('_regSuccessMsg', array('model' => $model), true, false);
        $model->sendRegSuccessLetter($msg);
        Yii::app()->clientScript->registerMetaTag('successful registration', 'title');
        $this->pageTitle  = Yii::t('front', 'Успешная регистрация') ;
		$this->render('regSuccess', array('model' => $model));
	}

	public function actionAddLang(){
		$html = $this->renderPartial('_lang_block', array(), true, false);
		echo CJSON::encode(array(
			'html' => $html,
			'success' => true,
		));
		Yii::app()->end();
	}

	public function actionRecoverPass() {
		$this->layout = '//layouts/grey';
		$this->pageTitle = Yii::t('front', 'Восстановление пароля');
		if(!is_null(Yii::app()->request->getPost('email'))){
            Clients::sendChangePass();
            $this->redirect('sendPass');
		}
		Yii::app()->clientScript->registerMetaTag('Password recovery', 'title');
		$this->render('recoverPass');
	}

	public function actionSendPass() {
		$this->layout = '//layouts/grey';
		$this->pageTitle = Yii::t('front', 'Восстановление пароля');
		$this->render('sendPass');
	}

	public function actionSuccessPass() {
		$this->layout = '//layouts/grey';
		$this->pageTitle = Yii::t('front', 'Пароль отправлен');
		$this->render('successPass');
	}

	public function actionChangePass($e) {
		$this->layout = '//layouts/grey';
		$this->pageTitle = Yii::t('front', 'Восстановление пароля');

		$model = Clients::model()->findByAttributes(array('email' => $e));
		if(is_null($model)){
			throw new CHttpException(404);
		}
        $post = Yii::app()->request->getPost('Clients');
		if(!is_null($post)){
			$model->password = md5($post['password']);
			if($model->validate()){
				$model->save();
				$this->redirect('successPass');
			} else {
				CVarDumper::dump($model->getErrors()); die;
			}
		}
		$this->render('changePass', array('model' => $model));
	}

	/**
	 * Displays the login page
	 */
	public function actionLogin()
	{
        $this->layout = '//layouts/grey';
		$this->pageTitle = Yii::t('front','Вход');
		$model=new LoginForm;
        $post = Yii::app()->request->getPost('ajax');
		// if it is ajax validation request
		if(!is_null($post) && $post === 'login-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
        $post = Yii::app()->request->getPost('LoginForm');
		// collect user input data
		if(!is_null($post))
		{
			$model->attributes = $post;
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect('/');
		}
		Yii::app()->clientScript->registerMetaTag('Log In', 'title');
		// display the login form
		$this->render('login',array('model'=>$model));
	}

	/**
	 * Logs out the current user and redirect to homepage.
	 */
	public function actionLogout()
	{
		Yii::app()->user->logout();
		$this->redirect(Yii::app()->homeUrl);
	}

	protected function performAjaxValidation($model)
	{
        $post = Yii::app()->request->getPost('ajax');
		if(!is_null($post) && $post === 'clients-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}

	public function actionValidEmail(){
		$email = Yii::app()->request->getParam('email', false);
		$model = Clients::model()->findAllByAttributes(array('email' => $email));
		echo CJSON::encode(array(
			'success' => !empty($model),
		));
		Yii::app()->end();
	}

	public function actionGetLiqPay($id){
		$answer = json_decode(base64_decode(Yii::app()->request->getPost('data')), true);
		$successStatus = false;
		if(isset($answer['status']))
			if($answer['status'] == 'success' || $answer['status'] == 'sandbox' || $answer['status'] == 'processing')
				$successStatus =  true;

		if(true === $successStatus) {
			$model = Clients::model()->findByPk($id);
			$model->status = Clients::PAID_INVOICE;
			$model->pay_date = time();
			if($model->validate()) {
				$model->save();
			}
		}
	}

	public function actionSendQuestion() {
        $post = Yii::app()->request->getPost('Send');
		if(!is_null($post)) {
            Clients::sendQuestion();
		} else {
			throw new CHttpException(404);
		}
		$this->redirect('index');
	}

    public function actionGetLanguage() {
        $param = Yii::app()->request->getParam('language');
        Yii::app()->language = $param;
        Yii::app()->session['setlanguage'] = $param;
		$url = Yii::app()->request->getParam('url','index');
        if($url !== '/'){
            $url = Yii::app()->createUrl(substr($url, 3));
        } else {
            $url .= $param . '/';
        }
        $url = str_replace('language/', '', $url);
        $this->redirect($url);
    }

    /**
     * Creates chat with OPEN Status.
     * Probably, chat is creating by admin. That's why I use is_admin param(client=0, admin=1);
     * @return Chat $chat new chat object
     */
	public function startChat(){
        $chat = new Chat;
        $id = Yii::app()->user->id;

        if(isset(Yii::app()->user->role) && Yii::app()->user->role !== 'none') {
            $chat->is_admin = 1;
        } else {
            $id = Yii::app()->user->num;
            $chat->is_admin = 0;
        }

        $chat->client = $id;
        $chat->created_at = time();
        $chat->status = Chat::OPEN_STATUS;

        if($chat->validate()){
            $chat->save();
        } else {
            CVarDumper::dump($chat->getErrors()); die;
        }
        return $chat;
    }

    public function actionAddMessageToChat(){
		$msg = Yii::app()->request->getParam('msg');
		$get = Yii::app()->request->getParam('uploadfiles');
        if( !is_null( $get ) ){
            $data = Message::uplFiles();
            echo json_encode( $data );
        }
        $chat = Chat::model()->find(
            'client = :id AND status <> :status ',
            array(
                ':status' => Chat::CLOSED_STATUS,
                ':id' => Yii::app()->user->num,
            )
        );

		if (is_null($chat)) {
			$chat = $this->startChat();
		}

		$message = new Message();
        $message->addMsg($msg, $chat->id);
    }

    public function actionSearch()
    {
        Yii::app()->clientScript->registerMetaTag('Search', 'title');
        $this->pageTitle = Settings::model()->find()->meta_title . ' - Search';
        $search = new SiteSearchForm;
        $search->getparams();

        if ($search->validate()) {
            $params = ConsultationLang::getConsultsForSearch($search);
            $consultations =  $params['consultations'];
            $consult_pagination = $params['searchPagination'];
            $validator_fail = 0;
        } else {
            $consultations = array();
            $consult_pagination = null;
            $validator_fail = 1;
        }

		$consults = array();
		foreach ($consultations as $cons) {
			$consults[$cons->con_id] = Consultation::model()->findByAttributes(array('id' => $cons->con_id,));
		}
        $this->render('found', array(
            'con_pages' => $consult_pagination,
            'search' => $search,
            'consults' => $consults,
            'validator_fail' => $validator_fail,
        ));
    }

	public function actionRefreshChat(){
		$messages = Message::getMessages();
        $html = $this->renderPartial('/layouts/_chat', array(
            'messages' => $messages,
            ), true, false);

        echo CJSON::encode(array(
            'html' => $html,
            'success' => true,
        ));
        Yii::app()->end();
	}

	public function actionCloseChat() {
		$chat_id = Yii::app()->request->getParam('id');
		$chat = Chat::model()->findByPk($chat_id);
		$chat->status = Chat::CLOSED_STATUS;
		if($chat->validate()) {
			$html = '<p class="close_message">' . Yii::t('front', 'Вы завершили чат') . '</p>';
			if($chat->save()){
				echo CJSON::encode(array(
					'html' => $html,
					'success' => true,
				));
				Yii::app()->end();
			}
		} else {
			CVarDumper::dump($chat->getErrors());
		}
	}

    public function actionAddMessageToChat2(){
        $msg = Yii::app()->request->getPost('message');

        $chat = Chat::model()->find(
            'client = :id AND status <> :status ',
            array(
                ':status' => Chat::CLOSED_STATUS,
                ':id' => Yii::app()->user->num,
            )
        );

        if (is_null($chat)) {
            $chat = $this->startChat();
        }
        $message = new Message();
        $message->addMsg2($msg, $chat->id);
    }

	public function actionProfile1(){
		$user = Clients::model()->findByPk(Yii::app()->user->num);
		$this->render('profile', array(
			'model' => $user,
		));
	}

	public function actionDocuments(){
		$user = Clients::model()->findByPk(Yii::app()->user->num);
        $chat_docs = Chat::model()->findAllByAttributes(array('client' => Yii::app()->user->num));
		$this->render('documents', array(
			'model' => $user,
            'chat_docs' => $chat_docs
		));
	}

	public function actionHistory(){
		$user = Clients::model()->findByPk(Yii::app()->user->num);
        $criteria = new CDbCriteria;
        $criteria->condition='client = :client';
        $criteria->params=array(':client'=>Yii::app()->user->num);
        $criteria->order='created_at DESC';
        $chats = Chat::model()->findAll($criteria);
        $messages = Chat::getMessagesForHistory($chats);
        $json = Chat::getJson($chats);

		$this->render('history', array(
			'model' => $user,
            'messages' => $messages,
            'json' => $json,
            'chats' => $chats,
		));
	}

    public  function  actionDeleteDoc($id, $entity){
        if ($entity == 'chat') {
            $doc = Message::model()->findByPk($id);
            $doc->filename = '';
        } else {
            $doc = ClientsDocs::model()->findByPk($id);
            $doc->$entity = NULL;
        }

        if($doc->save()){
            $this->redirect($this->createUrl('documents'));
        }
    }

    public  function  actionRenameDoc(){
        $id = Yii::app()->request->getParam('id');
        $entity = Yii::app()->request->getParam('entity');
        $new_name = Yii::app()->request->getParam('new_name');
        if (!is_null($new_name)) {
            if ($entity !== 'chat') {
                $doc = ClientsDocs::model()->findByPk($id);
                $doc->rename($entity, $new_name);
            } else {
                $doc = Message::model()->findByPk($id);
                $doc->rename($new_name);
            }
        } else {
            echo CJSON::encode(array(
                'new_name' => $new_name,
            ));
        }
    }
}