<?php
$name='Roboto-BoldItalic';
$type='TTF';
$desc=array (
  'Ascent' => 928,
  'Descent' => -244,
  'CapHeight' => 711,
  'Flags' => 262148,
  'FontBBox' => '[-437 -271 1236 1061]',
  'ItalicAngle' => 0,
  'StemV' => 165,
  'MissingWidth' => 249,
);
$up=-73;
$ut=49;
$ttffile='/vagrant/www/xabina/protected/ext/mpdf/ttfonts/Roboto-BoldItalic.ttf';
$TTCfontID='0';
$originalsize=165864;
$sip=false;
$smp=false;
$BMPselected=false;
$fontkey='robotoBI';
$panose=' 0 0 0 0 0 0 0 0 0 0 0 0';
$haskerninfo=false;
$unAGlyphs=false;
?>