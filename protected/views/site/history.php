<?php
/** @var $this SiteController */
?>
<div class="content-wrapper-white profile_container history">
    <div class="profile_wrapper">
        <?php $this->renderPartial('_profileTop', array('model' => $model)); ?>
        <div class="chat_block pull-left">
            <div style="display: block" id="intercom-history">
                <div class="intercom_content">
                    <?php foreach($chats as $chat):?>
                        <div class="dashed_border_data">
                            <p class="past_time"><?php echo Chat::rus_date('j M Y, H:i', $chat->created_at); ?></p>
                        </div>
                        <?php foreach($chat->message as $m): ?>
                            <div class="<?php echo $m->sender == 1 ? 'client_question' : 'operator_answer'; ?>">
                                <?php if($m->sender == 2): ?>
                                    <div class="image_block">
                                        <?php echo Operators::getSenderAvatar($m->chat_id); ?>
                                        <p><?php echo Operators::getOperatorLogin($m->chat_id); ?></p>
                                    </div>
                                <?php endif; ?>
                                <div class="content_block">
                                    <div class="content">
                                        <p><?php echo $m->msg?></p>
                                    </div>
                                    <?php if(!is_null($m->filename) && $m->filename !== ''): ?>
                                        <p class="file_chat_view">
                                            <a href="<?php echo '/uploadFiles/chat/' . $m->chat_id . '/' . $m->file ?>" download>
                                                <?php echo $m->filename ?>
                                            </a>
                                        </p>
                                    <?php endif; ?>
                                    <span class="past_time">
                                        <?php echo date('H:i:s', $m->created_at)?>
                                    </span>
                                </div>
                            </div>
                        <?php endforeach; ?>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>
        <div class="pull-right">
            <div id="chat-calendar-history"></div>
            <script type="application/javascript">
                    $(document).ready(function () {
                        var eventData = [
                           <?php echo $json ?>
                        ];
                        $("#chat-calendar-history").zabuto_calendar({
                            language:'<?php echo Yii::app()->session['setlanguage']; ?>',
                            data: eventData
                        });
                    });
                </script>
        </div>
    </div>
</div>
