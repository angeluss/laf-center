<?php
/** @var $this SiteController */
?>
<div class="content-wrapper-white profile_container docs">
    <div class="profile_wrapper">
        <?php $this->renderPartial('_profileTop', array('model' => $model)); ?>
        <div id="documents_block_profile">
            <h3><?php echo Yii::t('profile', 'Документы и файлы');?></h3>
            <table class="doc-list">
                <thead>
                    <tr>
                        <th><?php echo Yii::t('profile', 'Файл')?></th>
                        <th><?php echo Yii::t('profile', 'Автор')?></th>
                        <th></th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(!empty($model->doc) && $model->doc[0]->passport != ''): ?>
                        <tr>
                            <td>
                                <p class="doc-name">
                                    <?php echo Yii::t('profile', 'Паспорт') ?>
                                </p>
                                <p>
                                    <a id="pathToDir-passport" href="/uploadFiles/clientsPhoto/<?php echo $model->id . '/' . $model->doc[0]->passport?>" target="_blank">
                                        <?php echo $model->getDocName($model->doc[0]->passport) ?>
                                    </a>
                                </p>
                                <div id="collapseDocPassport" class="collapse rename_block">
                                    <input type="text" value=" <?php echo $model->getDocName($model->doc[0]->passport)?>">
                                    <a data-toggle="collapse"
                                       data-url="<?php echo Yii::app()->createUrl('/site/renameDoc')?>"
                                       data-id="<?php echo $model->doc[0]->id ?>"
                                       data-type="passport"
                                       data-client_id="<?php echo$model->id?>"
                                       type="button"
                                       data-block="collapseDocPassport"
                                       class="btn btn-default"
                                       href="#collapseDocPassport"
                                       id="collapseDocPassportLink"
                                       aria-expanded="false"
                                       aria-controls="collapseDocPassport">
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                    </a>
                                    <a type="button" class="btn btn-default" data-toggle="collapse" href="#collapseDocPassport" aria-expanded="false" aria-controls="collapseDocPassport">
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true">

                                        </span>
                                    </a>
                                </div>
                            </td>
                            <td><?php echo Yii::t('profile', 'Я') ?></td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="glyphicon glyphicon-option-vertical" aria-hidden="true"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li><a href="/uploadFiles/clientsPhoto/<?php echo $model->id . '/' . $model->doc[0]->passport?>" download><?php echo Yii::t('profile', 'Скачать') ?></a></li>
                                        <li><a data-toggle="collapse" href="#collapseDocPassport" aria-expanded="false" aria-controls="collapseDocPassport" > <?php echo Yii::t('profile', 'Переименовать') ?></a></li>
                                        <li><a href="<?php echo Yii::app()->createUrl('/site/deleteDoc', array(
                                                'id'=>$model->doc[0]->id,
                                                'entity'=> 'passport'

                                            ));?>">  <?php echo Yii::t('profile', 'Удалить') ?></a></li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    <?php endif; ?>
                    <?php if(!empty($model->doc) &&  $model->doc[0]->visa != ''): ?>
                        <tr>
                            <td>
                                <p class="doc-name">
                                    <?php echo Yii::t('profile', 'Виза') ?>
                                </p>
                                <p>
                                    <a
                                        id="pathToDir-visa"
                                        href="/uploadFiles/clientsPhoto/<?php echo $model->id . '/' . $model->doc[0]->visa?>"
                                        target="_blank"
                                    >
                                        <?php echo $model->getDocName($model->doc[0]->visa) ?>
                                    </a>
                                </p>
                                <div id="collapseDocVisa" class="collapse rename_block">
                                    <input type="text" value=" <?php echo $model->getDocName($model->doc[0]->visa)?>">
                                    <a data-toggle="collapse"
                                       data-url="<?php echo Yii::app()->createUrl('/site/renameDoc')?>"
                                       data-id="<?php echo $model->doc[0]->id ?>"
                                       data-type="passport"
                                       data-client_id="<?php echo$model->id?>"
                                       id="collapseDocVisaLink"
                                       type="button"
                                       data-block="collapseDocVisa"
                                       class="btn btn-default"
                                       href="#collapseDocVisa"
                                       aria-expanded="false"
                                       aria-controls="collapseDocVisa"
                                    >
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                    </a>
                                    <a type="button"
                                       class="btn btn-default"
                                       data-toggle="collapse"
                                       href="#collapseDocVisa"
                                       aria-expanded="false"
                                       aria-controls="collapseDocVisa"
                                    >
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                    </a>
                                </div>
                            </td>
                            <td><?php echo Yii::t('profile', 'Я') ?></td>
                            <td>
                                <div class="btn-group">
                                    <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <span class="glyphicon glyphicon-option-vertical" aria-hidden="true"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="/uploadFiles/clientsPhoto/<?php echo $model->id . '/' . $model->doc[0]->visa?>" download>
                                                <?php echo Yii::t('profile', 'Скачать') ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a data-toggle="collapse" href="#collapseDocVisa" aria-expanded="false" aria-controls="collapseDocVisa" >
                                                <?php echo Yii::t('profile', 'Переименовать') ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo Yii::app()->createUrl('/site/deleteDoc', array('id'=>$model->doc[0]->id, 'entity'=> 'visa'));?>">
                                                <?php echo Yii::t('profile', 'Удалить') ?>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    <?php endif; ?>
                    <?php if(!empty($model->doc) && $model->doc[0]->vpu != ''): ?>
                        <tr>
                            <td>
                                <p class="doc-name">
                                    <?php echo Yii::t('profile', 'Вид на жительство') ?>
                                </p>
                                <p>
                                    <a id="pathToDir-vpu"
                                       href="/uploadFiles/clientsPhoto/<?php echo $model->id . '/' . $model->doc[0]->vpu?>"
                                       target="_blank"
                                    >
                                        <?php echo  $model->getDocName($model->doc[0]->vpu)?>
                                    </a>
                                </p>
                                <div id="collapseDocVpu" class="collapse rename_block">
                                    <input type="text" value=" <?php echo $model->getDocName($model->doc[0]->vpu)?>">
                                    <a data-toggle="collapse"
                                       data-url="<?php echo Yii::app()->createUrl('/site/renameDoc')?>"
                                       data-id="<?php echo $model->doc[0]->id ?>"
                                       data-type="passport"
                                       data-client_id="<?php echo$model->id?>"
                                       id="collapseDocVpuLink"
                                       type="button"
                                       data-block="collapseDocVpu"
                                       class="btn btn-default"
                                       href="#collapseDocVpu"
                                       aria-expanded="false"
                                       aria-controls="collapseDocVpu"
                                    >
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                    </a>
                                    <a type="button"
                                       class="btn btn-default"
                                       data-toggle="collapse"
                                       href="#collapseDocVpu"
                                       aria-expanded="false"
                                       aria-controls="collapseDocVpu"
                                    >
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                    </a>
                                </div>
                            </td>
                            <td><?php echo Yii::t('profile', 'Я') ?></td>
                            <td>
                                <div class="btn-group">
                                    <button type="button"
                                            class="btn btn-default dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"
                                    >
                                        <span class="glyphicon glyphicon-option-vertical" aria-hidden="true"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="/uploadFiles/clientsPhoto/<?php echo $model->id . '/' . $model->doc[0]->vpu?>" download>
                                                <?php echo Yii::t('profile', 'Скачать') ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a data-toggle="collapse" href="#collapseDocVpu" aria-expanded="false" aria-controls="collapseDocVpu" >
                                                <?php echo Yii::t('profile', 'Переименовать') ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo Yii::app()->createUrl('/site/deleteDoc', array('id'=>$model->doc[0]->id, 'entity'=> 'vpu'));?>">
                                                <?php echo Yii::t('profile', 'Удалить') ?>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    <?php endif; ?>
                    <?php if(!empty($model->doc) && $model->doc[0]->photo != ''): ?>
                        <tr>
                            <td>
                                <p class="doc-name">
                                    <?php echo Yii::t('profile', 'Фотография') ?>
                                </p>
                                <p>
                                    <a
                                        id="pathToDir-photo"
                                        href="/uploadFiles/clientsPhoto/<?php echo $model->id . '/' . $model->doc[0]->photo?>"
                                        target="_blank"
                                    >
                                        <?php echo $model->getDocName($model->doc[0]->photo) ?>
                                    </a>
                                </p>
                                <div id="collapseDocPhoto" class="collapse rename_block">
                                    <input type="text" value=" <?php echo $model->getDocName($model->doc[0]->photo)?>">
                                    <a data-toggle="collapse"
                                       data-url="<?php echo Yii::app()->createUrl('/site/renameDoc')?>"
                                       data-id="<?php echo $model->doc[0]->id ?>"
                                       data-type="passport"
                                       data-client_id="<?php echo$model->id?>"
                                       id="collapseDocPhotoLink"
                                       type="button"
                                       data-block="collapseDocPhoto"
                                       class="btn btn-default"
                                       href="#collapseDocPhoto"
                                       aria-expanded="false"
                                       aria-controls="collapseDocPhoto"
                                    >
                                        <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                    </a>
                                    <a type="button"
                                       class="btn btn-default"
                                       data-toggle="collapse"
                                       href="#collapseDocPhoto"
                                       aria-expanded="false"
                                       aria-controls="collapseDocPhoto"
                                    >
                                        <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                    </a>
                                </div>
                            </td>
                            <td><?php echo Yii::t('profile', 'Я') ?></td>
                            <td>
                                <div class="btn-group">
                                    <button type="button"
                                            class="btn btn-default dropdown-toggle"
                                            data-toggle="dropdown"
                                            aria-haspopup="true"
                                            aria-expanded="false"
                                    >
                                        <span class="glyphicon glyphicon-option-vertical" aria-hidden="true"></span>
                                    </button>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="/uploadFiles/clientsPhoto/<?php echo $model->id . '/' . $model->doc[0]->photo?>" download><?php echo Yii::t('profile', 'Скачать') ?></a></li>
                                        <li>
                                            <a data-toggle="collapse"
                                               href="#collapseDocPhoto"
                                               aria-expanded="false"
                                               aria-controls="collapseDocPhoto"
                                            >
                                                <?php echo Yii::t('profile', 'Переименовать') ?>
                                            </a>
                                        </li>
                                        <li>
                                            <a href="<?php echo Yii::app()->createUrl('/site/deleteDoc', array('id'=>$model->doc[0]->id, 'entity'=> 'photo'));?>">
                                                <?php echo Yii::t('profile', 'Удалить') ?>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </td>
                        </tr>
                    <?php endif; ?>
                    <?php foreach ($chat_docs as $chat): ?>
                        <?php if (!empty($chat->message)) : ?>
                            <?php foreach ($chat->message as $m): ?>
                                <?php if ($m->filename!=='' && $m->filename!==NULL ): ?>
                                <tr>
                                    <td>
                                        <p class="doc-name">
                                            <?php echo Yii::t('profile', 'Документы из чата') ?>
                                        </p>
                                        <p>
                                            <a id="pathToDir-<?php echo $m->id ?>"
                                               href="/uploadFiles/chat/<?php echo $m->chat_id . '/'.$m->created_at.'_'.$m->filename?>"
                                               target="_blank"
                                            >
                                                <?php echo $m->filename ?>
                                            </a>
                                        </p>
                                        <div id="collapseDoc-<?php echo $m->id ?>" class="collapse rename_block chat_doc_rename">
                                            <input type="text" value=" <?php echo $m->filename ?>">
                                            <a data-toggle="collapse"
                                               data-url="<?php echo Yii::app()->createUrl('/site/renameDoc')?>"
                                               data-id="<?php echo $m->id?>"
                                               data-type="chat"
                                               data-client_id="<?php echo$m->id?>"
                                               type="button"
                                               data-block="collapseDoc-<?php echo $m->id ?>"
                                               class="btn btn-default chat_doc_rename_link"
                                               href="#collapseDoc-<?php echo $m->id ?>"
                                               aria-expanded="false"
                                               aria-controls="collapseDoc-<?php echo $m->id ?>"
                                            >
                                                <span class="glyphicon glyphicon-ok" aria-hidden="true"></span>
                                            </a>
                                            <a type="button"
                                               class="btn btn-default"
                                               data-toggle="collapse"
                                               href="#collapseDoc-<?php echo $m->id ?>"
                                               aria-expanded="false"
                                               aria-controls="collapseDoc-<?php echo $m->id ?>"
                                            >
                                                <span class="glyphicon glyphicon-remove" aria-hidden="true"></span>
                                            </a>
                                        </div>
                                    </td>
                                    <td>
                                        <?php echo $m->sender == 1 ? Yii::t('profile', 'Я') : Yii::t('profile', 'Оператор'); ?>
                                    </td>
                                    <td>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <span class="glyphicon glyphicon-option-vertical" aria-hidden="true"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="/uploadFiles/chat/<?php echo $m->chat_id . '/'.$m->created_at.'_'.$m->filename?>" download><?php echo Yii::t('profile', 'Скачать') ?></a></li>
                                                <li><a data-toggle="collapse" href="#collapseDoc-<?php echo $m->id ?>" aria-expanded="false" aria-controls="collapseDoc-<?php echo $m->id ?>" > <?php echo Yii::t('profile', 'Переименовать') ?></a></li>
                                                <li><a href="<?php echo Yii::app()->createUrl('/site/deleteDoc', array(
                                                        'id'=>$m->id,
                                                        'entity'=> 'chat'

                                                    ));?>">  <?php echo Yii::t('profile', 'Удалить') ?></a></li>
                                            </ul>
                                        </div>
                                    </td>
                                </tr>
                                <?php  endif; ?>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>