<select  name="Lang[]" size="" class="lang_input" id="lang_input">
    <option value="">
        <?php echo Yii::t('front', 'Предпочитаемый язык общения') ?>
    </option>
    <?php foreach(ClientsLanguages::getLang() as $id => $lang):?>
        <option value="<?php echo $id?>">
            <?php echo $lang; ?>
        </option>
    <?php endforeach; ?>
</select>
<a class="select-button">&#119119;</a>