<div id="main_content" class="clearfix">
    <div class="content-wrapper-white profile_container registration-block">
        <?php $this->renderPartial('_profileTop', array('model' => $model)); ?>
        <div class="tab-content">
            <div id="profile_info" class="tab-pane fade in active">
                <div class="form_edit_profile_info">
                    <form method="post" enctype="multipart/form-data" id="form_register">
                        <div class="notification">
                            <p><?php echo Yii::t('front', 'Регистрационные данные') ?></p>
                        </div>
                        <div class="registration-block-wrapper">
                            <ul>
                                <li>
                                    <p class="form_has_errors" style="display: none; color:red">
                                        <?php echo Yii::t('front', 'Форма содержит ошибки. Пожалуйста, исправьте их.');?>
                                    </p>
                                    <p>
                                        <label for="surname"><?php echo Yii::t('front', 'Surname, First Name') ?></label>
                                    </p>
                                    <div>
                                        <input placeholder="<?php echo Yii::t('front', 'Surname, First Name') ?>" value="<?php echo $model->fullname ?>" type="text" id="fullname" name="Clients[fullname]"  />
                                        <p id="error_fullname" style="display:none; color: red;"></p>
                                    </div>
                                    <p><?php echo Yii::t('front', 'Латиницей как в паспорте') ?></p>
                                </li>
                                <li>
                                    <label for="name"><?php echo Yii::t('front', 'Прізвище, ім’я, по-батькові') ?></label>
                                    <p>
                                        <input placeholder="<?php echo Yii::t('front', 'Прізвище, ім’я, по-батькові') ?>" type="text" id="name" value="<?php echo $model->fullname_uk ?>" name="Clients[fullname_uk]" />
                                    </p>
                                    <p id="error_name" style="display:none; color: red;"></p>
                                    <p><?php echo Yii::t('front', 'На украинском языке, как в регистрационных документах') ?></p>
                                </li>
                                <li class="birth-date">
                                    <div>
                                        <p><?php echo Yii::t('front', 'Пол') ?></p>
                                        <input type="radio" name="Clients[gender]" id="male" value="1" <?php echo ($model->gender==1) ? 'checked' : '' ?> />
                                        <label for="male"><?php echo Yii::t('front', 'М') ?></label>
                                        <input type="radio" name="Clients[gender]" value='2' id="female" <?php echo ($model->gender==2) ? 'checked' : '' ?> />
                                        <label for="female"><?php echo Yii::t('front', 'Ж') ?></label>
                                    </div>
                                    <div>
                                        <p><?php echo Yii::t('front', 'Дата рождения') ?></p>
                                        <input name="Clients[bitrhdate]" id="birthdate" placeholder="<?php echo Yii::t('front', 'Дата рождения') ?>" value="<?php echo $model->bitrhdate ?>"  type="text" class="date-input" />
                                        <i class="fa fa-calendar"></i>
                                        <p id="error_birthdate" style="display:none; color: red;"></p>
                                    </div>
                                </li>
                                <li>
                                    <label><?php echo Yii::t('profile', 'Страна') ?></label>
                                    <div class="select-outer">
                                        <select id="country" name="Clients[country]" size="">
                                            <?php foreach($countries as $id => $info): ?>
                                                <option value="<?php echo $id ?>" <?php if ($info['title'] == $model->country) echo 'selected' ?>>
                                                    <?php echo $info['title'] ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                        <a class="select-button">&#119119;</a>
                                    </div>
                                    <p id="error_country" style="display:none; color: red;"></p>
                                </li>
                                <li>
                                    <label><?php echo Yii::t('front', 'Предпочитаемый язык общения') ?></label>
                                    <div class="select-outer">
                                        <div class="lang_selects">
                                            <select  name="Lang[]" size="" class="lang_input" id="lang_input">
                                                <option value="">
                                                    <?php echo Yii::t('front', 'Предпочитаемый язык общения') ?>
                                                </option>
                                                <?php foreach(ClientsLanguages::getLang() as $id => $lang_n):?>
                                                    <option value="<?php echo $id?>"
                                                        <?php if ($id == $lang->lang) echo 'selected' ?>>
                                                        <?php echo $lang_n; ?>
                                                    </option>
                                                <?php endforeach; ?>
                                            </select>
                                            <a class="select-button">&#119119;</a>
                                        </div>
                                    </div>
                                    <p id="error_lang_input" style="display:none; color: red;"></p>
                                </li>
                                <li>
                                    <label><?php echo Yii::t('front', 'ВУЗ') ?></label>
                                    <div class="select-outer">
                                        <select  id="vuz" name="Clients[vuz]" size="">
                                            <?php foreach($vuses as $code => $title): ?>
                                                <option value="<?php echo $code?>" <?php if ($code == $model->vuz) echo 'selected' ?>>
                                                    <?php echo $title?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                        <a class="select-button">&#119119;</a>
                                    </div>
                                    <p id="error_vuz" style="display:none; color: red;"></p>
                                </li>
                                <li>
                                    <label><?php echo Yii::t('profile', 'Область') ?></label>
                                    <div class="select-outer">
                                        <select  name="Clients[oblast]" id="oblast" size="">
                                            <?php foreach(Country::getOblast() as $id => $title): ?>
                                                <option value="<?php echo $id ?>" <?php if ($id == $model->oblast) echo 'selected' ?>>
                                                    <?php echo $title ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                        <a class="select-button">&#119119;</a>
                                    </div>
                                    <p id="error_oblast" style="display:none; color: red;"></p>
                                </li>
                                <li>
                                    <label><?php echo Yii::t('profile', 'Район области') ?></label>
                                    <div class="select-outer">
                                        <select id="region_areas" name="Clients[district]" size="">
                                            <?php foreach(Country::getDistrict() as $id => $title):?>
                                                <option value = "<?php echo $id ?>" <?php if ($id == $model->district) echo 'selected' ?>>
                                                    <?php echo $title ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                        <a class="select-button">&#119119;</a>
                                    </div>
                                    <p id="error_region_areas" style="display:none; color: red;"></p>
                                </li>
                                <li class="input-city hidden">
                                    <label><?php echo Yii::t('profile', 'Город') ?></label>
                                    <input type="text" placeholder="<?php echo Yii::t('front', 'Введите город') ?>" id="input-city" value="<?php echo $model->city ?>"  name="Clients[city]" size="" />
                                </li>
                                <li class="select-district hidden">
                                    <label><?php echo Yii::t('profile', 'Район') ?></label>
                                    <div class="select-outer">
                                        <select id="select-district" name="Clients[area]" size="" >
                                            <?php foreach(Country::getHDistrict() as $id => $title):?>
                                                <option value = <?php echo $id ?>>
                                                    <?php echo $title ?>
                                                </option>
                                            <?php endforeach; ?>
                                        </select>
                                        <a class="select-button">&#119119;</a>
                                    </div>
                                </li>
                                <li>
                                    <label><?php echo Yii::t('profile', 'Адрес') ?></label>
                                    <input type="text" placeholder="<?php echo Yii::t('front', 'Введите ваш адрес (улица, дом, квартира)') ?>" id="input-address" value="<?php echo $model->street ?>"  name="Clients[street]" size="">
                                    <p id="error_input_address" style="display:none; color: red;"></p>
                                </li>
                                    <?php if(isset($model->doc[0]->photo) && $model->doc[0]->photo != ''):?>
                                        <li class="files_upload-block withfoto">
                                            <div class="pull-left">
                                                <img width="160px" src="/uploadFiles/clientsPhoto/<?php echo $model->id . '/' . $model->doc[0]->photo?>">
                                            </div>
                                            <div class="docsblock pull-left">
                                                <p class=""><?php echo Yii::t('profile', 'Фотография') ?></p>
                                                <a id="pathToDir-photo" href="/uploadFiles/clientsPhoto/<?php echo $model->id . '/' . $model->doc[0]->photo; ?>" target="_blank">
                                                    <?php echo $model->getDocName($model->doc[0]->photo) ?>
                                                </a>
                                            </div>
                                            <div class="pull-right">
                                                <p>
                                                    <?php echo Yii::t('profile', 'Прикрепить другой файл') ?>
                                                </p>
                                                <div class="fileform">
                                                    <p class="file-name-title"><?php echo Yii::t('front', 'Выбрать файл') ?></p>
                                                    <div class="selectbutton"><?php echo Yii::t('front', 'Обзор') ?></div>
                                                    <?php echo CHtml::activeFileField($doc, 'uplPhoto', array('id'=>'upload-foto-docs')); ?>
                                                </div>
                                            </div>
                                            <div class="clearfix"></div>
                                        </li>

                                <?php else : ?>
                                    <li class="files_upload-block">
                                            <p>
                                                <?php echo Yii::t('profile', 'Прикрепить другие документы') ?>
                                            </p>
                                            <p>
                                                <?php echo Yii::t('front', 'Наличие документов не обязательно, но крайне желательно') ?>
                                            </p>
                                            <div class="pull-left">
                                                - <?php echo Yii::t('front', 'фотография 3х4') ?>
                                            </div>
                                            <div class="fileform pull-right">
                                                <p class="file-name-title"><?php echo Yii::t('front', 'Выбрать файл') ?></p>
                                                <div class="selectbutton"><?php echo Yii::t('front', 'Обзор') ?></div>
                                                <?php echo CHtml::activeFileField($doc, 'uplPhoto', array('id'=>'upload-foto-docs')); ?>
                                            </div>
                                            <div class="clearfix"></div>
                                        </li>
                                <?php endif; ?>
                                <?php if(!empty($model->doc) && $model->doc[0]->visa): ?>
                                    <li class="files_upload-block">
                                        <div class="pull-left">
                                            <img width="160px" src="/uploadFiles/clientsPhoto/<?php echo $model->id . '/' . $model->doc[0]->visa; ?>">
                                        </div>
                                        <div class="docsblock pull-left">
                                            <p class=""><?php echo Yii::t('front', 'Виза') ?></p>
                                            <a id="pathToDir-photo" href="/uploadFiles/clientsPhoto/<?php echo $model->id . '/' . $model->doc[0]->visa; ?>" target="_blank">
                                                <?php echo $model->getDocName($model->doc[0]->visa) ?>
                                            </a>
                                        </div>
                                        <div class="fileform pull-right">
                                            <p class="file-name-title"><?php echo Yii::t('front', 'Выбрать файл') ?></p>
                                            <div class="selectbutton"><?php echo Yii::t('front', 'Обзор') ?></div>
                                            <?php echo CHtml::activeFileField($doc, 'uplVisa', array('id'=>'upload-visa-docs')); ?>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>
                                <?php else : ?>
                                    <li class="files_upload-block">
                                        <p>
                                            <?php echo Yii::t('profile', 'Прикрепить другие документы') ?>
                                        </p>
                                        <p>
                                            <?php echo Yii::t('front', 'Наличие документов не обязательно, но крайне желательно') ?>
                                        </p>
                                        <div class="pull-left">
                                            - <?php echo Yii::t('front', 'виза') ?>
                                        </div>
                                        <div class="fileform pull-right">
                                            <p class="file-name-title"><?php echo Yii::t('front', 'Выбрать файл') ?></p>
                                            <div class="selectbutton"><?php echo Yii::t('front', 'Обзор') ?></div>
                                            <?php echo CHtml::activeFileField($doc, 'uplVisa', array('id'=>'upload-visa-docs')); ?>
                                        </div>
                                    </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                        <div class="notification">
                            <p><?php echo Yii::t('front', 'Паспортные данные') ?></p>
                        </div>
                        <div class="registration-block-wrapper">
                            <ul>
                                <li>
                                    <p>
                                        <label><?php echo Yii::t('front', 'Номер паспорта') ?></label>
                                        <input name="Clients[passport]" placeholder="<?php echo Yii::t('front', 'Номер паспорта') ?>" type="text" value="<?php echo $model->passport ?>" id="number-pass" />
                                        <span id="error_number_pass" style="display:none; color: red;"></span>
                                    </p>
                                </li>
                                <li class="passport-dates">
                                    <div class="pull-left">
                                        <label class="pull-left"><?php echo Yii::t('front', 'Дата выдачи') ?></label>
                                        <input type="text" id="date_pass" placeholder="<?php echo Yii::t('front', 'Дата выдачи') ?>" name="Clients[date_passport]" value="<?php echo $model->date_passport ?>" class="date-input" />
                                        <label class="pull-right"><i class="fa fa-calendar"></i></label>
                                        <p id="error_date_pass" style="display:none; color: red;"></p>
                                    </div>
                                    <div class="pull-right">
                                        <label class="pull-left"><?php echo Yii::t('front', 'Срок действия') ?></label>
                                        <input type="text" id="till_pass" placeholder="<?php echo Yii::t('front', 'Срок действия') ?>" name="Clients[passport_till]" value="<?php echo $model->passport_till ?>" class="date-input" />
                                        <label class="pull-right"><i class="fa fa-calendar"></i></label>
                                        <p id="error_till_pass" style="display:none; color: red;"></p>
                                    </div>
                                </li>
                                <div class="clearfix"></div>
                                <?php if(!empty($model->doc) && $model->doc[0]->passport): ?>
                                    <li class="files_upload-block withfoto">
                                        <div class="pull-left">
                                            <img width="160px" src="/uploadFiles/clientsPhoto/<?php echo $model->id . '/' . $model->doc[0]->passport?>">
                                        </div>
                                        <div class="docsblock pull-left">
                                            <p class=""><?php echo Yii::t('front', 'Паспорт') ?></p>
                                            <a id="pathToDir-photo" href="/uploadFiles/clientsPhoto/<?php echo $model->id . '/' . $model->doc[0]->passport; ?>" target="_blank">
                                                <?php echo $model->getDocName($model->doc[0]->passport); ?>
                                            </a>
                                        </div>
                                        <div class="pull-right">
                                            <p>
                                                <?php echo Yii::t('profile', 'Прикрепить другой файл') ?>
                                            </p>
                                            <div class="fileform">
                                                <p class="file-name-title"><?php echo Yii::t('front', 'Выбрать файл') ?></p>
                                                <div class="selectbutton"><?php echo Yii::t('front', 'Обзор') ?></div>
                                                <?php echo CHtml::activeFileField($doc, 'uplPassport', array('id'=>'upload-pass-docs')); ?>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>
                                <?php else : ?>
                                    <li class="files_upload-block">
                                        <p>
                                            <?php echo Yii::t('front', 'Прикрепить скан или фотографию паспорта'); ?>
                                        </p>
                                        <p>
                                            <?php echo Yii::t('front', 'Наличие документов не обязательно, но крайне желательно') ?>
                                        </p>
                                        <div class="fileform pull-left">
                                            <p class="file-name-title">
                                                <?php echo Yii::t('front', 'Выбрать файл') ?>
                                            </p>
                                            <div class="selectbutton">
                                                <?php echo Yii::t('front', 'Обзор') ?>
                                            </div>
                                            <?php echo CHtml::activeFileField($doc, 'uplPassport', array('id'=>'upload-pass-docs')); ?>
                                        </div>
                                    </li>
                                <?php endif; ?>
                                <li>
                                    <p>
                                        <input name="Clients[tax_number]" placeholder="<?php echo Yii::t('front', 'Регистрационный номер плательщика налогов') ?>" type="text" />
                                    </p>
                                    <p>
                                        <?php echo Yii::t('front', 'Регистрационный номер учетной карточки плательщика налогов') ?>
                                    </p>
                                    <p><?php echo Yii::t('front', 'Заполнять в случае наличия') ?></p>
                                </li>
                            </ul>
                        </div>
                        <div class="notification">
                            <p>
                                <?php echo Yii::t('front', 'Удостоверение на временное проживание на территории Украины (вид на жительство)') ?>
                            </p>
                            <p><?php echo Yii::t('front', 'Заполнять в случае наличия') ?></p>
                        </div>
                        <div class="registration-block-wrapper">
                            <ul>
                                <li>
                                    <p>
                                        <input type="checkbox" name="Clients[vpu]" id="no-certificate" <?php echo ($model->vpu == 1) ? 'checked' : '' ?> />
                                        <label for="no-certificate"><?php echo Yii::t('front', 'Удостоверение отсутствует (еще не выдано)') ?></label>
                                    </p>
                                </li>
                                <div class="clearfix"></div>
                                <li class="license_block1">
                                    <div class="pull-left">
                                        <label for="serial" class="pull-left"><?php echo Yii::t('front', 'Серия') ?></label>
                                        <input name="Clients[vpu_serie]" placeholder="<?php echo Yii::t('front', 'Серия') ?>" value="<?php echo $model->vpu_serie ?>" type="text" id="serial" />
                                    </div>
                                    <div class="pull-right">
                                        <label for="serial-number" class="pull-left"><?php echo Yii::t('front', 'Номер') ?></label>
                                        <input name="Clients[vpu_number]" placeholder="<?php echo Yii::t('front', 'Номер') ?>" type="text" value="<?php echo $model->vpu_number ?>" id="serial-number" />
                                    </div>
                                    <div class="clearfix"></div>
                                </li>
                                <li class="license_block2">
                                    <label class=""><?php echo Yii::t('front', 'Срок действия') ?></label>
                                    <input name="Clients[vpu_till]" placeholder="<?php echo Yii::t('front', 'Срок действия') ?>" type="text" value="<?php echo $model->vpu_till ?>" class="date-input" />
                                    <label class="pull-right"><i class="fa fa-calendar"></i></label>
                                </li>
                                <li class="license_block3">
                                    <label for="vpu_from" class="pull-left"><?php echo Yii::t('front', 'Орган, выдавший удостоверение') ?></label>
                                    <p>
                                        <input name="Clients[vpu_from]" placeholder="<?php echo Yii::t('front', 'Орган, выдавший удостоверение') ?>" value="<?php echo $model->vpu_from ?>" type="text" />
                                    </p>
                                </li>
                                <?php if(!empty($model->doc) && $model->doc[0]->vpu): ?>
                                    <li class="files_upload-block license_block4 withfoto">
                                        <div class="pull-left">
                                            <img width="160px" src="/uploadFiles/clientsPhoto/<?php echo $model->id . '/' . $model->doc[0]->vpu; ?>">
                                        </div>
                                        <div class="docsblock pull-left">
                                            <p class=""><?php echo Yii::t('front', 'Удостоверение') ?></p>
                                            <a id="pathToDir-photo" href="/uploadFiles/clientsPhoto/<?php echo $model->id . '/' . $model->doc[0]->vpu; ?>" target="_blank">
                                                <?php echo $model->getDocName($model->doc[0]->vpu) ?>
                                            </a>
                                        </div>
                                        <div class="pull-right">
                                            <p><?php echo Yii::t('profile', 'Прикрепить другой файл') ?></p>
                                            <div class="fileform">
                                                <p class="file-name-title"><?php echo Yii::t('front', 'Выбрать файл')?></p>
                                                <div class="selectbutton"><?php echo Yii::t('front', 'Обзор') ?></div>
                                                <?php echo CHtml::activeFileField($doc, 'uplVpu', array('id'=>'upload-vpu-docs')); ?>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                    </li>
                                <?php else : ?>
                                    <li class="files_upload-block license_block4">
                                    <p>
                                        <?php echo Yii::t('front', 'Прикрепить скан или фотографию удостоверения') ?>
                                    </p>
                                    <p>
                                        <?php echo Yii::t('front', 'Наличие копии не обязательно, но крайне желательно') ?>
                                    </p>
                                    <div class="fileform pull-left">
                                        <p class="file-name-title">
                                            <?php echo Yii::t('front', 'Выбрать файл') ?>
                                        </p>
                                        <div class="selectbutton">
                                            <?php echo Yii::t('front', 'Обзор') ?>
                                        </div>
                                        <?php echo CHtml::activeFileField($doc, 'uplVpu', array('id'=>'upload-vpu-docs')); ?>
                                    </div>
                                </li>
                                <?php endif; ?>
                            </ul>
                        </div>
                        <div class="notification">
                            <p><?php echo Yii::t('front', 'Контактная информация') ?></p>
                        </div>
                        <div class="registration-block-wrapper">
                            <ul>
                                <li>
                                    <p>
                                        <label><?php echo Yii::t('front', 'Телефон') ?></label>
                                    </p>
                                    <p>
                                        <input name="Clients[phone]" placeholder="<?php echo Yii::t('front', 'Телефон') ?>" type="text" value="<?php echo $model->phone ?>" id="phone" />
                                    </p>
                                    <p id="error_phone" style="display:none; color: red;"></p>
                                </li>
                                <li>
                                    <label><?php echo Yii::t('front', 'Фактический адрес проживания') ?></label>
                                    <p>
                                        <input name="Clients[address_reg]" placeholder="<?php echo Yii::t('front', 'Фактический адрес проживания') ?>" type="text" value="<?php echo $model->address_reg ?>" id="fact-address" />
                                    </p>
                                </li>
                                <li class="" style="">
                                    <label><?php echo Yii::t('profile', 'Email') ?></label>
                                    <p>
                                        <input name="Clients[email]" placeholder="Email" type="email" value="<?php echo $model->email ?>" id="email" />
                                    </p>
                                    <p id="error_email" style="display:none; color: red;"></p>
                                    <p>
                                        <?php echo Yii::t('front', 'Используется для входа в систему и отправки уведомлений') ?>
                                    </p>
                                </li>
                                <li class="showpassword" style="display: none">
                                    <p>
                                        <input name="Clients[pwd]"  placeholder="<?php echo Yii::t('front', 'Пароль') ?>" type="password" value="<?php echo $model->password ?>" id="password" />
                                    </p>
                                    <p id="error_password" style="display:none; color: red;"></p>
                                    <p id="pass_tip">
                                        <?php echo Yii::t('front', 'Не менее 6 символов') ?>
                                    </p>
                                    <span class="pass-icon show"></span>
                                    <span class="pass-icon hide" style="display: none;"></span>
                                </li>
                                <li>
                                    <a href="javascript:" id="show_pwd_link">
                                        <?php echo Yii::t('profile', 'Изменить пароль') ?>
                                    </a>
                                </li>
                            </ul>
                            <div class="reg_button">
                                <button id="prof_but"><?php echo Yii::t('profile', 'Сохранить') ?></button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

