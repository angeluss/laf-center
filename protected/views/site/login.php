<?php
/* @var $this SiteController */
/* @var $model LoginForm */
/* @var $form CActiveForm  */

?>
<div class="content-wrapper-white login-block">
    <div class="login-wrapper">
        <div class="form">
            <?php $form = $this->beginWidget('CActiveForm', array(
                'id' => 'login-form',
                'enableClientValidation' => false,
                'clientOptions' => array(
                    'validateOnSubmit' => false,
                ),
                'htmlOptions' => array(
                    'class' => 'login-form',
                ),
            )); ?>
            <ul>
                <li>
                    <?php echo $form->labelEx($model,'email'); ?>
                    <p>
                        <?php echo $form->textField($model,'email'); ?>
                    </p>
                    <?php echo $form->error($model,'email'); ?>
                </li>
                <li>
                    <?php echo $form->labelEx($model, 'password'); ?>
                    <p><?php echo $form->passwordField($model, 'password'); ?></p>
                    <?php echo $form->error($model, 'password'); ?>
                    <span id="pass_icon_show" class="pass-icon show"></span>
                    <span id="pass_icon_hide" class="pass-icon hide" style="display: none;"></span>
                </li>
                <li>
                    <div class="login-submit-block">
                        <div class="login-btn">
                            <?php echo CHtml::submitButton(Yii::t('front', 'Войти')); ?>
                        </div>
                        <div class="login-links">
                            <a href="<?php echo Yii::app()->createUrl('site/recoverPass'); ?>">
                                <?php echo Yii::t('front', 'Восстановление пароля') ?>
                            </a>
                            <span>|</span>
                            <a href="<?php echo Yii::app()->createUrl('site/oferta'); ?>">
                                <?php echo Yii::t('front', 'Регистрация') ?>
                            </a>
                        </div>
                    </div>
                </li>
            </ul>
            <?php $this->endWidget(); ?>
        </div><!-- form -->
    </div>
</div>
