<div class="content-wrapper-white login-block restore-block">
    <div class="login-wrapper">
        <form method="post" id="form_change" class="login-form respass-form">
            <ul>
                <li>
                    <label for="email"><?php echo Yii::t('front', 'Новый пароль') ?></label>
                    <p>
                        <input class="" placeholder="password" type="password" id="Clients_password" name="Clients[password]" />
                    </p>
                    <p id="error_password" style="display:none; color: red;"></p>
                    <p class="respass-message"><?php echo Yii::t('front', 'Введите новый пароль') ?></p>
                </li>
                <li>
                    <div class="login-submit-block">
                        <div class="login-btn respass-btn">
                            <?php echo CHtml::submitButton(Yii::t('front', 'Изменить'), array('id' => 'chg_pwd')); ?>
                        </div>
                    </div>
                </li>
            </ul>
        </form>
        <p id="js_msgs"
           style="display: none"
           data-empty="<?php echo Yii::t('front', 'Пожалуйста, заполните это поле') ?>"
           data-small="<?php echo Yii::t('front', 'Не менее 6 символов') ?>" ></p>
    </div>
</div>

