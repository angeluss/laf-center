<?php
/* @var $this SiteController */
$this->pageTitle = Settings::model()->find()->meta_title;
?>
<div class="content-wrapper-white con-block">
    <div class="reg_list pull-left">
        <?php
            echo Yii::t(
                'front',
                Blocks::model()
                    ->findByAttributes(array('title' => 'why_it_need'))
                    ->getContent(Yii::app()->session['setlanguage'])
                    ->text
            );
        ?>
    </div>
    <div class="reg_list pull-right">
        <div class="legal-advice pull-left">
            <h5 class="right-col-title"><?php echo Yii::t('front', 'Правовые консультации'); ?></h5>
            <ul class="question-link">
                <?php if ($random_consult): ?>
                    <?php foreach ($random_consult as $consult): ?>
                        <li>
                            <?php if(Yii::app()->user->isGuest): ?>
                                <a id="no_reg_modal_link" href="javascript:">
                                    <?php echo $consult->getContent(Yii::app()->session['setlanguage'])->title; ?>
                                </a>
                            <?php else: ?>
                                <a href="<?php echo Yii::app()->createUrl('consultation/view', array('id' => $consult->id)); ?>">
                                    <?php echo $consult->getContent(Yii::app()->session['setlanguage'])->title; ?>
                                </a>
                            <?php endif; ?>
                        </li>
                    <?php  endforeach; ?>
                <?php endif; ?>
            </ul>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="links-for-clients">
        <?php if(Yii::app()->user->isGuest || !isset(Yii::app()->user->num) || Yii::app()->user->num == 0): ?>
            <div class="pull-left reg_button" id="index_top_reg_button" data-url="<?php echo Yii::app()->createUrl('site/oferta'); ?>">
                <h4><?php echo Yii::t('front', 'Станьте клиентом'); ?></h4>
                <p><?php echo Yii::t('front', 'Регистрация и договор публичной офферты'); ?></p>
            </div>
        <?php else: ?>
            <div class="reg_button" id="index_top_chat_button">
                <div class="aligner">
                    <span class="quest_icon"></span>
                    <p>
                        <?php echo Yii::t('front', 'Задать вопрос'); ?>
                        <span class="status gray-color">online</span>
                    </p>
                </div>
            </div>
        <?php endif; ?>
        <?php if(Yii::app()->user->isGuest): ?>
            <a class="pull-left" href="<?php echo Yii::app()->createUrl('/site/login'); ?>">
                <?php echo Yii::t('front', 'Вход для клиентов') ?>
            </a>
        <?php endif; ?>
    </div>
</div>
<div class="content-wrapper-yellow advantages">
    <?php
        echo Yii::t(
            'front',
            Blocks::model()->findByAttributes(array('title' => 'advantages_block'))
                ->getContent(Yii::app()->session['setlanguage'])
                ->text
        );
    ?>
<div class="clearfix"></div>
</div>
<div class="content-wrapper-white information-block">
    <div class="reg_list pull-left">
        <?php
            echo Yii::t(
                'front',
                Blocks::model()->findByAttributes(array('title' => 'phones_dov_block'))
                    ->getContent(Yii::app()->session['setlanguage'])
                    ->text
            );
        ?>
    </div>
<!--    <div class="reg_list pull-right">-->
<!--        --><?php
//            echo Yii::t(
//                'front',
//                Blocks::model()->findByAttributes(array('title' => 'useful_info'))
//                    ->getContent(Yii::app()->session['setlanguage'])
//                    ->text
//            );
//        ?>
<!--    </div>-->
    <div class="reg_list pull-right no-border">
        <?php
            echo Yii::t(
                'front',
                Blocks::model()->findByAttributes(array('title' => 'useful_info_all'))
                    ->getContent(Yii::app()->session['setlanguage'])
                    ->text
            );
        ?>
        <div class="clearfix"></div>
        <div class="pull-left dmsu-icon"></div>
        <?php
            echo Yii::t(
                'front',
                Blocks::model()->findByAttributes(array('title' => 'gol_upravl'))
                    ->getContent(Yii::app()->session['setlanguage'])
                    ->text
            );
        ?>
    </div>
    <div class="clearfix"></div>
</div>
<div class="content-wrapper-yellow advantages system-block">
    <?php
        echo Yii::t(
            'front',
            Blocks::model()->findByAttributes(array('title' => 'how_works'))
                ->getContent(Yii::app()->session['setlanguage'])
                ->text
        );
    ?>
    <div class="clearfix"></div>
    <?php if(Yii::app()->user->isGuest || !isset(Yii::app()->user->num) || Yii::app()->user->num == 0): ?>
        <div id="reg_button_down" class="reg_button" data-url="<?php echo Yii::app()->createUrl('site/oferta'); ?>">
            <h4><?php echo Yii::t('front','Станьте клиентом'); ?></h4>
            <p><?php echo Yii::t('front','Регистрация и договор публичной офферты'); ?></p>
        </div>
    <?php else: ?>
        <div class="_button">
            <div class="aligner">
                <span class="quest_icon"></span>
                <p>
                    <?php echo Yii::t('front', 'Задать вопрос'); ?>
                    <span class="status gray-color">online</span>
                </p>
            </div>
        </div>
    <?php endif; ?>
</div>
