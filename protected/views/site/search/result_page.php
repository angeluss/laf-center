<div class="news-content clearfix">
    <p class="action-time">
    <?php $lang_page = PageContents::getLangPage($data); ?>
    <?php $page_alias = Pages::model()->findByPk($data->page_id); ?>
    <p class="title">
        <a href="<?php echo Yii::app()->createUrl('page/'.$page_alias->alias); ?>">
            <?php echo Yii::t('front','Перейти'); ?>
        </a>
    </p>
    <p class="article"></p>
</div>
