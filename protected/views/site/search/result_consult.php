<div class="news-content clearfix">
    <p class="action-time">
        <?php $content = News::model()->getRelationName(Yii::app()->session['setlanguage']); ?>
    </p>
    <p class="title">
        <?php if(Yii::app()->user->isGuest): ?>
            <a class="result_consult_no_reg_link" href="javascript:">
                <?php echo $data->$content->title; ?>
            </a>
        <?php else: ?>
            <a href="<?php echo Yii::app()->createUrl('consultation/view', array('id' =>$data->id)); ?>">
                <?php echo $data->$content->title; ?>
            </a>
        <?php endif; ?>
    </p>
    <p class="article"></p>
</div>
