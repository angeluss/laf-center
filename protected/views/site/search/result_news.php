<div class="news-content clearfix">
    <p class="action-time">
        <?php $content = News::model()->getRelationName(Yii::app()->session['setlanguage']); ?>
    </p>
    <p class="title">
        <a href="<?php echo Yii::app()->createUrl('news/view', array('id' =>$data->id)); ?>">
            <?php echo $data->$content->title; ?>
        </a>
    </p>
    <p class="article"><?php echo $data->$content->short_desc; ?></p>
    <p class="article"></p>
</div>
