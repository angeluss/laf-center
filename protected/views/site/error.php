<div class="page-wrapper">
        <div class="wrapper">
            <div class="error-header">
                <div class="header-box">
                    <a class="error_link_home" href="<?php echo Yii::app()->createUrl('site/index');?>">
                    <div style="cursor: pointer" class="backward-text">
                        <i class="fa fa-long-arrow-left"></i>
                        <div><?php echo Yii::t('front', 'Центр правового содействия иностранным гражданам'); ?></div>
                    </div>
                    </a>
                    <div class="page-title">
                        <?php echo Yii::t('front', 'Ошибка 404'); ?>
                    </div>
                </div>
            </div>
            <div class="main-content">
                <div class="main-wrapper">
                    <div class="error-message-title">
                        <?php echo Yii::t('front', 'Страница не найдена'); ?>
                    </div>
                    <div class="message-content">
                        <?php echo Yii::t('front', 'Запрашиваемая страница не найдена или не существует.'); ?>
                        <br/>
                        <?php echo Yii::t('front', 'Проверьте правильность написания адреса или воспользуйтесь поиском.'); ?>
                    </div>
                    <div class="search-wrap">
                        <form action="" autocomplete="on">
                            <input id="search" name="search" type="text" placeholder="<?php echo Yii::t('front','Поиск') ?>">
                            <input id="search_submit" value="Rechercher" type="submit">
                        </form>
                    </div>
                    <a href="<?php echo Yii::app()->createUrl('site/index');?>">
                        <?php echo Yii::t('front', 'Вернуться на главную'); ?>
                    </a>
                </div>
            </div>
            <div class="error-footer">
                <div class="copyright">
                    <p><?php echo Yii::t('front', '2015 Центр правового содействия иностранным гражданам'); ?></p>

                    <p><i><?php echo Yii::t('front', 'Все права защищены'); ?></i></div></p>
            </div>
        </div>
    </div>
