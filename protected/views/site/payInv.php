<div id="main_content" class="clearfix">
    <div class="content-wrapper-white registration-block payinv-block">
        <?php echo $this->renderPartial('_invoice', array('model' => $model, 'req' => $req, 'print' => false)); ?>
        <div id="top">
            <?php echo CHtml::link(
                Yii::t('front', 'Печать'),
                array('site/printInvoice', 'id' => $model->id),
                array('class' => 'print_button', 'target' => '_blank')
            ); ?>
        </div>
        <?php echo CHtml::button(
            Yii::t('front', 'Далее'),
            array(
                'class'     => 'next_button',
                'data-url'  => Yii::app()->createUrl('/site/regSuccess', array('id' => $model->id)),
                'id'        => 'pay_inv_next_btn',
                )
        ); ?>
    </div>
</div>
