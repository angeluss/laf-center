<style type="text/css">
    body {
        margin: 0;
        padding: 0;
        background: #647276;
        color: #000;
    }
    p {
        margin:2px auto 2px 2px;
        font:12px Arial, Helvetica, sans-serif;
    }
    #page {
        background: #fff;
        margin: 20px auto;
        padding: 40px 0 300px;
        box-shadow: 0 2px 12px rgba(0, 0, 0, 0.4);
        width: 100%;
    }
    #page table {
        margin: 0 auto;
    }
    #top a {
        display:inline-block;
        background:#9c392c;
        padding:10px 20px;
        border-radius:4px;
        font: bold 14px/1.4 Helvetica, Arial, sans-serif;
        color:#fff;
        text-decoration:none;
        text-transform: uppercase;
        border: 2px solid;
    }
    @media print {
        body {background: white;}
        #top {display: none;}
        #page {box-shadow: none;padding:0;}
    }
    @page {
        margin: 0;
    }
</style>
<div id="page">
    <table class="MsoNormalTable" border="0" cellspacing="0" cellpadding="0" width="688" style="width:515.7pt;border-collapse:collapse">
        <tbody>
        <?php foreach(array('Повідомлення', 'Квитанція') as $title):?>
            <tr style="height:4.5pt">
                <td width="112" nowrap="" rowspan="2" valign="bottom" style="width:84.1pt;border:none;border-right:solid windowtext 1.5pt;padding:0 5.4pt 0 5.4pt;height:4.5pt">
                    <p class="MsoNormal">
                        <b>
                            <span style="font-size:10.0pt;font-family:'Arial','sans-serif'">
                                <?php echo $title ?>
                            </span>
                        </b>
                    </p>
                </td>
                <td width="16" nowrap="" rowspan="21" valign="bottom" style="width:11.8pt;border:none;border-bottom:solid windowtext 1.5pt;padding:0 5.4pt 0 5.4pt;height:4.5pt">
                    <p class="MsoNormal" style="text-indent:7.4pt">
                        <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">&nbsp;</span>
                    </p>
                </td>
                <td width="560" nowrap="" colspan="12" valign="bottom" style="width:419.8pt;padding:0 5.4pt 0 5.4pt;height:4.5pt">
                    <p class="MsoNormal">
                        <span style="font-size:2.0pt;font-family:'Arial','sans-serif'">&nbsp;</span>
                    </p>
                </td>
            </tr>
            <tr style="height:10.5pt">
                <td width="128" nowrap="" colspan="2" style="width:96.15pt;padding:0 5.4pt 0 5.4pt;height:10.5pt">
                    <p class="MsoNormal" style="margin-left:5.4pt;text-indent:-5.4pt">
                        <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">
                            Отримувач платежу
                        </span>
                    </p>
                </td>
                <td width="432" colspan="10" style="width:323.65pt;padding:0 5.4pt 0 5.4pt;height:10.5pt">
                    <p class="MsoNormal">
                        <b>
                            <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">
                                <?php echo $req->recepient ?>
                            </span>
                        </b>
                    </p>
                </td>
            </tr>
            <tr style="height:2.5pt">
                <td width="112" nowrap="" rowspan="19" valign="bottom" style="width:84.1pt;border-top:none;border-left:none;border-bottom:solid windowtext 1.5pt;border-right:solid windowtext 1.5pt;padding:0 5.4pt 0 5.4pt;height:2.5pt">
                    <p class="MsoNormal"><span style="font-size:8.0pt;font-family:'Arial','sans-serif'">&nbsp;</span></p>
                </td>
                <td width="560" colspan="12" valign="bottom" style="width:419.8pt;padding:0 5.4pt 0 5.4pt;height:2.5pt">
                    <p class="MsoNormal">
                        <span style="font-size:1.0pt;font-family:'Arial','sans-serif'">&nbsp;</span>
                    </p>
                </td>
            </tr>
            <tr style="height:2.85pt">
                <td width="429" nowrap="" colspan="9" valign="top" style="width:321.5pt;border:solid black 1.0pt;padding:0 5.4pt 0 5.4pt;height:2.85pt">
                    <p class="MsoNormalCxSpMiddle" align="center" style="text-align:center;line-height:8.0pt">
                        <b>
                            <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">
                                <?php echo $req->account ?>
                            </span>
                        </b>
                    </p>
                </td>
                <td width="20" nowrap="" rowspan="3" style="width:14.9pt;border:none;padding:0 5.4pt 0 5.4pt;height:2.85pt">
                    <p class="MsoNormal">
                        <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">&nbsp;</span>
                    </p>
                </td>
                <td width="111" nowrap="" colspan="2" style="width:83.4pt;border:solid black 1.0pt;padding:0 5.4pt 0 5.4pt;height:2.85pt">
                    <p class="MsoNormal" align="center" style="text-align:center">
                        <b>
                            <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">
                                <?php echo $req->id_code  ?>
                            </span>
                        </b>
                    </p>
                </td>
            </tr>
            <tr style="height:5.65pt">
                <td width="429" nowrap="" colspan="9" style="width:321.5pt;border:none;padding:0 5.4pt 0 5.4pt;height:5.65pt">
                    <p class="MsoNormal" align="center" style="text-align:center">
                        <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">
                            Поточний рахунок отримувача
                        </span>
                    </p>
                </td>
                <td width="111" nowrap="" colspan="2" rowspan="2" style="width:83.4pt;padding:0 5.4pt 0 5.4pt;height:2.85pt">
                    <p class="MsoNormal" align="center" style="text-align:center">
                        <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">
                            Ідентифікаційний
                        </span>
                    </p>
                    <p class="MsoNormal" align="center" style="text-align:center">
                        <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">
                            код отримувача
                        </span>
                    </p>
                </td>
            </tr>
            <tr style="height:5.65pt">
                <td width="128" nowrap="" colspan="2" style="width:96.15pt;padding:0 5.4pt 0 5.4pt;height:5.65pt">
                    <p class="MsoNormal" style="margin-left:5.4pt;text-indent:-5.4pt">
                        <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">
                            Дата валютування
                        </span>
                    </p>
                </td>
                <td width="300" nowrap="" colspan="7" style="width:225.35pt;padding:0 5.4pt 0 5.4pt;height:5.65pt">
                    <p class="MsoNormal">
                        <span lang="EN-US" style="font-size:8.0pt;font-family:'Arial','sans-serif'">&nbsp;</span>
                    </p>
                </td>
            </tr>
            <tr style="height:2.85pt">
                <td width="429" nowrap="" colspan="9" style="width:321.5pt;border:solid windowtext 1.0pt;border-right:solid black 1.0pt;padding:0 5.4pt 0 5.4pt;height:2.85pt">
                    <p class="MsoNormal" align="center" style="text-align:center">
                        <b>
                            <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">
                                <?php echo $req->bank ?>
                            </span>
                        </b>
                    </p>
                </td>
                <td width="131" nowrap="" colspan="3" style="width:98.3pt;border-top:solid windowtext 1.0pt;border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;padding:0 5.4pt 0 5.4pt;height:2.85pt">
                    <p class="MsoNormal" align="center" style="text-align:center">
                        <b>
                            <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">
                                <?php echo $req->bank_code ?>
                            </span>
                        </b>
                    </p>
                </td>
            </tr>
            <tr style="height:4.25pt">
                <td width="429" nowrap="" colspan="9" style="width:321.5pt;border:none;padding:0 5.4pt 0 5.4pt;height:4.25pt">
                    <p class="MsoNormal" align="center" style="text-align:center">
                        <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">
                            Установа банку
                        </span>
                    </p>
                </td>
                <td width="131" nowrap="" colspan="3" style="width:98.3pt;padding:0 5.4pt 0 5.4pt;height:4.25pt">
                    <p class="MsoNormal">
                        <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">
                            Код установи банку
                        </span>
                    </p>
                </td>
            </tr>
            <tr style="height:7.0pt">
                <td width="560" nowrap="" colspan="12" style="width:419.8pt;border:none;border-bottom:solid windowtext 1.0pt;padding:0 5.4pt 0 5.4pt;height:7.0pt">
                    <p class="MsoNormal">
                        <b>
                            <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">
                                <?php echo $model->fullname_uk ?>
                            </span>
                        </b>
                    </p>
                </td>
            </tr>
            <tr style="height:4.25pt">
                <td width="560" nowrap="" colspan="12" style="width:419.8pt;border:none;padding:0 5.4pt 0 5.4pt;height:4.25pt">
                    <p class="MsoNormal" align="center">
                        <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">
                            Прізвище, ім'я та по батькові, адреса платника
                        </span>
                    </p>
                </td>
            </tr>
            <tr style="height:4.9pt">
                <td width="197" nowrap="" colspan="5" rowspan="2" style="width:148.1pt;padding:0 5.4pt 0 5.4pt;height:4.9pt">
                    <p class="MsoNormal">
                        <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">&nbsp;</span>
                    </p>
                </td>
                <td width="362" nowrap="" colspan="7" style="width:271.7pt;border:solid black 1.0pt;padding:0 5.4pt 0 5.4pt;height:4.9pt">
                    <p class="MsoNormal" align="center" style="text-align:center">
                        <b>
                            <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">
                                <?php echo $model->tax_number ?>
                            </span>
                        </b>
                    </p>
                </td>
            </tr>
            <tr style="height:3.55pt">
                <td width="362" nowrap="" colspan="7" style="width:271.7pt;padding:0 5.4pt 0 5.4pt;height:3.55pt">
                    <p class="MsoNormal" align="center" style="text-align:center">
                        <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">
                            Ідентифікаційний номер клієнта
                        </span>
                    </p>
                </td>
            </tr>
            <tr style="height:13.3pt">
                <td width="165" nowrap="" colspan="4" style="width:123.85pt;border:solid windowtext 1.0pt;padding:0 5.4pt 0 5.4pt;height:13.3pt">
                    <p class="MsoNormal">
                        <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">
                            Призначення платежу
                        </span>
                    </p>
                </td>
                <td width="395" nowrap="" colspan="8" style="width:295.95pt;border-top:solid windowtext 1.0pt;  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;  padding:0 5.4pt 0 5.4pt;height:13.3pt">
                    <p class="MsoNormal">
                        <b>
                            <span lang="RU" style="font-size:8.0pt;font-family:'Arial','sans-serif'">
                                Оплата юридичних послуг від клієнта <?php echo $model->card_id ?>
                            </span>
                        </b>
                    </p>
                </td>
            </tr>
            <tr style="height:5.65pt">
                <td width="165" nowrap="" colspan="4" valign="top" style="width:123.85pt;border:solid windowtext 1.0pt;  border-top:none;padding:0 5.4pt 0 5.4pt;height:5.65pt">
                    <p class="MsoNormal">
                        <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">
                            Період платежу
                        </span>
                    </p>
                </td>
                <td width="395" nowrap="" colspan="8" style="width:295.95pt;border-top:none;  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;padding:0 5.4pt 0 5.4pt;height:5.65pt">
                    <p class="MsoNormal">
                        <b>
                           від <?php echo date('d.m.Y') ?> до <?php echo date('d.m.Y', strtotime("+1 year")) ?>
                        </b>
                    </p>
                </td>
            </tr>
            <tr style="height:3.5pt">
                <td width="165" nowrap="" colspan="4" valign="top" style="width:123.85pt;border:solid windowtext 1.0pt; border-top:none;padding:0 5.4pt 0 5.4pt;height:3.5pt">
                    <p class="MsoNormal">
                        <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">
                            Код виду платежу
                        </span>
                    </p>
                </td>
                <td width="395" nowrap="" colspan="8" style="width:295.95pt;border-top:none; border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid windowtext 1.0pt;  padding:0 5.4pt 0 5.4pt;height:3.5pt">
                    <p class="MsoNormal">
                        <i>
                            <span style="font-size:8.0pt;font-family:'Arial','sans-serif';color:#3366FF">&nbsp;</span>
                        </i>
                    </p>
                </td>
            </tr>
            <tr style="height:5.65pt">
                <td width="165" nowrap="" colspan="4" style="width:123.85pt;border:solid windowtext 1.0pt;  border-top:none;padding:0 5.4pt 0 5.4pt;height:5.65pt">
                    <p class="MsoNormal">
                        <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">
                            Код бюджетної класифікації
                        </span>
                    </p>
                </td>
                <td width="395" nowrap="" colspan="8" style="width:295.95pt;border-top:none;  border-left:none;border-bottom:solid windowtext 1.0pt;border-right:solid black 1.0pt;  padding:0 5.4pt 0 5.4pt;height:5.65pt">
                    <p class="MsoNormal">
                        <b>
                            <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">

                            </span>
                        </b>
                    </p>
                </td>
            </tr>
            <tr style="height:1.0pt">
                <td width="326" nowrap="" colspan="7" rowspan="3" style="width:244.35pt;padding:0 5.4pt 0 5.4pt;height:1.0pt">
                    <p class="MsoNormal">
                        <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">&nbsp;</span>
                    </p>
                </td>
                <td width="234" nowrap="" colspan="5" style="width:175.45pt;padding:0 5.4pt 0 5.4pt;  height:1.0pt">
                    <p class="MsoNormal">
                        <b>
                            <span style="font-size:2.0pt;font-family:'Arial','sans-serif'">&nbsp;</span>
                        </b>
                    </p>
                </td>
            </tr>
            <tr style="height:8.4pt">
                <td width="76" nowrap="" style="width:2.0pt;padding:0 5.4pt 0 5.4pt;height:8.4pt">
                    <p class="MsoNormal">
                        <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">
                            Сума
                        </span>
                    </p>
                </td>
                <td width="158" nowrap="" colspan="4" style="width:118.75pt;border:solid windowtext 1.0pt;  border-right:solid black 1.0pt;padding:0 5.4pt 0 5.4pt;height:8.4pt">
                    <p class="MsoNormal" align="right" style="text-align:right">
                        <b>
                            <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">
                                <?php echo $req->amount ?>.00 грн.
                            </span>
                        </b>
                        <b>
                            <span lang="RU" style="font-size:8.0pt;font-family:'Arial','sans-serif'">&nbsp;</span>
                        </b>
                    </p>
                </td>
            </tr>
            <tr style="height:4.95pt">
                <td width="76" nowrap="" style="width:2.0pt;padding:0 5.4pt 0 5.4pt;  height:4.95pt">
                    <p class="MsoNormal">
                        <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">
                            Пеня
                        </span>
                    </p>
                </td>
                <td width="158" nowrap="" colspan="4" style="width:118.75pt;border-top:none;  border-left:solid windowtext 1.0pt;border-bottom:solid windowtext 1.0pt;  border-right:solid black 1.0pt;padding:0 5.4pt 0 5.4pt;height:4.95pt">
                    <p class="MsoNormal" align="right" style="text-align:right">
                        <i>
                            <span style="font-size:8.0pt;font-family:'Arial','sans-serif';color:#3366FF">&nbsp;</span>
                        </i>
                    </p>
                </td>
            </tr>
            <tr style="height:4.2pt">
                <td width="114" nowrap="" valign="bottom" style="width:85.35pt;padding:0 5.4pt 0 5.4pt;  height:4.2pt">
                    <p class="MsoNormal">
                        <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">
                            Підпис платника
                        </span>
                    </p>
                </td>
                <td width="153" colspan="5" valign="bottom" style="width:114.7pt;border:none;  border-bottom:solid black 1.0pt;padding:0 5.4pt 0 5.4pt;height:4.2pt">
                    <p class="MsoNormal">
                        <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">&nbsp;</span>
                    </p>
                </td>
                <td width="59" nowrap="" style="width:44.3pt;padding:0 5.4pt 0 5.4pt;  height:4.2pt">
                    <p class="MsoNormal">
                        <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">&nbsp;</span>
                    </p>
                </td>
                <td width="76" nowrap="" style="width:2.0pt;padding:0 5.4pt 0 5.4pt;  height:4.2pt">
                    <p class="MsoNormal">
                        <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">
                            Усього
                        </span>
                    </p>
                </td>
                <td width="158" nowrap="" colspan="4" style="width:118.75pt;border-top:none;  border-left:solid windowtext 1.0pt;border-bottom:solid black 1.0pt;  border-right:solid black 1.0pt;padding:0 5.4pt 0 5.4pt;height:4.2pt">
                    <p class="MsoNormal" align="right" style="text-align:right">
                        <b>
                            <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">
                                <?php echo $req->amount ?>.00 грн.
                            </span>
                        </b>
                        <b>
                            <span lang="RU" style="font-size:8.0pt;font-family:'Arial','sans-serif'">&nbsp;&nbsp;</span>
                        </b>
                    </p>
                </td>
            </tr>
            <tr style="height:5.65pt">
            <td width="560" nowrap="" colspan="12" valign="bottom" style="width:419.8pt;border:none;border-bottom:solid windowtext 1.5pt;padding:0 5.4pt 0 5.4pt;height:5.65pt">
                <p class="MsoNormal">
                    <span style="font-size:8.0pt;font-family:'Arial','sans-serif'">&nbsp;</span>
                </p>
            </td>
        </tr>
        <?php endforeach; ?>
        <tr>
            <td width="112" style="border:none"></td>
            <td width="16" style="border:none"></td>
            <td width="114" style="border:none"></td>
            <td width="14" style="border:none"></td>
            <td width="14" style="border:none"></td>
            <td width="23" style="border:none"></td>
            <td width="32" style="border:none"></td>
            <td width="69" style="border:none"></td>
            <td width="59" style="border:none"></td>
            <td width="76" style="border:none"></td>
            <td width="27" style="border:none"></td>
            <td width="20" style="border:none"></td>
            <td width="11" style="border:none"></td>
            <td width="109" style="border:none"></td>
        </tr>
        </tbody>
    </table>
    <?php if($print === true): ?>
        <script>
            window.print()
        </script>
    <?php endif; ?>
</div>
