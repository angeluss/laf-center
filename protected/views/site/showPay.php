<div id="main_content" class="clearfix">
    <div class="content-wrapper-white payment_for_services">
        <?php if(Yii::app()->user->hasFlash('success')):?>
            <div class="no_pay_msg">
                <h2><?php echo Yii::app()->user->getFlash('success'); ?></h2>
            </div>
        <?php endif; ?>
        <div class="message-wrap">
            <p class="warning">
                <?php echo Yii::t('front', 'Индивидуальный код для обращений будет активирован после оплаты' ) ?>
            </p>
            <p class="price">
                <?php echo Requisites::model()->find()->amount ?>
                <span><?php echo Yii::t('front', 'пакет услуг на 1 год' ) ?></span>
            </p>
        </div>
        <form method="post">
            <div class="payment-method-block">
                <p><?php echo Yii::t('front', 'Способ оплаты' ) ?></p>
                <div>
                    <p>
                        <input type="radio" id="liq" name="location" value="1"  checked />
                        <label for="liq"><?php echo Yii::t('front', 'Банковская карта' ) ?>
                            <span class="method-icons"></span>
                        </label>
                    </p>
                    <p>
                        <input type="radio" name="location" value='2' id="inv" checked />
                        <label for="inv"><?php echo Yii::t('front', 'Квитанция для оплаты наличными' ) ?></label>
                    </p>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="reg_button">
                <button class="next_button"><?php echo Yii::t('front', 'Продолжить' ) ?></button>
            </div>
        </form>
    </div>
</div>