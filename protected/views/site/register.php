<!--<div id="main_content" class="clearfix">-->
    <div class="content-wrapper-white registration-block">
        <form method="post" enctype="multipart/form-data" id="form_register">
            <i id="validate_email" data-url="<?php echo Yii::app()->createUrl('site/validEmail')?>"></i>
            <div class="notification">
                <p><?php echo Yii::t('front', 'Регистрационные данные') ?></p>
            </div>

            <input id="text_for_validation" style="visibility: hidden; height: 0px;" value="<?php echo Yii::t('front', 'Пожалуйста, заполните это поле') ?>"/>
            <ul>
                <li>
                    <p class="form_has_errors" style="display: none; color:red">
                        <?php echo Yii::t('front', 'Форма содержит ошибки. Пожалуйста, исправьте их.');?>
                    </p>
                    <i class="fa fa-requared-field"></i>
                    <label for="fullname"><?php echo Yii::t('front', 'Surname, First Name') ?></label>
                    <div>
                        <input placeholder="<?php echo Yii::t('front', 'Surname, First Name') ?>" type="text" id="fullname" name="Clients[fullname]"  />
                        <p id="error_fullname" style="display:none; color: red;"></p>
                    </div>
                    <p class="note"><?php echo Yii::t('front', 'Латиницей как в паспорте') ?></p>
                </li>
                <li>
                    <i class="fa fa-requared-field"></i>
                    <label for="name"><?php echo Yii::t('front', 'Прізвище, ім’я, по-батькові') ?></label>
                    <p>
                        <input placeholder="<?php echo Yii::t('front', 'Прізвище, ім’я, по-батькові') ?>" type="text" id="name" name="Clients[fullname_uk]"/>
                    </p>
                    <p id="error_name" style="display:none; color: red;"></p>
                    <p class="note"><?php echo Yii::t('front', 'На украинском языке, как в регистрационных документах') ?></p>
                </li>
                <li class="birth-date clearfix">
                    <div>
                        <p> <i class="fa fa-requared-field"></i><?php echo Yii::t('front', 'Пол') ?></p>
                        <input type="radio" name="Clients[gender]" id="male" value="1" checked />
                        <label for="male"><?php echo Yii::t('front', 'М') ?></label>
                        <input type="radio" name="Clients[gender]" value='2' id="female" />
                        <label for="female"><?php echo Yii::t('front', 'Ж') ?></label>
                    </div>
                    <div class="birthday">
                        <p>
                            <i class="fa fa-requared-field date-input-icon"></i>
                            <?php echo Yii::t('front', 'Дата рождения') ?>
                                </p>



                        <input name="Clients[bitrhdate]" id="birthdate" placeholder="<?php echo Yii::t('front', 'Дата рождения') ?>"  type="text" class="date-input" />
                        <i class="fa fa-calendar"></i>
                        <p id="error_birthdate" style="display:none; color: red;"></p>
                    </div>
                </li>
                <li>
                    <i class="fa fa-requared-field"></i>
                    <label for="country"><?php echo Yii::t('front', 'Гражданство') ?></label>
                    <div class="select-outer">
                        <select id="country" name="Clients[country]" size="">
                            <option value>
                                <?php echo Yii::t('front', 'Гражданство') ?>
                            </option>
                            <?php foreach($countries as $id => $info): ?>
                                <option value="<?php echo $id ?>">
                                    <?php echo $info['title'] ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                        <a class="select-button">&#119119;</a>
                    </div>
                    <p id="error_country" style="display:none; color: red;"></p>
                </li>
                <li>
                    <i class="fa fa-requared-field"></i>
                    <label for="lang_input"><?php echo Yii::t('front', 'Предпочитаемый язык общения') ?></label>
                    <div class="select-outer">
                        <div class="lang_selects">
                            <?php $this->renderPartial('_lang_block'); ?>
                        </div>
                    </div>
                    <p id="error_lang_input" style="display:none; color: red;"></p>
                </li>
                <li>
                    <i class="fa fa-requared-field"></i>
                    <label for="vuz"><?php echo Yii::t('front', 'ВУЗ') ?></label>
                    <div class="select-outer">
                        <select  id="vuz" name="Clients[vuz]" size="">
                            <option value="">
                                <?php echo Yii::t('front', 'ВУЗ') ?>
                            </option>
                            <?php foreach($vuses as $code => $title): ?>
                                <option value="<?php echo $code?>">
                                    <?php echo $title?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                        <a class="select-button">&#119119;</a>
                    </div>
                    <p id="error_vuz" style="display:none; color: red;"></p>
                </li>
                <li>
                    <i class="fa fa-requared-field"></i>
                    <label for="oblast"><?php echo Yii::t('profile', 'Область') ?></label>
                    <div class="select-outer">
                        <select  name="Clients[oblast]" id="oblast" size="">
                            <option value="">
                                <?php echo Yii::t('front', 'Выберите область') ?>
                            </option>
                            <?php foreach(Country::getOblast() as $id => $title): ?>
                                <option value="<?php echo $id ?>">
                                    <?php echo $title ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                        <a class="select-button">&#119119;</a>
                    </div>
                    <p id="error_oblast" style="display:none; color: red;"></p>
                </li>
                <li>
                    <i class="fa fa-requared-field"></i>
                    <label for="region_areas"><?php echo Yii::t('profile', 'Район области') ?></label>
                    <div class="select-outer">
                        <select id="region_areas" name="Clients[district]" size="">
                            <option value="">
                                <?php echo Yii::t('front', 'Выберите район области') ?>
                            </option>
                            <?php foreach(Country::getDistrict() as $id => $title):?>
                                <option value = <?php echo $id ?>>
                                    <?php echo $title ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                        <a class="select-button">&#119119;</a>
                    </div>
                    <p id="error_region_areas" style="display:none; color: red;"></p>
                </li>
                <li class="input-city hidden">
                    <i class="fa fa-requared-field"></i>
                    <label for="input-city"><?php echo Yii::t('profile', 'Город') ?></label>
                    <input type="text" placeholder="<?php echo Yii::t('front', 'Введите город') ?>" id="input-city"  name="Clients[city]" size="" />
                </li>
                <li class="select-district hidden">
                    <i class="fa fa-requared-field"></i>
                    <label for="select-district"><?php echo Yii::t('profile', 'Район') ?></label>
                    <div class="select-outer">
                        <select id="select-district" name="Clients[area]" size="" >
                            <?php foreach(Country::getHDistrict() as $id => $title):?>
                                <option value = <?php echo $id ?>>
                                    <?php echo $title ?>
                                </option>
                            <?php endforeach; ?>
                        </select>
                        <a class="select-button">&#119119;</a>
                    </div>
                </li>
                <li>
                    <i class="fa fa-requared-field"></i>
                    <label for="input-address"><?php echo Yii::t('profile', 'Адрес') ?></label>
                    <input type="text" placeholder="<?php echo Yii::t('front', 'Введите ваш адрес (улица, дом, квартира)') ?>" id="input-address"  name="Clients[street]" size="" />
                    <p id="error_input_address" style="display:none; color: red;"></p>
                </li>
                <li class="files_upload-block">
                    <p><?php echo Yii::t('front', 'Прикрепить другие документы') ?></p>
                    <p><?php echo Yii::t('front', 'Наличие документов не обязательно, но крайне желательно') ?></p>
                    <div class="pull-left">
                        - <?php echo Yii::t('front', 'фотография 3х4') ?>
                    </div>
                    <div class="fileform pull-right">
                        <p class="file-name-title"></p>
                        <div class="selectbutton"><?php echo Yii::t('front', 'Обзор') ?></div>
                        <?php echo CHtml::activeFileField($doc, 'uplPhoto', array('id'=>'upload-photo-docs')); ?>
                    </div>
                    <div class="clearfix"></div>
                    <div class="pull-left">
                        - <?php echo Yii::t('front', 'виза') ?>
                    </div>
                    <div class="fileform pull-right">
                        <p class="file-name-title"></p>
                        <div class="selectbutton"><?php echo Yii::t('front', 'Обзор') ?></div>
                        <?php echo CHtml::activeFileField($doc, 'uplVisa', array('id'=>'upload-visa-docs')); ?>
                    </div>
                </li>
            </ul>
            <div class="notification">
                <p><?php echo Yii::t('front', 'Паспортные данные') ?></p>
            </div>
            <ul>
                <li>
                    <i class="fa fa-requared-field"></i>
                    <p>
                        <label for="number-pass"><?php echo Yii::t('front', 'Номер паспорта') ?></label>
                        <input name="Clients[passport]" placeholder="<?php echo Yii::t('front', 'Номер паспорта') ?>" type="text" id="number-pass" />
                        <span id="error_number_pass" style="display:none; color: red;"></span>
                    </p>
                </li>
                <li class="passport-dates clearfix">
                    <div class="pull-left">
                        <i class="fa fa-requared-field"></i>
                        <label class="pull-left" for="date_pass" class="pull-left"><?php echo Yii::t('front', 'Дата выдачи') ?></label>
                        <input type="text" id="date_pass" placeholder="<?php echo Yii::t('front', 'Дата выдачи') ?>" name="Clients[date_passport]" class="date-input" />
                        <i class="fa fa-calendar"></i>
                        <p id="error_date_pass" style="display:none; color: red;"></p>
                    </div>
                    <div class="pull-right">
                        <i class="fa fa-requared-field"></i>
                        <label for="till_pass" class="pull-left"><?php echo Yii::t('front', 'Срок действия') ?></label>
                        <input type="text" id="till_pass" placeholder="<?php echo Yii::t('front', 'Срок действия') ?>" name="Clients[passport_till]" class="date-input" />
                        <i class="fa fa-calendar"></i>
                        <p id="error_till_pass" style="display:none; color: red;"></p>
                    </div>
                </li>
                <li>
                    <div class="clearfix"></div>
                </li>
                <li class="files_upload-block">
                    <p><?php echo Yii::t('front', 'Прикрепить скан или фотографию паспорта') ?></p>
                    <p><?php echo Yii::t('front', 'Наличие документов не обязательно, но крайне желательно') ?></p>
                    <div class="fileform pull-left">
                        <p class="file-name-title"></p>
                        <div class="selectbutton"><?php echo Yii::t('front', 'Обзор') ?></div>
                        <?php echo CHtml::activeFileField($doc, 'uplPassport', array('id'=>'upload-passport-docs')); ?>
                    </div>
                </li>
                <li>
                    <p>
                        <input name="Clients[tax_number]" placeholder="<?php echo Yii::t('front', 'Регистрационный номер плательщика налогов') ?>" type="text" />
                    </p>
                    <p><?php echo Yii::t('front', 'Регистрационный номер учетной карточки плательщика налогов') ?></p>
                    <p><?php echo Yii::t('front', 'Заполнять в случае наличия') ?></p>
                </li>
            </ul>
            <div class="notification">
                <p>
                    <?php echo Yii::t('front', 'Удостоверение на временное проживание на территории Украины (вид на жительство)') ?>
                </p>
                <p>
                    <?php echo Yii::t('front', 'Заполнять в случае наличия') ?>
                </p>
            </div>
            <ul class="license">
                <li class="clearfix">
                    <p>
                        <input type="checkbox" name="Clients[vpu]" id="no-certificate" />
                        <label for="no-certificate"><?php echo Yii::t('front', 'Удостоверение отсутствует (еще не выдано)') ?></label>
                    </p>
                </li>
                <li class="license_block1">
                    <div class="pull-left">
                        <label for="serial" class="pull-left"><?php echo Yii::t('front', 'Серия') ?></label>
                        <input name="Clients[vpu_serie]" placeholder="<?php echo Yii::t('front', 'Серия') ?>" type="text" id="serial" />
                    </div>
                    <div class="pull-right">
                        <label for="serial-number" class="pull-left"><?php echo Yii::t('front', 'Номер') ?></label>
                        <input name="Clients[vpu_number]" placeholder="<?php echo Yii::t('front', 'Номер') ?>" type="text" id="serial-number" />
                    </div>
                    <div class="clearfix"></div>
                </li>
                <li class="license_block2">
                    <label class=""><?php echo Yii::t('front', 'Срок действия') ?></label>
                    <input name="Clients[vpu_till]" placeholder="<?php echo Yii::t('front', 'Срок действия') ?>" type="text" class="date-input" />
                    <label class="calendar-icon"><i class="fa fa-calendar"></i></label>
                </li>
                <li class="license_block3">
                    <label for="vpu_from" class="pull-left"><?php echo Yii::t('front', 'Орган, выдавший удостоверение') ?></label>
                    <p>
                        <input id="vpu_from" name="Clients[vpu_from]" placeholder="<?php echo Yii::t('front', 'Орган, выдавший удостоверение') ?>" type="text" />
                    </p>
                </li>
                <li class="files_upload-block license_block4">
                    <p><?php echo Yii::t('front', 'Прикрепить скан или фотографию удостоверения') ?></p>
                    <p><?php echo Yii::t('front', 'Наличие копии не обязательно, но крайне желательно') ?></p>
                    <div class="fileform pull-left">
                        <p class="file-name-title"></p>
                        <div class="selectbutton"><?php echo Yii::t('front', 'Обзор') ?></div>
                        <?php echo CHtml::activeFileField($doc, 'uplVpu', array('id'=>'upload-vpu-docs')); ?>
                    </div>
                </li>
            </ul>
            <div class="notification">
                <p><?php echo Yii::t('front', 'Контактная информация') ?></p>
            </div>
            <ul>
                <li>
                    <i class="fa fa-requared-field"></i>
                    <p>
                        <label for="phone"><?php echo Yii::t('front', 'Телефон') ?></label>
                    </p>
                    <p>
                        <input name="Clients[phone]" placeholder="<?php echo Yii::t('front', 'Телефон') ?>" type="text" id="phone" />
                    </p>
                    <p id="error_phone" style="display:none; color: red;"></p>
                </li>
                <li>
                    <label for="fact-address"><?php echo Yii::t('front', 'Фактический адрес проживания') ?></label>
                    <p>
                        <input name="Clients[address_reg]" placeholder="<?php echo Yii::t('front', 'Фактический адрес проживания') ?>" type="text" id="fact-address" />
                    </p>
                </li>
                <li>
                    <i class="fa fa-requared-field"></i>
                    <label for="email"><?php echo Yii::t('profile', 'Email') ?></label>
                    <p>
                        <input name="Clients[email]" placeholder="Email" type="email" id="email" />
                    </p>
                    <p id="error_email" style="display:none; color: red;"></p>
                    <p>
                        <?php echo Yii::t('front', 'Используется для входа в систему и отправки уведомлений') ?>
                    </p>
                </li>
                <li>
                    <i class="fa fa-requared-field"></i>
                    <label for="password"><?php echo Yii::t('front', 'Пароль') ?></label>
                    <p>
                        <input name="Clients[password]"  placeholder="<?php echo Yii::t('front', 'Пароль') ?>" type="password" id="password" />
                    </p>
                    <p id="error_password" style="display:none; color: red;"></p>
                    <p id="pass_tip"><?php echo Yii::t('front', 'Не менее 6 символов') ?></p>
                    <span class="pass-icon show"></span>
                    <span class="pass-icon hide" style="display: none;"></span>
                </li>
            </ul>
            <div class="reg_button">
                <button id="reg_but"><?php echo Yii::t('front', 'Продолжить') ?></button>
            </div>
        </form>
    </div>
<!--</div>-->
