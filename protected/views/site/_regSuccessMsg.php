<p>
    <b>
        <?php echo Yii::t('front', 'Уважаемый') . ' ' . $model->fullname_uk . ',' ?>
    </b>
    <br/>
    <?php echo Yii::t('front', 'спасибо за то, что доверили нам Вашу защиту.') ?>
</p>
<p>
    <?php echo Yii::t('front', 'Ваш индивидуальный код') ?>
    <b>
        <?php echo $model->card_id?>
    </b>
    <br/>
    <?php echo Yii::t('front', 'будет активирован после проверки оплаты.') ?>
</p>
