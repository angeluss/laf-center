<?php
/**
 * @var $this SiteController
 * @var $model Clients
 */
?>
<a name="profile"></a>
<div class="top_profile">
    <div class="profile_user_block">
        <div class="header_info_profile">
            <div class="pull-left left_name_block">
                <div class="avatar-box">
                    <?php if (!empty($model->doc)): ?>
                        <?php if( $model->doc[0]->photo !== '' && $model->doc[0]->photo !== NULL): ?>
                            <img class='profile_client_avatar' src="/uploadFiles/clientsPhoto/<?php echo $model->id . '/' . $model->doc[0]->photo?>" alt="" />
                        <?php else: ?>
                            <img class='profile_client_avatar' src="/uploadFiles/noavatar.png" alt="" />
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
                <div class="client_name pull-left">
                    <p><?php echo $model->fullname; ?></p>
                    <p><?php echo $model->fullname_uk; ?></p>
                </div>
                <span class="country-img-box pull-left"> <?php echo $model->getCountry(); ?></span>
            </div>
            <ul class="nav nav-pills pull-right">
                <li>
                    <a class="<?php echo $this->action->Id == 'profile'? 'active_action' : '' ?>" href="<?php echo Yii::app()->createUrl('/site/profile');?>#profile">
                        <?php echo Yii::t('profile', 'Профиль'); ?>
                    </a>
                </li>
                <li>
                    <a class="<?php echo $this->action->Id == 'documents'? 'active_action' : '' ?>" href="<?php echo Yii::app()->createUrl('/site/documents');?>#profile">
                        <?php echo Yii::t('profile', 'Документы'); ?>
                    </a>
                </li>
                <li>
                    <a class="<?php echo $this->action->Id == 'history'? 'active_action' : '' ?>" href="<?php echo Yii::app()->createUrl('/site/history');?>#profile">
                        <?php echo Yii::t('profile', 'История обращений'); ?>
                    </a>
                </li>
            </ul>
        </div>
        <!-- add div block to vertically align 2 titles-->
    </div>
</div>
