<div class="content-wrapper-white login-block restore-block">
    <div class="login-wrapper">
        <form id="recover_form_pass" method='post' class="login-form respass-form">
            <ul>
                <li>
                    <label for="email">Email</label>
                    <p>
                        <input class="" placeholder="Email" type="email" id="email" name="email" data-url="<?php echo Yii::app()->createUrl('site/validEmail'); ?>" />
                        <span id="error_email"></span>
                    </p>
                    <p class="respass-message">
                        <?php echo Yii::t('front', 'На вашу электронную почту будет отправлено письмо с дальнейшими инструкциями') ?>
                    </p>
                </li>
                <li>
                    <div class="login-submit-block">
                        <div class="login-btn respass-btn">
                            <input id="recover_form_pass_btn" type="submit" value="<?php echo Yii::t('front', 'Восстановить') ?>" />
                        </div>
                        <div class="login-links">
                            <a href="<?php echo Yii::app()->createUrl('site/login'); ?>">
                                <?php echo Yii::t('front', 'Войти') ?>
                            </a>
                            <span>|</span>
                            <a href="<?php echo Yii::app()->createUrl('site/oferta'); ?>">
                                <?php echo Yii::t('front', 'Регистрация') ?>
                            </a>
                        </div>
                    </div>
                </li>
            </ul>
        </form>
    </div>
</div>
