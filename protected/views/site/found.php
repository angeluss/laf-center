<div class="content-wrapper-white search-result">
    <?php if ($validator_fail == 1): ?>
         <h2>
             <?php echo Yii::t('admin', 'Запрос слишком короткий (менее 3 символов), попробуйте еще раз.'); ?>
         </h2>
    <?php else : ?>
        <ul class="contents">
            <li class="content_tab" id="consult_tab" style="display: block">
                <?php if($consults): ?>
                    <h1><?php echo Yii::t('front', 'Правовые консультации'); ?></h1>
                    <?php foreach($consults as $consult): ?>
                        <?php
                            $this->renderPartial('search/result_consult', array(
                                'data' => $consult,
                                'search' => $search->string,
                            ));
                        ?>
                    <?php endforeach; ?>
                    <?php $this->widget('CLinkPager', array('pages'=>$con_pages,)); ?>
                <?php else : ?>
                    <h1><?php echo Yii::t('front', 'Нет результата'); ?></h1>
                    <hr />

                <?php endif;  ?>
            </li>
        </ul>
    <?php endif; ?>
</div>
