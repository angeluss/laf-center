<div class="content-wrapper-white login-block restore-block">
    <div class="login-wrapper">
        <ul>
            <li>
                <p class="respass-message"><?php echo Yii::t('front', 'Пароль успешно изменен') ?></p>
            </li>
            <li>
                <div class="login-submit-block">
                    <div class="login-btn respass-btn">
                        <input type="button"
                               data-url="<?php echo Yii::app()->createUrl('site/login'); ?>"
                               value="<?php echo Yii::t('front', 'Вход') ?>"
                               id="recover_to_login"
                        />
                    </div>
                </div>
            </li>
        </ul>
    </div>
</div>
