<div id="main_content" class="clearfix">
    <div class="content-wrapper-white reg-success-block">
        <div class="reg-success-wrapper">
            <div class="call-icon-box">
                <img src="/image/reg_success-icon.png" alt=""/>
            </div>
            <div class="reg-success-info">
                <p class="main-message"><?php echo Yii::t('front', 'Регистрация прошла успешно') ?></p>
                <div class="info-message">
                    <?php $this->renderpartial('_regSuccessMsg', array('model' => $model)); ?>
                    <p>
                        <?php echo Yii::t('front', 'На email, указанный при регистрации') ?>
                        <br/>
                        <?php echo Yii::t('front', 'будет отправлено уведомление.') ?>
                    </p>
                    <a href="<?php echo Yii::app()->createUrl('site/index'); ?>">
                        <?php echo Yii::t('front', 'Вернуться на сайт') ?>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>