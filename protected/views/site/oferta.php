<div id="main_content" class="clearfix">
    <div class="content-wrapper-white oferta-block">
        <div>
            <?php echo $model->content ?>
            <div class="clearfix"></div>
            <form method="post">
                <div class="agree_block">
                    <p>
                        <input type="checkbox" name="offerta-agree" id="offerta-agree" />
                        <label for="offerta-agree">
                            <?php echo Yii::t('front','Я принимаю условия публичной оферты') ?>
                        </label>
                    </p>
                </div>
                <div class="reg_button">
                    <button disabled="disabled"><?php echo Yii::t('front', 'Продолжить') ?></button>
                </div>
            </form>
        </div>
        <div class="clearfix"></div>
    </div>
</div>