<div id="main_content" class="clearfix">
    <div class="content-wrapper-white registration-block payment_block">
        <div class="payment_content">
            <p><?php echo Yii::t('front', 'Внимание! Платеж пока осуществляется в тестовом режиме!'); ?></p>
            <div class="reg_button">
                <?php /* Генерится на сайте ликпея. Запросят разные суммы - придется через CURL вручную отправлять */ ?>
                <form method="POST" accept-charset="utf-8" action="https://www.liqpay.com/api/checkout">
                    <input type="hidden" name="data" value="<?php echo $data ?>" />
                    <input type="hidden" name="signature" value="<?php echo $signature ?>" />
                    <input type="image" src="/images/paymeny_btn.jpg" name="btn_text" />
                </form>
            </div>
        </div>
    </div>
</div>