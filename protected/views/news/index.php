<div class="content-wrapper-white news-page">
    <div class="left-column">
        <div class="column-wrapper">
            <div class="left-column-title">
                <p><?php echo Yii::t('front','Новости'); ?></p>
            </div>
            <div class="news-image">
                <img src="/image/centr_news_block.png" alt=""/>
            </div>
            <?php if ($tag_model !== ''): ?>
                <div>
                    <?php echo Yii::t('front','Поиск новостей по тегу '); echo $tag_model->title; ?>
                </div>
            <?php endif; ?>
            <div class="wrapper-left-col">
                <?php $this->widget('zii.widgets.CListView', array(
                        'dataProvider'=>$dataProvider,
                        'itemView'=>'_ajax_news',
                        'ajaxUpdate'=>true,
                        'template'=>"{items}\n{pager}",
                        'id'=>'news_widget_list',
                        'pager'=>array(
                            'class'=>'CLinkPager',
                            'prevPageLabel' => Yii::t('front','Предыдущие новости'),
                            'nextPageLabel' => Yii::t('front','Следующие новости'),
                            'firstPageLabel'=>false,
                            'lastPageLabel'=>false,
                            'maxButtonCount'=>20,// defalut 10
                            'header'=>false,
//                            'cssFile'=>'/css/pager.css', // устанавливаем свой .css файл
                            'htmlOptions'=>array('class'=>'pager'),
                        ),
                    )); ?>
            </div>
        </div>
    </div> <!-- end of left-column -->
    <div class="right-column">
        <div class="legal-advice" style="margin-top: -25px;">
            <h5 class="right-col-title"><?php echo Yii::t('front','Правовые консультации'); ?></h5>
            <ul class="question-link">
                <?php if (isset($random_consult)): ?>
                    <?php foreach ($random_consult as $consult): ?>
                        <li>
                            <a href="<?php echo Yii::app()->createUrl('consultation/view', array('id' =>$consult->id)); ?>">
                                <?php  echo $consult->getContent(Yii::app()->session['setlanguage'])->title; ?>
                            </a>
                        </li>
                    <?php  endforeach; ?>
                <?php endif; ?>
            </ul>
        </div>
        <div class="useful-info">
            <?php
                echo Yii::t(
                    'front',
                    Blocks::model()->findByAttributes(
                        array('title' => 'useful_info')
                    )->getContent(Yii::app()->session['setlanguage'])->text);
            ?>
        </div>
        <div class="state-contacts">
            <div class="state-img-box">
                <img src="/image/claf-state-image.png" alt=""/>
            </div>
            <div class="state-info">
                <?php
                    echo Yii::t(
                        'front',
                        Blocks::model()->findByAttributes(
                            array('title' => 'gol_upravl')
                        )->getContent(Yii::app()->session['setlanguage'])->text);
                ?>
            </div>
        </div>
    </div> <!-- end of right-column -->
    <div class="clearfix"></div>
</div>