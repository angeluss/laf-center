<div class="content-wrapper-white one-more-news">
    <div class="left-column">
        <div class="column-wrapper">
            <div class="go-backward">
                <i class="fa fa-long-arrow-left"></i>
                <a href="."><?php echo Yii::t('front','Новости'); ?></a>
            </div>
            <div class="news-content extended-news clearfix">
                <div>
                    <p class="action-time"><?php echo date('j.m, H:i',  $model->created_at); ?></p>
                    <p class="title extended-title">
                        <a href="javascript:">
                            <?php echo $model->getContent(Yii::app()->session['setlanguage'])->title; ?>
                        </a>
                    </p>
                    <div class="article extended-article">
                        <?php echo  $model->getContent(Yii::app()->session['setlanguage'])->content; ?>
                    </div>
                    <div class="article-footer">
                        <div class="social-icons-box">
                            <a href=""><i class="fa fa-facebook-official btn-success"></i></a>
                            <a href=""><i class="fa fa-twitter-square btn-danger"></i></a>
                            <a href=""><i class="fa fa-envelope-square"></i></a>
                            <a href=""><i class="fa fa-print"></i></a>
                            <a href=""><i class="fa fa-plus-square"></i></a>
                        </div>
                        <?php if ($model->link): ?>
                            <div class="source-data">
                                <a href="//<?php echo $model->link ?>" target="_blank">
                                    <?php echo Yii::t('front','По материалам ...   '); ?>
                                </a>
                            </div>
                        <?php endif; ?>
                        <div class="alias-list">
                            <?php foreach($tags as $key => $tag): ?>
                                <a href="<?php echo Yii::app()->createUrl('news/getByTag', array('id' =>$tags[$key]['id'])); ?>">
                                    <span class="element-box">
                                       <?php echo $tags[$key]['title']; ?>
                                    </span>
                                </a>
                           <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="page-links clearfix">
                <div class="backward-link half-block">
                    <?php echo $model->urlPrev; ?>
                </div>
                <div class="forward-link half-block">
                    <?php echo $model->urlNext; ?>
                </div>
            </div>
            <div class="read-more clearfix">
                <p class="read-more-title"><?php echo Yii::t('front','Читайте также'); ?></p>
                <?php foreach ($random_news as $random_new): ?>
                    <?php if ($random_new->id!==$model->id): ?>
                        <div class="read-more-list clearfix">
                            <div class="read-article">
                                <div class="added-time">
                                    <span class="day-month">
                                        <?php echo date('j.m, H:i',  $random_new->updated_at); ?>
                                    </span>
                                    <span></span>
                                </div>
                                <div class="article-text">
                                    <a href="<?php echo Yii::app()->createUrl('news/view', array('id' =>$random_new->id)); ?>">
                                        <?php
                                            echo substr($random_new->getContent(Yii::app()->session['setlanguage'])->title, 0, 100);
                                        ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                <?php endforeach; ?>
            </div>
        </div>
    </div> <!-- end of left column -->
    <div class="right-column">
        <?php //@TODO кажется, следующий див вообще нужно удалить?>
        <div class="recent-news clearfix" style="visibility: hidden;">
            <p class="read-more-title recent-news-title"<?php echo Yii::t('front','Последние новости'); ?>></p>
            <div class="read-more-list clearfix">
                <div class="read-article">
                    <div class="added-time recent-news-time">
                        <span class="day-month">14:40</span>
                        <span></span>
                    </div>
                    <div class="article-text recent-news-art">
                        <a href="#">
                            ГФСУ об исправлении ошибок в Отчете о суммах налоговых льгот
                        </a>
                    </div>
                </div>
            </div>
            <div class="read-more-list clearfix" style="visibility: hidden;">
                <div class="read-article">
                    <div class="added-time recent-news-time"><span class="day-month">12:00</span> <span></span></div>
                    <div class="article-text recent-news-art"><a href="#">Изовитова требует от Петренко ответа по Вишневскому</a></div>
                </div>
            </div>
        </div>
        <?php if(Yii::app()->user->isGuest || !isset(Yii::app()->user->num) || Yii::app()->user->num == 0) : ?>
            <div class="about-centre">
                <div class="outside-wrapper">
                    <div class="wrapper">
                        <h5 class="right-col-title"><?php echo Yii::t('front','О центре'); ?></h5>
                        <p class="about-article">
                            <?php
                                echo Yii::t(
                                    'front',
                                    ' Центр предоставляет правовую помощь иностранным
                                    гражданам в городе Харькове по вопросам миграционного,
                                    административного, уголовного, гражданского, налогового,
                                    семейного права.'
                                );
                            ?>
                        </p>
                        <div class="link-wrapper">
                            <a href="#">
                                <?php echo Yii::t('front','Подробнее'); ?>
                            </a>
                        </div>
                        <?php if(Yii::app()->user->isGuest): ?>
                            <div id="reg_button_new" class="reg_button" data-url="<?php echo Yii::app()->createUrl('site/oferta'); ?>">
                                <h4><?php echo Yii::t('front','Станьте клиентом'); ?></h4>
                                <p><?php echo Yii::t('front','Регистрация и договор публичной офферты'); ?></p>
                            </div>
                            <div class="reg_list">
                                <?php
                                    echo Yii::t(
                                        'front',
                                        Blocks::model()->findByAttributes(
                                            array('title' => 'how_it_works_block')
                                        )->getContent(Yii::app()->session['setlanguage'])->text);
                                ?>
                            </div>
                        <?php else: ?>
                            <p></p>
                        <?php endif; ?>
                    </div>
                </div>
            </div><!-- end of about-centre part -->
        <?php endif;?>
    </div>
    <div class="clearfix"></div>
</div>
