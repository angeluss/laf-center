<div class="news-content clearfix">
    <p class="action-time">
        <?php
            $content = News::model()->getRelationName(Yii::app()->session['setlanguage']);
            echo date('j.m, H:i',$data->updated_at);
        ?>
    </p>
    <p class="title">
        <a href="<?php echo Yii::app()->createUrl('news/view', array('id' =>$data->id)); ?>">
            <?php echo $data->$content->title; ?>
        </a>
    </p>
    <div class="article-img-box">
        <?php
            if ($data->img !== '') {
                $img = News::getImg($data->img, $data->id, 'article-img-wrap');
                echo $img;
            }
        ?>
    </div>
    <p class="article">
        <?php echo $data->$content->short_desc; ?>
    </p>
</div>