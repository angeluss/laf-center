<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta name="viewport" content="width=device-width, user-scalable=no">
        <title><?php echo $this->pageTitle ?></title>
        <link rel="shortcut icon" href="/image/favicon/favicon.ico" type="image/x-icon" />
        <?php
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerScriptFile('//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js');
        $cs->registerCssFile('//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
        $cs->registerCssFile('//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css');
        $cs->registerCssFile($baseUrl . '/css/style_site.css');
        $cs->registerCssFile($baseUrl . '/css/login.css');
        $cs->registerCssFile($baseUrl . '/css/news_styles.css');
        $cs->registerScriptFile('//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js');
        $cs->registerScriptFile($baseUrl . '/js/fancybox/jquery.fancybox.pack.js');
        $cs->registerScriptFile($baseUrl . '/js/fancybox/jquery.mousewheel-3.0.6.pack.js');
        $cs->registerScriptFile($baseUrl . '/js/jquery.mCustomScrollbar.concat.min.js');
        $cs->registerScriptFile($baseUrl . '/js/global.js');
        $cs->registerScriptFile($baseUrl . '/js/dev.js');
        $cs->registerScriptFile($baseUrl . '/js/onclicks.js');
        $cs->registerScriptFile($baseUrl . '/js/zabuto_calendar.min.js');
        $cs->registerScriptFile('//arrow.scrolltotop.com/arrow89.js');
        ?>
    </head>
    <body class="cms_page_wrapper">
        <div class="main_wrapper home_page">
            <div id="header-register" class="clearfix">
                <div class="top_header clearfix">
                    <div class="top_wrapper">
                        <p id="back_from_grey_link" data-url="<?php echo Yii::app()->createUrl('site/index');?>" style="cursor:pointer">
                            <i class="fa fa-long-arrow-left"></i>
                            <?php echo Yii::t('front', 'Центр правового содействия ') ?>
                            <span> <?php echo Yii::t('front', 'иностранным гражданам') ?></span>
                        </p>
                        <div class="language-links-wrapper">
                            <p><?php echo Yii::t('front', $this->pageTitle) ?></p>
                            <ul class="language-links">
                                <li class="<?php echo Yii::app()->session['setlanguage'] == 'ru' ? 'active_lang' : ''?>">
                                    <a href="<?php echo Yii::app()->urlManager->createUrl('/site/getLanguage', array('language'=>'ru', 'url'=> Yii::app()->request->url)); ?>" >Русский</a>
                                </li>
                                <li class="<?php echo Yii::app()->session['setlanguage'] == 'uk' ? 'active_lang' : ''?>">
                                    <a href="<?php echo Yii::app()->urlManager->createUrl('/site/getLanguage', array('language'=>'uk', 'url'=> Yii::app()->request->url)); ?>" >Українська</a>
                                </li>
                                <li class="<?php echo Yii::app()->session['setlanguage'] == 'en' ? 'active_lang' : ''?>">
                                    <a href="<?php echo Yii::app()->urlManager->createUrl('/site/getLanguage', array('language'=>'en', 'url'=> Yii::app()->request->url)); ?>" >English</a>
                                </li>
                                <li class="<?php echo Yii::app()->session['setlanguage'] == 'fr' ? 'active_lang' : ''?>">
                                    <a href="<?php echo Yii::app()->urlManager->createUrl('/site/getLanguage', array('language'=>'fr', 'url'=> Yii::app()->request->url)); ?>" >Français</a>
                                </li>
                                <li class="<?php echo Yii::app()->session['setlanguage'] == 'tr' ? 'active_lang' : ''?>">
                                    <a href="<?php echo Yii::app()->urlManager->createUrl('/site/getLanguage', array('language'=>'tr', 'url'=> Yii::app()->request->url)); ?>" >Türkmen</a>
                                </li>
                                <li class="<?php echo Yii::app()->session['setlanguage'] == 'ar' ? 'active_lang' : ''?>">
                                    <a href="<?php echo Yii::app()->urlManager->createUrl('/site/getLanguage', array('language'=>'ar', 'url'=> Yii::app()->request->url)); ?>" >العربية</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div id="main_content" class="clearfix">
                <?php echo $content; ?>
            </div>
            <div class="grey_footer">
                <p class="copyight_p">
                    <span class="copyright">&copy;</span>
                    <?php echo Yii::t('front', '2015 Центр правового содействия иностранным гражданам'); ?>
                </p>
                <p class="rights_reserved">
                    <?php echo Yii::t('front', 'Все права защищены'); ?>
                </p>
            </div>
        </div>
    </body>
</html>