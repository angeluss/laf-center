<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<meta name="language" content="en">
    <?php
    $baseUrl = Yii::app()->baseUrl;
    $cs = Yii::app()->getClientScript();
    $cs->registerCssFile('//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
    $cs->registerCssFile($baseUrl . '/css/screen.css');
    $cs->registerCssFile($baseUrl . '/css/print.css');
    $cs->registerCssFile($baseUrl . '/css/main.css');
    $cs->registerCssFile($baseUrl . '/css/form.css');
    $cs->registerCssFile($baseUrl . '/css/style_site.css');
    $cs->registerScriptFile('https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js');
    ?>
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
	<![endif]-->
    <link rel="shortcut icon" href="/image/favicon/favicon.ico" type="image/x-icon" />
	<title><?php echo $this->pageTitle; ?></title>
</head>

<body>
NO HEADER
<div class="main_wrapper home_page cms_page">
<div id="header" class="clearfix">
    <div class="top_header clearfix">
        <div class="top_wrapper">
            <div class="lang_holder clearfix">
                <div class="lang_icon">
                    <img src="/image/lang_icon.png" />
                </div>
                <ul class="lang_menu">
                    <li>
                        <a href="#" title="english">English</a>
                    </li>
                    <li class="active">
                        <a href="#" title="Русский">Русский</a>
                    </li>
                </ul>
            </div>
            <div class="center_block">
                <span class="phone"><?php echo Settings::model()->find()->phone; ?></span>
                <ul>
                    <li><a href="#"><?php echo Yii::t('front', 'Онлайн-консультация'); ?></a></li>
                    <li><a href="#"><?php echo Yii::t('front', 'Вход'); ?></a></li>
                </ul>
            </div>
            <div class="social_block">
                <a href="//<?php echo Settings::model()->find()->fb ?>" >
                    <i class="fa fa-facebook-official"></i>
                    <span>
                        <?php echo Yii::t('front', 'Сообщество в Facebook'); ?>
                    </span>
                </a>
            </div>
        </div>
    </div>
    <div class="header_wrapper clearfix">
        <ul id="main_nav" class="clearfix">
            <li>
                <a href="<?php echo Yii::app()->createUrl('page/about_us')?>" title="<?php echo Yii::t('front','О нас'); ?>">
                    <?php echo Yii::t('front','О нас'); ?>
                </a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->createUrl('news')?>" title="<?php echo Yii::t('front','Новости'); ?>">
                    <?php echo Yii::t('front','Новости'); ?>
                </a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->createUrl('page/prices')?>" title="<?php echo Yii::t('front','Услуги и Цены'); ?>">
                    <?php echo Yii::t('front','Услуги и Цены'); ?>
                </a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->createUrl('page/consult')?>" title="<?php echo Yii::t('front','Правовые консультации'); ?>">
                    <?php echo Yii::t('front','Правовые консультации'); ?>
                </a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->createUrl('page/info')?>" title="<?php echo Yii::t('front','Полезная информация'); ?>">
                    <?php echo Yii::t('front','Полезная информация'); ?>
                </a>
            </li>
            <li>
                <a href="http://laf-center.ukrainianforum.net/" title="<?php echo Yii::t('front','Форум'); ?>" target="_blank">
                    <?php echo Yii::t('front','Форум'); ?>
                </a>
            </li>
            <li>
                <a href="<?php echo Yii::app()->createUrl('page/contact_us')?>" title="<?php echo Yii::t('front','Контакты'); ?>">
                    <?php echo Yii::t('front','Контакты'); ?>
                </a>
            </li>
        </ul>
        <div class="search_block">
            <input type="text" value="" placeholder="<?php echo Yii::t('front', 'Поиск'); ?>" />
        </div>
        <div class="header_bottom">

            <div class="about">
                <h1><a href="/"><?php echo Yii::t('front', 'Центр правовой поддержки иностранных граждан'); ?></a></h1>
                <div class="desc">
                    <p>
                    <?php
                        echo Yii::t('front', 'Центр предоставляет правовую помощь иностранным гражданам в городе
                        Харькове по вопросам миграционного, административного, уголовного, гражданского, налогового,
                        семейного права.');
                    ?>
                    </p>
                    <p>
                    <?php
                        echo Yii::t('front', 'Клиенты Центра могут получить консультацию, обратившись в колл-центр или
                        направив запрос в адрес центра.');
                    ?>
                    </p>
                    <p>
                    <?php
                        echo Yii::t('front', 'Консультации по вопросам юридического и бухгалтерского сопровождения
                        хозяйственной деятельности (бизнеса) предоставляются Правовым центром “Альфа Омега”');
                    ?>
                    </p>
                </div>

            </div>
            <div class="register_info">
                <div id="no_header_reg_btn" class="reg_button" data-url="<?php echo Yii::app()->createUrl('site/oferta', array('lang'=>DEFAULT_LANG)); ?>">
                    <h4><?php echo Yii::t('front','Станьте клиентом'); ?></h4>
                    <p><?php echo Yii::t('front','Регистрация и договор публичной офферты'); ?></p>
                </div>
                <div class="reg_list">
                    <h5><?php echo Yii::t('front','Как это работает?'); ?></h5>
                    <ul>
                        <li>
                            <span class="number">1</span>
                            <span><?php echo Yii::t('front','Зарегистрируйтесь и получите индивидуальный код клиента'); ?></span>
                        </li>
                        <li>
                            <span class="number">2</span>
                            <span><?php echo Yii::t('front','Ознакомьтесь с часто задаваемыми вопросами'); ?></span>
                        </li>
                        <li>
                            <span class="number">3</span>
                            <span><?php echo Yii::t('front','Если Вы не нашли ответ на свой вопрос, обратитесь к консультанту онлайн или по телефону'); ?></span>
                        </li>
                        <li>
                            <span class="number">4</span>
                            <span><?php echo Yii::t('front','Назовите свой индивидуальный код'); ?></span>
                        </li>
                        <li>
                            <span class="number">5</span>
                            <span><?php echo Yii::t('front','Объясните проблему'); ?></span>
                        </li>
                        <li>
                            <span class="number">6</span>
                            <span><?php echo Yii::t('front','Получите необходимую помощь'); ?></span>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<div id="main_content" class="clearfix" style="min-height: 600px">
    <?php echo $content; ?>
</div>
<footer>
    <img src="/image/map.png" align="middle">
    <div class="contacts_block_wrapper">
        <ul class="centr-for-cityzen">
            <li class="header-list" >
                <?php echo Yii::t('front','Центр правового содействия иностранным гражданам'); ?>
            </li>
            <li>
            <?php
                echo Yii::t('front','Наша команда состоит из профессиональных юристов, имеющих достаточный опыт
                юридической практики. Специалисты Центра работают с клиентами, основываясь на глубоких практических и
                теоретических знаниях. Мы ответственно относимся к поставленным задачам, придерживаемся принципа
                конфеденциальности и партнерства.');
            ?>
            </li>
        </ul>
        <ul>
            <li class="header-list">
                <?php echo Yii::t('front','Главный офис'); ?>
            </li>
            <li>
                <?php echo Settings::model()->find()->address?>
                <?php echo Settings::model()->find()->contact_email?>
            </li>
        </ul>
        <ul>
            <li class="header-list">
                <?php echo Yii::t('front','Колл-центр'); ?>
            </li>
            <li>
                <?php echo Settings::model()->find()->phone; ?>
                <?php echo Settings::model()->find()->call_centr; ?>

                skype: <?php echo Settings::model()->find()->skype; ?>
            </li>
        </ul>
        <ul>
            <li class="header-list">
                <?php echo Yii::t('front','График работы'); ?>
            </li>
            <li>
                <?php echo Yii::t('front','пн-пт: 9:00 — 17:30'); ?>
                <?php echo Yii::t('front','сб-вс: выходные'); ?>

                <?php echo Yii::t('front','Колл-центр:'); ?>
                <?php echo Yii::t('front','круглосуточно'); ?>
            </li>
        </ul>
    </div>
    <div class="clearfix"></div>
    <div class="language-links-wrapper">
        <ul class="language-links">
            <li>
                <a href=".">
                    English
                </a>
            </li>
            <li>
                <a href=".">
                    Français
                </a>
            </li>
            <li>
                <a href=".">
                    Русский
                </a>
            </li>
            <li>
                <a href=".">
                    Українська
                </a>
            </li>
            <li>
                <a href=".">
                    Türkmen
                </a>
            </li>
            <li>
                <a href=".">
                    العربية
                </a>
            </li>
        </ul>
    </div>
    <div class="social_block">
        <a href="//<?php echo Settings::model()->find()->fb ?>" >
            <i class="fa fa-facebook-official"></i>
            <span>
                <?php echo Yii::T('front', 'Сообщество в Facebook'); ?>
            </span>
        </a>
    </div>
</footer>
</div>

</body>
</html>
