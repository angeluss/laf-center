<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <title><?php echo $this->pageTitle?></title>
    <link rel="shortcut icon" href="/image/favicon/favicon.ico" type="image/x-icon" />
<?php
    $baseUrl = Yii::app()->baseUrl;
    $cs = Yii::app()->getClientScript();
    $cs->registerCssFile('//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css');
    $cs->registerCssFile('//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css');
    $cs->registerCssFile($baseUrl . '/css/jquery.mCustomScrollbar.css');
    $cs->registerCssFile($baseUrl . '/css/style_site.css');
    $cs->registerCssFile($baseUrl . '/js/fancybox/jquery.fancybox.css');
    $cs->registerCssFile($baseUrl . '/css/zabuto_calendar.css');
    $cs->registerCssFile($baseUrl . '/css/bootstrap.min.css');
    $cs->registerCssFile($baseUrl . '/css/news_styles.css');
    $cs->registerScriptFile('//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js');
    $cs->registerScriptFile('//ajax.googleapis.com/ajax/libs/jqueryui/1.11.4/jquery-ui.min.js');
    $cs->registerScriptFile('//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js');
    $cs->registerScriptFile($baseUrl . '/js/jquery.simplemodal-1.4.4.js');
    $cs->registerScriptFile($baseUrl . '/js/fancybox/jquery.fancybox.pack.js');
    $cs->registerScriptFile($baseUrl . '/js/fancybox/jquery.mousewheel-3.0.6.pack.js');
    $cs->registerScriptFile($baseUrl . '/js/jquery.mCustomScrollbar.concat.min.js');
    $cs->registerScriptFile($baseUrl . '/js/global.js');
    $cs->registerScriptFile($baseUrl . '/js/onclicks.js');
    $cs->registerScriptFile($baseUrl . '/js/zabuto_calendar.min.js');
    $cs->registerScriptFile('//arrow.scrolltotop.com/arrow89.js');
?>
</head>

<body>
    <div class="main_wrapper home_page cms_page">
        <div id="header" class="clearfix">
            <div class="top_header clearfix">
                <div class="top_wrapper">
                    <div class="lang_holder clearfix">
                       <div class="lang_icon">
                            <img src="/image/lang_icon.png" />
                            <ul class="lang_menu" style="display: none">
                                <a href="<?php echo Yii::app()->urlManager->createUrl('/site/getLanguage', array('language'=>'ru', 'url'=> Yii::app()->request->url)); ?>" >
                                <li class="active">
                                Русский
                                </li>
                                </a>
                                <a href="<?php echo Yii::app()->urlManager->createUrl('/site/getLanguage', array('language'=>'uk', 'url'=> Yii::app()->request->url)); ?>" >
                                <li>
                                    Український
                                </li>
                                </a>
                                <a href="<?php echo Yii::app()->urlManager->createUrl('/site/getLanguage', array('language'=>'en', 'url'=> Yii::app()->request->url)); ?>" >
                                    <li>
                                        English
                                    </li>
                                </a>
                                <a href="<?php echo Yii::app()->urlManager->createUrl('/site/getLanguage', array('language'=>'fr', 'url'=> Yii::app()->request->url)); ?>" >
                                    <li>
                                        Français
                                    </li>
                                </a>
                                <a href="<?php echo Yii::app()->urlManager->createUrl('/site/getLanguage', array('language'=>'tr', 'url'=> Yii::app()->request->url)); ?>" >
                                    <li>
                                        Türkmen
                                    </li>
                                </a>
                                <a href="<?php echo Yii::app()->urlManager->createUrl('/site/getLanguage', array('language'=>'ar', 'url'=> Yii::app()->request->url)); ?>" >
                                    <li>
                                        العربية
                                    </li>
                                </a>
                            </ul>
                        </div>
                   </div>
                    <div class="pull-left quick_change_lang">
                        <a
                               href="<?php echo Yii::app()->urlManager->createUrl('/site/getLanguage', array('language'=>'en', 'url'=> Yii::app()->request->url)); ?>"
                               class="<?php echo Yii::app()->session['setlanguage'] == 'en' ? 'active' : ''?>"
                               >
                               English
                           </a>
                        <a
                            href="<?php echo Yii::app()->urlManager->createUrl('/site/getLanguage', array('language'=>'ru', 'url'=> Yii::app()->request->url)); ?>"
                            class="<?php echo Yii::app()->session['setlanguage'] == 'ru' ? 'active' : ''?>"
                            >
                            Русский
                        </a>
                    </div>
                    <div class="center_block">
                        <span class="phone"><a href="tel:<?php echo Settings::model()->find()->phone; ?>"><?php echo Settings::model()->find()->phone; ?></a></span>
                        <ul>
                            <?php if(Yii::app()->user->isGuest || !isset(Yii::app()->user->num) || Yii::app()->user->num == 0): ?>
                                <li>
                                    <a id="no_reg_consult_link" href="javascript:">
                                        <?php echo Yii::t('front', 'Онлайн-консультация'); ?>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo Yii::app()->createUrl('/site/login'); ?>">
                                        <?php echo Yii::t('front', 'Вход'); ?>
                                    </a>
                                </li>
                            <?php else: ?>
                                <li>
                                    <a id="no_chat_consult_link" href="javascript:">
                                        <?php echo Yii::t('front', 'Онлайн-консультация'); ?>
                                    </a>
                                </li>
                                <li>
                                    <a href="<?php echo Yii::app()->createUrl('/site/logout'); ?>">
                                        <?php echo Yii::t('front', 'Выход'); ?>
                                    </a>
                                </li>
                            <?php endif; ?>
                        </ul>
                    </div>
                    <div class="social_block">
                        <a href="//<?php echo Settings::model()->find()->fb ?>" >
                            <i class="fa fa-facebook-official"></i>
                            <span>
                                <?php echo Yii::t('front', 'Сообщество в Facebook'); ?>
                            </span>
                        </a>
                    </div>
                </div>
            </div>
            <div id="no_reg_modal" style="display:none;" class="online-consultation-block">
                <div class="onl-cons-wrapper">
                    <div class="close-btn cons-btn">
                        <a class="modalCloseImg simplemodal-close" href="#"><i class="fa fa-times"></i></a>
                    </div>
                    <div class="onl-cons-content clearfix">
                        <div class="wrapper">
                            <div class="call-icon-box">
                                <img src="/image/call-icon-img.png" alt=""/>
                            </div>
                            <div class="onl-cons-desc">
                                <p class="onl-cons-title">
                                    <?php echo Yii::t('front', 'Консультации доступны только зарегистрированным клиентам'); ?>
                                </p>
                                <div class="onl-cons-message">
                                    <?php echo Yii::t('front', 'Клиенты Центра могут получить консультацию, '); ?><br>
                                    <?php echo Yii::t('front', 'обратившись в колл-центр или направив запрос на наш адрес.'); ?>
                                </div>
                            </div>
                            <div class="online-links">
                                <a href="<?php echo Yii::app()->createUrl('/site/login')?>" class="active">
                                    <?php echo Yii::t('front', 'Вход для клиентов'); ?>
                                </a>
                                <span>|</span>
                                <a href="<?php echo Yii::app()->createUrl('/page/prices')?>">
                                    <?php echo Yii::t('front', 'Услуги и Цены'); ?>
                                </a>
                                <span>|</span>
                                <a href="<?php echo Yii::app()->createUrl('/page/contact_us')?>">
                                    <?php echo Yii::t('front', 'Контакты'); ?>
                                </a>
                            </div>
                        </div>
                        <div class="bottom-reg-info">
                            <div class="reg-wrapper">
                                <div class="reg-btn" data-url="<?php echo Yii::app()->createUrl('/site/oferta') ?>">
                                    <h4><?php echo Yii::t('front', 'Станьте клиентом'); ?></h4>
                                    <p><?php echo Yii::t('front', 'Регистрация и договор публичной офферты'); ?></p>
                                </div>
                                <div class="list-title"><?php echo Yii::t('front', 'Как это работает?'); ?></div>
                                <ol class="sequence-list">
                                    <li>
                                        <p><?php echo Yii::t('front', 'Зарегистрируйтесь и получите индивидуальный код клиента'); ?></p>
                                    </li>
                                    <li>
                                        <p><?php echo Yii::t('front', 'Ознакомьтесь с часто задаваемыми вопросами'); ?></p>
                                    </li>
                                    <li>
                                        <p>
                                            <?php echo Yii::t('front', 'Если Вы не нашли ответ на свой вопрос обратитесь к консультанту онлайн или по телефону'); ?>

                                        </p>
                                    </li>
                                    <li>
                                        <p><?php echo Yii::t('front', 'Назовите свой индивидуальный код'); ?></p>
                                    </li>
                                    <li>
                                        <p><?php echo Yii::t('front', 'Объясните проблему'); ?></p>
                                    </li>
                                    <li>
                                        <p><?php echo Yii::t('front', 'Получите необходимую помощь'); ?></p>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <?php //@TODO ?>
            <div id="no_chat_modal" style="display:none;" class="online-consultation-block">
                <div class="onl-cons-wrapper">
                    <div class="close-btn cons-btn">
                        <a class="modalCloseImg simplemodal-close" href="#"><i class="fa fa-times"></i></a>
                    </div>
                    <div class="onl-cons-content clearfix">
                        <div class="wrapper">
                            <div class="call-icon-box">
                                <img src="/image/call-icon-img.png" alt=""/>
                            </div>
                            <div class="onl-cons-desc">
                                <p class="onl-cons-title">
                                    <?php echo Yii::t('front', 'Чат находится в разработке. А пока Вы можете отправить вопрос нам на почту'); ?>
                                </p>
                            </div>
                            <div class="online-links">
                                <form action="<?php echo Yii::app()->createUrl('site/sendQuestion') ?>" method="POST" id="send_form">
                                    <label for="Send_email"> <?php echo Yii::t('front', ' Ваш E-mail'); ?></label>
                                    <input value="<?php echo isset(Yii::app()->user->email) ? Yii::app()->user->email : '' ?>" type="text" name="Send[email]" id="Send_email" /><br><br><br>
                                    <label for="Send_text"><?php echo Yii::t('front', 'Ваш вопрос'); ?></label><br>
                                    <textarea name="Send[text]" id="Send_text"></textarea><br>
                                     <div class="reg_button">
                                    <input onclick="sendQuestion(this)" type="button" name="Send" value="<?php echo Yii::t('front', 'Отправить'); ?>" />
                                     </div>
                                </form>
                                <script>
                                    function sendQuestion (btn){
                                        var form = $(btn).closest('form');
                                        var email = $('#Send_email').val();
                                        var text = $('#Send_text').val();
                                        var success = true;
                                        if(email.length == 0){
                                            $('#Send_email').attr('style', 'border:1px solid red !important');
                                            success = false;
                                        }
                                        if(text.length == 0){
                                            $('#Send_text').attr('style', 'border:1px solid red');
                                            success = false;
                                        }
                                        if(success){
                                            $(form).submit();
                                        }
                                    }
                                </script>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
            <div class="header_wrapper clearfix">
                <ul id="main_nav" class="clearfix">
                    <?php foreach(MainMenu::model()->findAll(array('order'=>'position')) as $item): ?>
                        <?php if($item->status == 1):  ?>
                            <li>
                                <a href="<?php echo Yii::app()->createUrl($item->link)?>" title="<?php echo Yii::t('front', $item->title); ?>">
                                    <?php echo Yii::t('front',$item->title); ?>
                                </a>
                            </li>
                        <?php elseif($item->status == 2) : ?>
                            <li>
                                <a href="<?php echo $item->link ?>" title="<?php echo Yii::t('front', $item->title); ?>" target="_blank">
                                    <?php echo Yii::t('front', $item->title); ?>
                                </a>
                            </li>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>
                <?php $this->widget('SiteSearch'); ?>
                <div class="header_bottom">
                    <div class="about">
                        <h1>
                            <a href="<?php echo Yii::app()->createUrl('site/index')?>">
                                <?php echo Yii::t('front', Blocks::model()->findByAttributes(array('title' => 'main_title'))->getContent(Yii::app()->session['setlanguage'])->text); ?>
                            </a>
                        </h1>
                        <div class="desc">
                            <p>
                                <?php echo Yii::t('front', Blocks::model()->findByAttributes(array('title' => 'desc_1'))->getContent(Yii::app()->session['setlanguage'])->text); ?>
                            </p>
                            <p>
                                <?php echo Yii::t('front', Blocks::model()->findByAttributes(array('title' => 'desc_2'))->getContent(Yii::app()->session['setlanguage'])->text); ?>
                            </p>
                            <p>
                                <?php echo Yii::t('front', Blocks::model()->findByAttributes(array('title' => 'desc_3'))->getContent(Yii::app()->session['setlanguage'])->text); ?>
                            </p>
                        </div>
                    </div>
                    <div class="opacity_block"></div>
                    <div class="register_info <?php echo (Yii::app()->user->isGuest || !isset(Yii::app()->user->num) || Yii::app()->user->num == 0) ? 'guest_block_info':''; ?>">
                        <?php if(Yii::app()->user->isGuest || !isset(Yii::app()->user->num) || Yii::app()->user->num == 0) : ?>
                            <div id="reg_button_top" class="reg_button" data-url="<?php echo Yii::app()->createUrl('site/oferta'); ?>">
                                <h4><?php echo Yii::t('front','Станьте клиентом'); ?></h4>
                                <p><?php echo Yii::t('front','Регистрация и договор публичной офферты'); ?></p>
                            </div>
                            <div class="reg_list">
                                <?php echo Yii::t('front', Blocks::model()->findByAttributes(array('title' => 'how_it_works_block'))->getContent(Yii::app()->session['setlanguage'])->text); ?>
                            </div>
                        <?php else : ?>
                            <div class="my-profile">
                                <div class="profile-top">
                                    <span class="profile-title">
                                        <?php echo Yii::t('profile', 'Мой профиль'); ?>
                                    </span>
                                    <a href="<?php echo Yii::app()->createUrl('/site/logout'); ?>" class="logout_link">
                                        <?php echo Yii::t('front', 'Выход'); ?>
                                    </a>
                                    <a class="edit_profile_link" href="<?php echo Yii::app()->createUrl('/site/profile');?>">
                                        <?php echo Yii::t('profile', 'Редактировать'); ?>
                                    </a>
                                </div>
                                <div class="profile_bottom">
                                    <?php if(Clients::getCurrentUserStatus() == Clients::GET_CARD): ?>
                                        <p class="client_name paid" >
                                            <?php echo Yii::app()->user->name; ?>
                                        </p>
                                        <p class="paid_notice2 paid">
                                            <?php echo Yii::t('profile', 'индивидуальный код');?>
                                        </p>
                                        <span class="client_code">
                                            <?php echo Clients::getCurrentUserCardId(); ?>
                                        </span>
                                        <div class="btn-group">
                                            <a href="javascript:" class="stories_link_profile dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <?php echo Yii::t('profile', 'Профиль'); ?>
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="profile_drop_menu dropdown-menu">
                                                <li>
                                                    <a href="<?php echo Yii::app()->createUrl('/site/profile');?>"><?php echo Yii::t('profile', 'Редактировать'); ?></a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo Yii::app()->createUrl('/site/documents');?>"><?php echo Yii::t('profile', 'Документы'); ?></a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo Yii::app()->createUrl('/site/history');?>"><?php echo Yii::t('profile', 'История обращений'); ?></a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo Yii::app()->createUrl('/site/logout'); ?>"><?php echo Yii::t('front', 'Выход'); ?></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="mob_app_button" style="display: none" data-msg="<?php echo Yii::t('profile', 'В разработке'); ?>">
                                            <div class="aligner_app">
                                                <span class="app_icon"></span>
                                                <span>Available on the</span>
                                                <span class="app_store_span">App Store</span>
                                                <span><?php echo Yii::t('profile', 'Приложение для поддержки'); ?>  </span>
                                            </div>
                                        </div>
                                        <div id="sh_button" class="reg_button">
                                            <div class="aligner">
                                                <span class="quest_icon"></span>
                                                <p>
                                                    <?php echo Yii::t('front', 'Задать вопрос'); ?>
                                                    <span class="status gray-color">online</span>
                                                </p>
                                            </div>
                                        </div>
                                        <p class="paid_notice2">
                                            <?php echo Yii::t('profile', 'пакет услуг оплачен до') .' '. Clients::getPaidTill();?>
                                        </p>
                                    <?php else : ?>
                                        <p class="client_name not_paid pull-left" >
                                            <?php echo Yii::app()->user->name; ?>
                                        </p>
                                        <div class="btn-group">
                                            <a href="javascript:" class="stories_link_profile dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                <?php echo Yii::t('profile', 'Профиль'); ?>
                                                <i class="fa fa-angle-down"></i>
                                            </a>
                                            <ul class="profile_drop_menu_notpaid profile_drop_menu dropdown-menu">
                                            <?php if(Clients::getCurrentUserStatus() !== Clients::NOT_ACTIVE_STATUS || Clients::getCurrentUserStatus() === Clients::OFFER_ACCEPTED): ?>
                                                <li>
                                                    <a href="<?php echo Yii::app()->createUrl('/site/showPay', array('id' => Yii::app()->user->num));?>">
                                                        <span class="button payment_button"><?php echo Yii::t('profile', 'оплатить'); ?></span>
                                                        <span class="pull-left">
                                                        <p class="profile_price"><?php echo Requisites::model()->find()->amount . Yii::t('admin', ' грн'); ?></p>
                                                        <p class="profile_price-desc"><?php echo Yii::t('front', 'пакет услуг на 1 год'); ?></p>
                                                            </span>
                                                    </a>
                                                </li>
                                            <?php endif; ?>
                                                <li>
                                                    <a href="<?php echo Yii::app()->createUrl('/site/profile');?>"><?php echo Yii::t('profile', 'Редактировать'); ?></a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo Yii::app()->createUrl('/site/documents');?>"><?php echo Yii::t('profile', 'Документы'); ?></a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo Yii::app()->createUrl('/site/history');?>"><?php echo Yii::t('profile', 'История обращений'); ?></a>
                                                </li>
                                                <li>
                                                    <a href="<?php echo Yii::app()->createUrl('/site/logout'); ?>"><?php echo Yii::t('front', 'Выход'); ?></a>
                                                </li>
                                            </ul>
                                        </div>
                                        <div class="clearfix"></div>
                                        <p class="paid_notice">
                                            <?php echo Yii::t('profile', 'Ваш индивидуальный код для обращений будет активирован после проверки оплаты');?>
                                        </p>
                                        <p class="paid_notice2">
                                            <?php echo Yii::t('profile', 'Банковский перевод может занимать до 3 дней.');?>
                                        </p>
                                        <div class="reg_button not_active">
                                            <div class="aligner">
                                                <span class="quest_icon"></span>
                                                <p><?php echo Yii::t('front', 'Задать вопрос'); ?></p>
                                            </div>
                                        </div>
                                    <?php endif; ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
        <div id="main_content" class="clearfix">
            <?php echo $content; ?>
        </div>
        <footer>
            <div class="top_footer">
                <div class="consultation-block-back">
                    <h1><?php echo Yii::t('front', Blocks::model()->findByAttributes(array('title' => 'add_consult_text'))->getContent(Yii::app()->session['setlanguage'])->text); ?></h1>
                    <div class="consult_button"><?php echo Yii::t('front', 'Заказать консультацию'); ?></div>
                    <p><?php echo Yii::t('front', 'Правовой центр "Альфа-Омега"'); ?></p>
                </div>
            </div>
            <div class="mapping">
                <?php $lang = isset(Yii::app()->session['setlanguage']) ? Yii::app()->session['setlanguage'] : 'uk' ?>
                <iframe width="815" height="372" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=place_id:ChIJsW2MQe-gJ0ERsk4GxEwiNag&key=AIzaSyDP_Y_EbEtpMKeNDGtHHlId_fMFli5TgaI&language=<?php echo $lang; ?>"></iframe>
           </div>
            <div class="contacts_block_wrapper">
                <table class="desctop_vision">
                    <tr>
                        <th width="422px">
                            <p class="header-list">
                                <?php echo Yii::t('front',Blocks::model()->findByAttributes(array('title' => 'footer_text_title'))->getContent(Yii::app()->session['setlanguage'])->text); ?>
                            </p>
                        </th>
                        <th width="145px">
                            <p class="header-list"><?php echo Yii::t('front','Главный офис'); ?></p>
                        </th>
                        <th width="136px">
                            <p class="header-list">
                                <?php echo Yii::t('front','Колл-центр'); ?>
                            </p>
                        </th>
                        <th width="113px">
                            <p class="header-list">
                                <?php echo Yii::t('front','График работы'); ?>
                            </p>
                        </th>
                    </tr>
                    <tr>
                        <td>
                            <p>
                                <?php echo Yii::t('front',Blocks::model()->findByAttributes(array('title' => 'footer_text'))->getContent(Yii::app()->session['setlanguage'])->text); ?>
                            </p>
                        </td>
                        <td>
                            <p class="address">
                                <?php echo Settings::model()->find()->getAddress(Yii::app()->session['setlanguage']) ?>
                            </p>
                            <p class="big_padding">
                                <a href="mailto:<?php echo Settings::model()->find()->contact_email?>"><?php echo Settings::model()->find()->contact_email?></a>
                            </p>
                        </td>
                        <td>
                            <p><a class="pone_cont" href="tel:<?php echo Settings::model()->find()->phone; ?>">
                                <?php echo Settings::model()->find()->phone; ?>
                                    </a>
                            </p>
                            <p>
                                <a class="pone_cont" href="tel:<?php echo Settings::model()->find()->call_centr; ?>">
                                <?php echo Settings::model()->find()->call_centr; ?>
                                    </a>
                            </p>
                            <p class="big_padding">
                                skype: <a href="skype:<?php echo Settings::model()->find()->skype; ?>?call"> <?php echo Settings::model()->find()->skype; ?></a>
                            </p>
                        </td>
                        <td>
                            <?php echo Yii::t('front', Blocks::model()->findByAttributes(array('title' => 'schedule'))->getContent(Yii::app()->session['setlanguage'])->text); ?>
                        </td>
                    </tr>
                </table>
                <div class="mobile_view">
                    <p class="header-list">
                        <?php echo Yii::t('front', Blocks::model()->findByAttributes(array('title' => 'footer_text_title'))->getContent(Yii::app()->session['setlanguage'])->text); ?>
                    </p>
                    <p>
                        <?php echo Yii::t('front', Blocks::model()->findByAttributes(array('title' => 'schedule'))->getContent(Yii::app()->session['setlanguage'])->text); ?>
                    </p>
                    <p class="header-list">
                        <?php echo Yii::t('front','Главный офис'); ?>
                    </p>
                    <p>
                        <?php echo Yii::t('front', Settings::model()->find()->address); ?>
                        <a href="mailto:<?php echo Settings::model()->find()->contact_email?>"><?php echo Settings::model()->find()->contact_email?></a>
                    </p>
                    <p class="header-list"> <?php echo Yii::t('front','Колл-центр'); ?></p>
                    <p>
                        <a href="tel:<?php echo Settings::model()->find()->phone; ?>">
                        <?php echo Settings::model()->find()->phone; ?>
                            </a>
                    </p>
                    <p>
                        <a href="tel:<?php echo Settings::model()->find()->call_centr; ?>">
                        <?php echo Settings::model()->find()->call_centr; ?>
                            </a>
                    </p>
                    <p>
                        skype:<a href="skype:<?php echo Settings::model()->find()->skype; ?>?call"> <?php echo Settings::model()->find()->skype; ?></a>
                    </p>
                    <p class="header-list">
                        <?php echo Yii::t('front','График работы'); ?>
                    </p>
                </div>
            </div>
            <div class="clearfix"></div>
            <div class="bottom-footer-wrapper">
                <div class="language-links-wrapper">
                   <div class="elements_footer_wrapper">
                       <ul class="language-links pull-left">
                            <li>
                                <a href="<?php echo Yii::app()->urlManager->createUrl('/site/getLanguage', array('language'=>'ru', 'url'=> Yii::app()->request->url)); ?>" >Русский</a>
                            </li>
                            <li>
                                <a href="<?php echo Yii::app()->urlManager->createUrl('/site/getLanguage', array('language'=>'uk', 'url'=> Yii::app()->request->url)); ?>" >Українська</a>
                            </li>
                            <li>
                                <a href="<?php echo Yii::app()->urlManager->createUrl('/site/getLanguage', array('language'=>'en', 'url'=> Yii::app()->request->url)); ?>" >English</a>
                            </li>
                            <li>
                                <a href="<?php echo Yii::app()->urlManager->createUrl('/site/getLanguage', array('language'=>'fr', 'url'=> Yii::app()->request->url)); ?>" >Français</a>
                            </li>
                            <li>
                                <a href="<?php echo Yii::app()->urlManager->createUrl('/site/getLanguage', array('language'=>'tr', 'url'=> Yii::app()->request->url)); ?>" >Türkmen</a>
                            </li>
                            <li>
                                <a href="<?php echo Yii::app()->urlManager->createUrl('/site/getLanguage', array('language'=>'ar', 'url'=> Yii::app()->request->url)); ?>" >العربية</a>
                            </li>
                        </ul>
                       <ul class="language-links pull-right social_footer_block">
                           <li>
                                <a href="//<?php echo Settings::model()->find()->fb ?>" >
                                    <i class="fa fa-facebook-official"></i>
                                    <span><?php echo Yii::T('front', 'Сообщество в Facebook'); ?></span>
                                </a>
                            </li>
                       </ul>
                   </div>
                   <div class="clearfix"></div>
                </div>
                <div class="elements_footer_wrapper">
                    <p>
                        <?php echo Yii::t('front', 'Информация на этом сайте носит исключительно обзорный характер и не может
                        считаться юридической консультацией.'); ?>
                    </p>
                    <p>
                        <span><?php echo Yii::t('front','© 2015  Центр правового содействия иностранным гражданам'); ?></span>
                        <span><i><?php echo Yii::t('front','Все права защищены'); ?></i></span>
                    </p>
                </div>
            </div>
        </footer>
        <div id="intercom">
            <div class="intercom_header">
                <div class="menu_icon">
                    <span>
                        <i class="fa fa-bars"></i>
                    </span>
                </div>
                <div class="intercom_title">
                    <h2><?php echo Yii::t('front', 'Онлайн-консультация'); ?></h2>
                </div>
                <div class="close_icon">
                    <i class="fa fa-times"></i>
                </div>
            </div>
            <div class="intercom_content">
                <?php $this->renderPartial('/layouts/_chat', array('messages' => Message::getMessages()))?>
            </div>
            <div class="message_holder">
                <a class="close_chat_link" href="javascript:"
                   data-msg="<?php echo Yii::t('front', 'Завершить чат?'); ?>"
                   data-url="<?php echo Yii::app()->createUrl('/site/closeChat')?>"
                >
                    <?php echo Yii::t('front', 'Завершить чат')?>
                </a>
                <div class="message_block">
                    <form id="send_msg_form" method="post" enctype="multipart/form-data" data-url="<?php echo Yii::app()->createUrl('/site/addMessageToChat2')?>">
                        <textarea name="message" id="client_message"></textarea>
                        <input type="hidden" name="id" id="chatID" data-url="<?php echo Yii::app()->createUrl('site/refreshChat')?>" value="<?php echo Chat::issetChat()?>" />
                        <div class="form_bottom">
                            <span><i class="fa fa-paperclip"></i></span>
                            <span id="file_name"></span>
                            <input id="client_file" type="file" name="choose_file" />
                            <button id="send_msg" class="submit_btn"><?php echo Yii::t('front', 'Отправить'); ?></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
