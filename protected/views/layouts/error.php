<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Error-page</title>
        <?php
        $baseUrl = Yii::app()->baseUrl;
        $cs = Yii::app()->getClientScript();
        $cs->registerCssFile('https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css');
        $cs->registerCssFile($baseUrl . '/css/style.css');
        $cs->registerScriptFile($baseUrl . '/js/onclicks.js');
        ?>
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    </head>
    <body>
        <div class="error-page">
            <?php echo $content; ?>
        </div>
    </body>
</html>