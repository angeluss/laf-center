<?php foreach($messages as $m): ?>
    <div class="<?php echo  $m->sender == 1 ? 'client_question' : 'operator_answer'  ?>">
        <?php if($m->sender == 2): ?>
            <div class="image_block">
                <?php echo Operators::getSenderAvatar($m->chat_id); ?>
                <p><?php echo Operators::getOperatorLogin($m->chat_id); ?></p>
            </div>
        <?php endif; ?>
        <div class="content_block">
            <div class="content">
                <p><?php echo $m->msg?></p>
            </div>
              <?php if(!is_null($m->filename) && $m->filename !== ''): ?>
                  <p class="file_chat_view">
                        <a href="<?php echo '/uploadFiles/chat/' . $m->chat_id . '/' . $m->file ?>" download>
                            <?php echo $m->filename ?>
                        </a>
                      </p>
            <?php endif; ?>
            <span class="past_time"><?php echo date('H:i:s', $m->created_at)?></span>
        </div>
    </div>
<?php endforeach; ?>
