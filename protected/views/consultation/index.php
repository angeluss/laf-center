<div class="content-wrapper-white news-page consultation-page">
    <div class="left-column">
        <div class="column-wrapper">
            <div class="left-column-title">
                <h6><?php echo Yii::t('front','Правовые консультации'); ?></h6>
            </div>
        </div>
        <div class="type_of_consults">
            <?php if ($law_branches): ?>
                <div class="wrapper_law_branches">
                    <ul>
                    <?php foreach ($law_branches as $law_branch): ?>
                        <li>
                           <a href="#<?php echo $law_branch->getAnchor() ?>">
                               <?php echo $law_branch->getContent(Yii::app()->session['setlanguage'])->title ?>
                           </a>
                        </li>
                    <?php endforeach; ?>
                </ul>
                </div>
                <?php foreach ($law_branches as $branch) : ?>
                    <a name="<?php  echo $branch->getAnchor(); ?>">
                        <h6><?php  echo $branch->getContent(Yii::app()->session['setlanguage'])->title; ?></h6>
                    </a>
                    <p class="description">
                        <?php  echo $branch->getContent(Yii::app()->session['setlanguage'])->short_desc ?>
                    </p>
                    <?php $consults = Consultation::model()->findAll(
                    array(
                        'condition' => 'type_consult='.$branch->id,
                    )
                ); ?>
                    <?php if ($consults) : ?>
                        <?php foreach($consults as $con): ?>
                            <ul class="links consultation-children-list">
                                <li>
                                    &mdash;&nbsp;&nbsp;
                                    <?php if(Yii::app()->user->isGuest): ?>
                                        <a class="no_reg_consult_link" href="javascript:" >
                                            <?php
                                                if ($con->getContent(Yii::app()->session['setlanguage'])->title) {
                                                    echo $con->getContent(Yii::app()->session['setlanguage'])->title;
                                                }
                                            ?>
                                        </a>
                                    <?php else: ?>
                                        <a href="<?php echo Yii::app()->createUrl('consultation/view', array('id' =>$con->id)); ?>">
                                            <?php
                                                if ($con->getContent(Yii::app()->session['setlanguage'])->title) {
                                                    echo $con->getContent(Yii::app()->session['setlanguage'])->title;
                                                } else {
                                                    echo 'error_title';
                                                }
                                            ?>
                                        </a>
                                    <?php endif; ?>
                                </li>
                            </ul>
                        <?php endforeach; ?>
                    <?php else : ?>
                        <p class="disabled_consultations">
                            <?php echo Yii::t('front','Нет доступных консультаций'); ?>
                        </p>
                    <?php endif; ?>
                    <div class="branches_desc_text pull-left">
                        <p>
                            <b>
                                <?php echo Yii::t('front','Не нашли ответ на нужный вопрос?'); ?>
                            </b>
                        </p>
                        <p>
                            <?php echo Yii::t('front','Наши юристы всегда готовы предоставить детальную информацию по интересующим Вас темам.') ?>
                        </p>
                    </div>
                    <div style="display:none" class="reg_button pull-right">
                        <h4>
                            <?php echo  Yii::t('front','Заказать консультацию'); ?>
                        </h4>
                    </div>
                    <div class="clearfix"></div>
                <?php endforeach; ?>
            <?php else: ?>
                <?php //Этот момент тоже явно должен быть. ?>
            <?php endif; ?>
        </div>
    </div>
       <!-- end of left-column -->
    <div class="right-column">
        <div class="about-centre">
            <div class="outside-wrapper">
                <div class="wrapper">
                    <h5 class="right-col-title"><?php echo Yii::t('front','О центре'); ?></h5>
                    <p class="about-article">
                        <?php
                            echo Blocks::model()->findByAttributes(
                                array('title' => 'desc_1')
                            )->getContent(Yii::app()->session['setlanguage'])->text;
                        ?>
                    </p>
                    <div class="link-wrapper">
                        <a href="<?php echo MainMenu::model()->findByPk(1)->link; ?>" target="_blank">
                            <?php echo Yii::t('front','Подробнее'); ?>
                        </a>
                    </div>
                    <?php if(Yii::app()->user->isGuest): ?>
                        <div id="reg_button_consult" class="reg_button" data-url="<?php echo Yii::app()->createUrl('site/oferta', array('lang'=>DEFAULT_LANG)); ?>">
                            <h4><?php echo Yii::t('front','Станьте клиентом'); ?></h4>
                            <p><?php echo Yii::t('front','Регистрация и договор публичной офферты'); ?></p>
                        </div>
                        <div class="reg_list">
                            <?php
                                echo Yii::t(
                                    'front',
                                    Blocks::model()->findByAttributes(
                                        array('title' => 'how_it_works_block')
                                    )->getContent(Yii::app()->session['setlanguage'])->text);
                            ?>
                        </div>
                    <?php else: ?>
                        <p></p>
                    <?php endif; ?>
                </div>
            </div>
        </div><!-- end of about-centre -->
        <div class="useful-info">
            <?php
                echo Yii::t(
                    'front',
                    Blocks::model()->findByAttributes(
                        array('title' => 'useful_info')
                    )->getContent(Yii::app()->session['setlanguage'])->text);
            ?>
        </div>
        <div class="state-contacts">
            <div class="state-img-box">
                <img src="/image/claf-state-image.png" alt=""/>
            </div>
            <div class="state-info">
                <?php
                    echo Yii::t(
                        'front',
                        Blocks::model()->findByAttributes(
                            array('title' => 'gol_upravl')
                        )->getContent(Yii::app()->session['setlanguage'])->text);
                ?>
            </div>
        </div>
    </div>
    <!-- end of right-column -->
    <div class="clearfix"></div>
</div>