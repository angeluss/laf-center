<div class="content-wrapper-white one-more-const">
    <div class="left-column">
        <div class="column-wrapper">
            <div class="go-backward">
                <i class="fa fa-long-arrow-left"></i>
                <a href="."><?php echo Yii::t('front','Правовые консультации'); ?></a>
            </div>
            <div class="news-content extended-news clearfix">
                <div>
                    <p class="title extended-title"><a href="#"><?php echo $model->getContent(Yii::app()->session['setlanguage'])->title; ?></a></p>
                    <div class="article extended-article">
                        <?php echo  $model->getContent(Yii::app()->session['setlanguage'])->content; ?>
                    </div>
                </div>
            </div>
        </div>
    </div> <!-- end of left column -->
    <div class="clearfix"></div>
</div>
