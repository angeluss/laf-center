<?php
/* @var $this ApLogsController */
/* @var $model ApLogs */

$this->breadcrumbs=array(
	'Ap Logs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List ApLogs', 'url'=>array('index')),
	array('label'=>'Manage ApLogs', 'url'=>array('admin')),
);
?>

<h1>Create ApLogs</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>