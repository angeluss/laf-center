<?php
/* @var $this ApLogsController */
/* @var $model ApLogs */

$this->breadcrumbs=array(
	'Ap Logs'=>array('index'),
	$model->id,
);

$this->menu=array(
	array('label'=>'List ApLogs', 'url'=>array('index')),
	array('label'=>'Create ApLogs', 'url'=>array('create')),
	array('label'=>'Update ApLogs', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete ApLogs', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage ApLogs', 'url'=>array('admin')),
);
?>

<h1>View ApLogs #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'user_id',
		'desc',
		'area',
		'date',
	),
)); ?>
