<?php
/* @var $this ApLogsController */
/* @var $model ApLogs */

$this->breadcrumbs=array(
	'Ap Logs'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List ApLogs', 'url'=>array('index')),
	array('label'=>'Create ApLogs', 'url'=>array('create')),
	array('label'=>'View ApLogs', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage ApLogs', 'url'=>array('admin')),
);
?>

<h1>Update ApLogs <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>