<?php
/* @var $this ApLogsController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Ap Logs',
);

$this->menu=array(
	array('label'=>'Create ApLogs', 'url'=>array('create')),
	array('label'=>'Manage ApLogs', 'url'=>array('admin')),
);
?>

<h1>Ap Logs</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
