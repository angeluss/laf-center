<div class="content-wrapper-white partners-page">
    <?php foreach($partners as $partner):?>
         <div class="partners">
            <a href="<?php echo $partner->url; ?>" class="partner-link" target="_blank">
                <p class="partner-name">
                    <?php
                        echo $partner->getContent(Yii::app()->session['setlanguage'])->title ?
                            $partner->getContent(Yii::app()->session['setlanguage'])->title
                            : '';
                    ?>
                </p>
                <?php echo Partner::getImg($partner->img, $partner->id, 'prt-img')?>
            </a>
            <div class="partner-desc">
                <?php echo  $partner->getContent(Yii::app()->session['setlanguage'])->desc ?>
            </div>
        </div>
    <?php endforeach; ?>
    <div class="clearfix"></div>
</div>


