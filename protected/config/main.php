<?php

// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'CLAF',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.admin.models.*',
		'application.admin_panel.models.*',
		'application.models.*',
		'application.components.*',
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool

		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'123',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','::1'),
		),
		'admin_panel'=>array(
			'preload'=>array('booster'),
			'components'=>array(
				'booster' => array(
					'class' => 'ext.booster.components.Booster',
				),
			),
		),
		'operator_panel'=>array(
			'preload'=>array('booster'),
			'components'=>array(
				'booster' => array(
					'class' => 'ext.booster.components.Booster',
				),
			),
		),
		'admin'=>array(
			'preload'=>array('booster'),
			'components'=>array(
				'booster' => array(
					'class' => 'ext.booster.components.Booster',
				),
			),
		),
	),

	// application components
	'components'=>array(
        'session' => array (
            'autoCreateSessionTable'=>false,
            'class'=>'system.web.CDbHttpSession',
            'connectionID'=>'db',
            'sessionTableName' => 'YiiSession',
            'timeout'=>'6000',
        ),
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
		),

        'booster' => array(
            'class' => 'ext.booster.components.Booster',
        ),

		// uncomment the following to enable URLs in path-format

		'urlManager'=>array(
			'class'=>'application.components.UrlManager',
			'urlFormat'=>'path',
            'showScriptName' => false,
			'rules'=>array(
				'<language:(ru|uk|en|fr|tr|ar)>/' => 'site/index',
                '<language:(ru|uk|en|fr|tr|ar)>/page/<alias>' => '/page/index',
				'<language:(ru|uk|en|fr|tr|ar)>/<controller:\w+>/<id:\d+>'=>'<controller>/view',
				'<language:(ru|uk|en|fr|tr|ar)>/<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
				'<language:(ru|uk|en|fr|tr|ar)>/<controller:\w+>/<action:\w+>'=>'<controller>/<action>',
                '<language:(ru|uk|en|fr|tr|ar)>/<module:\w+>/<controller:\w+>/<action:[\w-]+>' => '<module>/<controller>/<action>',
                '<language:(ru|uk|en|fr|tr|ar)>/<module:\w+>' => '<module>',
			),
		),


		// database settings are configured in database.php
		'db'=>require(dirname(__FILE__).'/database.php'),


        'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'site/error',
		),

		'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning',
				),
				// uncomment the following to show log messages on web pages

//				array(
//					'class'=>'CWebLogRoute',
//				),

			),
		),

	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'webmaster@example.com',
	),
);
