jQuery( document ).ready(function() {
    jQuery('.lang_icon').on('click', function(){
        jQuery('.lang_menu').toggle();
    });
    jQuery('.useful_information').on('click', function(){
        jQuery('.useful_information_dropdown').toggle();
        //jQuery("body").click(function(e) {
        //    if(jQuery(e.target).closest(".useful_information_dropdown").length==0)
        //        jQuery(".useful_information_dropdown").css("display","none");
        //});
    });

    jQuery('.pass-icon.show').on('click', function(){
        jQuery('#password').attr('type','text');
        jQuery('.pass-icon.show').hide();
        jQuery('.pass-icon.hide').show();
    });
    jQuery('.pass-icon.hide').on('click', function(){
        jQuery('#password').attr('type','password');
        jQuery('.pass-icon.show').show();
        jQuery('.pass-icon.hide').hide();
    });
    //jQuery('.date-input, .fa-calendar:before').datepicker();
    jQuery('.dropdown-element').on('click', function(){
        jQuery(this).next('.dropmenu').toggle();
    });
    jQuery("#offerta-agree").on('click', function(){
        jQuery('.agree_block').toggleClass('checked');
        jQuery('.reg_button').toggleClass('disabled');
    });


});
